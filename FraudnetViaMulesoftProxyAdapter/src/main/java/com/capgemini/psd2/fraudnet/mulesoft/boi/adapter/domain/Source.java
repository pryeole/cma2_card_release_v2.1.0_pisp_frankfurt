package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * Source
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Source {
	@SerializedName("sourceSystem")
	private SourceSystem sourceSystem;

	@SerializedName("sourceSubSystem")
	private SourceSubSystem sourceSubSystem;

	@SerializedName("sourceSystemGroupCountry")
	private SourceSystemGroupCountry sourceSystemGroupCountry;

	public Source sourceSystem(SourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
		return this;
	}

	/**
	 * Get sourceSystem
	 * 
	 * @return sourceSystem
	 **/
	@ApiModelProperty(required = true, value = "")
	public SourceSystem getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(SourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public Source sourceSubSystem(SourceSubSystem sourceSubSystem) {
		this.sourceSubSystem = sourceSubSystem;
		return this;
	}

	/**
	 * Get sourceSubSystem
	 * 
	 * @return sourceSubSystem
	 **/
	@ApiModelProperty(required = true, value = "")
	public SourceSubSystem getSourceSubSystem() {
		return sourceSubSystem;
	}

	public void setSourceSubSystem(SourceSubSystem sourceSubSystem) {
		this.sourceSubSystem = sourceSubSystem;
	}

	public Source sourceSystemGroupCountry(SourceSystemGroupCountry sourceSystemGroupCountry) {
		this.sourceSystemGroupCountry = sourceSystemGroupCountry;
		return this;
	}

	/**
	 * Get sourceSystemGroupCountry
	 * 
	 * @return sourceSystemGroupCountry
	 **/
	@ApiModelProperty(value = "")
	public SourceSystemGroupCountry getSourceSystemGroupCountry() {
		return sourceSystemGroupCountry;
	}

	public void setSourceSystemGroupCountry(SourceSystemGroupCountry sourceSystemGroupCountry) {
		this.sourceSystemGroupCountry = sourceSystemGroupCountry;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Source source = (Source) o;
		return Objects.equals(this.sourceSystem, source.sourceSystem)
				&& Objects.equals(this.sourceSubSystem, source.sourceSubSystem)
				&& Objects.equals(this.sourceSystemGroupCountry, source.sourceSystemGroupCountry);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sourceSystem, sourceSubSystem, sourceSystemGroupCountry);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Source {\n");

		sb.append("    sourceSystem: ").append(toIndentedString(sourceSystem)).append("\n");
		sb.append("    sourceSubSystem: ").append(toIndentedString(sourceSubSystem)).append("\n");
		sb.append("    sourceSystemGroupCountry: ").append(toIndentedString(sourceSystemGroupCountry)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
