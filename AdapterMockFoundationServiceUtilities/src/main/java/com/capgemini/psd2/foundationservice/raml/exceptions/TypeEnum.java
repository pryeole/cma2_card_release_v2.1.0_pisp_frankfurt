package com.capgemini.psd2.foundationservice.raml.exceptions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TypeEnum {
	
		    CRITICAL("CRITICAL"),

		    ERROR("ERROR"),

		    WARNING("WARNING"),

		    INFORMATION("INFORMATION"),

		    AUDIT("AUDIT"),

		    SUCCESSFUL("SUCCESSFUL");

	private String value;

	TypeEnum(String value) {
	    this.value = value;
	   }

	  @Override
	  @JsonValue
	  public String toString() {
	    return String.valueOf(value);
	  }

	  @JsonCreator
	  public static TypeEnum fromValue(String text) {
	    for (TypeEnum b : TypeEnum.values()) {
	      if (String.valueOf(b.value).equals(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
}
