package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * GCIS customer classification separating Industrial and Consumer
 */
public enum GcisCustomertype {
  
  INDIVIDUAL("Individual"),
  
  INDUSTRIAL("Industrial");

  private String value;

  GcisCustomertype(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static GcisCustomertype fromValue(String text) {
    for (GcisCustomertype b : GcisCustomertype.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

