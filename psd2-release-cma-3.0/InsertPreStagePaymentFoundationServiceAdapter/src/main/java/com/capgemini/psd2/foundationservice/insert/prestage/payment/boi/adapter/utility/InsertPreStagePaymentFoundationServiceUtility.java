/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.utility;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.transformer.InsertPreStagePaymentFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountInformationFoundationServiceDelegate.
 */
@Component
public class InsertPreStagePaymentFoundationServiceUtility {

	@Autowired
	InsertPreStagePaymentFoundationServiceTransformer insertPreStagePaymentFoundationServiceTransformer;

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationIdInReqHeader;
	
	@Value("${foundationService.platform}")
	private String platform; 
	
	@Value("${foundationService.userId}")
	private String userIdConstant; 
	
	@Value("${foundationService.channelId}")
	private String channelIdConstant; 
	
	public PaymentInstruction transformResponseFromAPIToFDForInsert(CustomPaymentSetupPOSTRequest paymentSetupPostReq, Map<String, String> params) {
		return insertPreStagePaymentFoundationServiceTransformer.transformPaymentInformationAPIToFDForInsert(paymentSetupPostReq, params);
	}
	
	public PaymentSetupStagingResponse transformResponseFromFDToAPIForInsert(ValidationPassed validationPassed) {
		return insertPreStagePaymentFoundationServiceTransformer.transformPaymentInformationFDToAPIForInsert(validationPassed);
	}
	
	public PaymentInstruction transformResponseFromAPIToFDForUpdate(CustomPaymentSetupPOSTResponse paymentSetupPostResp, Map<String, String> params) {
		return insertPreStagePaymentFoundationServiceTransformer.transformPaymentInformationAPIToFDForUpdate(paymentSetupPostResp, params);
	}
	
	public PaymentSetupStagingResponse transformResponseFromFDToAPIForUpdate(ValidationPassed validationPassed) {
		return insertPreStagePaymentFoundationServiceTransformer.transformPaymentInformationFDToAPIForUpdate(validationPassed);
	}
	
	public String getFoundationServiceURL(String paymentId, String baseURL, String endURL){
		
		if(NullCheckUtils.isNullOrEmpty(paymentId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		if(NullCheckUtils.isNullOrEmpty(baseURL) || NullCheckUtils.isNullOrEmpty(endURL) ) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return baseURL + "/" + paymentId + "/" +endURL;
	}
	
	public CustomPaymentSetupPOSTResponse transformResponseFromFDToAPI(PaymentInstruction paymentInstruction){
		return insertPreStagePaymentFoundationServiceTransformer.transformPaymentRetrieval(paymentInstruction);
	}
	
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		String userId = params.get(PSD2Constants.USER_IN_REQ_HEADER);
		httpHeaders.add(userInReqHeader, NullCheckUtils.isNullOrEmpty(userId) ? userIdConstant : userId);
		String channelId = params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER);
		httpHeaders.add(channelInReqHeader, NullCheckUtils.isNullOrEmpty(channelId) ? channelIdConstant : channelId);
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeader, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(correlationIdInReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		return httpHeaders;
	}
}
