package com.capgemini.psd2.pisp.payment.submission.platform.adapter;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface PaymentsPlatformAdapter {
	
	//Methods for DemosticPayment Payments v3.0 API
	public PaymentsPlatformResource getIdempotentPaymentsPlatformResource(long idempotencyDuration, String paymentType);
	public PaymentsPlatformResource updatePaymentsPlatformResource(PaymentsPlatformResource submissionPlatformResource);
	public PaymentsPlatformResource retrievePaymentsPlatformResource(String paymentSubmissionId);
	public PaymentsPlatformResource retrievePaymentsResourceByConsentId(String consentId);
	public void createInvalidPaymentSubmissionPlatformResource(PaymentsPlatformResource paymentsPlatformResource,PaymentResponseInfo validationDetails, String currentDateInISOFormat);
	public PaymentsPlatformResource retrievePaymentsPlatformResource(String paymentId, String paymentConsentId);
	public PaymentsPlatformResource createInitialPaymentsPlatformResource(CustomPaymentStageIdentifiers paymentStageIdentifiers, String setupCreationDateTime);
	public PaymentsPlatformResource retrievePaymentsPlatformResourceByPaySubmissionIdAndType(String submissionId,
			String paymentType);
	public PaymentsPlatformResource retrievePaymentsPlatformResourceByBackwardSubmissionId(String submissionId);
}
