package com.capgemini.psd2.pisp.payments.consents.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payments.consents.service.DomesticStandingOrderConsentsService;

/**
 * Controller Class for Domestic Standing-Order  API. It's a part of CMA3.0 PISP specification
 *
 */

@RestController
public class DomesticStandingOrderController 
{	
	@Autowired
	DomesticStandingOrderConsentsService domesticStandingOrderSetupService;
	
	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;
 
	@PostMapping(value="/domestic-standing-order-consents",consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<CustomDStandingOrderConsentsPOSTResponse> createDomesticStandingSetupResource(@RequestBody OBWriteDomesticStandingOrderConsent1 domesticStandingOrderRequest){
		
		CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderConsentsPOSTRequest();
		
		customDomesticStandingOrderPOSTRequest.setData(domesticStandingOrderRequest.getData());
		customDomesticStandingOrderPOSTRequest.setRisk(domesticStandingOrderRequest.getRisk());
		try {
			CustomDStandingOrderConsentsPOSTResponse customDomesticStandingOrderPOSTResponse = domesticStandingOrderSetupService.createDomesticStandingOrderConsentResource(customDomesticStandingOrderPOSTRequest);
			return new ResponseEntity<>(customDomesticStandingOrderPOSTResponse,HttpStatus.CREATED);
		}catch(PSD2Exception psd2Exception) {
			if(Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;
			
			return new ResponseEntity<>(HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));
		}
	}
	
	@GetMapping(value="/domestic-standing-order-consents/{ConsentId}",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public ResponseEntity<CustomDStandingOrderConsentsPOSTResponse> retrieveDomesticStandingOrderConsentResource(PaymentRetrieveGetRequest paymentRetrieveRequest){
		try{
			CustomDStandingOrderConsentsPOSTResponse paymentConsentsResponse = domesticStandingOrderSetupService.retrieveDomesticStandingOrderConsentsResource(paymentRetrieveRequest);
			return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.OK);		
		}catch(PSD2Exception psd2Exception){			
			if(Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;			
			return new ResponseEntity<>(HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));			
		}		
	}
}
