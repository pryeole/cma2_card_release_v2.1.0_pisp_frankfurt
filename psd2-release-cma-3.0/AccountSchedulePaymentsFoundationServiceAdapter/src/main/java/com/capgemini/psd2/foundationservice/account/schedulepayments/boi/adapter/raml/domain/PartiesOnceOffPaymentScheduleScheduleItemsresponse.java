package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * PartiesOnceOffPaymentScheduleScheduleItemsresponse
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-03T11:38:41.280+05:30")
public class PartiesOnceOffPaymentScheduleScheduleItemsresponse {
  @SerializedName("total")
  private Double total = null;

  @SerializedName("oneOffPaymentInstructionsList")
  private List<OneOffPaymentInstruction> oneOffPaymentInstructionsList = new ArrayList<OneOffPaymentInstruction>();

  public PartiesOnceOffPaymentScheduleScheduleItemsresponse total(Double total) {
    this.total = total;
    return this;
  }

   /**
   * Get total
   * @return total
  **/
  @ApiModelProperty(required = true, value = "")
  public Double getTotal() {
    return total;
  }

  public void setTotal(Double total) {
    this.total = total;
  }

  public PartiesOnceOffPaymentScheduleScheduleItemsresponse oneOffPaymentInstructionsList(List<OneOffPaymentInstruction> oneOffPaymentInstructionsList) {
    this.oneOffPaymentInstructionsList = oneOffPaymentInstructionsList;
    return this;
  }

  public PartiesOnceOffPaymentScheduleScheduleItemsresponse addOneOffPaymentInstructionsListItem(OneOffPaymentInstruction oneOffPaymentInstructionsListItem) {
    this.oneOffPaymentInstructionsList.add(oneOffPaymentInstructionsListItem);
    return this;
  }

   


  public List<OneOffPaymentInstruction> getOneOffPaymentInstructionsList() {
	return oneOffPaymentInstructionsList;
}

public void setOneOffPaymentInstructionsList(List<OneOffPaymentInstruction> oneOffPaymentInstructionsList) {
	this.oneOffPaymentInstructionsList = oneOffPaymentInstructionsList;
}

@Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartiesOnceOffPaymentScheduleScheduleItemsresponse partiesOnceOffPaymentScheduleScheduleItemsresponse = (PartiesOnceOffPaymentScheduleScheduleItemsresponse) o;
    return Objects.equals(this.total, partiesOnceOffPaymentScheduleScheduleItemsresponse.total) &&
        Objects.equals(this.oneOffPaymentInstructionsList, partiesOnceOffPaymentScheduleScheduleItemsresponse.oneOffPaymentInstructionsList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(total, oneOffPaymentInstructionsList);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartiesOnceOffPaymentScheduleScheduleItemsresponse {\n");
    
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    oneOffPaymentInstructionsList: ").append(toIndentedString(oneOffPaymentInstructionsList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

