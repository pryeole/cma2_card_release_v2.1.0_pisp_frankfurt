package com.capgemini.psd2.consent.list.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.consent.list.data.ConsentListResponse;
import com.capgemini.psd2.consent.list.service.ConsentListService;
import com.capgemini.psd2.consent.list.utilities.ConsentListUtilities;

@RestController
public class ConsentListController {

	@Autowired
	private ConsentListService consentListService;

	@RequestMapping(value = "/consents/{userid}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ConsentListResponse getConsentList(@PathVariable("userid") String userId,
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "tenantId", required = true)String tenantId,
			@RequestParam(value = "consentType", required = false)String consentType) {

		ConsentListUtilities.validateInputs(userId, status);
		
		return consentListService.getConsentList(userId, status, tenantId, consentType);
	}
}
