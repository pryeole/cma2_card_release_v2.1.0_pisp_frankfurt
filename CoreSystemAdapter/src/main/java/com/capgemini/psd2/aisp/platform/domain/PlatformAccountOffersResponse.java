package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadOffer1;

public class PlatformAccountOffersResponse {
	
	//CMA2 response
	private OBReadOffer1 obReadOffer1;
	
	//Addtional Information
	private Map<String,String> addtionalInformation;
	
	public OBReadOffer1 getoBReadOffer1()
	{
		return obReadOffer1;
	}
	
	public void setoBReadOffer1(OBReadOffer1 obReadOffer1)
	{
		this.obReadOffer1 = obReadOffer1;
	}
	
	public Map<String, String> getAddtionalInformation()
	{
		return addtionalInformation;
	}
	
	public void setAdditionalInformation(Map<String,String> addtionalInformation)
	{
		this.addtionalInformation =addtionalInformation;
	}

}
