package com.capgemini.psd2.fraudsystem.routing.adapter.test.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.routing.adapter.impl.FraudSystemRoutingAdapter;
import com.capgemini.psd2.fraudsystem.routing.adapter.routing.FraudSystemAdapterFactoryImpl;

public class FraudSystemAdapterFactoryImplTest {

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private FraudSystemAdapterFactoryImpl factory;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSetApplicationContext() {
		factory.setApplicationContext(new ApplicationContextMock());
	}

	@Test
	public void testGetAdapterInstance() {
		FraudSystemAdapter adapter = new FraudSystemRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(adapter);
		FraudSystemAdapter fraudSystemAdapter = factory.getAdapterInstance("testString");
		assertNotNull(fraudSystemAdapter);
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		applicationContext = null;
	}
}