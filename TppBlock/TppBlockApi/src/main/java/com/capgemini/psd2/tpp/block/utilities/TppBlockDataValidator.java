package com.capgemini.psd2.tpp.block.utilities;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.internal.apis.domain.TppBlockInput;
import com.capgemini.psd2.tpp.block.domain.TppBlockPathVariables;
import com.capgemini.psd2.utilities.NullCheckUtils;

public class TppBlockDataValidator {

	private TppBlockDataValidator() {
		throw new IllegalAccessError("Utility Class");
	}

	public static void validateData(TppBlockInput body, TppBlockPathVariables pathvariables) {
		if (NullCheckUtils.isNullOrEmpty(body)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}

		if (NullCheckUtils.isNullOrEmpty(pathvariables.getTppId())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_TPP_ID_PROVIDED);
		}

		if (NullCheckUtils.isNullOrEmpty(body.getAction())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_BLOCK_ACTION_PROVIDED);
		}

		if (NullCheckUtils.isNullOrEmpty(body.getDescription())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_BLOCK_DESCRIPTION_PROVIDED);
		}
	}
}