package com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;

public interface FundsConfirmationConsentRepository extends MongoRepository<OBFundsConfirmationConsentDataResponse1, String> {

	public OBFundsConfirmationConsentDataResponse1 findByConsentIdAndCmaVersionIn(String consentId,List<String> cmaVersion);
}
