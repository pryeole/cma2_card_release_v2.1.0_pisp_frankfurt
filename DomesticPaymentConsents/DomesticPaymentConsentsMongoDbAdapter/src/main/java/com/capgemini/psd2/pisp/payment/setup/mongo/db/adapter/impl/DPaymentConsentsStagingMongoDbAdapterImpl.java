package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DPaymentConsentsFoundationRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.adapter.repository.DPaymentV1SetupFoundationRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.CustomDomesticPaymentSetupCma1POSTResponse;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.transformer.DPaymentConsentsStageTransformer;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("dPaymentConsentsStagingMongoDbAdapter")
public class DPaymentConsentsStagingMongoDbAdapterImpl implements DomesticPaymentStagingAdapter {

	@Autowired
	private DPaymentV1SetupFoundationRepository dPaymentSetupBankRepositoryCma1;

	@Autowired
	private DPaymentConsentsFoundationRepository dPaymentConsentsBankRepository;

	@Autowired
	private DPaymentConsentsStageTransformer dPaymentConsentsStageTransformer;

	@Autowired
	private RequestHeaderAttributes reqAttributes;
	
	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;
	
	@Autowired
	private SandboxValidationUtility utility;

	@Override
	public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(
			CustomDPaymentConsentsPOSTRequest customRequest, CustomPaymentStageIdentifiers customStageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		CustomDPaymentConsentsPOSTResponse paymentConsentsBankResource = transformDomesticPaymentConsentsToBankResource(
				customRequest);

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();

		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		// Sandbox Secondary Identification Validation
		String secondaryIdentification = customRequest.getData().getInitiation().getCreditorAccount()
				.getSecondaryIdentification();
		if (utility.isValidSecIdentification(secondaryIdentification)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));
		}

		String consentId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		String cutOffDateTime = utility.getMockedCutOffDtTm();
		
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		paymentConsentsBankResource.getData().setCreationDateTime(customRequest.getCreatedOn());
		paymentConsentsBankResource.getData().setConsentId(consentId);
		
		// removing charges block as this is not returned for BOI on domestic payment APIs
		if(!isSandboxEnabled){
			List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, null);
			paymentConsentsBankResource.getData().setCharges(charges);
		}
		paymentConsentsBankResource.getData().setExpectedExecutionDateTime(expectedExecutionDateTime);
		paymentConsentsBankResource.getData().setExpectedSettlementDateTime(expectedSettlementDateTime);
		paymentConsentsBankResource.getData().setCutOffDateTime(cutOffDateTime);
		paymentConsentsBankResource.setConsentPorcessStatus(processConsentStatusEnum);

		if (consentId != null) {
			OBExternalConsentStatus1Code status = calculateCMAStatus(processConsentStatusEnum, successStatus,
					failureStatus);
			paymentConsentsBankResource.getData().setStatus(status);
			paymentConsentsBankResource.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			dPaymentConsentsBankRepository.save(paymentConsentsBankResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		return paymentConsentsBankResource;
	}

	@Override
	public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		try {
			String setupCmaVersion = customPaymentStageIdentifiers.getPaymentSetupVersion();
			String paymentConsentId = customPaymentStageIdentifiers.getPaymentConsentId();

			CustomDPaymentConsentsPOSTResponse paymentBankResource;

			// Retrieve cma1 staged resource
			if (setupCmaVersion.equals(PaymentConstants.CMA_FIRST_VERSION)) {
				CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = dPaymentSetupBankRepositoryCma1
						.findOneByDataPaymentId(paymentConsentId);
				paymentBankResource = dPaymentConsentsStageTransformer
						.transformCma1ToCma3Response(paymentCma1BankResource);
			}

			// Retrieve cma3 staged resource
			else
				paymentBankResource = dPaymentConsentsBankRepository.findOneByDataConsentId(paymentConsentId);

			if (paymentBankResource == null || paymentBankResource.getData() == null
					|| paymentBankResource.getData().getInitiation() == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));

			// sandbox Error Scenario
			String initiationAmount = paymentBankResource.getData().getInitiation().getInstructedAmount().getAmount();
			if ("GET".equals(reqAttributes.getMethodType())) {
				if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
				}
			}

			paymentBankResource.setId(null);

			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	private CustomDPaymentConsentsPOSTResponse transformDomesticPaymentConsentsToBankResource(
			CustomDPaymentConsentsPOSTRequest paymentConsentsRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(paymentConsentsRequest);
		return JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentFoundationRequest,
				CustomDPaymentConsentsPOSTResponse.class);
	}

	private OBExternalConsentStatus1Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS) ? successStatus : failureStatus;
	}

}
