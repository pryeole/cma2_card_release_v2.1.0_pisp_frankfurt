package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.delegate.DomesticPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.transformer.DomesticPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentConsentsFoundationServiceDelegateTest {
	
	@InjectMocks
	DomesticPaymentConsentsFoundationServiceDelegate delegate;
	
	@Mock
	DomesticPaymentConsentsFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testcreatePaymentRequestHeadersNIGB(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"domesticPaymentConsentBaseURL","baseUrl");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		ReflectionTestUtils.setField(delegate,"apiChannelBrand","apiChannelBrand");
	//	ReflectionTestUtils.setField(delegate,"apiChannelBrandValue","BOIUK");
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		
		
		stageIden.setPaymentSetupVersion("2.0");
		HttpHeaders header=delegate.createPaymentRequestHeaders(stageIden, params);
		assertNotNull(header);
		
		
	}
	
	@Test
	public void testcreatePaymentRequestHeadersROI(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"domesticPaymentConsentBaseURL","baseUrl");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		ReflectionTestUtils.setField(delegate,"apiChannelBrand","apiChannelBrand");
	//	ReflectionTestUtils.setField(delegate,"apiChannelBrandValue","BOIROI");
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		stageIden.setPaymentSetupVersion("1.0");
		HttpHeaders header=delegate.createPaymentRequestHeaders(stageIden, params);
		assertNotNull(header);
		
		
	}
	
	@Test
	public void testcreatePaymentRequestHeadersPostNIGB(){
		ReflectionTestUtils.setField(delegate,"apiChannelCode","apiChannelCode");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"sourcesystem","PSD2API");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		ReflectionTestUtils.setField(delegate,"apiChannelBrand","BOIUK");
	//	ReflectionTestUtils.setField(delegate,"apiChannelBrandValue","BOIUK");
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		stageIden.setPaymentSetupVersion("2.0");
		HttpHeaders header=delegate.createPaymentRequestHeadersPost(stageIden, params);
		assertNotNull(header);
		
		
	}
	
	@Test
	public void testcreatePaymentRequestHeadersPostROI(){
		ReflectionTestUtils.setField(delegate,"apiChannelCode","apiChannelCode");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"sourcesystem","PSD2API");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		ReflectionTestUtils.setField(delegate,"apiChannelBrand","BOIROI");
	//	ReflectionTestUtils.setField(delegate,"apiChannelBrandValue","BOIROI");
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIROI");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		stageIden.setPaymentSetupVersion("1.0");
		HttpHeaders header=delegate.createPaymentRequestHeadersPost(stageIden, params);
		assertNotNull(header);
		
		
	}
	
	@Test
	public void testgetPaymentFoundationServiceURL(){
		String version="1.0";
		String PaymentInstuctionProposalId="1007";
		delegate.getPaymentFoundationServiceURL( PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}
	
	@Test
	public void testpostPaymentFoundationServiceURL(){
		String version="1.0";
		String PaymentInstuctionProposalId="1007";
		delegate.postPaymentFoundationServiceURL();
		assertNotNull(delegate);
	}
	
	@Test
	public void transformDomesticPaymentConsentsFromAPIToFDForInsertTest(){
		CustomDPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomDPaymentConsentsPOSTRequest();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params=new HashMap<>();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		
		Mockito.when(transformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params)).thenReturn(paymentInstructionProposal);
		delegate.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
		
	}
	
	@Test
	public void transformDomesticPaymentConsentsFromFDToAPIForInsertTest(){
		CustomDPaymentConsentsPOSTResponse paymentConsentsRequest = new CustomDPaymentConsentsPOSTResponse();
		PaymentInstructionProposal paymentInstructionProposalResponse = new PaymentInstructionProposal();
		Map<String, String> params=new HashMap<>();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		
		Mockito.when(transformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse)).thenReturn(paymentConsentsRequest);
		delegate.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse);
		
	}
}
