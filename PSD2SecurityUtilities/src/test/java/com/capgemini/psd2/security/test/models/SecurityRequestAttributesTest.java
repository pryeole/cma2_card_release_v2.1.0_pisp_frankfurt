/*package com.capgemini.psd2.security.test.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.security.models.AuthorizedTokenDTO;
import com.capgemini.psd2.security.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.validation.ConsentTypeCodeEnum;

public class SecurityRequestAttributesTest {

	private SecurityRequestAttributes  secReqAttributes;
	
	@Before
	public void setUp() throws Exception {
		secReqAttributes = new SecurityRequestAttributes();
		AuthorizedTokenDTO authorizedTokenDTO = new AuthorizedTokenDTO();
		authorizedTokenDTO.setFlowType(ConsentTypeCodeEnum.AccountRequestId.toString());
		secReqAttributes.setAuthorizedTokenDTO(authorizedTokenDTO );
		secReqAttributes.setCallBackEncUrl("/test");
		secReqAttributes.setFlowType(ConsentTypeCodeEnum.AccountRequestId.toString());
		secReqAttributes.setCode("code");
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("flowType", ConsentTypeCodeEnum.AccountRequestId.toString());
		secReqAttributes.setParamMap(paramMap );
		secReqAttributes.setRequestIdParamName(ConsentTypeCodeEnum.AccountRequestId.name());
	}
	
	@Test
	public void test(){
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(),secReqAttributes.getAuthorizedTokenDTO().getFlowType());
		assertNotNull("/test",secReqAttributes.getCallBackEncUrl());
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(), secReqAttributes.getFlowType());
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(),secReqAttributes.getParamMap().get("flowType"));
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.name(),secReqAttributes.getRequestIdParamName());
	    assertEquals("code", secReqAttributes.getCode());
	}
	
}
*/