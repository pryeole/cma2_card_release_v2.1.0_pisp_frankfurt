package com.capgemini.psd2.consent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerAccountInfoTest {

	private CustomerAccountInfo customerAccountInfo;
	
	@Before
	public void setUp() throws Exception {
		customerAccountInfo = new CustomerAccountInfo();
	    customerAccountInfo.setAccountName("Savings");
	    customerAccountInfo.setAccountNSC("12345");
	    customerAccountInfo.setAccountNumber("5238348989542309");
	    customerAccountInfo.setAccountType("Checking");
	    customerAccountInfo.setCurrency("EUR");
	    customerAccountInfo.setNickname("John");
	    customerAccountInfo.setUserId("2563634");
	}
	
	@Test
	public void test(){
		assertEquals("Savings",customerAccountInfo.getAccountName());
		assertEquals("12345",customerAccountInfo.getAccountNSC());
		assertEquals("5238348989542309",customerAccountInfo.getAccountNumber());
		assertEquals("Checking",customerAccountInfo.getAccountType());
		assertEquals("EUR",customerAccountInfo.getCurrency());
		assertEquals("John",customerAccountInfo.getNickname());
		assertEquals("2563634",customerAccountInfo.getUserId());
	}
	
	@Test 
	public void testHashCode(){
		CustomerAccountInfo custAccountInfo = new CustomerAccountInfo();
		custAccountInfo.setAccountName("Savings");
		custAccountInfo.setAccountNSC("12345");
		custAccountInfo.setAccountNumber("5238348989542309");
		custAccountInfo.setAccountType("Checking");
		custAccountInfo.setCurrency("EUR");
		custAccountInfo.setNickname("John");
		custAccountInfo.setUserId("2563634");
		assertEquals(customerAccountInfo.hashCode(), custAccountInfo.hashCode());
	}
	
	@Test 
	public void testEqualsTrue(){
		CustomerAccountInfo custAccountInfo = new CustomerAccountInfo();
		custAccountInfo.setAccountName("Savings");
		custAccountInfo.setAccountNSC("12345");
		custAccountInfo.setAccountNumber("5238348989542309");
		custAccountInfo.setAccountType("Checking");
		custAccountInfo.setCurrency("EUR");
		custAccountInfo.setNickname("John");
		custAccountInfo.setUserId("2563634");
		assertTrue(custAccountInfo.equals(customerAccountInfo));
	}
	@Test 
	public void testEqualsFalse(){
		CustomerAccountInfo custAccountInfo = new CustomerAccountInfo();
		custAccountInfo.setAccountName("Savings");
		custAccountInfo.setAccountNSC("123456");
		custAccountInfo.setAccountNumber("523834898");
		custAccountInfo.setAccountType("Checking");
		custAccountInfo.setCurrency("EUR");
		custAccountInfo.setNickname("John");
		custAccountInfo.setUserId("2563634");
		assertFalse(custAccountInfo.equals(customerAccountInfo));
	}
	
}
