package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadParty1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountPartyValidator")
@ConfigurationProperties("app")
public class AccountPartyValidatorImpl implements AISPCustomValidator<OBReadParty1, OBReadParty1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Override
	public OBReadParty1 validateRequestParams(OBReadParty1 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadParty1 obReadParty1) {
		if (resValidationEnabled)
			executeAccountPartyResponseSwaggerValidations(obReadParty1);
		return true;
	}
	private void executeAccountPartyResponseSwaggerValidations(OBReadParty1 obReadParty1) {
		psd2Validator.validate(obReadParty1);
	}


	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
	}



}