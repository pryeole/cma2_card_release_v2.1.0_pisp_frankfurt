package com.capgemini.psd2.pisp.standing.order.mongodb.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;

public interface DStandingOrderConsentFoundationRepository extends MongoRepository<CustomDStandingOrderConsentsPOSTResponse, String> {

	public CustomDStandingOrderConsentsPOSTResponse findOneByDataConsentId(String consentId);
}
