package com.capgemini.psd2.security.test.exceptions;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;

public class PSD2AuthenticationExceptionTest {

	private PSD2AuthenticationException psd2AuthException;
	private ErrorInfo errorInfo ;
	private SCAConsentErrorCodeEnum errorCodeEnum;
	
	@Before
	public void setUp() throws Exception {
	
	    errorInfo = new ErrorInfo("100", "Error message", "Detail error message", "400");
		errorInfo.setDetailErrorMessage("Detail Error Message");
		errorInfo.setErrorCode("100");
		errorInfo.setErrorMessage("Error Message");
		psd2AuthException = new PSD2AuthenticationException("Error Found", errorInfo);
		errorCodeEnum = SCAConsentErrorCodeEnum.INVALID_REQUEST;
	}
	@Test
	public void test(){
		psd2AuthException.getErrorInfo();
	}
	@SuppressWarnings("static-access")
	@Test
	public void testPopulateAuthenticationFailedException(){
		assertNotNull(psd2AuthException.populateAuthenticationFailedException(errorInfo));
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testPopulateAuthenticationFailedExceptionWithSecurityErrorCodeEnum(){
		assertNotNull(psd2AuthException.populateAuthenticationFailedException(errorCodeEnum ));
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testPopulateAuthenticationFailedExceptionWithSecurityErrorCodeEnumAndDetailMessage(){
		assertNotNull(psd2AuthException.populateAuthenticationFailedException(errorCodeEnum, errorInfo.getDetailErrorMessage()));
	}
	
	
}
