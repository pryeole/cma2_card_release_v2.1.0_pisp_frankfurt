// public/js/app.js
"use strict";
var consentApp = angular.module("consentApp", ["ui.router", "ui.bootstrap", "blockUI", "isoCurrency", "pascalprecht.translate", "fraudAnalyzer", "consentPartials"]);

consentApp.value("envVariables", {
    blockChain: false
});

consentApp.run(["$rootScope", "config", "$window", "$state", "$location", "$fraudAnalyze", function ($rootScope, config, $window, $state, $location, $fraudAnalyze) {
    var consentType = $("#consentType").val();
    var authorisationRenewal = $("#renewalAuthorisation").val();
    $rootScope.$on("$stateChangeSuccess", function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        if (toState.url !== config.applicationType) {
            if ((toState.name === "accountAccess") ||
                (toState.name === "pispAccount") ||
                (toState.name === "cispAccount") ||
                (toState.name === "aispAuthorisationRenewal")) {
                event.preventDefault();
                return false;
            } else {
                if ((toState.name === "aispReview" && toParams.accountRequestDetails === null) ||
                    (toState.name === "pispReview" && toParams.paymentDetails === null) ||
                    (toState.name === "aispAuthorisationRenewal")
                ) {
                    event.preventDefault();
                    return false;
                }
            }
            return;
        }
    });
    $fraudAnalyze.loaded.then(function () {
        $fraudAnalyze.init = function () {
            if (!$("#boiukppm").length) {
                $("body").append("<input type='hidden' id='boiukppm' name='boiukppm'>");
                $("body").append("<input type='hidden' id='boiukprefs2' name='boiukprefs2'>");
                var fraudHdmInfoData = angular.fromJson($("#fraudHdmInfo").val());
                var args = [];
                args[0] = decodeURIComponent(fraudHdmInfoData.resUrl); // using decodeURIComponent() for decode the url
                args[1] = decodeURIComponent(fraudHdmInfoData.hdmUrl);
                args[2] = fraudHdmInfoData.hdmInputName;
                $window.boiukns.boiukfn("boiukppm", args);
            }
        };
        $fraudAnalyze.capture = function (dateTime) {
            $window.boiukns.boiukcfn("boiukppm");
            var headers = {};
            headers["X-fn-device-jsc"] = $("#boiukppm").val();
            headers["X-fn-device-hdim-payload"] = $("#boiukprefs2").val();
            headers["X-fn-event-time"] = dateTime;
            return headers;
        };
    });
}]);

consentApp.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "blockUIConfig", "$translateProvider", "$qProvider",
    "$translatePartialLoaderProvider", "config", "$fraudAnalyzeProvider",
    function ($stateProvider, $urlRouterProvider, $httpProvider, blockUIConfig, $translateProvider, $qProvider, $translatePartialLoaderProvider, config, $fraudAnalyzeProvider) {
        var $ = window.jQuery;
        var staticContent = angular.fromJson($("#staticContent").val());
        $translatePartialLoaderProvider.addPart("index");
        $translateProvider.translations("en", staticContent);
        $translateProvider.preferredLanguage("en");
        $translateProvider.fallbackLanguage("en");
        $translateProvider.useSanitizeValueStrategy("sceParameters");

        var $jscFilePath = jQuery("#jsc-file-path").val();
        if ($jscFilePath && $jscFilePath.length) {
            $fraudAnalyzeProvider.setJsCollectors([$jscFilePath]);
        }

        blockUIConfig.templateUrl = "views/loading-spinner.html";
        $qProvider.errorOnUnhandledRejections(false);
        $httpProvider.interceptors.push(["$q", "blockUI", "$timeout", function ($q, blockUI, $timeout) {
            return {
                request: function (config) {
                    blockUI.start();
                    return config;
                },
                requestError: function (rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                },
                response: function (response) {
                    blockUI.stop();
                    return response;
                },
                responseError: function (rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                }
            };
        }]);


        $stateProvider.
            state("accountAccess", {
                params: { selectedAccountDetails: null },
                "url": "/account-access",
                templateUrl: "views/account-access.html",
                controller: "AccountAccessCtrl",
                resolve: {
                    fontResolved: ["fontloaderService", function (fontloaderService) {
                        return fontloaderService.isFontLoad();
                    }]
                }
            }).
            state("aispReview", {
                params: { accountRequestDetails: null },
                "url": "/aisp-review",
                templateUrl: "views/aisp-review.html",
                controller: "AispReviewCtrl"
            }).
            state("aispAuthorisationRenewal", {
                params: { accountRequestDetails: null },
                "url": "/aisp-authorisation-renewal",
                templateUrl: "views/aisp-authorisation-renewal.html",
                controller: "AispAuthorisationRenewalCtrl"
            }).
            state("pispAccount", {
                params: {
                    pispContractDetails: null
                },
                "url": "/pisp-account",
                templateUrl: "views/pisp-account.html",
                controller: "PispAccountCtrl",
                resolve: {
                    fontResolved: ["fontloaderService", function (fontloaderService) {
                        return fontloaderService.isFontLoad();
                    }]
                }
            }).
            state("pispReview", {
                params: {
                    paymentDetails: null
                },
                "url": "/pisp-review",
                templateUrl: "views/pisp-review.html",
                controller: "PispReviewCtrl"
            }).
            state("cispAccount", {
                // params: { selectedAccountDetails: null },
                "url": "/cisp-account",
                templateUrl: "views/funds-check.html",
                controller: "FundsCheckCtrl"
            });

        var authorisationRenewal = $("#renewalAuthorisation").val();
        var consentType = $("#consentType").val();
        if (authorisationRenewal === "true" && consentType === config.aispConsent) {
            config.applicationType = "/aisp-authorisation-renewal";
        }
        else {
            if (consentType === config.pispConsent) {
                config.applicationType = "/pisp-account";
            } else if (consentType === config.aispConsent) {
                config.applicationType = "/account-access";
            } else {
                config.applicationType = "/cisp-account";
            }
        }
        $urlRouterProvider.otherwise(config.applicationType);
    }]);

// Constants for application
"use strict";
consentApp.constant("config", {
    accTypeChecking: "Checking",
    accTypeCredit: "CreditCard",
    minConsentPeriod: 1,
    maxConsentPeriod: 180,
    minTransHistoryPeriod: 1,
    maxTransHistoryPeriod: 36,
    popupTimings:8000,
    lang: "en",
    defaultCssTheme: "boi-uk-theme.css",
    logoPath: "img/boi_logo.svg",
    aispConsent:"AISP",
    pispConsent:"PISP",
    cispConsent:"CISP",
    nsc: "SortCodeAccountNumber",
    iban: "IBAN",
    pan:"PAN",
    searchOnAccounts: false,
    chromeBorderStartVersion:45,
    chromeBorderEndVersion:47,
    notSeeingVisibleAcctCount:10,
    pageSize:10,
    maxPaginationSize:5,
    maskAccountNumberLength:4,
    totalNumberVisibleRows:3,
    paymentType:"DomesticScheduledPayments",
    standingOrderPaymentType:"DomesticStandingOrdersPayments",
    modelpopupConfig: {
        open: function() {/**/},
        "modelpopupTitle": "Title",
        "modelpopupBodyContent": "Content",
        "escBtn":false,
        "btn": {
            "okbtn": { "visible": true, "label": "", "action": null },
            "cancelbtn": { "visible": true, "label": "", "action": null }
        }
    }
});

"use strict";
angular.module("consentApp").controller("AccountAccessCtrl", ["$scope", "$uibModal", "AcctService", "$filter", "$translate", "$state",
    "ConsentService", "config", "blockUI", "$window", "$timeout", "$sce", "$fraudAnalyze",
    function ($scope, $uibModal, AcctService, $filter, $translate, $state, ConsentService, config, blockUI, $window, $timeout, $sce, $fraudAnalyze, fontResolved) {
        var $ = window.jQuery;
        $scope.init = function () {
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.accReq = null;
            $scope.returnTppbtn = false;
            $scope.noAccountFound = false;
            $scope.allAccounts = null;
            $scope.errorData = null;
            $scope.retry = null;
            $scope.pageSize = config.pageSize;
            $scope.notSeeingVisibleAcctCount = config.notSeeingVisibleAcctCount;
            $scope.notSeeingInfo = false;
            $scope.maxPaginationSize = config.maxPaginationSize;
            $scope.searchOnAccounts = config.searchOnAccounts;
            $scope.maskAccountNumberLength = config.maskAccountNumberLength;
            $scope.AccountsByPage = {};
            $scope.accountsData = [];
            $scope.selAccounts = [];
            $scope.searchText = "";
            $scope.currentPage = 1;
            $scope.isAllAccountSelected = false;
            $scope.filteredAccts = [];
            $scope.modelPopUpConf = config.modelpopupConfig;
            $scope.isDataFound = false;
            $scope.consentType = 'aisp';
            var accountDetail = false;
            var regularPayment = false;
            var accountTransactions = false;
            var statements = false;
            var accountFeature = false;
            $scope.callFromTppButton = false;

            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.isDataFound = true;
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        }, 0);
                    } else {
                        $scope.returnTppbtn = true;
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.accReq = angular.fromJson($("#account-request").val());
                        $scope.allAccounts = angular.fromJson($("#psuAccounts").val()).Data.Account;
                        if (!$scope.allAccounts.length) {
                            $scope.returnTppbtn = true;
                            $scope.noAccountFound = true;
                            throw null;
                        }
                        $scope.allAccounts = AcctService.updateAcctData($scope.allAccounts);
                        if ($scope.allAccounts.length >= $scope.notSeeingVisibleAcctCount) {
                            $scope.notSeeingInfo = true;
                        }
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.accountReqDetails = $state.params.accountRequestDetails;
                        $scope.permissionsList = $scope.accReq.Data.Permissions;
                        $scope.expiryDate = $scope.ignoreTimeZone($scope.accReq.Data.ExpirationDateTime);
                        $scope.termsConditn = false;
                        $scope.isAccSelected = false;

                        $scope.tillDate = $scope.ignoreTimeZone($scope.accReq.Data.TransactionToDateTime);
                        $scope.fromDate = $scope.ignoreTimeZone($scope.accReq.Data.TransactionFromDateTime);

                        $scope.permissionListData = [];
                        $scope.permissionExists = [];
                        angular.forEach($scope.permissionsList, function (perList) {
                            if (perList !== "ReadTransactionsCredits" && perList !== "ReadTransactionsDebits") {
                                if (perList === "ReadTransactionsBasic") {
                                    if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                        perList = "AllReadTransactionsBasic";
                                    } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                        perList = "ReadTransactionsCreditsBasic";
                                    } else {
                                        perList = "ReadTransactionsDebitsBasic";
                                    }
                                } else if (perList === "ReadTransactionsDetail") {
                                    if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                        perList = "AllReadTransactionsDetail";
                                    } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                        perList = "ReadTransactionsCreditsDetail";
                                    } else {
                                        perList = "ReadTransactionsDebitsDetail";
                                    }
                                }
                                $scope.permissionListData.push(perList);
                                angular.forEach($scope.permissionListData, function (perList) {
                                    if (perList === "ReadAccountsBasic" || perList === "ReadAccountsDetail" || perList === "ReadBalances" || perList === "ReadPAN") {
                                        accountDetail = true;
                                    }
                                    else if (perList === "ReadBeneficiariesBasic" || perList === "ReadBeneficiariesDetail" || perList === "ReadStandingOrdersBasic" || perList === "ReadStandingOrdersDetail" || perList === "ReadDirectDebits" || perList === "ReadScheduledPaymentsBasic" || perList === "ReadScheduledPaymentsDetail") {
                                        regularPayment = true;
                                    }
                                    else if (perList === "AllReadTransactionsBasic" || perList === "ReadTransactionsCreditsBasic" || perList === "ReadTransactionsDebitsBasic" || perList === "AllReadTransactionsDetail" || perList === "ReadTransactionsCreditsDetail" || perList === "ReadTransactionsDebitsDetail") {
                                        accountTransactions = true;
                                    }
                                    else if (perList === "ReadStatementsBasic" || perList === "ReadStatementsDetail") {
                                        statements = true;
                                    }
                                    else if (perList === "ReadProducts") {
                                        accountFeature = true;
                                    }
                                });

                                $scope.permissionExists[0] = accountDetail;
                                $scope.permissionExists[1] = regularPayment;
                                $scope.permissionExists[2] = accountTransactions;
                                $scope.permissionExists[3] = statements;
                                $scope.permissionExists[4] = accountFeature;
                            }
                        });
                        // code for Info Section.............
                        var impInfo = "AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.";
                        var impInfoData = ["IMPORTANT_INFORMATION_HEADING1", "IMPORTANT_INFORMATION_HEADING2",
                            "IMPORTANT_INFORMATION_HEADING3", "IMPORTANT_INFORMATION_HEADING4",
                            "IMPORTANT_INFORMATION_HEADING5", "IMPORTANT_INFORMATION_DESCRIPTION1",
                            "IMPORTANT_INFORMATION_DESCRIPTION2", "IMPORTANT_INFORMATION_DESCRIPTION3",
                            "IMPORTANT_INFORMATION_DESCRIPTION4", "IMPORTANT_INFORMATION_DESCRIPTION5"];
                        $scope.infoData = {};

                        $translate(impInfoData.map(function (key) { return impInfo + key; })).then(function (translations) {
                            for (var i = 1; i <= 5; i++) {
                                if (translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i] !== "" ||
                                    translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i] !== "") {
                                    $scope.infoData[i] = {};
                                    $scope.infoData[i].title = translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i];
                                    $scope.infoData[i].desc = $sce.trustAsHtml(translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION"
                                        + i]);
                                }
                            }
                        });

                        impInfoSection();

                        $scope.tppInfo = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";
                        $scope.disabledMsgShow = false;
                        angular.forEach($scope.allAccounts, function (pacct) {
                            pacct.selected = false;
                            $scope.accountDisabledFilter(pacct);
                        });
                        $scope.pagination();
                        $scope.isSelectedAll();
                        $scope.$watch("currentPage", function (newVal, oldVal) {
                            $("#" + newVal).focus();
                            $scope.selectAllAcctBtnChecked();
                            if (newVal !== oldVal) {
                                $scope.isSelectedAll();
                            }
                        }, true);

                        //document.getElementsByClassName("permission-data").removeAttribute("aria-controls");
                        //$('a').removeAttr('aria-controls');
                        

                    } catch (e) {
                        $scope.returnTppbtn = true;
                        if ($scope.noAccountFound) {
                            $scope.createCustomErr({ "errorCode": "502" });
                        }
                        else {
                            $scope.createCustomErr({ "errorCode": "999" });
                        }

                    }
                }
            }// code for Info Section.............
            var ns = "AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.";
            var jDATA = ["IMPORTANT_INFORMATION_HEADING1", "IMPORTANT_INFORMATION_HEADING2",
                "IMPORTANT_INFORMATION_HEADING3", "IMPORTANT_INFORMATION_HEADING4",
                "IMPORTANT_INFORMATION_HEADING5", "IMPORTANT_INFORMATION_DESCRIPTION1",
                "IMPORTANT_INFORMATION_DESCRIPTION2", "IMPORTANT_INFORMATION_DESCRIPTION3",
                "IMPORTANT_INFORMATION_DESCRIPTION4", "IMPORTANT_INFORMATION_DESCRIPTION5"];
            $scope.infoData = {};
            $translate(jDATA.map(function (key) { return ns + key; })).then(function (translations) {
                for (var i = 1; i <= 5; i++) {
                    if (translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i] !== "" ||
                        translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i] !== "") {
                        $scope.infoData[i] = {};
                        $scope.infoData[i].title = translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i];
                        $scope.infoData[i].desc = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i]);
                    }
                }
            });

            $translate(["AISP.HEADER.LOGOURL", "AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "AISP.HEADER.ACCOUNT_ACCESS_LABEL", "AISP.HEADER.THIRD_PARTY_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ALL_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_MSG",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_ACCOUNT_MSG",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CHECKBOX_SCREENREADER_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_SCREENREADER_TITLE",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECT_INFO_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECTED_INFO_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECTED_OF_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_ALL_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_NONE_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_VIEW_REQUESTED_PERMISSION",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_NOT_SEEING_ACCOUNTS",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_SCREENREADER_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_LABEL",
                "AISP.ACCOUNT_ACCESS_FOOTER.ABOUT_US_LABEL", "AISP.ACCOUNT_ACCESS_FOOTER.PRIVACY_POLICY_LABEL",
                "AISP.ACCOUNT_ACCESS_FOOTER.TNC_LABEL", "AISP.ACCOUNT_ACCESS_FOOTER.HELP_LABEL",
                "AISP.ACCOUNT_ACCESS_FOOTER.LINK_TOOLTIP_NEW_WINDOW",
                "AISP.ACCOUNT_ACCESS_FOOTER.REGULATORY_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.AUTHORISE_ACCOUNT_ACESS_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.PAGE_DESCRIPTION.PAGE_DESCRIPTION_LABEL",
                "AISP.ACCOUNT_ACCESS_FOOTER.ABOUT_US_URL", "AISP.ACCOUNT_ACCESS_FOOTER.PRIVACY_POLICY_URL",
                "AISP.ACCOUNT_SELECTION_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL",
                "AISP.ACCOUNT_ACCESS_FOOTER.TNC_URL", "AISP.ACCOUNT_ACCESS_FOOTER.HELP_URL", "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_SCREENREADER_MSG",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.AUTHORISE_ACCOUNT_ACTION_BUTTON_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.accountAccessText = translations["AISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                    $scope.thirdPartyText = translations["AISP.HEADER.THIRD_PARTY_LABEL"];
                    $scope.accountSelText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER"];
                    $scope.preUserTitleText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL"];
                    $scope.postUserTitleText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL"];
                    $scope.logoUrl = translations["AISP.HEADER.LOGOURL"];
                    $scope.bankLogoImgAlt = translations["AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.selAccountText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER"];
                    $scope.selAccountTableCaptionText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_TABLE_CAPTION"];
                    $scope.accountSelTitleText = translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_DESCRIPTION.PAGE_DESCRIPTION_HEADER"];
                    $scope.accountSelDescriptionText = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.PAGE_DESCRIPTION.PAGE_DESCRIPTION_LABEL"]);
                    $scope.selAllText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ALL_COLUMN_HEADER"];
                    $scope.nickNameText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER"];
                    $scope.acctNoText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER"];
                    $scope.currencyText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER"];
                    $scope.acctTypeText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER"];
                    $scope.balanceText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER"];
                    $scope.checkboxScrLabel = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CHECKBOX_SCREENREADER_LABEL"];
                    $scope.selectedAccountText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER"];
                    $scope.selectedAccountTableCaptionText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                    $scope.removeText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.REMOVE_COLUMN_HEADER"];
                    $scope.continueBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL"];
                    $scope.retryBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL"];
                    $scope.denyBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL"];
                    $scope.selectAllBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_ALL_BUTTON_LABEL"];
                    $scope.selectNoneBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.SELECT_NONE_BUTTON_LABEL"];
                    $scope.authoriseAcctAcessBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.AUTHORISE_ACCOUNT_ACESS_BUTTON_LABEL"];
                    $scope.backToTopBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL"];
                    $scope.viewReqstPerBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_LABEL"];
                    $scope.notSeeingText = translations["AISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT"];
                    $scope.findOutWhyText = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_LABEL"]);
                    $scope.findOutWhyTitle = translations["AISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_SCREENREADER_TITLE"];
                    $scope.AccountSelectInfoText = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECT_INFO_LABEL"];
                    $scope.AccountSelectedInfoText = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECTED_INFO_LABEL"];
                    $scope.AccountSelectOfText = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECTED_OF_LABEL"];
                    $scope.aboutUsText = translations["AISP.ACCOUNT_ACCESS_FOOTER.ABOUT_US_LABEL"];
                    $scope.cookieText = translations["AISP.ACCOUNT_ACCESS_FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tncText = translations["AISP.ACCOUNT_ACCESS_FOOTER.TNC_LABEL"];
                    $scope.helpText = translations["AISP.ACCOUNT_ACCESS_FOOTER.HELP_LABEL"];
                    $scope.regulatoryText = translations["AISP.ACCOUNT_ACCESS_FOOTER.REGULATORY_LABEL"];
                    $scope.aboutUsUrl = translations["AISP.ACCOUNT_ACCESS_FOOTER.ABOUT_US_URL"];
                    $scope.privacyPolicyUrl = translations["AISP.ACCOUNT_ACCESS_FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncUrl = translations["AISP.ACCOUNT_ACCESS_FOOTER.TNC_URL"];
                    $scope.helpUrl = translations["AISP.ACCOUNT_ACCESS_FOOTER.HELP_URL"];
                    $scope.returntotppbtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL"];
                    $scope.tooltipTt = translations["AISP.ACCOUNT_ACCESS_FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                    $scope.backToTopBtnVewRequestPermissionTitle = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_VIEW_REQUESTED_PERMISSION"];
                    $scope.backToTopBtnNotSeeingAcctTitle = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_NOT_SEEING_ACCOUNTS"];
                    $scope.viewRequestedButtonTitle = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_SCREENREADER_LABEL"];
                    $scope.fromText = translations["AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL"];
                    $scope.toText = translations["AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL"];
                    $scope.preConsentValidityText = translations["AISP.ACCOUNT_SELECTION_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL"];
                    $scope.postConsentValidityText = translations["AISP.ACCOUNT_SELECTION_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL"];
                    $scope.noConsentValidityText = translations["AISP.ACCOUNT_SELECTION_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL"];
                    $scope.disabledPopoverMsg = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_MSG"]);
                    $scope.disabledPopoverScrMsg = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_POPOVER_SCREENREADER_MSG"];
                    $scope.disabledAccountMsg = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.DISABLED_ACCOUNT_MSG"]);
                    $scope.cancelBtnPopText = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL"];
                    $scope.authoriseAccessActionText = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.AUTHORISE_ACCOUNT_ACTION_BUTTON_SCREENREADER_LABEL"];
                });
        };
        $scope.createCustomErr = function (error) {
            $scope.isDataFound = true;
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "800";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };
        $scope.pagination = function () {
            $scope.pages = 1;
            $scope.currentPage = 1;
            $scope.AccountsByPage = AcctService.paged($scope.allAccounts, $scope.pageSize);
            $scope.accountsData = $scope.AccountsByPage[$scope.currentPage];
        };
        // Select all account while clicking on single check box
        $scope.accountToggleAll = function (toggleStatus) {
            angular.forEach($scope.AccountsByPage[$scope.currentPage], function (acct) {
                if (acct.accountDisabled) {
                    acct.selected = false;
                } else {
                    acct.selected = toggleStatus;
                }
            });
            $scope.selectedAccountList();
        };

        $scope.isSelectedAll = function () {
            var selectedAcct = $filter("filter")($scope.AccountsByPage[$scope.currentPage], { "selected": true });
            var disabledAcct = $filter("filter")($scope.AccountsByPage[$scope.currentPage], { "accountDisabled": false });
            //code for select all button focus for jaws
            if (window.event && window.event.target.type === "button") {
                $timeout(function () {
                    $(".accountselectionBtn").focus();
                });
            }
            if (selectedAcct.length === disabledAcct.length && disabledAcct.length > 0) {
                $scope.isAllAccountSelected = true;
            } else {
                $scope.isAllAccountSelected = false;
            }
        };
        // Account selected and deselect on checkbox change
        $scope.selectedAccountList = function () {
            $scope.selAccounts = $filter("filter")($scope.allAccounts, { "selected": true });
            if ($scope.selAccounts.length) {
                $scope.isAccSelected = true;
                $scope.AccountSelectCounterText = $scope.selAccounts.length + " " + $scope.AccountSelectOfText + " " + $scope.allAccounts.length;
            } else {
                $scope.isAccSelected = false;
            }
            $scope.isSelectedAll();
        };

        /* Allow Button Submission Code  */
        $scope.allowSubmission = function () {
            var resumePath = $("#resumePath").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var payLoad = {};
            payLoad["accountDetails"] = AcctService.updateAcctData($scope.selAccounts);
            payLoad["fsHeaders"] = $("#fsHeaders").val();

            var correlationId = $("#correlationId").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.accountRequest($scope.consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openAuthoriseAcessModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                        $($form).appendTo("body");
                        $($form).submit();
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };
        /* Cancel button code here........ */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.cancelRequest($scope.consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openCancelAuthoriseModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        $window.location.href = resp.data.model.redirectUri;
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };

        $scope.returnToThirdparty = function () {
            $scope.callFromTppButton = true;
            $scope.cancelSubmission();
        }

        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };
        //    Deny modal
        $scope.openDenyModal = function () {
            $translate(["AISP.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_HEADER", "AISP.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.DENY_AUTHORISATION_POPUP.YES_BUTTON_LABEL",
                "AISP.DENY_AUTHORISATION_POPUP.NO_BUTTON_LABEL", "AISP.DENY_AUTHORISATION_POPUP.CLOSE_BUTTON_LABEL",
                "AISP.DENY_AUTHORISATION_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL", "AISP.DENY_AUTHORISATION_POPUP.PRESS_NO_SCREENREADER_LABEL", "AISP.DENY_AUTHORISATION_POPUP.PRESS_YES_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_HEADER"];
                    var popupContent = translations["AISP.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent = popupContent;
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.DENY_AUTHORISATION_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["AISP.DENY_AUTHORISATION_POPUP.NO_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["AISP.DENY_AUTHORISATION_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.cancelRequestScrLabel = translations["AISP.DENY_AUTHORISATION_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.DENY_AUTHORISATION_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.yesBtnScrLabel = translations["AISP.DENY_AUTHORISATION_POPUP.PRESS_YES_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "denypopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };

        //authorise access modal
        $scope.openAuthoriseAcessModal = function () {
            $translate(["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER",
                "AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT",
                "AISP.ACCESS_AUTHORISATION_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = '';
                    $scope.modelPopUpConf.logoUrl = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                    $scope.modelPopUpConf.authoriseModalOpenScrLabel = translations["AISP.ACCESS_AUTHORISATION_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };
        //Joint Account ifo popupContent
        $scope.openJointAcctPopup = function () {
            $translate(["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_POPUP_HEADER",
                "AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_POPUP_MESSAGE",
                "AISP.JOINT_ACCOUNT_POPUP.OK_BUTTON_LABEL",
                "AISP.JOINT_ACCOUNT_POPUP.OK_BUTTON_SCREENREADER_LABEL",
                "AISP.JOINT_ACCOUNT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL",
                "AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_FIND_OUT_LINK",
                "AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_LINK_SCREENREADER",
                "AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_HELP_LINK"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = $sce.trustAsHtml(translations["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_POPUP_MESSAGE"]);
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.JOINT_ACCOUNT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.JOINT_ACCOUNT_POPUP.OK_BUTTON_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.openjointActScrLabel = translations["AISP.JOINT_ACCOUNT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.jointAcctFindOutLink = translations["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_FIND_OUT_LINK"];
                    $scope.modelPopUpConf.jointActLinkLabel = translations["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_LINK_SCREENREADER"];
                    $scope.modelPopUpConf.jointAcctHelpLink = translations["AISP.JOINT_ACCOUNT_POPUP.JOINT_ACCOUNT_HELP_LINK"];
                });
            $scope.modelPopUpConf.modelpopupType = "jointAccInfo";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };


        //cancel authorise  modal
        $scope.openCancelAuthoriseModal = function () {
            $translate(["AISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "AISP.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER",
                "AISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST", "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["AISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["AISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.logoUrl = translations["AISP.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();

        };

        /* sessionTimeoutSubmission button code here........ */
        $scope.openSessionOutModal = function () {
            $translate(["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL", "AISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL",
                "AISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["AISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        $scope.accountDisabledFilter = function (acct) {
            if (acct.AdditionalInformation && acct.AdditionalInformation["ACCOUNT-PERMISSION"] && acct.AdditionalInformation["ACCOUNT-PERMISSION"].indexOf("Display Only Account") !== -1) {
                acct.accountDisabled = true;
            } else {
                acct.accountDisabled = false;
            }
        };

        $scope.selectAllAcctBtnChecked = function () {
            $scope.selectAllAccBtnEnabled = true;
            $scope.enabledAccounts = $filter("filter")($scope.AccountsByPage[$scope.currentPage], { "accountDisabled": false });
            if ($scope.enabledAccounts.length > 0) {
                $scope.selectAllAccBtnEnabled = true;
            } else {
                $scope.selectAllAccBtnEnabled = false;
            }
        };
        $scope.ignoreTimeZone = function (val) {
            if (val !== undefined && val !== "" && val !== null) {
                return new Date(val.substring(0, val.indexOf("T")));
            } else {
                return null;
            }
        };
        $scope.scrollRedirect = function ($event, id) {
            $event.preventDefault();
            document.getElementById(id).scrollIntoView();
        };

        var impInfoSection = function () {

            var impSubInfo = "AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.";
            var impInfoAllData = [
                'IMPORTANT_INFORMATION_SECTION_HEADING',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_HEADING1',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_HEADING1',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_HEADING1',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_PARAGRAPH1',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_PARAGRAPH1',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_PARAGRAPH1',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_PARAGRAPH2',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_PARAGRAPH2',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_PARAGRAPH2',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM1',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM2',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM3',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM4',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM1',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM2',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM3',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM4',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM1',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM2',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM3',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM4'
            ];

            $scope.subHeadingInfo = {};

            $scope.impInfoSection = {
                mainHeading: 'IMPORTANT_INFORMATION_SECTION_HEADING',
                subHeading: [{
                    heading: 'IMPORTANT_INFORMATION_HEADING1',
                    para: ['IMPORTANT_INFORMATION_PARAGRAPH1',
                        'IMPORTANT_INFORMATION_PARAGRAPH2',
                        'IMPORTANT_INFORMATION_PARAGRAPH3'
                    ],
                    list: [
                        'IMPORTANT_INFORMATION_LIST_ITEM1',
                        'IMPORTANT_INFORMATION_LIST_ITEM2',
                        'IMPORTANT_INFORMATION_LIST_ITEM3',
                        'IMPORTANT_INFORMATION_LIST_ITEM4'
                    ]
                },
                {
                    heading: 'IMPORTANT_INFORMATION_HEADING1',
                    para: ['IMPORTANT_INFORMATION_PARAGRAPH1',
                        'IMPORTANT_INFORMATION_PARAGRAPH2',
                        'IMPORTANT_INFORMATION_PARAGRAPH3'
                    ],
                    list: ['IMPORTANT_INFORMATION_LIST_ITEM1',
                        'IMPORTANT_INFORMATION_LIST_ITEM2',
                        'IMPORTANT_INFORMATION_LIST_ITEM3',
                        'IMPORTANT_INFORMATION_LIST_ITEM4']
                },
                {
                    heading: 'IMPORTANT_INFORMATION_HEADING1',
                    para: ['IMPORTANT_INFORMATION_PARAGRAPH1',
                        'IMPORTANT_INFORMATION_PARAGRAPH2',
                        'IMPORTANT_INFORMATION_PARAGRAPH3'
                    ],
                    list: ['IMPORTANT_INFORMATION_LIST_ITEM1',
                        'IMPORTANT_INFORMATION_LIST_ITEM2',
                        'IMPORTANT_INFORMATION_LIST_ITEM3',
                        'IMPORTANT_INFORMATION_LIST_ITEM4']
                }]
            };

            $translate(impInfoAllData.map(function (key) { return impSubInfo + key; })).then(function (translations) {
                $scope.infoMainHeading = translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_HEADING"];
                for (var i = 0; i <= $scope.impInfoSection.subHeading.length; i++) {
                    $scope.subHeadingInfo[i] = {};
                    $scope.subHeadingInfo[i].heading = translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_" + i + "." + $scope.impInfoSection.subHeading[i].heading];
                    $scope.subHeadingInfo[i].para = [];
                    $scope.subHeadingInfo[i].list = [];
                    for (var j = 0; j <= $scope.impInfoSection.subHeading[i].para.length; j++) {
                        var str = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_" + i + "." + $scope.impInfoSection.subHeading[i].para[j]]);
                        $scope.subHeadingInfo[i].para.push(str);
                    }

                    for (var k = 0; k <= $scope.impInfoSection.subHeading[i].list.length; k++) {
                        var listStr = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_" + i + "." + $scope.impInfoSection.subHeading[i].list[k]]);
                        $scope.subHeadingInfo[i].list.push(listStr);
                    }
                }
            });            
        }

        $scope.init();       

        $(document).on('click', '.accordion-toggle', function (e) {
            $(this).removeAttr('aria-controls');
        });
          
    }]);

"use strict";

angular.module("consentApp").controller("AispAuthorisationRenewalCtrl", ["$scope", "$state", "$translate", "ConsentService", "AcctService", "blockUI",
    "$window", "$uibModal", "$filter", "config", "$timeout", "$sce", "$fraudAnalyze",
    function ($scope, $state, $translate, ConsentService, AcctService, blockUI, $window, $uibModal, $filter, config, $timeout, $sce, $fraudAnalyze) {
        /* start code of Agree/submit button clicked */
        $scope.init = function () {
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.pageError = angular.fromJson($("#error").val());            
            $scope.AccountsByPage = {};
            $scope.pageSize = config.pageSize;
            $scope.maxPaginationSize = config.maxPaginationSize;
            $scope.searchOnAccounts = config.searchOnAccounts;
            $scope.maskAccountNumberLength = config.maskAccountNumberLength;
            $scope.modelPopUpConf = config.modelpopupConfig;
            $scope.consentType = 'aisp';
            $scope.returnTppbtn = false;
            var accountDetail = false;
            var regularPayment = false;
            var accountTransactions = false;
            var statements = false;
            var accountFeature = false;
            $scope.callFromTppButton = false;
            $scope.noAccountFound = false;

            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        }, 0);
                    } else {
                        $scope.returnTppbtn = true;
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.accountReqDetails = angular.fromJson($("#account-request").val());
                        $scope.permissionsList = $scope.accountReqDetails.Data.Permissions;
                        $scope.selectedAcctTableData = angular.fromJson($("#psuAccounts").val()).Data.Account;
                        $scope.expiryDate = $scope.ignoreTimeZone($scope.accountReqDetails.Data.ExpirationDateTime);
                        $scope.termsConditn = false;
                        $scope.totalNumberVisibleRows = config.totalNumberVisibleRows;
                        $scope.isCollapsed = $scope.selectedAcctTableData.length > $scope.totalNumberVisibleRows;
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.tppName = $scope.tppInfoData.applicationName;
                        $scope.tppInfo = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";
                        $scope.maskAccountNumberLength = config.maskAccountNumberLength;
                        $scope.tillDate = $scope.ignoreTimeZone($scope.accountReqDetails.Data.TransactionToDateTime);
                        $scope.fromDate = $scope.ignoreTimeZone($scope.accountReqDetails.Data.TransactionFromDateTime);
                        $scope.allAccounts = AcctService.updateAcctData($scope.selectedAcctTableData);
                        if (!$scope.allAccounts.length) {
                            $scope.returnTppbtn = true;
                            $scope.noAccountFound = true;
                            throw null;
                        }
                        $scope.permissionListData = [];
                        $scope.permissionExists = [];
                        angular.forEach($scope.permissionsList, function (perList) {
                            if (perList !== "ReadTransactionsCredits" && perList !== "ReadTransactionsDebits") {
                                if (perList === "ReadTransactionsBasic") {
                                    if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                        perList = "AllReadTransactionsBasic";
                                    } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                        perList = "ReadTransactionsCreditsBasic";
                                    } else {
                                        perList = "ReadTransactionsDebitsBasic";
                                    }
                                } else if (perList === "ReadTransactionsDetail") {
                                    if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1 && $scope.permissionsList.indexOf("ReadTransactionsDebits") !== -1) {
                                        perList = "AllReadTransactionsDetail";
                                    } else if ($scope.permissionsList.indexOf("ReadTransactionsCredits") !== -1) {
                                        perList = "ReadTransactionsCreditsDetail";
                                    } else {
                                        perList = "ReadTransactionsDebitsDetail";
                                    }
                                }
                                $scope.pagination();
                                $scope.permissionListData.push(perList);

                                angular.forEach($scope.permissionListData, function (perList) {
                                    if (perList === "ReadAccountsBasic" || perList === "ReadAccountsDetail" || perList === "ReadBalances" || perList === "ReadPAN") {
                                        accountDetail = true;
                                    }
                                    else if (perList === "ReadBeneficiariesBasic" || perList === "ReadBeneficiariesDetail" || perList === "ReadStandingOrdersBasic" || perList === "ReadStandingOrdersDetail" || perList === "ReadDirectDebits" || perList === "ReadScheduledPaymentsBasic" || perList === "ReadScheduledPaymentsDetail") {
                                        regularPayment = true;
                                    }
                                    else if (perList === "AllReadTransactionsBasic" || perList === "ReadTransactionsCreditsBasic" || perList === "ReadTransactionsDebitsBasic" || perList === "AllReadTransactionsDetail" || perList === "ReadTransactionsCreditsDetail" || perList === "ReadTransactionsDebitsDetail") {
                                        accountTransactions = true;
                                    }
                                    else if (perList === "ReadStatementsBasic" || perList === "ReadStatementsDetail") {
                                        statements = true;
                                    }
                                    else if (perList === "ReadProducts") {
                                        accountFeature = true;
                                    }
                                });

                                $scope.permissionExists[0] = accountDetail;
                                $scope.permissionExists[1] = regularPayment;
                                $scope.permissionExists[2] = accountTransactions;
                                $scope.permissionExists[3] = statements;
                                $scope.permissionExists[4] = accountFeature;

                            }
                        });
                    } catch (e) {
                        $scope.returnTppbtn = true;
                        if ($scope.noAccountFound) {
                            $scope.createCustomErr({ "errorCode": "502" });
                        } else {
                            $scope.createCustomErr({ "errorCode": "999" });
                        }
                    }
                }
            }
            // code for Info Section.............

            impInfoSection();

            var ns = "AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.";
            var jDATA = ["IMPORTANT_INFORMATION_HEADING1", "IMPORTANT_INFORMATION_HEADING2",
                "IMPORTANT_INFORMATION_HEADING3", "IMPORTANT_INFORMATION_HEADING4",
                "IMPORTANT_INFORMATION_HEADING5", "IMPORTANT_INFORMATION_DESCRIPTION1",
                "IMPORTANT_INFORMATION_DESCRIPTION2", "IMPORTANT_INFORMATION_DESCRIPTION3",
                "IMPORTANT_INFORMATION_DESCRIPTION4", "IMPORTANT_INFORMATION_DESCRIPTION5"];
            $scope.infoData = {};
            $translate(jDATA.map(function (key) { return ns + key; })).then(function (translations) {
                for (var i = 1; i <= 5; i++) {
                    if (translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i] !== "" ||
                        translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i] !== "") {
                        $scope.infoData[i] = {};
                        $scope.infoData[i].title = translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_HEADING" + i];
                        $scope.infoData[i].desc = $sce.trustAsHtml(translations["AISP.REVIEW_CONFIRM_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_DESCRIPTION" + i]);
                    }
                }
            });

            $translate(["AISP.HEADER.LOGOURL", "AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "AISP.HEADER.ACCOUNT_ACCESS_LABEL", "AISP.HEADER.THIRD_PARTY_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.AUTHORISATION_RENEWAL_HEADER",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART1",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART2",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART3",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART4",
                "AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART5",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER",
                "AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_ALL_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_LESS_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT",
                "AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_HEADER",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL",
                "AISP.REVIEW_CONFIRM_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.RENEW_AUTHORISATION_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL",
                "AISP.FOOTER.ABOUT_US_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_LABEL",
                "AISP.FOOTER.PRIVACY_POLICY_LABEL", "AISP.FOOTER.TNC_LABEL",
                "AISP.FOOTER.HELP_LABEL", "AISP.FOOTER.REGULATORY_LABEL",
                "AISP.FOOTER.ABOUT_US_URL", "AISP.FOOTER.PRIVACY_POLICY_URL",
                "AISP.FOOTER.TNC_URL", "AISP.FOOTER.HELP_URL",
                "AISP.ACCOUNT_ACCESS_FOOTER.ABOUT_US_LABEL", "AISP.ACCOUNT_ACCESS_FOOTER.PRIVACY_POLICY_LABEL",
                "AISP.ACCOUNT_ACCESS_FOOTER.TNC_LABEL", "AISP.ACCOUNT_ACCESS_FOOTER.HELP_LABEL",
                "AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_SCREENREADER_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_VIEW_REQUESTED_PERMISSION",
                "AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW", "AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION",
                "AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.CONFIRM_BUTTON_ENABLE_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.accountAccessText = translations["AISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                    $scope.thirdPartyText = translations["AISP.HEADER.THIRD_PARTY_LABEL"];
                    $scope.accountRenewalText = translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.AUTHORISATION_RENEWAL_HEADER"];
                    $scope.instructionTextPart1 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART1"]);
                    $scope.instructionTextPart2 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART2"]);
                    $scope.instructionTextPart3 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART3"]);
                    $scope.instructionTextPart4 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART4"]);
                    $scope.instructionTextPart5 = $sce.trustAsHtml(translations["AISP.AUTHORISATION_RENEWAL_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_LABEL_PART5"]);
                    $scope.nickNameText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER"];
                    $scope.acctNoText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER"];
                    $scope.currencyText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER"];
                    $scope.acctTypeText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER"];
                    $scope.balanceText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER"];
                    $scope.selectedAccountText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_HEADER"];
                    $scope.selectedAccountTableCaptionText = translations["AISP.ACCOUNT_SELECTION_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                    $scope.showAllText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_ALL_LABEL"];
                    $scope.showLessText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SHOW_LESS_LABEL"];
                    $scope.permissionsListHeaderText = translations["AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_HEADER"];
                    $scope.transactionAccessDateHeader = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER"];
                    $scope.fromDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL"];
                    $scope.fromText = translations["AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.FROM_DATE_LABEL"];
                    $scope.toText = translations["AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL"];
                    $scope.noConsentLabel = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL"];
                    $scope.notSeeingText = translations["AISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT"];
                    $scope.toDateLabel = translations["AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.TO_DATE_LABEL"];
                    $scope.preTillDateLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL"];
                    $scope.expiryInvalidDateLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CONSENT_VALIDITY.EXPIRY_INVALID_DATE_LABEL"];
                    $scope.postTillDateLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CONSENT_VALIDITY.POST_TILL_DATE_LABEL"];
                    $scope.denyAuthorisationButtonLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL"];
                    $scope.pressDenyScreenReaderLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL"];
                    $scope.allowAuthorisationButtonLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.RENEW_AUTHORISATION_BUTTON_LABEL"];
                    $scope.allowBtnScrlabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.CONFIRM_BUTTON_ENABLE_SCREENREADER_LABEL"];
                    $scope.backToTopBtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL"];
                    $scope.backToTopBtnNotSeeingAcctTitle = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_VIEW_REQUESTED_PERMISSION"];
                    $scope.selectedRenewAuthAccountTableCaptionText = translations["AISP.REVIEW_CONFIRM_PAGE.SELECTED_ACCOUNT_TABLE.SELECTED_ACCOUNT_TABLE_CAPTION"];
                    $scope.viewReqstPerBtn = translations["AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_LABEL"];
                    $scope.viewRequestedButtonTitle = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.VIEW_REQUESTED_PERMISSION_BUTTON_SCREENREADER_LABEL"];
                    $scope.logoUrl = translations["AISP.HEADER.LOGOURL"];
                    $scope.bankLogoImgAlt = translations["AISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.aboutUsText = translations["AISP.ACCOUNT_ACCESS_FOOTER.ABOUT_US_LABEL"];
                    $scope.cookieText = translations["AISP.ACCOUNT_ACCESS_FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tncText = translations["AISP.ACCOUNT_ACCESS_FOOTER.TNC_LABEL"];
                    $scope.helpText = translations["AISP.ACCOUNT_ACCESS_FOOTER.HELP_LABEL"];
                    $scope.regulatoryText = translations["AISP.FOOTER.REGULATORY_LABEL"];
                    $scope.aboutUsUrl = translations["AISP.FOOTER.ABOUT_US_URL"];
                    $scope.privacyPolicyUrl = translations["AISP.FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncUrl = translations["AISP.FOOTER.TNC_URL"];
                    $scope.helpUrl = translations["AISP.FOOTER.HELP_URL"];
                    $scope.tooltipTt = translations["AISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                    $scope.returntotppbtn = translations["AISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL"];
                });
        };

        $scope.createCustomErr = function (error) {
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "800";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };
        $scope.allowSubmission = function () {
            var resumePath = $("#resumePath").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var payLoad = {};
            payLoad["accountDetails"] = AcctService.updateAcctData($scope.allAccounts);
            payLoad["fsHeaders"] = $("#fsHeaders").val();
            var correlationId = $("#correlationId").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
           
            ConsentService.accountRequest($scope.consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openAuthoriseAcessModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                        $($form).appendTo("body");
                        $($form).submit();
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };
        $scope.pagination = function () {
            $scope.pages = 1;
            $scope.currentPage = 1;
            $scope.AccountsByPage = AcctService.paged($scope.allAccounts, $scope.pageSize);
            $scope.accountsData = $scope.AccountsByPage[$scope.currentPage];
        };

        /* cancle Button Submission Code  */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.cancelRequest($scope.consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openCancelAuthoriseModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        $window.location.href = resp.data.model.redirectUri;
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };

        $scope.returnToThirdparty = function () {
            $scope.callFromTppButton = true;
            $scope.cancelSubmission();
        }

        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };

        //cancel authorise  modal
        $scope.openCancelAuthoriseModal = function () {
            $translate(["AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER",
                "AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST", "AISP.AUTHORISATION_RENEWAL_PAGE.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.logoUrl = translations["AISP.AUTHORISATION_RENEWAL_PAGE.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.AUTHORISATION_RENEWAL_PAGE.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();

        };

        /* Cancel model dialog box */

        $scope.openModal = function () {
            $translate(["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_HEADER",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.YES_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.CLOSE_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.GO_BACK_BUTTON_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.PRESS_NO_SCREENREADER_LABEL",
                "AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.PRESS_YES_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_HEADER"];
                    // var popupContent = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE"]
                    // + $scope.tppInfo + translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.DENY_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.GO_BACK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.yesBtnScrLabel = translations["AISP.AUTHORISATION_RENEWAL_PAGE.DENY_AUTHORISATION_POPUP.PRESS_YES_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };

        //authorise access modal
        $scope.openAuthoriseAcessModal = function () {
            $translate(["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER",
                "AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["AISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.logoUrl = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };
        $scope.openSessionOutModal = function () {
            $translate(["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["AISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["AISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        $scope.ignoreTimeZone = function (val) {
            if (val !== undefined && val !== "" && val !== null) {
                return new Date(val.substring(0, val.indexOf("T")));
            } else {
                return null;
            }
        };
        $scope.toggleDisable = function ($event) {
            if ($event.keyCode === "32") {
                $event.preventDefault();
                return false;
            }
            else {
                $scope.$parent.toggleOpen();
                return true;
            }
        };
        $scope.scrollRedirect = function ($event, id) {
            $event.preventDefault();
            document.getElementById(id).scrollIntoView();
        };

        var impInfoSection = function () {

            var impSubInfo = "AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.";
            var impInfoAllData = [
                'IMPORTANT_INFORMATION_SECTION_HEADING',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_HEADING1',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_HEADING1',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_HEADING1',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_PARAGRAPH1',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_PARAGRAPH1',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_PARAGRAPH1',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_PARAGRAPH2',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_PARAGRAPH2',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_PARAGRAPH2',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM1',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM2',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM3',
                'IMPORTANT_INFORMATION_SECTION_0.IMPORTANT_INFORMATION_LIST_ITEM4',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM1',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM2',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM3',
                'IMPORTANT_INFORMATION_SECTION_1.IMPORTANT_INFORMATION_LIST_ITEM4',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM1',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM2',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM3',
                'IMPORTANT_INFORMATION_SECTION_2.IMPORTANT_INFORMATION_LIST_ITEM4'
            ];

            $scope.subHeadingInfo = {};

            $scope.impInfoSection = {
                mainHeading: 'IMPORTANT_INFORMATION_SECTION_HEADING',
                subHeading: [{
                    heading: 'IMPORTANT_INFORMATION_HEADING1',
                    para: ['IMPORTANT_INFORMATION_PARAGRAPH1',
                        'IMPORTANT_INFORMATION_PARAGRAPH2',
                        'IMPORTANT_INFORMATION_PARAGRAPH3'
                    ],
                    list: [
                        'IMPORTANT_INFORMATION_LIST_ITEM1',
                        'IMPORTANT_INFORMATION_LIST_ITEM2',
                        'IMPORTANT_INFORMATION_LIST_ITEM3',
                        'IMPORTANT_INFORMATION_LIST_ITEM4'
                    ]
                },
                {
                    heading: 'IMPORTANT_INFORMATION_HEADING1',
                    para: ['IMPORTANT_INFORMATION_PARAGRAPH1',
                        'IMPORTANT_INFORMATION_PARAGRAPH2',
                        'IMPORTANT_INFORMATION_PARAGRAPH3'
                    ],
                    list: ['IMPORTANT_INFORMATION_LIST_ITEM1',
                        'IMPORTANT_INFORMATION_LIST_ITEM2',
                        'IMPORTANT_INFORMATION_LIST_ITEM3',
                        'IMPORTANT_INFORMATION_LIST_ITEM4']
                },
                {
                    heading: 'IMPORTANT_INFORMATION_HEADING1',
                    para: ['IMPORTANT_INFORMATION_PARAGRAPH1',
                        'IMPORTANT_INFORMATION_PARAGRAPH2',
                        'IMPORTANT_INFORMATION_PARAGRAPH3'
                    ],
                    list: ['IMPORTANT_INFORMATION_LIST_ITEM1',
                        'IMPORTANT_INFORMATION_LIST_ITEM2',
                        'IMPORTANT_INFORMATION_LIST_ITEM3',
                        'IMPORTANT_INFORMATION_LIST_ITEM4']
                }]
            };

            $translate(impInfoAllData.map(function (key) { return impSubInfo + key; })).then(function (translations) {
                $scope.infoMainHeading = translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_HEADING"];
                for (var i = 0; i <= $scope.impInfoSection.subHeading.length; i++) {
                    $scope.subHeadingInfo[i] = {};
                    $scope.subHeadingInfo[i].heading = translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_" + i + "." + $scope.impInfoSection.subHeading[i].heading];
                    $scope.subHeadingInfo[i].para = [];
                    $scope.subHeadingInfo[i].list = [];
                    for (var j = 0; j <= $scope.impInfoSection.subHeading[i].para.length; j++) {
                        var str = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_" + i + "." + $scope.impInfoSection.subHeading[i].para[j]]);
                        $scope.subHeadingInfo[i].para.push(str);
                    }

                    for (var k = 0; k <= $scope.impInfoSection.subHeading[i].list.length; k++) {
                        var listStr = $sce.trustAsHtml(translations["AISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION.IMPORTANT_INFORMATION_SECTION_" + i + "." + $scope.impInfoSection.subHeading[i].list[k]]);
                        $scope.subHeadingInfo[i].list.push(listStr);
                    }
                }
            });
        }
        $scope.init();

        $(document).on('click', '.accordion-toggle', function (e) {
            $(this).removeAttr('aria-controls');
        });
    }]);

"use strict";
angular.module("consentApp").controller("PispAccountCtrl", ["$scope", "$rootScope", "$state", "$filter", "$uibModal",
    "ConsentService", "AcctService", "config", "$fraudAnalyze", "blockUI", "$translate", "$window", "$timeout", "$sce",
    function ($scope, $rootScope, $state, $filter, $uibModal, ConsentService, AcctService, config, $fraudAnalyze, blockUI, $translate, $window, $timeout, $sce, fontResolved) {
        $scope.init = function () {
            var $ = window.jQuery;
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.modelPopUpConf = config.modelpopupConfig;
            $scope.psuAcct = null;
            $scope.errorData = null;
            $scope.returnTppbtn = false;
            $scope.retry = null;
            $scope.isDataFound = false;
            $scope.notSeeingInfo = false;
            $scope.pageSize = config.pageSize;
            $scope.currentPage = 1;
            $scope.maxPaginationSize = config.maxPaginationSize;
            $scope.notSeeingVisibleAcctCount = config.notSeeingVisibleAcctCount;
            $scope.AccountsByPage = {};
            $scope.accountsData = [];
            $scope.consentType = 'pisp';
            $scope.noDataText = '';
            $scope.schedulePayment = false;
            $scope.paymentScheduleDate = null;
            $scope.isAccPreSelected = false;
            $scope.callFromTppButton = false;
            $scope.noAccountFound = false;

            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.isDataFound = true;
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        });
                    } else {
                        $scope.returnTppbtn = true;
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.paymentinfo = angular.fromJson($("#paymentSetup").val());
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.tppInfo = $sce.trustAsHtml("<b>" + $scope.tppInfoData.applicationName + ".</b>");
                        if ($scope.paymentinfo
                            && $scope.paymentinfo.paymentType
                            && $scope.paymentinfo.paymentType === config.paymentType) {
                            $scope.schedulePayment = true;
                            if ($scope.paymentinfo.customDSPData && $scope.paymentinfo.customDSPData.requestedExecutionDateTime) {
                                $scope.paymentScheduleDate = $scope.ignoreTimeZone($scope.paymentinfo.customDSPData.requestedExecutionDateTime);
                            }
                        }

                        if ($scope.paymentinfo
                            && $scope.paymentinfo.paymentType
                            && $scope.paymentinfo.paymentType === config.standingOrderPaymentType) {
                            this.standingPayment = true;
                            $scope.checkForStandingOrderPayment();
                        }

                        $scope.psuAcct = angular.fromJson($("#psuAccounts").val());
                        $scope.accountSelected = angular.fromJson($("#accountSelected").val());
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.futureDatedPayment = true;

                        $scope.payInstBy = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";

                        if (!$scope.psuAcct.Data.Account.length) {
                            $scope.returnTppbtn = true;
                            $scope.noAccountFound = true;
                            throw null;
                        }
                        $scope.maskAccountNumberLength = config.maskAccountNumberLength;
                        $scope.isAccSelected = false;
                        $scope.termsConditn = false;
                        $scope.stopCnfirm = true;

                        $scope.accData = AcctService.updateAcctData($scope.psuAcct.Data.Account);
                        if ($scope.accData.length >= $scope.notSeeingVisibleAcctCount) {
                            $scope.notSeeingInfo = true;
                        }
                        if ($scope.accountSelected || $scope.psuAcct.Data.Account.length === 1) {
                            $scope.selectedAcctObject = $scope.accData[0];
                            $scope.stopCnfirm = false;

                            if ($scope.accountSelected) {
                                $scope.isAccPreSelected = true;
                            }
                        }



                        $scope.payeeDetails = $scope.paymentinfo;
                        $scope.payeeName = $scope.payeeDetails.creditorDetails ? $scope.payeeDetails.creditorDetails.Name : $scope.noDataText;



                        if ($scope.payeeDetails.creditorDetails &&
                            $scope.payeeDetails.creditorDetails.SecondaryIdentification &&
                            $scope.payeeDetails.remittanceDetails &&
                            $scope.payeeDetails.remittanceDetails.Unstructured &&
                            $scope.payeeDetails.remittanceDetails.Reference) {
                            $scope.payeeReference = $scope.payeeDetails.creditorDetails.SecondaryIdentification;
                        } else if ($scope.payeeDetails.creditorDetails && $scope.payeeDetails.creditorDetails.SecondaryIdentification &&
                            $scope.payeeDetails.remittanceDetails &&
                            $scope.payeeDetails.remittanceDetails.Reference) {
                            $scope.payeeReference = $scope.payeeDetails.creditorDetails.SecondaryIdentification;

                        } else if ($scope.payeeDetails.creditorDetails &&
                            $scope.payeeDetails.creditorDetails.SecondaryIdentification) {
                            $scope.payeeReference = $scope.payeeDetails.creditorDetails.SecondaryIdentification;
                        } else if ($scope.payeeDetails.remittanceDetails &&
                            $scope.payeeDetails.remittanceDetails.Reference &&
                            $scope.payeeDetails.remittanceDetails.Unstructured) {
                            $scope.payeeReference = $scope.payeeDetails.remittanceDetails.Reference;
                        } else if ($scope.payeeDetails.remittanceDetails &&
                            $scope.payeeDetails.remittanceDetails.Reference) {
                            $scope.payeeReference = $scope.payeeDetails.remittanceDetails.Reference;
                        } else if ($scope.payeeDetails.remittanceDetails &&
                            $scope.payeeDetails.remittanceDetails.Unstructured) {
                            $scope.payeeReference = null;
                        } else {
                            $scope.payeeReference = null;
                        }

                        if ($scope.payeeDetails && $scope.payeeDetails.amountDetails) {
                            $scope.amount = $scope.payeeDetails.amountDetails.Amount;
                            $scope.currency = $scope.payeeDetails.amountDetails.Currency;
                        }

                        if ($scope.payeeDetails.creditorDetails) {
                            switch ($scope.payeeDetails.creditorDetails.SchemeName) {
                                case "UK.OBIE.SortCodeAccountNumber":
                                    if ($scope.payeeDetails.creditorDetails.Identification) {
                                        $scope.accountNo = $scope.payeeDetails.creditorDetails.Identification.substr(6);
                                        $scope.bic = $scope.payeeDetails.creditorDetails.Identification.substr(0, 6);
                                    }
                                    break;
                                default:
                                    if ($scope.payeeDetails.creditorDetails.Identification) {
                                        $scope.accountNo = $scope.payeeDetails.creditorDetails.Identification;
                                    }
                                    break;
                            }
                        }
                        angular.forEach($scope.accData, function (pacct) {
                            pacct.selected = false;
                        });
                        $scope.pagination();
                        //code for back button
                        if ($state.params.pispContractDetails !== null) {
                            $scope.selectedAcctObject = $state.params.pispContractDetails.accountDetails;
                            $scope.selectAccount($scope.selectedAcctObject, event);
                        }

                    } catch (e) {
                        $scope.returnTppbtn = true;
                        if ($scope.noAccountFound) {
                            $scope.createCustomErr({ "errorCode": "502" });
                        }
                        else {
                            $scope.createCustomErr({ "errorCode": "999" });
                        }
                    }
                }
            }

            $translate(["PISP.HEADER.LOGOURL", "PISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "PISP.HEADER.ACCOUNT_ACCESS_LABEL", "PISP.HEADER.THIRD_PARTY_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_DETAILS_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_INITIATED_BY_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_NAME_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.AMOUNT_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_REFERENCE_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAY_FROM_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.NO_DATA_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_BIC_NSC_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_IBAN_ACCOUNT_NUMBER_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.SELECT_ACCOUNT_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_SCREENREADER_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.FUTURE_DATE_PAYMENT_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_ACCOUNT_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_ACCOUNT_INTRO_TEXT_FOR_ONE_ELIGIBLE_ACCT",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_ACCOUNT_INTRO_TEXT_FOR_PRE_SELECTED_ACCT",
                "PISP.ACCOUNT_SELECTION_PAGE.INFORMATION_OF_CUTOFF_TIME_AND_TRANSFER_LIMIT",
                "PISP.ACCOUNT_SELECTION_PAGE.INFORMATION_OF_CUTOFF_TIME_AND_TRANSFER_LIMIT_LINK_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.RESTRICTED_ACCOUNTS.RESTRICTED_ACCOUNTS_HEADING1",
                "PISP.ACCOUNT_SELECTION_PAGE.RESTRICTED_ACCOUNTS.RESTRICTED_ACCOUNTS_DESCRIPTION1",
                "PISP.ACCOUNT_SELECTION_PAGE.CAN_BE_FOUND_HERE_LINK_SCREENREADER_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_SCREENREADER_TITLE",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECT_INFO_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER",
                "PISP.ACCOUNT_SELECTION_PAGE.CUT_OFF_TIMES.CUT_OFF_TIMES_HEADING1",
                "PISP.ACCOUNT_SELECTION_PAGE.CUT_OFF_TIMES.CUT_OFF_TIMES_DESCRIPTION1",
                "PISP.ACCOUNT_SELECTION_PAGE.CUT_OFF_TIMES.CUT_OFF_TIMES_DESCRIPTION2",
                "PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_HEADING1",
                "PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_DESCRIPTION1",
                "PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_DESCRIPTION2",
                "PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_DESCRIPTION3",
                "PISP.FOOTER.ABOUT_US_LABEL", "PISP.FOOTER.PRIVACY_POLICY_LABEL",
                "PISP.FOOTER.TNC_LABEL", "PISP.FOOTER.HELP_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.STANDING_ORDER_PAYMENT_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.FIRST_PAYMENT_DATE",
                "PISP.ACCOUNT_SELECTION_PAGE.LAST_PAYMENT_DATE",
                "PISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION_HEADING",
                "PISP.ACCOUNT_SELECTION_PAGE.FREQUENCY",
                "PISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.FUTURE_DATE_PAYMENT_TEXT",
                "PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_DATE",
                "PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_SCREENREADER_LABEL_SELECTED_ACCOUNT",
                "PISP.FOOTER.REGULATORY_LABEL",
                "PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL_DETAIL",
                "PISP.FOOTER.ABOUT_US_URL",
                "PISP.FOOTER.PRIVACY_POLICY_URL", "PISP.FOOTER.TNC_URL",
                "PISP.FOOTER.HELP_URL", "PISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"]).then(function (translations) {

                    $scope.standingOrderPaymentText = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.STANDING_ORDER_PAYMENT_TEXT"]);
                    $scope.firstPaymentText = translations["PISP.ACCOUNT_SELECTION_PAGE.FIRST_PAYMENT_DATE"];
                    $scope.lastPaymentText = translations["PISP.ACCOUNT_SELECTION_PAGE.LAST_PAYMENT_DATE"];
                    $scope.paymentFrequencyText = translations["PISP.ACCOUNT_SELECTION_PAGE.FREQUENCY"];
                    $scope.accountAccessText = translations["PISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                    $scope.paymentDateText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_DATE"];
                    $scope.futureDatePaymentText = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.FUTURE_DATE_PAYMENT_TEXT"]);
                    $scope.thirdPartyText = translations["PISP.HEADER.THIRD_PARTY_LABEL"];
                    $scope.accountSelText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER"];
                    $scope.paymentCheckText = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL"]);
                    $scope.paymentCheckTextForAccounts = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL_DETAIL"]);
                    $scope.logoUrl = translations["PISP.HEADER.LOGOURL"];
                    $scope.bankLogoImgAlt = translations["PISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.paymentInfoText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_DETAILS_HEADER"];
                    $scope.paymtIntByText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_INITIATED_BY_LABEL"];
                    $scope.payNameText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_NAME_LABEL"];
                    $scope.PayAmountText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.AMOUNT_LABEL"];
                    $scope.payRefText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYEE_REFERENCE_LABEL"];
                    $scope.payFromText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAY_FROM_LABEL"];
                    $scope.notSeeingText = translations["PISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT"];
                    $scope.selAccountText = translations["PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER"];
                    $scope.paymentBicNsc = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_BIC_NSC_LABEL"];
                    $scope.paymentIbanAcc = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.PAYMENT_IBAN_ACCOUNT_NUMBER_LABEL"];
                    $scope.selectAccountText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.SELECT_ACCOUNT_LABEL"];
                    $scope.nickNameText = translations["PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.NICK_NAME_COLUMN_HEADER"];
                    $scope.acctNoText = translations["PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_NUMBER_COLUMN_HEADER"];
                    $scope.currencyText = translations["PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.CURRENCY_COLUMN_HEADER"];
                    $scope.acctTypeText = translations["PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.ACCOUNT_TYPE_COLUMN_HEADER"];
                    $scope.balanceText = translations["PISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER"];
                    $scope.noDataText = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYEE_TABLE.NO_DATA_LABEL"];
                    $scope.contBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL"];
                    $scope.rtrBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL"];
                    $scope.cancelBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL"];
                    $scope.backToTopBtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL"];
                    $scope.backToTopBtnLbl = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_SCREENREADER_LABEL"];
                    $scope.backToTopBtnLblForPreAcc = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_SCREENREADER_LABEL_SELECTED_ACCOUNT"];
                    $scope.findoutWhyLink = translations["PISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_LABEL"];
                    $scope.findoutWhyScrText = translations["PISP.ACCOUNT_SELECTION_PAGE.FIND_OUT_WHY_SCREENREADER_TITLE"];
                    $scope.AccountSelectInfoText = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECT_INFO_LABEL"];
                    $scope.returntotppbtn = translations["PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL"];
                    $scope.importantSectionHeading = translations["PISP.ACCOUNT_SELECTION_PAGE.IMPORTANT_INFORMATION_HEADING"];
                    $scope.transferLimitHeaderText = translations["PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_HEADING1"];
                    $scope.transferLimitDesc1Text = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_DESCRIPTION1"]);
                    $scope.transferLimitDesc2Text = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_DESCRIPTION2"]);
                    $scope.transferLimitDesc3Text = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.TRANSFER_LIMIT.TRANSFER_LIMIT_DESCRIPTION3"]);
                    $scope.restrictedAccountHeaderText = translations["PISP.ACCOUNT_SELECTION_PAGE.RESTRICTED_ACCOUNTS.RESTRICTED_ACCOUNTS_HEADING1"];
                    $scope.restrictedAccountDesc1Text = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.RESTRICTED_ACCOUNTS.RESTRICTED_ACCOUNTS_DESCRIPTION1"]);
                    $scope.cutoffTimeHeaderText = translations["PISP.ACCOUNT_SELECTION_PAGE.CUT_OFF_TIMES.CUT_OFF_TIMES_HEADING1"];
                    $scope.cutoffTimeDesc1Text = translations["PISP.ACCOUNT_SELECTION_PAGE.CUT_OFF_TIMES.CUT_OFF_TIMES_DESCRIPTION1"];
                    $scope.cutoffTimeDesc2Text = translations["PISP.ACCOUNT_SELECTION_PAGE.CUT_OFF_TIMES.CUT_OFF_TIMES_DESCRIPTION2"];
                    $scope.aboutUsText = translations["PISP.FOOTER.ABOUT_US_LABEL"];
                    $scope.cookieText = translations["PISP.FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tncText = translations["PISP.FOOTER.TNC_LABEL"];
                    $scope.helpText = translations["PISP.FOOTER.HELP_LABEL"];
                    $scope.regulatoryText = translations["PISP.FOOTER.REGULATORY_LABEL"];
                    $scope.aboutUsUrl = translations["PISP.FOOTER.ABOUT_US_URL"];
                    $scope.privacyPolicyUrl = translations["PISP.FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncUrl = translations["PISP.FOOTER.TNC_URL"];
                    $scope.helpUrl = translations["PISP.FOOTER.HELP_URL"];
                    $scope.tooltipTt = translations["PISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                    $scope.paymentAcctTxt = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_ACCOUNT_TEXT"];
                    $scope.paymentAcctIntroOneAcct = translations["PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_ACCOUNT_INTRO_TEXT_FOR_ONE_ELIGIBLE_ACCT"];
                    $scope.paymentIntroTxtPreSelect = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.PAYMENT_ACCOUNT_INTRO_TEXT_FOR_PRE_SELECTED_ACCT"]);
                    $scope.infoOfCutoffAndTransLimit = $sce.trustAsHtml(translations["PISP.ACCOUNT_SELECTION_PAGE.INFORMATION_OF_CUTOFF_TIME_AND_TRANSFER_LIMIT"]);
                    $scope.infoOfCutoffAndTransLimitLinkTxt = translations["PISP.ACCOUNT_SELECTION_PAGE.INFORMATION_OF_CUTOFF_TIME_AND_TRANSFER_LIMIT_LINK_TEXT"];
                    $scope.transferLmtScreenReadTxt = translations["PISP.ACCOUNT_SELECTION_PAGE.CAN_BE_FOUND_HERE_LINK_SCREENREADER_TEXT"];
                });
        };

        $scope.createPayment = function () {
            var selectedAccounts = $filter("filter")($scope.accData, function (d) {
                return d.Account.Identification === $scope.selectedAcctObject.Account.Identification;
            });
            var payLoad = {};
            payLoad["accountDetails"] = AcctService.updateAcctData(selectedAccounts);
            payLoad["fsHeaders"] = $("#fsHeaders").val();
            var resumePath = $("#resumePath").val();
            var correlationId = $("#correlationId").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.accountRequest($scope.consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openAuthoriseAcessModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                        $("#user_oauth_approval").appendTo($form);
                        $($form).appendTo("body");
                        $($form).submit();
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };


        $scope.selectAccount = function (data, event) {
            if (event) {
                event.preventDefault();
            }
            if (data !== null && data !== "") {
                $scope.selectedAcctObject = data;
                $scope.stopCnfirm = false;
                angular.forEach($scope.accData, function (pacct) {
                    if (pacct.HashedValue !== $scope.selectedAcctObject.HashedValue) {
                        pacct.selected = false;
                    }
                });
            } else {
                $scope.selectedAcctObject = null;
                $scope.stopCnfirm = true;
                $scope.isAccSelected = false;
            }
        };

        $scope.pagination = function () {
            $scope.pages = 1;
            $scope.currentPage = 1;
            $scope.AccountsByPage = $scope.isAccPreSelected ? AcctService.paged([$scope.accData[0]], $scope.pageSize) : AcctService.paged($scope.accData, $scope.pageSize);
            $scope.accountsData = $scope.AccountsByPage[$scope.currentPage];
        };

        /* Cancel button code here........ */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.cancelRequest($scope.consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openCancelAuthoriseModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        $window.location.href = resp.data.model.redirectUri;
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };

        $scope.returnToThirdparty = function () {
            $scope.callFromTppButton = true;
            $scope.cancelSubmission();
        }


        //cancel authorise  modal
        $scope.openCancelAuthoriseModal = function () {
            $translate(["PISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "PISP.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER",
                "PISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "PISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["PISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["PISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["PISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.logoUrl = translations["PISP.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();

        };

        //authorise access success modal
        $scope.openAuthoriseAcessModal = function () {
            $translate(["PISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "PISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER",
                "PISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "PISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["PISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["PISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];

                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["PISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                    $scope.modelPopUpConf.logoUrl = translations["PISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        /* Cancel model dialog box */
        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };

        $scope.openCancelModal = function () {
            $translate(["PISP.CANCEL_POPUP.CANCEL_POPUP_HEADER",
                "PISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE", "PISP.CANCEL_POPUP.YES_BUTTON_LABEL",
                "PISP.CANCEL_POPUP.NO_BUTTON_LABEL", "PISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL",
                "PISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL", "PISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL",
                "PISP.CANCEL_POPUP.PRESS_YES_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["PISP.CANCEL_POPUP.CANCEL_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["PISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["PISP.CANCEL_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["PISP.CANCEL_POPUP.NO_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["PISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.cancelRequestScrLabel = translations["PISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["PISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.yesBtnScrLabel = translations["PISP.CANCEL_POPUP.PRESS_YES_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };
        $scope.openSessionOutModal = function () {
            $translate(["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "PISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL", "PISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL",
                "PISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["PISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["PISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["PISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["PISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };
        $scope.scrollRedirect = function ($event, id) {
            $event.preventDefault();
            document.getElementById(id).scrollIntoView();
        };

        $scope.ignoreTimeZone = function (val) {
            if (val !== undefined && val !== "" && val !== null) {
                return new Date(val.substring(0, val.indexOf("T")));
            } else {
                return null;
            }
        };

        $scope.checkForStandingOrderPayment = function () {
            if ($scope.paymentinfo.customDSOData && $scope.paymentinfo.customDSOData.firstPaymentDateTime) {
                $scope.firstPaymentDate = $scope.ignoreTimeZone($scope.paymentinfo.customDSOData.firstPaymentDateTime);
            }
            if ($scope.paymentinfo.customDSOData && $scope.paymentinfo.customDSOData.finalPaymentDateTime) {
                $scope.finalPaymentDate = $scope.ignoreTimeZone($scope.paymentinfo.customDSOData.finalPaymentDateTime);
            }
            if ($scope.paymentinfo.customDSOData && $scope.paymentinfo.customDSOData.frequency) {
                $scope.paymentFrequency = $scope.paymentinfo.customDSOData.frequency;
            }
        }

        $scope.createCustomErr = function (error) {
            $scope.isDataFound = true;
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "999";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };

        $scope.init();
    }]);

"use strict";
angular.module("consentApp").controller("FundsCheckCtrl", ["$scope", "$rootScope", "$state", "$filter", "$uibModal",
    "ConsentService", "AcctService", "config", "$fraudAnalyze", "blockUI", "$translate", "$window", "$timeout", "$sce",
    function ($scope, $rootScope, $state, $filter, $uibModal, ConsentService, AcctService, config, $fraudAnalyze, blockUI, $translate, $window, $timeout, $sce, fontResolved) {
        $scope.init = function () {
            var $ = window.jQuery;
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $scope.pageError = angular.fromJson($("#error").val());
            $scope.modelPopUpConf = config.modelpopupConfig;
            $scope.psuAcct = null;
            $scope.errorData = null;
            $scope.returnTppbtn = false;
            $scope.retry = null;
            $scope.isDataFound = false;
            $scope.notSeeingInfo = false;
            $scope.pageSize = config.pageSize;
            $scope.currentPage = 1;
            $scope.maxPaginationSize = config.maxPaginationSize;
            $scope.AccountsByPage = {};
            $scope.accountsData = [];
            $scope.errorMessage = false;
            $scope.schemaName = null;
            $scope.accountNo = null;
            $scope.consentType = 'cisp';
            $scope.noAccountFound = false;

            if (!navigator.cookieEnabled) {
                $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
            } else {
                if ($scope.pageError) {
                    if ($scope.pageError.errorCode === "731") {
                        $scope.isDataFound = true;
                        $scope.redirectUri = $("#resumePath").val();
                        $scope.sessiontimeoutflag = true;
                        $timeout(function () {
                            $scope.openSessionOutModal();
                        });
                    } else {
                        $scope.returnTppbtn = true;
                        $scope.createCustomErr($scope.pageError);
                    }
                } else {
                    try {
                        $scope.psuAcct = angular.fromJson($("#psuAccounts").val());
                        $scope.accReq = angular.fromJson($("#account-request").val());
                        $scope.tppInfoData = angular.fromJson($("#tppInfo").val());
                        $scope.expiryDate = $scope.ignoreTimeZone($scope.accReq.Data.ExpirationDateTime);
                        $scope.tppName = $scope.tppInfoData.applicationName;
                        $scope.tppInfo = $scope.tppInfoData.applicationName + " (" + $scope.tppInfoData.tppName + ")";
                        if (!$scope.psuAcct.Data.Account.length) {
                            $scope.returnTppbtn = true;
                            $scope.noAccountFound = true;
                            throw null;
                        }
                        $scope.psuAcct = AcctService.updateAcctData($scope.psuAcct.Data.Account)[0];
                        $scope.accountName = $scope.psuAcct.Account.Name;
                        $scope.accountNo = $scope.psuAcct.Account.Identification.substr(-4);
                        switch ($scope.psuAcct.Account.SchemeName) {
                            case config.pan:
                                $scope.pan = true;
                                break;
                            default:
                                $scope.ibanNsc = true;
                                break;
                        }
                        $scope.maskAccountNumberLength = config.maskAccountNumberLength;
                        $scope.termsConditn = false;
                        $scope.stopCnfirm = true;

                    } catch (e) {
                        $scope.returnTppbtn = true;
                        if ($scope.noAccountFound) {
                            $scope.createCustomErr({ "errorCode": "502" });
                        }
                        else {
                            $scope.createCustomErr({ "errorCode": "999" });
                        }
                    }
                }
            }

            $translate(["CISP.HEADER.LOGOURL", "CISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "CISP.HEADER.ACCOUNT_ACCESS_LABEL", "CISP.HEADER.THIRD_PARTY_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER",
                "CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAYMENT_CHECK_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.ACCOUNT_TABLE_HEADER",
                "CISP.ACCOUNT_SELECTION_PAGE.PAGE_DESCRIPTION.PAGE_DESCRIPTION_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.ACCOUNT_NAME_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.IBAN_NSC_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAN_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.AMOUNT_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.ACCOUNT_DETAILS_HEADER",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.NO_DATA_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAYMENT_BIC_NSC_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAYMENT_IBAN_ACCOUNT_NUMBER_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.SELECT_ACCOUNT_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_SCREENREADER_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.CONFIRMATION_FUND_REQUESTS_FROM_THIRD_PARTY",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECT_INFO_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.SELECT_ACCOUNT_HEADER",
                "CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_HEADING1",
                "CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_DESCRIPTION1",
                "CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_DESCRIPTION2",
                "CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_DESCRIPTION3",
                "CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_HEADING1",
                "CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION1",
                "CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION2",
                "CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION_WITHOUT_DATE",
                "CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION3",
                "CISP.FOOTER.ABOUT_US_LABEL", "CISP.FOOTER.PRIVACY_POLICY_LABEL",
                "CISP.FOOTER.TNC_LABEL", "CISP.FOOTER.HELP_LABEL",
                "CISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT",
                "CISP.FOOTER.REGULATORY_LABEL",
                "CISP.FOOTER.ABOUT_US_URL",
                "CISP.FOOTER.PRIVACY_POLICY_URL", "CISP.FOOTER.TNC_URL",
                "CISP.FOOTER.HELP_URL", "CISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"]).then(function (translations) {

                    $scope.accountAccessText = translations["CISP.HEADER.ACCOUNT_ACCESS_LABEL"];
                    $scope.thirdPartyText = translations["CISP.HEADER.THIRD_PARTY_LABEL"];
                    $scope.accountSelText = translations["CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.ACCOUNT_SELECTION_HEADER"];
                    $scope.preUserTitleText = translations["CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_PRE_LABEL"];
                    $scope.postUserTitleText = translations["CISP.ACCOUNT_SELECTION_PAGE.PAGE_INSTRUCTION.PAGE_INSTRUCTION_POST_LABEL"];
                    $scope.accountSelDescriptionText = $sce.trustAsHtml(translations["CISP.ACCOUNT_SELECTION_PAGE.PAGE_DESCRIPTION.PAGE_DESCRIPTION_LABEL"]);
                    $scope.logoUrl = translations["CISP.HEADER.LOGOURL"];
                    $scope.bankLogoImgAlt = translations["CISP.HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.acctDetailText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.ACCOUNT_DETAILS_HEADER"];
                    $scope.acctNameText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.ACCOUNT_NAME_LABEL"];
                    $scope.ibanNscText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.IBAN_NSC_LABEL"];
                    $scope.panText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAN_LABEL"];
                    $scope.PayAmountText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.AMOUNT_LABEL"];
                    $scope.payFromText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAY_FROM_LABEL"];
                    $scope.notSeeingText = translations["CISP.ACCOUNT_SELECTION_PAGE.NOT_SEEING_TEXT"];
                    $scope.paymentBicNsc = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAYMENT_BIC_NSC_LABEL"];
                    $scope.paymentIbanAcc = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.PAYMENT_IBAN_ACCOUNT_NUMBER_LABEL"];
                    $scope.selectAccountText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.SELECT_ACCOUNT_LABEL"];
                    $scope.balanceText = translations["CISP.ACCOUNT_SELECTION_PAGE.SELECT_ACCOUNT_TABLE.BALANCE_COLUMN_HEADER"];
                    $scope.noDataText = translations["CISP.ACCOUNT_SELECTION_PAGE.ACCOUNT_TABLE.NO_DATA_LABEL"];
                    $scope.contBtn = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL"];
                    $scope.rtrBtn = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETRY_BUTTON_LABEL"];
                    $scope.cancelBtn = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.CANCEL_BUTTON_LABEL"];
                    $scope.backToTopBtn = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_BUTTON_LABEL"];
                    $scope.backToTopBtnForFundCheck = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.BACK_TO_TOP_SCREENREADER_LABEL"];
                    $scope.confirmationForThirdParty = translations["CISP.ACCOUNT_SELECTION_PAGE.CONFIRMATION_FUND_REQUESTS_FROM_THIRD_PARTY"];
                    $scope.AccountSelectInfoText = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.ACCOUNT_SELECT_INFO_LABEL"];
                    $scope.returntotppbtn = translations["CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.RETURN_TO_TPP_BUTTON_LABEL"];
                    $scope.consentExpirationHeaderText = translations["CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_HEADING1"];
                    $scope.consentExpirationDesc1Text = $sce.trustAsHtml(translations["CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION1"]);
                    $scope.consentExpirationDesc2Text = $sce.trustAsHtml(translations["CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION2"]);
                    $scope.consentExpirationDescWithoutDate = translations["CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION_WITHOUT_DATE"];
                    $scope.consentExpirationDesc3Text = $sce.trustAsHtml(translations["CISP.ACCOUNT_SELECTION_PAGE.CONSENT_EXPIRATION.CONSENT_EXPIRATION_DESCRIPTION3"]);
                    $scope.cutoffTimeHeaderText = translations["CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_HEADING1"];
                    $scope.cutoffTimeDesc1Text = translations["CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_DESCRIPTION1"];
                    $scope.cutoffTimeDesc2Text = translations["CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_DESCRIPTION2"];
                    $scope.cutoffTimeDesc3Text = translations["CISP.ACCOUNT_SELECTION_PAGE.PLEASE_NOTE.PLEASE_NOTE_DESCRIPTION3"];
                    $scope.aboutUsText = translations["CISP.FOOTER.ABOUT_US_LABEL"];
                    $scope.cookieText = translations["CISP.FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tncText = translations["CISP.FOOTER.TNC_LABEL"];
                    $scope.helpText = translations["CISP.FOOTER.HELP_LABEL"];
                    $scope.regulatoryText = translations["CISP.FOOTER.REGULATORY_LABEL"];
                    $scope.aboutUsUrl = translations["CISP.FOOTER.ABOUT_US_URL"];
                    $scope.privacyPolicyUrl = translations["CISP.FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncUrl = translations["CISP.FOOTER.TNC_URL"];
                    $scope.helpUrl = translations["CISP.FOOTER.HELP_URL"];
                    $scope.tooltipTt = translations["CISP.FOOTER.LINK_TOOLTIP_NEW_WINDOW"];
                });
        };

        $scope.authorisePaymentDetails = function () {
            var payLoad = {};
            payLoad["accountDetails"] = AcctService.updateAcctData([$scope.psuAcct]);
            payLoad["fsHeaders"] = $("#fsHeaders").val();
            var resumePath = $("#resumePath").val();
            var correlationId = $("#correlationId").val();
            var today = new Date();
            var dateTime = today.toISOString();
            var headers = {};
            if ($window.boiukns) {
                headers = $fraudAnalyze.capture(dateTime);
            }
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.accountRequest($scope.consentType, payLoad, resumePath, correlationId, refreshTokenRenewalFlow, headers).then(function (resp) {
                $scope.errorMessage = false;
                if (resp.status === 200) {
                    $scope.openAuthoriseAcessModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        var $form = $("<form id=\"consentForm\" method=\"POST\" action=" + resp.data.model.redirectUri + "></form>");
                        $("#user_oauth_approval").appendTo($form);
                        $($form).appendTo("body");
                        $($form).submit();
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };

        /* Cancel button code here........ */
        $scope.cancelSubmission = function () {
            var resumePath = $("#resumePath").val();
            var reqData = null;
            var correlationId = $("#correlationId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var refreshTokenRenewalFlow = $("#renewalAuthorisation").val();
            ConsentService.cancelRequest($scope.consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag).then(function (resp) {
                if (resp.status === 200) {
                    $scope.openCancelAuthoriseModal();
                    $timeout(function () {
                        $scope.modelPopUpConf.modalInstance.dismiss();
                        blockUI.start();
                        $('button').blur();
                        $window.location.href = resp.data.model.redirectUri;
                    }, config.popupTimings);
                }
            },
                function (error) {
                    $scope.errorMessage = {};
                    AcctService.errorFallbackInline(error, $scope, blockUI);
                });
        };

        //cancel authorise  modal
        $scope.openCancelAuthoriseModal = function () {
            $translate(["CISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "CISP.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER",
                "CISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "CISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["CISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["CISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["CISP.CANCEL_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.logoUrl = translations["CISP.CANCEL_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();

        };

        //authorise access success modal
        $scope.openAuthoriseAcessModal = function () {
            $translate(["CISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER",
                "CISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER",
                "CISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE",
                "CISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST",
                "AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["CISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_HEADER"];
                    var popupContent = translations["CISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_PRE"];
                    $scope.modelPopUpConf.modelpopupBodyContent1 = popupContent;
                    $scope.modelPopUpConf.modelpopupBodyContent2 = translations["CISP.ACCESS_AUTHORISATION_POPUP.ACCESS_AUTHORISATION_POPUP_MESSAGE_POST"];
                    $scope.modelPopUpConf.logoUrl = translations["CISP.ACCESS_AUTHORISATION_POPUP.LOGO_HEADER"];
                    $scope.modelPopUpConf.modalBankLogoImgAlt = translations["AISP.ACCESS_AUTHORISATION_POPUP.LOGO_TEXT"];
                });
            $scope.modelPopUpConf.modelpopupType = "authorisepopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };

        /* Cancel model dialog box */
        $scope.sessionSubmission = function () {
            $window.location.href = $scope.redirectUri;
        };

        $scope.openCancelModal = function () {
            $translate(["CISP.CANCEL_POPUP.CANCEL_POPUP_HEADER",
                "CISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE", "CISP.CANCEL_POPUP.YES_BUTTON_LABEL",
                "CISP.CANCEL_POPUP.NO_BUTTON_LABEL", "CISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL",
                "CISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL", "CISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["CISP.CANCEL_POPUP.CANCEL_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["CISP.CANCEL_POPUP.CANCEL_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["CISP.CANCEL_POPUP.YES_BUTTON_LABEL"];
                    $scope.modelPopUpConf.btn.cancelbtn.label = translations["CISP.CANCEL_POPUP.NO_BUTTON_LABEL"];
                    $scope.modelPopUpConf.closebtn = translations["CISP.CANCEL_POPUP.CLOSE_BUTTON_LABEL"];
                    $scope.modelPopUpConf.cancelRequestScrLabel = translations["CISP.CANCEL_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["CISP.CANCEL_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "cancelpopup";
            $scope.modelPopUpConf.escBtn = true;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.cancelSubmission;
            $scope.modelPopUpConf.btn.cancelbtn.visible = true;
            $scope.modelPopUpConf.backdrop = true;
            $scope.modelPopUpConf.open();
        };
        $scope.openSessionOutModal = function () {
            $translate(["CISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER",
                "CISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "CISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL", "CISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL",
                "CISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"]).then(function (translations) {
                    $scope.modelPopUpConf.modelpopupTitle = translations["CISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADER"];
                    $scope.modelPopUpConf.modelpopupBodyContent = translations["CISP.SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                    $scope.modelPopUpConf.btn.okbtn.label = translations["CISP.SESSION_TIMEOUT_POPUP.OK_BUTTON_LABEL"];
                    $scope.modelPopUpConf.sessionTimeoutScrLabel = translations["CISP.SESSION_TIMEOUT_POPUP.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                    $scope.modelPopUpConf.noBtnScrLabel = translations["CISP.SESSION_TIMEOUT_POPUP.PRESS_NO_SCREENREADER_LABEL"];
                });
            $scope.modelPopUpConf.modelpopupType = "sessionTimeOutpopup";
            $scope.modelPopUpConf.escBtn = false;
            $scope.modelPopUpConf.btn.okbtn.visible = true;
            $scope.modelPopUpConf.btn.cancelbtn.visible = false;
            $scope.modelPopUpConf.btn.okbtn.action = $scope.sessionSubmission;
            $scope.modelPopUpConf.backdrop = false;
            $scope.modelPopUpConf.open();
        };
        $scope.scrollRedirect = function ($event, id) {
            $event.preventDefault();
            document.getElementById(id).scrollIntoView();
        };

        $scope.createCustomErr = function (error) {
            $scope.isDataFound = true;
            $scope.retry = $("#retry-url").val() || null;
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "999";
            $scope.errorData.correlationId = error ? error.correlationId : null;
        };

        $scope.ignoreTimeZone = function (val) {
            if (val !== undefined && val !== "" && val !== null) {
                return new Date(val.substring(0, val.indexOf("T")));
            } else {
                return null;
            }
        };

        $scope.init();
    }]);

"use strict";
angular.module("consentApp").directive("pageHeader",
    function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/acct-header.html"
        };
    });

"use strict";
angular.module("consentApp").directive("aispPageFooter",
    function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/aisp-footer.html"
        };
    });

// public/js/directives/acct-pagination.js
"use strict";
angular.module("consentApp").directive("acctPagination", ["config", function (config) {
    return {
        restrict: "E",
        scope: {
            acctPage: "=",
            maxSize: "=",
            forceEllipse: "@",
            rotate: "@",
            filteredAccounts: "=",
            currentPage: "=",
            accountsByPage: "="
        },
        templateUrl: "views/acct-pagination.html",

        link: function (scope) {
            scope.pages = [];
            scope.totalAccounts = [];
            scope.$watch("accountsByPage", function (newVal) {
                if (newVal) {
                    scope.pages = scope.range(newVal.length);
                    if (scope.filteredAccounts) {
                        scope.totalAccounts = scope.filteredAccounts.length;
                    }
                }
            }, true);

            scope.chromeBrowserVersion = function () {
                var userAgent = navigator.userAgent;
                var browserfullVersion;// = "" + parseFloat(navigator.appVersion);
                var browserOffsetVersion, browserMajorVersion;
                if ((browserOffsetVersion = userAgent.indexOf("Chrome")) !== -1) {
                    browserfullVersion = userAgent.substring(browserOffsetVersion + 7);
                    browserMajorVersion = parseInt("" + browserfullVersion, 10);
                }
                return browserMajorVersion;
            };
            scope.applyChromeBorder = false;
            var chromeVersion = scope.chromeBrowserVersion();
            if (config.chromeBorderStartVersion <= chromeVersion && config.chromeBorderEndVersion >= chromeVersion) {
                scope.applyChromeBorder = true;
            }
            scope.range = function (input, total) {
                var ret = [];
                if (!total) {
                    total = input;
                    input = 0;
                }
                for (var i = input; i < total; i++) {
                    ret.push(i);
                }
                return ret;
            };
        }
    };
}]);

"use strict";
angular.module("consentApp").directive("errorNotice",
    ["$translate", "$sce", function ($translate, $sce) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                errorData: "="

            },
            templateUrl: "views/error-notice.html",
            link: {
                pre: function ($scope, el, attrs) {
                    $scope.$watch("errorData", function () {
                        var errorCodeMsg = "ERROR_MESSAGES." + $scope.errorData.errorCode;
                        var errorCodeMsgDesc = "ERROR_MESSAGES." + $scope.errorData.errorCode + "_DETAILS";
                        var correlationCodeMsg = "";
                        $scope.errorCssClass = ($scope.errorData.errorCSSClass === "warning") ? "page-alert-warning" : "page-alert-danger";
                        if ($scope.errorData.correlationId) {
                            correlationCodeMsg = $scope.errorData.correlationId;
                        }

                        $translate(["ERROR_MESSAGES.UNABLE_REQUEST_PROCESS",
                            "ERROR_MESSAGES.ALERT_LABEL", errorCodeMsg, errorCodeMsgDesc,
                            "ERROR_MESSAGES.CORRELATION_LABEL", correlationCodeMsg]).then(function (translations) {
                                $scope.alertText = translations["ERROR_MESSAGES.ALERT_LABEL"] + ":";
                                $scope.errorMsgText = (translations[errorCodeMsg] === errorCodeMsg) ?
                                    $sce.trustAsHtml(translations["ERROR_MESSAGES.UNABLE_REQUEST_PROCESS"]) :
                                    $sce.trustAsHtml(translations[errorCodeMsg]);
                                $scope.corRelationText = translations["ERROR_MESSAGES.CORRELATION_LABEL"] + ":";
                                $scope.corRelationMsgText = translations[correlationCodeMsg];
                                if (translations[errorCodeMsgDesc] !== errorCodeMsgDesc && translations[errorCodeMsgDesc] !== "") {
                                    $scope.isErrorMsgTextDescription = true;
                                    $scope.errorMsgTextDescription = $sce.trustAsHtml(translations[errorCodeMsgDesc]);
                                }
                            });
                    });

                }
            }
        };
    }]);

"use strict";
angular.module("consentApp").directive("modelPopUp", ["$uibModal", function($uibModal) {
    return {
        restrict: "E",
        scope: {
            modal: "="
        },
        link: function($scope, el, attrs) {
            $scope.modal.open = function() {
                $scope.modalInstance = $uibModal.open({
                    templateUrl: "views/modalPopUp.html",
                    animation: true,
                    backdrop: $scope.modal.backdrop,
                    keyboard: $scope.modal.escBtn,
                    windowClass: "pop-confirm",
                    scope: $scope,
                    controller: "modalCtrl"
                });
                $scope.modalInstance.result.then(function() {
                    //success
                });
                $scope.modal.modalInstance = $scope.modalInstance;
            };
        }
    };
}]);
angular.module("consentApp").controller("modalCtrl", ["$scope", "$uibModalInstance", function($scope, $uibModalInstance) {

    $scope.cancelBtnClicked = function() {
        $uibModalInstance.dismiss("cancel");
    };

    $scope.okBtnClicked = function() {
        $uibModalInstance.dismiss("cancel");
        $scope.modal.btn.okbtn.action();
    };

}]);

"use strict";
angular.module("consentApp").directive("customUibAccordian", ["$timeout", function ($timeout) {
    return {
        restrict: "E",
        scope: {
            pl: "=",
            heading: "="
        },
        templateUrl: "views/customUibAccordian.html",
        link: function (scope, ele) {
            scope.oneAtATime = true;
            $timeout(function () {
                $(".panel-heading", ele).removeAttr("role");
                $("a", ele).addClass("panel-title").unwrap();
            });
            scope.toggleDisable = function ($event) {
                if ($event.keyCode === 32) {
                    $event.preventDefault();
                    return false;
                }
                return true;
            };
        }
    };
}]);

"use strict";
angular.module("consentApp").factory("AcctService", ["blockUI", function (blockUI) {
    function searchUtil(item, toSearch) {
        return (item.accountNumber.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
    }
    return {
        searched: function (valLists, toSearch) {
            return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
        },
        paged: function (valLists, pageSize) {
            var retVal = [];
            for (var i = 0; i < valLists.length; i++) {
                if (i % pageSize === 0) {
                    retVal[Math.floor(i / pageSize) + 1] = [valLists[i]];
                } else {
                    retVal[Math.floor(i / pageSize) + 1].push(valLists[i]);
                }
            }
            return retVal;
        },
        errorFallback: function (error, $scope, blockUI) {
            if (error.data.exception && error.data.exception.errorCode === "731") {
                $scope.redirectUri = error.data.redirectUri;
                $scope.openSessionOutModal();
            } else {
                $scope.errorData = {};
                $scope.errorData.errorCode = error.data.exception ? error.data.exception.errorCode : "800";
                $scope.errorData.correlationId = error.headers("correlationId") || null;
                blockUI.stop();
            }
        },
        errorFallbackInline: function (error, $scope, blockUI) {
            if (error.data.exception && error.data.exception.errorCode === "731") {
                $scope.redirectUri = error.data.redirectUri;
                $scope.errorMessage = null;
                $scope.openSessionOutModal();
            } else {
                $scope.errorMessage = {};
                $scope.errorMessage.errorCode = error.data.exception ? error.data.exception.errorCode : "800";
                $scope.errorMessage.correlationId = error.headers("correlationId") || null;
                blockUI.stop();
            }
        },

        updateAcctData: function(acctsData){
          var accts = angular.copy(acctsData);
          accts.forEach(function(acct){
            if(acct.Account.length){
              acct.Account = acct.Account[0];
            }else{
              acct.Account = [acct.Account];
            }
          });
          return accts;
        }
    };
}]);

"use strict";
angular.module("consentApp").service("ConsentService", ["$http", function ($http) {
    return {
        accountRequest: function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, headers) {
            return $http.post("./" + consentType + "/consent?resumePath=" + resumePath + "&correlationId="
            + correlationId + "&refreshTokenRenewalFlow=" + refreshTokenRenewalFlow, reqData, { "headers": headers });
        },
        cancelRequest: function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag) {
            return $http.put("./" + consentType + "/cancelConsent?resumePath=" + resumePath + "&correlationId="
            + correlationId + "&refreshTokenRenewalFlow=" + refreshTokenRenewalFlow + "&serverErrorFlag=" + serverErrorFlag, reqData);
        },
        fundsCheckRequest: function (consentType, reqData, resumePath, user, correlationId) {
            return $http.post("./" + consentType + "/preAuthorisation?resumePath=" + resumePath + "&x-user-id="
            + user + "&correlationId=" + correlationId, reqData);
        },
        checkSession: function (consentType, reqData, resumePath, correlationId) {
            return $http.get("./" + consentType + "/checkSession?correlationId=" + correlationId + "&resumePath="
            + resumePath, reqData);
        }
    };
}]);

"use strict";
angular.module("consentApp").factory("fontloaderService", ["$q", "$window", "blockUI", function ($q, $window, blockUI) {
    return {
        isFontLoad: function () {
            blockUI.start();
            var deferred = $q.defer();
            $window.WebFont.load({
                custom: {
                    families: ["OpenSans-Bold", "OpenSans-Italic",
                        "OpenSans-Light", "OpenSans-Regular", "OpenSans-Semibold"]
                },
                active: function (familyName, fvd) {
                    deferred.resolve("font loaded");
                    blockUI.stop();
                }
            });
            return deferred.promise;
        }
    };
}]);

"use strict";
angular.module("consentApp").filter("maskNumber", function () {
  return function (str, charLength) {
    return "~" + str.substr(str.length - charLength, charLength);
  };
});
