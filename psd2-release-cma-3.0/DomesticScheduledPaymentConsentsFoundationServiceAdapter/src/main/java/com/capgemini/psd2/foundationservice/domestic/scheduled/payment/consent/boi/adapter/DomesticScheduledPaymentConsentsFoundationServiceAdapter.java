package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter;

import java.text.ParseException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.client.DomesticScheduledPaymentConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.delegate.DomesticScheduledPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.transformer.DomesticScheduledPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.rest.client.model.RequestInfo;
@Component("domesticScheduledPaymentConsentsFoundationServiceAdapter")
public class DomesticScheduledPaymentConsentsFoundationServiceAdapter implements DomesticScheduledPaymentStagingAdapter {
	@Autowired
	private DomesticScheduledPaymentConsentsFoundationServiceDelegate domSchPayConsDelegate;

	@Autowired
	private DomesticScheduledPaymentConsentsFoundationServiceClient domSchPayConsClient;

	@Autowired
	private DomesticScheduledPaymentConsentsFoundationServiceTransformer domSchPayConsTransformer;

	@Override
	public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(
			CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		
		RequestInfo requestInfo=new RequestInfo();
		HttpHeaders httpHeaders=domSchPayConsDelegate.createPaymentRequestHeadersPost(requestInfo, params);
		String domesticScheduledPaymentConsentPostURL=domSchPayConsDelegate.postPaymentFoundationServiceURL(customStageIdentifiers.getPaymentSetupVersion());
		requestInfo.setUrl(domesticScheduledPaymentConsentPostURL);
		ScheduledPaymentInstructionProposal instructionProposalRequest = null;
		instructionProposalRequest = domSchPayConsDelegate.transformDomesticConsentResponseFromAPIToFDForInsert(domesticScheduledPaymentRequest, params);

		ScheduledPaymentInstructionProposal instructionProposalResponse =domSchPayConsClient.restTransportForDomesticPaymentFoundationServicePost(requestInfo,instructionProposalRequest,ScheduledPaymentInstructionProposal.class , httpHeaders);
		return domSchPayConsTransformer.transformDomesticConsentResponseFromFDToAPIForInsert(instructionProposalResponse, params);
		
	}
	
	@Override
	public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = domSchPayConsDelegate.createPaymentRequestHeaders(requestInfo, params);
		String domesticPaymentURL = domSchPayConsDelegate.getPaymentFoundationServiceURL(customPaymentStageIdentifiers.getPaymentSetupVersion(),
				customPaymentStageIdentifiers.getPaymentConsentId());
		requestInfo.setUrl(domesticPaymentURL);
		ScheduledPaymentInstructionProposal paymentInstructionProposal = domSchPayConsClient.restTransportForDomesticScheduledPaymentFoundationService(requestInfo, ScheduledPaymentInstructionProposal.class, httpHeaders);
		return domSchPayConsTransformer.transformDomesticScheduledPaymentResponse(paymentInstructionProposal, params);
	}

	/*@Override
	public void updateStagedDomesticScheduledPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}*/

}
