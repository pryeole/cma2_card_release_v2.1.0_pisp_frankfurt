package com.capgemini.tpp.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements   {
  @JsonProperty("additionalInfo")
  private String additionalInfo = null;

  @JsonProperty("allow-unclassified-characters")
  private Boolean allowUnclassifiedCharacters = null;

  @JsonProperty("case-sensitive-validation")
  private Boolean caseSensitiveValidation = null;

  @JsonProperty("character-sets")
  private List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets> characterSets = null;

  @JsonProperty("description")
  private String description = null;

  /**
   * The behavior to use when performing the match. Only applicable when requirement type is \"regular-expressions\"
   */
  public enum MatchBehaviorEnum {
    REQUIRE_MATCH("require-match"),
    
    REJECT_MATCH("reject-match");

    private String value;

    MatchBehaviorEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static MatchBehaviorEnum fromValue(String text) {
      for (MatchBehaviorEnum b : MatchBehaviorEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("match-behavior")
  private MatchBehaviorEnum matchBehavior = null;

  @JsonProperty("match-pattern")
  private String matchPattern = null;

  @JsonProperty("max-consecutive-length")
  private Integer maxConsecutiveLength = null;

  @JsonProperty("max-password-length")
  private Integer maxPasswordLength = null;

  @JsonProperty("min-password-difference")
  private Integer minPasswordDifference = null;

  @JsonProperty("min-unique-characters")
  private Integer minUniqueCharacters = null;

  @JsonProperty("requirementSatisfied")
  private Boolean requirementSatisfied = null;

  @JsonProperty("type")
  private String type = null;

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements additionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
    return this;
  }

   /**
   * The message associated with this failure.
   * @return additionalInfo
  **/
  @ApiModelProperty(value = "The message associated with this failure.")


  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements allowUnclassifiedCharacters(Boolean allowUnclassifiedCharacters) {
    this.allowUnclassifiedCharacters = allowUnclassifiedCharacters;
    return this;
  }

   /**
   * whether to allow passwords to contain characters not listed in any set. Only applicable when requirement type is \"character-set\"
   * @return allowUnclassifiedCharacters
  **/
  @ApiModelProperty(value = "whether to allow passwords to contain characters not listed in any set. Only applicable when requirement type is \"character-set\"")


  public Boolean getAllowUnclassifiedCharacters() {
    return allowUnclassifiedCharacters;
  }

  public void setAllowUnclassifiedCharacters(Boolean allowUnclassifiedCharacters) {
    this.allowUnclassifiedCharacters = allowUnclassifiedCharacters;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements caseSensitiveValidation(Boolean caseSensitiveValidation) {
    this.caseSensitiveValidation = caseSensitiveValidation;
    return this;
  }

   /**
   * Whether the validation will be case-sensitive or case-insensitive. Only applicable when requirement type is \"repeated-characters\" or \"unique-characters\"
   * @return caseSensitiveValidation
  **/
  @ApiModelProperty(value = "Whether the validation will be case-sensitive or case-insensitive. Only applicable when requirement type is \"repeated-characters\" or \"unique-characters\"")


  public Boolean getCaseSensitiveValidation() {
    return caseSensitiveValidation;
  }

  public void setCaseSensitiveValidation(Boolean caseSensitiveValidation) {
    this.caseSensitiveValidation = caseSensitiveValidation;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements characterSets(List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets> characterSets) {
    this.characterSets = characterSets;
    return this;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements addCharacterSetsItem(PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets characterSetsItem) {
    if (this.characterSets == null) {
      this.characterSets = new ArrayList<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets>();
    }
    this.characterSets.add(characterSetsItem);
    return this;
  }

   /**
   * The characters and minimum count for each set. Only applicable when requirement type is \"character-set\"
   * @return characterSets
  **/
  @ApiModelProperty(value = "The characters and minimum count for each set. Only applicable when requirement type is \"character-set\"")

  @Valid

  public List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets> getCharacterSets() {
    return characterSets;
  }

  public void setCharacterSets(List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets> characterSets) {
    this.characterSets = characterSets;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements description(String description) {
    this.description = description;
    return this;
  }

   /**
   * The human-readable description of the password requirement.
   * @return description
  **/
  @ApiModelProperty(value = "The human-readable description of the password requirement.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements matchBehavior(MatchBehaviorEnum matchBehavior) {
    this.matchBehavior = matchBehavior;
    return this;
  }

   /**
   * The behavior to use when performing the match. Only applicable when requirement type is \"regular-expressions\"
   * @return matchBehavior
  **/
  @ApiModelProperty(value = "The behavior to use when performing the match. Only applicable when requirement type is \"regular-expressions\"")


  public MatchBehaviorEnum getMatchBehavior() {
    return matchBehavior;
  }

  public void setMatchBehavior(MatchBehaviorEnum matchBehavior) {
    this.matchBehavior = matchBehavior;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements matchPattern(String matchPattern) {
    this.matchPattern = matchPattern;
    return this;
  }

   /**
   * The regular expression that passwords are evaluated against.Only applicable when requirement type is \"regular-expressions\"
   * @return matchPattern
  **/
  @ApiModelProperty(value = "The regular expression that passwords are evaluated against.Only applicable when requirement type is \"regular-expressions\"")


  public String getMatchPattern() {
    return matchPattern;
  }

  public void setMatchPattern(String matchPattern) {
    this.matchPattern = matchPattern;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements maxConsecutiveLength(Integer maxConsecutiveLength) {
    this.maxConsecutiveLength = maxConsecutiveLength;
    return this;
  }

   /**
   * The maximum number of consecutive repeated characters that will be allowed. Only applicable when requirement type is \"repeated-characters\"
   * @return maxConsecutiveLength
  **/
  @ApiModelProperty(value = "The maximum number of consecutive repeated characters that will be allowed. Only applicable when requirement type is \"repeated-characters\"")


  public Integer getMaxConsecutiveLength() {
    return maxConsecutiveLength;
  }

  public void setMaxConsecutiveLength(Integer maxConsecutiveLength) {
    this.maxConsecutiveLength = maxConsecutiveLength;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements maxPasswordLength(Integer maxPasswordLength) {
    this.maxPasswordLength = maxPasswordLength;
    return this;
  }

   /**
   * The maximum number of characters that passwords will be required to have. Only applicable when requirement type is \"length\"
   * @return maxPasswordLength
  **/
  @ApiModelProperty(value = "The maximum number of characters that passwords will be required to have. Only applicable when requirement type is \"length\"")


  public Integer getMaxPasswordLength() {
    return maxPasswordLength;
  }

  public void setMaxPasswordLength(Integer maxPasswordLength) {
    this.maxPasswordLength = maxPasswordLength;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements minPasswordDifference(Integer minPasswordDifference) {
    this.minPasswordDifference = minPasswordDifference;
    return this;
  }

   /**
   * The minimum number of differences that must be observed between the former password and the new password. Only applicable when requirement type is \"similarity\"
   * @return minPasswordDifference
  **/
  @ApiModelProperty(value = "The minimum number of differences that must be observed between the former password and the new password. Only applicable when requirement type is \"similarity\"")


  public Integer getMinPasswordDifference() {
    return minPasswordDifference;
  }

  public void setMinPasswordDifference(Integer minPasswordDifference) {
    this.minPasswordDifference = minPasswordDifference;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements minUniqueCharacters(Integer minUniqueCharacters) {
    this.minUniqueCharacters = minUniqueCharacters;
    return this;
  }

   /**
   * The minimum number of unique characters that the password must contain. Only applicable when requirement type is \"unique-characters\"
   * @return minUniqueCharacters
  **/
  @ApiModelProperty(value = "The minimum number of unique characters that the password must contain. Only applicable when requirement type is \"unique-characters\"")


  public Integer getMinUniqueCharacters() {
    return minUniqueCharacters;
  }

  public void setMinUniqueCharacters(Integer minUniqueCharacters) {
    this.minUniqueCharacters = minUniqueCharacters;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements requirementSatisfied(Boolean requirementSatisfied) {
    this.requirementSatisfied = requirementSatisfied;
    return this;
  }

   /**
   * True if this requirement was satisfied.  False if not.
   * @return requirementSatisfied
  **/
  @ApiModelProperty(value = "True if this requirement was satisfied.  False if not.")


  public Boolean getRequirementSatisfied() {
    return requirementSatisfied;
  }

  public void setRequirementSatisfied(Boolean requirementSatisfied) {
    this.requirementSatisfied = requirementSatisfied;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements type(String type) {
    this.type = type;
    return this;
  }

   /**
   * The type of password requirement.
   * @return type
  **/
  @ApiModelProperty(value = "The type of password requirement.")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements = (PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements) o;
    return Objects.equals(this.additionalInfo, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.additionalInfo) &&
        Objects.equals(this.allowUnclassifiedCharacters, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.allowUnclassifiedCharacters) &&
        Objects.equals(this.caseSensitiveValidation, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.caseSensitiveValidation) &&
        Objects.equals(this.characterSets, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.characterSets) &&
        Objects.equals(this.description, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.description) &&
        Objects.equals(this.matchBehavior, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.matchBehavior) &&
        Objects.equals(this.matchPattern, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.matchPattern) &&
        Objects.equals(this.maxConsecutiveLength, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.maxConsecutiveLength) &&
        Objects.equals(this.maxPasswordLength, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.maxPasswordLength) &&
        Objects.equals(this.minPasswordDifference, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.minPasswordDifference) &&
        Objects.equals(this.minUniqueCharacters, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.minUniqueCharacters) &&
        Objects.equals(this.requirementSatisfied, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.requirementSatisfied) &&
        Objects.equals(this.type, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(additionalInfo, allowUnclassifiedCharacters, caseSensitiveValidation, characterSets, description, matchBehavior, matchPattern, maxConsecutiveLength, maxPasswordLength, minPasswordDifference, minUniqueCharacters, requirementSatisfied, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements {\n");
    
    sb.append("    additionalInfo: ").append(toIndentedString(additionalInfo)).append("\n");
    sb.append("    allowUnclassifiedCharacters: ").append(toIndentedString(allowUnclassifiedCharacters)).append("\n");
    sb.append("    caseSensitiveValidation: ").append(toIndentedString(caseSensitiveValidation)).append("\n");
    sb.append("    characterSets: ").append(toIndentedString(characterSets)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    matchBehavior: ").append(toIndentedString(matchBehavior)).append("\n");
    sb.append("    matchPattern: ").append(toIndentedString(matchPattern)).append("\n");
    sb.append("    maxConsecutiveLength: ").append(toIndentedString(maxConsecutiveLength)).append("\n");
    sb.append("    maxPasswordLength: ").append(toIndentedString(maxPasswordLength)).append("\n");
    sb.append("    minPasswordDifference: ").append(toIndentedString(minPasswordDifference)).append("\n");
    sb.append("    minUniqueCharacters: ").append(toIndentedString(minUniqueCharacters)).append("\n");
    sb.append("    requirementSatisfied: ").append(toIndentedString(requirementSatisfied)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

