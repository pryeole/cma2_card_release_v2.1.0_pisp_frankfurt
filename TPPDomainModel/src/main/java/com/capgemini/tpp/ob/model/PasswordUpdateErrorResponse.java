package com.capgemini.tpp.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Extension of standard SCIM 2.0 error for password update errors
 */
@ApiModel(description = "Extension of standard SCIM 2.0 error for password update errors")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordUpdateErrorResponse   {
  @JsonProperty("detail")
  private String detail = null;

  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    PASSWORDUPDATEERROR("urn:pingidentity:scim:api:messages:2.0:PasswordUpdateError");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("scimType")
  private String scimType = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("urn:pingidentity:scim:api:messages:2.0:PasswordUpdateError")
  private PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError urnpingidentityscimapimessages20PasswordUpdateError = null;

  public PasswordUpdateErrorResponse detail(String detail) {
    this.detail = detail;
    return this;
  }

   /**
   * A detailed, human readable message.
   * @return detail
  **/
  @ApiModelProperty(value = "A detailed, human readable message.")


  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public PasswordUpdateErrorResponse schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public PasswordUpdateErrorResponse addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public PasswordUpdateErrorResponse scimType(String scimType) {
    this.scimType = scimType;
    return this;
  }

   /**
   * A SCIM detailed error keyword.
   * @return scimType
  **/
  @ApiModelProperty(value = "A SCIM detailed error keyword.")


  public String getScimType() {
    return scimType;
  }

  public void setScimType(String scimType) {
    this.scimType = scimType;
  }

  public PasswordUpdateErrorResponse status(String status) {
    this.status = status;
    return this;
  }

   /**
   * The HTTP status code (see Section 6 [RFC7231]) expressed as a JSON String.
   * @return status
  **/
  @ApiModelProperty(required = true, readOnly = true, value = "The HTTP status code (see Section 6 [RFC7231]) expressed as a JSON String.")
  @NotNull


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public PasswordUpdateErrorResponse urnpingidentityscimapimessages20PasswordUpdateError(PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError urnpingidentityscimapimessages20PasswordUpdateError) {
    this.urnpingidentityscimapimessages20PasswordUpdateError = urnpingidentityscimapimessages20PasswordUpdateError;
    return this;
  }

   /**
   * Get urnpingidentityscimapimessages20PasswordUpdateError
   * @return urnpingidentityscimapimessages20PasswordUpdateError
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError getUrnpingidentityscimapimessages20PasswordUpdateError() {
    return urnpingidentityscimapimessages20PasswordUpdateError;
  }

  public void setUrnpingidentityscimapimessages20PasswordUpdateError(PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError urnpingidentityscimapimessages20PasswordUpdateError) {
    this.urnpingidentityscimapimessages20PasswordUpdateError = urnpingidentityscimapimessages20PasswordUpdateError;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordUpdateErrorResponse passwordUpdateErrorResponse = (PasswordUpdateErrorResponse) o;
    return Objects.equals(this.detail, passwordUpdateErrorResponse.detail) &&
        Objects.equals(this.schemas, passwordUpdateErrorResponse.schemas) &&
        Objects.equals(this.scimType, passwordUpdateErrorResponse.scimType) &&
        Objects.equals(this.status, passwordUpdateErrorResponse.status) &&
        Objects.equals(this.urnpingidentityscimapimessages20PasswordUpdateError, passwordUpdateErrorResponse.urnpingidentityscimapimessages20PasswordUpdateError);
  }

  @Override
  public int hashCode() {
    return Objects.hash(detail, schemas, scimType, status, urnpingidentityscimapimessages20PasswordUpdateError);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdateErrorResponse {\n");
    
    sb.append("    detail: ").append(toIndentedString(detail)).append("\n");
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    scimType: ").append(toIndentedString(scimType)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    urnpingidentityscimapimessages20PasswordUpdateError: ").append(toIndentedString(urnpingidentityscimapimessages20PasswordUpdateError)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

