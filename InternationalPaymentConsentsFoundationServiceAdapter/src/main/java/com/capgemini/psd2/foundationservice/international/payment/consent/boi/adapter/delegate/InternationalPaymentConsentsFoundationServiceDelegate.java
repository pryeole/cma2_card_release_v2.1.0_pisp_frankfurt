package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.transformer.InternationalPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentConsentsFoundationServiceDelegate {
	
	@Autowired
	InternationalPaymentConsentsFoundationServiceTransformer internationalPaymentConsentsFoundationServiceTransformer;
	
	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;

	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;
	
	@Value("${foundationService.apiChannelBrand:#{X-API-CHANNEL-BRAND}}")
	private String apiChannelBrand;
	
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;
	
	@Value("${foundationService.apiChannelCode:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceId;
	
	@Value("${foundationService.internationalPaymentConsentPostBaseURL}")
	private String internationalPaymentConsentPostBaseURL;

	@Value("${foundationService.internationalPaymentConsentSetupVersion}")
	private String internationalPaymentConsentSetupVersion;
	
	@Value("${foundationService.channelBrand:#{X-API-CHANNEL-BRAND}}")
	private String channelBrand;
	
	
	public HttpHeaders createPaymentRequestHeadersPost(CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourcesystem, "PSD2API");
		
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER)))
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER)))
			httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));			
		
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)))
			httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
		
		if (customStageIdentifiers.getPaymentSetupVersion().equalsIgnoreCase("1.0"))
			httpHeaders.add(systemApiVersion, "1.1");
		else
			httpHeaders.add(systemApiVersion, "3.0");
		
		// Code to be used after Product changes
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
		}
	}
		return httpHeaders;
	}
	
	public String postPaymentFoundationServiceURL() {

		return internationalPaymentConsentPostBaseURL + "/" + internationalPaymentConsentSetupVersion
				+ "/international/payment-instruction-proposals";
	}
	
	public String getPaymentFoundationServiceURL(String paymentInstuctionProposalId) {
		return internationalPaymentConsentPostBaseURL + "/" + internationalPaymentConsentSetupVersion + "/"
				+ "international/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}
	
	public PaymentInstructionProposalInternational transformInternationalConsentResponseFromAPIToFDForInsert(
			CustomIPaymentConsentsPOSTRequest paymentConsentsRequest, Map<String, String> params) {
		
		return internationalPaymentConsentsFoundationServiceTransformer
				.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
	
	public CustomIPaymentConsentsPOSTResponse transformInternationalConsentResponseFromFDToAPIForInsert(
			PaymentInstructionProposalInternational paymentInstructionProposalResponse) {

		return internationalPaymentConsentsFoundationServiceTransformer
				.transformInternationalConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse);
	}


}
