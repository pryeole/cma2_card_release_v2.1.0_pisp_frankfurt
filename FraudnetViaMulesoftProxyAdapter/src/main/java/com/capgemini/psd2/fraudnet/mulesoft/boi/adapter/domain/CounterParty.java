package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

/**
 * CounterParty
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CounterParty {
  @SerializedName("accountInfo")
  private AccountInfo accountInfo;

  public CounterParty accountInfo(AccountInfo accountInfo) {
    this.accountInfo = accountInfo;
    return this;
  }

   /**
   * Get accountInfo
   * @return accountInfo
  **/
  @ApiModelProperty(required = true, value = "")
  public AccountInfo getAccountInfo() {
    return accountInfo;
  }

  public void setAccountInfo(AccountInfo accountInfo) {
    this.accountInfo = accountInfo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CounterParty counterParty = (CounterParty) o;
    return Objects.equals(this.accountInfo, counterParty.accountInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountInfo);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CounterParty {\n");
    
    sb.append("    accountInfo: ").append(toIndentedString(accountInfo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

