<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.capgemini.psd2</groupId>
	<artifactId>SaaSApplication</artifactId>
	<packaging>jar</packaging>
	<version>1901</version>
	<name>SaaSApplication</name>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.6.RELEASE</version>
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<jjwt.version>0.6.0</jjwt.version>
		<mongodb.version>3.3.0</mongodb.version>
		<docker.image.name>registry.capgeminiwebservices.com/alpha-bank-saas</docker.image.name>
		<docker.file.path>${basedir}/src/main/Docker/Dockerfile</docker.file.path>
		<docker.private.registry>registry.capgeminiwebservices.com</docker.private.registry>
		<sonar.core.codeCoveragePlugin>jacoco</sonar.core.codeCoveragePlugin>
		<sonar.jacoco.reportPath>${project.basedir}/coverage-reports/jacoco.exec</sonar.jacoco.reportPath>
		<!-- Excluding Configuration and Application classes from Sonar Coverage. 
			These will be tested with the Web Integraiton Tests -->
		<sonar.exclusions>**/config/*,**/SaasApplication.*,**/app.min.js,**/templates.min.js,**/libs.js,**/libs.min.js,src/main/resources/static/build/karma.conf.js,src/main/resources/static/build/tests/**/*.js,src/main/resources/static/node_modules/**,
			src/main/resources/lcov-report/**</sonar.exclusions>
		<sonar.javascript.coveragePlugin>lcov</sonar.javascript.coveragePlugin>
		<sonar.javascript.lcov.reportPaths>src/main/resources/lcov.info</sonar.javascript.lcov.reportPaths>
		<sonar.sources>src/main/java,src/main/resources</sonar.sources>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>Edgware.RELEASE</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2IdentityManagment</artifactId>
			<version>2.0.0</version>
		</dependency>
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>UIStaticContentAdapter</artifactId>
			<version>2.0.0</version>
		</dependency>
		
		<!-- Payment Platform Adapter -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PaymentSetupPlatformAdapter</artifactId>
			<version>1901</version>
		</dependency>

		<!-- Common Route for all payment types -->
		<dependency>
			<artifactId>PispScaConsentOperationsRoutingAdapter</artifactId>
			<groupId>com.capgemini.psd2</groupId>
			<version>1901</version>
		</dependency>


		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>FundsConfirmationConsentRoutingAdapter</artifactId>
			<version>1901</version>
		</dependency>
	
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>SaaSWebApplication</artifactId>
			<version>2.0.0</version>
		</dependency>
		
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>SaaSRoutingAdapter</artifactId>
			<version>2.0.0</version>
		</dependency>
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2SplunkLogger</artifactId>
			<version>2.0.0</version>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2KMSUtilities</artifactId>
			<version>2.0.0</version>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>FraudSystems</artifactId>
			<version>1810</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId> spring-boot-starter-actuator</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-logging-juli</artifactId>
			<version>8.0.20</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>
		
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>jstl</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-jasper</artifactId>
			<scope>provided</scope>
		</dependency>
	
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-logging-juli</artifactId>
			<version>8.0.20</version>
		</dependency>
		
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
			<scope>compile</scope>
		</dependency>
	
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2SCAConsentHelper</artifactId>
			<version>2.0.0</version>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>AccountRequestsRoutingAdapter</artifactId>
			<version>1901</version>
		</dependency>
		
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PISPConsentAdapter</artifactId>
			<version>2.0.0</version>
		</dependency>
		
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>CISPConsentAdapter</artifactId>
			<version>1.0.0</version>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<layout>ZIP</layout>
				</configuration>
			</plugin>
			<plugin>
				<groupId>io.fabric8</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.16.2</version>
				<configuration>
					<registry>${docker.private.registry}</registry>
					<verbose>true</verbose>
					<images>
						<image>
							<name>${docker.image.name}</name>
							<build>


								<dockerFile>${docker.file.path}</dockerFile>
								<assembly>
									<descriptorRef>artifact</descriptorRef>
								</assembly>
							</build>
						</image>
					</images>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.8</version>
				<configuration>
					<destFile>${sonar.jacoco.reportPath}</destFile>
					<dataFile>${sonar.jacoco.reportPath}</dataFile>
					<append>true</append>
				</configuration>
				<executions>
					<execution>
						<id>jacoco-initialize</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>jacoco-site</id>
						<phase>package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
