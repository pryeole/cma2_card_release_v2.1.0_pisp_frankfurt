package com.capgemini.psd2.pisp.stage.domain;

import com.capgemini.psd2.pisp.domain.OBPostalAddress6;

public class CustomISPData {
	private String requestedExecutionDateTime;
	private OBPostalAddress6 creditorPostalAddress;
	private String currencyOfTransfer;
	
	public String getRequestedExecutionDateTime() {
		return requestedExecutionDateTime;
	}

	public void setRequestedExecutionDateTime(String requestedExecutionDateTime) {
		this.requestedExecutionDateTime = requestedExecutionDateTime;
	}

	public OBPostalAddress6 getCreditorPostalAddress() {
		return creditorPostalAddress;
	}

	public void setCreditorPostalAddress(OBPostalAddress6 creditorPostalAddress) {
		this.creditorPostalAddress = creditorPostalAddress;
	}

	public String getCurrencyOfTransfer() {
		return currencyOfTransfer;
	}

	public void setCurrencyOfTransfer(String currencyOfTransfer) {
		this.currencyOfTransfer = currencyOfTransfer;
	}

	@Override
	public String toString() {
		return "CustomISPData [requestedExecutionDateTime=" + requestedExecutionDateTime + ", creditorPostalAddress="
				+ creditorPostalAddress + ", currencyOfTransfer=" + currencyOfTransfer + "]";
	}
}
