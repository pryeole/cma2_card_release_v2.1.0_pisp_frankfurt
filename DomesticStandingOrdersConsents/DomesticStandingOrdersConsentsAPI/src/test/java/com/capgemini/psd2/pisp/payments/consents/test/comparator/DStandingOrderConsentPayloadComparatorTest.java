package com.capgemini.psd2.pisp.payments.consents.test.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payments.consent.test.mockdata.DSOCPayloadComparatorTestMockData;
import com.capgemini.psd2.pisp.payments.consents.comparator.DStandingOrderConsentPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DStandingOrderConsentPayloadComparatorTest {

	@InjectMocks
	private DStandingOrderConsentPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCompareValueEquals0() {

		CustomDStandingOrderConsentsPOSTResponse response = DSOCPayloadComparatorTestMockData.getResponse();
		CustomDStandingOrderConsentsPOSTResponse adaptedStandingOrderConsentsResponse = DSOCPayloadComparatorTestMockData
				.getResponse();

		int i = comparator.compare(response, adaptedStandingOrderConsentsResponse);
		assertEquals(0, i);

	}

	@Test
	public void testCompareValueEquals1() {

		CustomDStandingOrderConsentsPOSTResponse response = DSOCPayloadComparatorTestMockData.getResponse();
		CustomDStandingOrderConsentsPOSTResponse adaptedStandingOrderConsentsResponse = DSOCPayloadComparatorTestMockData
				.getAdaptedStandingOrderConsentsResponse();
		int i = comparator.compare(response, adaptedStandingOrderConsentsResponse);
		assertEquals(1, i);
	}

	@Test
	public void testCompareValueEquals0_AuthMisMatch() {

		CustomDStandingOrderConsentsPOSTResponse response = DSOCPayloadComparatorTestMockData.getResponse();
		CustomDStandingOrderConsentsPOSTResponse adaptedStandingOrderConsentsResponse = DSOCPayloadComparatorTestMockData
				.getResponse();
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		int i = comparator.compare(response, adaptedStandingOrderConsentsResponse);
		assertEquals(1, i);

	}

	@Test
	public void testCompareStandingOrderDetails() {
		CustomDStandingOrderConsentsPOSTResponse standingOrderResponse = DSOCPayloadComparatorTestMockData
				.getStandingOrderConsentResponse();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = DSOCPayloadComparatorTestMockData.getResource();
		CustomDStandingOrderConsentsPOSTRequest request = DSOCPayloadComparatorTestMockData.getStandingOrderRequest();
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		int i = comparator.compareStandingOrderDetails(standingOrderResponse, request, paymentSetupPlatformResource);
		assertEquals(1, i);

	}

	@Test
	public void testCompareStandingOrderDetails_WithDebtorDetails() {
		CustomDStandingOrderConsentsPOSTResponse standingOrderResponse = DSOCPayloadComparatorTestMockData
				.getStandingOrderConsentResponse();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = DSOCPayloadComparatorTestMockData.getResource();
		CustomDStandingOrderConsentsPOSTRequest request = DSOCPayloadComparatorTestMockData.getStandingOrderRequest();

		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("BOI");

		int i = comparator.compareStandingOrderDetails(standingOrderResponse, request, paymentSetupPlatformResource);
		assertEquals(1, i);

	}

	@Test
	public void testCompareStandingOrderDetails_NullDebtorDetails() {
		CustomDStandingOrderConsentsPOSTResponse response = DSOCPayloadComparatorTestMockData
				.getStandingOrderConsentResponse();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = DSOCPayloadComparatorTestMockData.getResource();
		CustomDStandingOrderConsentsPOSTRequest request = DSOCPayloadComparatorTestMockData.getStandingOrderRequest();

		request.getData().getInitiation().setDebtorAccount(null);

		int i = comparator.compareStandingOrderDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);

	}

	@Test
	public void testCompareStandingOrderDetails_CheckFields() {
		CustomDStandingOrderConsentsPOSTResponse standingOrderConsentResponse = DSOCPayloadComparatorTestMockData
				.getStandingOrderConsentResponse();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = DSOCPayloadComparatorTestMockData.getResource();
		CustomDStandingOrderConsentsPOSTRequest request = DSOCPayloadComparatorTestMockData.getStandingOrderRequest();

		request.getData().getInitiation().setDebtorAccount(null);

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setBuildingNumber("14");
		deliveryAddress.setCountrySubDivision("Division");
		risk.setDeliveryAddress(deliveryAddress);
		standingOrderConsentResponse.setRisk(risk);
		request.setRisk(risk);

		int i = comparator.compareStandingOrderDetails(standingOrderConsentResponse, request,
				paymentSetupPlatformResource);
		assertEquals(1, i);

	}

	@Test
	public void testCompareStandingOrderDetails_CheckNullFields() {
		CustomDStandingOrderConsentsPOSTResponse standingOrderConsentResponse = DSOCPayloadComparatorTestMockData
				.getStandingOrderConsentResponse();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = DSOCPayloadComparatorTestMockData.getResource();
		CustomDStandingOrderConsentsPOSTRequest request = DSOCPayloadComparatorTestMockData.getStandingOrderRequest();

		request.getData().getInitiation().setDebtorAccount(null);

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLineList = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLineList);
		deliveryAddress.setCountrySubDivision(null);
		String addressLine = null;
		addressLineList.add(addressLine);
		risk.setDeliveryAddress(deliveryAddress);
		standingOrderConsentResponse.setRisk(risk);
		request.setRisk(risk);

		int i = comparator.compareStandingOrderDetails(standingOrderConsentResponse, request,
				paymentSetupPlatformResource);
		assertEquals(1, i);

	}
}
