package com.capgemini.psd2.pisp.international.payments.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("iPaymentsRoutingAdapter")
public class IPaymentsRoutingAdapterImpl implements InternationalPaymentsAdapter, ApplicationContextAware {

	private InternationalPaymentsAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSubmissionExecutionAdapter}")
	private String paymentSubmissionExecutionAdapter;

	/** The application context. */
	private ApplicationContext applicationContext;

	public InternationalPaymentsAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (InternationalPaymentsAdapter) applicationContext.getBean(adapterName);
		return beanInstance;
	}

	@Override
	public PaymentInternationalSubmitPOST201Response processPayments(CustomIPaymentsPOSTRequest requestBody,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {
		return getRoutingInstance(paymentSubmissionExecutionAdapter).processPayments(requestBody, paymentStatusMap,
				getHeaderParams(params));
	}

	@Override
	public PaymentInternationalSubmitPOST201Response retrieveStagedPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(paymentSubmissionExecutionAdapter)
				.retrieveStagedPaymentsResponse(customPaymentStageIdentifiers, getHeaderParams(params));
	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

}
