package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Document(collection = "paymentSetupPlatformResources")
@Document(collection = "#{@CollectionNameLocator.getPaymentSetupCollectionName()}")
public class PaymentConsentsPlatformResource {

	@Id
	private String id;

	// new 
	private String setupCmaVersion;
	private String paymentConsentId;
	private String paymentType;
	private String statusUpdateDateTime;
	private String tenantId;
	@JsonIgnore
	private String statusUpdatedDescription;

	private String status;
	private String createdAt;
	private String updatedAt;
	private String endToEndIdentification;
	private String tppDebtorDetails;
	private String tppDebtorNameDetails;
	private String instructionIdentification;

	private String idempotencyKey;

	private String tppCID;
	private String idempotencyRequest;
	private String tppLegalEntityName;

	// only for File Payment
	private String fileHash;

	// only for version cma1
	private String paymentId;

	public String getIdempotencyRequest() {
		return idempotencyRequest;
	}

	public void setIdempotencyRequest(String idempotencyRequest) {
		this.idempotencyRequest = idempotencyRequest;
	}

	public String getIdempotencyKey() {
		return idempotencyKey;
	}

	public void setIdempotencyKey(String idempotencyKey) {
		this.idempotencyKey = idempotencyKey;
	}

	public String getTppCID() {
		return tppCID;
	}

	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusUpdateDateTime() {
		return statusUpdateDateTime;
	}

	public void setStatusUpdateDateTime(String statusUpdateDateTime) {
		this.statusUpdateDateTime = statusUpdateDateTime;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getEndToEndIdentification() {
		return endToEndIdentification;
	}

	public void setEndToEndIdentification(String endToEndIdentification) {
		this.endToEndIdentification = endToEndIdentification;
	}

	public String getInstructionIdentification() {
		return instructionIdentification;
	}

	public void setInstructionIdentification(String instructionIdentification) {
		this.instructionIdentification = instructionIdentification;
	}

	public String getTppDebtorDetails() {
		return tppDebtorDetails;
	}

	public void setTppDebtorDetails(String tppDebtorDetails) {
		this.tppDebtorDetails = tppDebtorDetails;
	}

	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}

	public String getPaymentConsentId() {
		return paymentConsentId;
	}

	public void setPaymentConsentId(String paymentConsentId) {
		this.paymentConsentId = paymentConsentId;
	}

	public String getTppDebtorNameDetails() {
		return tppDebtorNameDetails;
	}

	public void setTppDebtorNameDetails(String tppDebtorNameDetails) {
		this.tppDebtorNameDetails = tppDebtorNameDetails;
	}

	public String getSetupCmaVersion() {
		return setupCmaVersion;
	}

	public void setSetupCmaVersion(String setupCmaVersion) {
		this.setupCmaVersion = setupCmaVersion;
	}

	public String getFileHash() {
		return fileHash;
	}

	public void setFileHash(String fileHash) {
		this.fileHash = fileHash;
	}

	public String getStatusUpdatedDescription() {
		return statusUpdatedDescription;
	}

	public void setStatusUpdatedDescription(String statusUpdatedDescription) {
		this.statusUpdatedDescription = statusUpdatedDescription;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	@Override
	public String toString() {
		return "PaymentConsentsPlatformResource [id=" + id + ", setupCmaVersion=" + setupCmaVersion
				+ ", paymentConsentId=" + paymentConsentId + ", paymentType=" + paymentType + ", statusUpdateDateTime="
				+ statusUpdateDateTime + ", statusUpdatedDescription=" + statusUpdatedDescription + ", status=" + status
				+ ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", endToEndIdentification="
				+ endToEndIdentification + ", tppDebtorDetails=" + tppDebtorDetails + ", tppDebtorNameDetails="
				+ tppDebtorNameDetails + ", instructionIdentification=" + instructionIdentification
				+ ", idempotencyKey=" + idempotencyKey + ", tppCID=" + tppCID + ", idempotencyRequest="
				+ idempotencyRequest + ", tppLegalEntityName=" + tppLegalEntityName + ", fileHash=" + fileHash
				+ ", paymentId=" + paymentId + "]";
	}

	
}
