package com.capgemini.psd2.integration.controller.test;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.integration.controller.CertThumbprintController;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class CertThumbprintControllerTest {
	

	private MockMvc mockMvc;

	@Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new CertThumbprintController()).build();
    }

	@Test
	public void computeThumbprint() throws Exception{
		ClassLoader classLoader = getClass().getClassLoader();
		URI uri = classLoader.getResource("_IBo6qaXBha2dGY--tqJlZ5Wrww.pem").toURI();
		Stream<String> lines  = Files.lines(Paths.get(uri));
		final StringBuilder builder = new StringBuilder();
		lines.forEach(str -> builder.append(str));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/certificate/thumbprint").header("X-SSL-Cert", builder.toString()).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
		
		Assert.assertTrue(mvcResult.getResponse().getContentAsString().contains("thumbprint"));
		lines.close();
	}
}