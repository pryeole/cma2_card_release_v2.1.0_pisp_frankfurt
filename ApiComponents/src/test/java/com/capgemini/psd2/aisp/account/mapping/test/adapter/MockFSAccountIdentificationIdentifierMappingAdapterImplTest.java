package com.capgemini.psd2.aisp.account.mapping.test.adapter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.IdntificationMapping;
import com.capgemini.psd2.aisp.account.mapping.adapter.MockFSAccountIdentificationIdentifierMappingAdapterImpl;
import com.capgemini.psd2.aisp.account.mapping.adapter.MockFSAccountMappingRepository;

public class MockFSAccountIdentificationIdentifierMappingAdapterImplTest {

	@Mock
	private MockFSAccountMappingRepository mockFSAccountMappingRepository;
	
	@InjectMocks
	MockFSAccountIdentificationIdentifierMappingAdapterImpl obj=new MockFSAccountIdentificationIdentifierMappingAdapterImpl();

	@Before
	public void setUp() {
	   MockitoAnnotations.initMocks(this);	
	}
	
	@Test
	public void testRetrieveAccountIdentificationMapping() {
		List<String> identifierList=new ArrayList<>();
		identifierList.add("123456");
		IdntificationMapping idntificationMapping=new IdntificationMapping();
		when(mockFSAccountMappingRepository.findByIdentifier(anyString())).thenReturn(idntificationMapping);
		obj.retrieveAccountIdentificationMapping(identifierList);
	}
}
