package com.capgemini.psd2.tpp.block.service.test.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;
import com.capgemini.psd2.tpp.block.service.impl.TppBlockServiceImpl;

public class TppBlockServiceImplTest {

	@Mock
	private TppBlockAdapter tppBlockAdapter;
	
	@InjectMocks
	private TppBlockServiceImpl obj=new TppBlockServiceImpl();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testUpdateTppStatus() {
		
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(false);
		
		when(tppBlockAdapter.fetchTppStatusDetails("nBRcYAcACnghbGFOBk","BOI")).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject(),anyString());
		obj.updateTppStatus("nBRcYAcACnghbGFOBk", ActionEnum.BLOCK, "malicious","BOI");
		
	}
	
	@Test(expected=Exception.class)
	public void testUpdateTppStatus1() {
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(true);
		when(tppBlockAdapter.fetchTppStatusDetails(anyString(), anyString())).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject(), anyString());
		obj.updateTppStatus("123456", ActionEnum.BLOCK, "malicious", anyString());	
	}
	
	@Test(expected=Exception.class)
	public void testUpdateTppStatus2() {
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(true);
		when(tppBlockAdapter.fetchTppStatusDetails(anyString(), anyString())).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject(), anyString());
		obj.updateTppStatus(null, ActionEnum.BLOCK, "malicious", null);	
	}
	
	
	@Test(expected=Exception.class)
	public void testUpdateTppStatus3() {
		TppStatusDetails tppStatus=new TppStatusDetails();
		tppStatus.setxBlock(false);
		//when(tppBlockAdapter.fetchTppStatusDetails(anyString())).thenReturn(tppStatus);
		doNothing().when(tppBlockAdapter).updateTppStatus(anyString(), anyObject(), anyString());
		obj.updateTppStatus("123456", ActionEnum.BLOCK, "malicious", anyString());
	}
}
