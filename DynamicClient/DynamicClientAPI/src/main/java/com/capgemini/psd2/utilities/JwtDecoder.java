package com.capgemini.psd2.utilities;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.nimbusds.jwt.JWT;

public final class JwtDecoder {

	/**
	 * 
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(JwtDecoder.class);

	private JwtDecoder(){}
	
	/**
	 * Decodes the token(String) and sets all the Token properties into RegistrationRequest
	 * class
	 * 
	 * @param token
	 * @return RegistrationRequest (class with all the token properties)
	 * @throws ParseException 
	 */
	public static RegistrationRequest decodeTokenIntoRegistrationRequest(JWT jwt){
		try{
			return JSONUtilities.getObjectFromJSONString(jwt.getJWTClaimsSet().toString(), RegistrationRequest.class);
		}catch(Exception e){
			LOGGER.error("Error while converting jwt into Registration model "+e.getMessage());
			throw ClientRegistrationException
			.populatePortalException(e.getMessage(),ClientRegistrationErrorCodeEnum.MANDATORY_REQUEST_PARAMS_MISSING);
		}
	}
	
	
}
