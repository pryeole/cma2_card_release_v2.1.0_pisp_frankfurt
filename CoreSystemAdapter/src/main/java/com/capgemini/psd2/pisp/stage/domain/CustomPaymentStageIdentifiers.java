package com.capgemini.psd2.pisp.stage.domain;

import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;

public class CustomPaymentStageIdentifiers {
	private String paymentConsentId;
	private String paymentSubmissionId;
	private PaymentTypeEnum paymentTypeEnum;
	private String PaymentSetupVersion;

	/**
	 * @return the paymentConsentId
	 */
	public String getPaymentConsentId() {
		return paymentConsentId;
	}

	/**
	 * @param paymentConsentId
	 *            the paymentConsentId to set
	 */
	public void setPaymentConsentId(String paymentConsentId) {
		this.paymentConsentId = paymentConsentId;
	}

	/**
	 * @return the paymentType
	 */
	public PaymentTypeEnum getPaymentTypeEnum() {
		return paymentTypeEnum;
	}

	/**
	 * @param paymentTypeEnum
	 *            the paymentType to set
	 */
	public void setPaymentTypeEnum(PaymentTypeEnum paymentTypeEnum) {
		this.paymentTypeEnum = paymentTypeEnum;
	}

	/**
	 * @return the paymentSetupVersion
	 */
	public String getPaymentSetupVersion() {
		return PaymentSetupVersion;
	}

	/**
	 * @param paymentSetupVersion
	 *            the paymentSetupVersion to set
	 */
	public void setPaymentSetupVersion(String paymentSetupVersion) {
		PaymentSetupVersion = paymentSetupVersion;
	}

	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}

	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}

	@Override
	public String toString() {
		return "CustomPaymentStageIdentifiers [paymentConsentId=" + paymentConsentId + ", paymentSubmissionId="
				+ paymentSubmissionId + ", paymentTypeEnum=" + paymentTypeEnum + ", PaymentSetupVersion="
				+ PaymentSetupVersion + "]";
	}

}
