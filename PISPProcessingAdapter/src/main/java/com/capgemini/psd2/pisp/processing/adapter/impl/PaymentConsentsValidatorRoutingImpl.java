package com.capgemini.psd2.pisp.processing.adapter.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;

@Component("paymentConsentRoutingValidator")
public class PaymentConsentsValidatorRoutingImpl<T, V>
		implements PaymentConsentsCustomValidator<T, V>, ApplicationContextAware {

	private PaymentConsentsCustomValidator<T, V> beanInstance;

	@Value("${app.customValidationBean:null}")
	private String beanName;

	private ApplicationContext applicationContext;

	public boolean validateConsentRequest(T t) {
		return getPaymentConsentsValidatorInstance(beanName).validateConsentRequest(t);
	}

	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		return getPaymentConsentsValidatorInstance(beanName).validateConsentsGETRequest(paymentRetrieveRequest);
	}

	public boolean validatePaymentConsentResponse(V v) {
		return getPaymentConsentsValidatorInstance(beanName).validatePaymentConsentResponse(v);
	}

	@SuppressWarnings("unchecked")
	public PaymentConsentsCustomValidator<T, V> getPaymentConsentsValidatorInstance(String beanName) {
		if (beanInstance == null)
			beanInstance = (PaymentConsentsCustomValidator<T, V>) applicationContext.getBean(beanName);
		return beanInstance;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

}
