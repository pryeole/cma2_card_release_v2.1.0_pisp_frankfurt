package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.PartiesPaymentBeneficiariesresponse;

public interface AccountBeneficiariesRepository extends MongoRepository<PartiesPaymentBeneficiariesresponse, String> {

	public PartiesPaymentBeneficiariesresponse findByUserId(String userId);
}
