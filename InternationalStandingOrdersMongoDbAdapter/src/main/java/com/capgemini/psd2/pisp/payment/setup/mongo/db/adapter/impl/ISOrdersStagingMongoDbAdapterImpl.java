package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.IStandingorderFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISOrdersFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;


@Lazy
@Conditional(MongoDbMockCondition.class)
@Component("internationalStandingOrderMongoAdapter")
public class ISOrdersStagingMongoDbAdapterImpl implements InternationalStandingOrderAdapter {

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;
	
	@Autowired
	private ISOrdersFoundationRepository repository;
	
	@Override
	public CustomIStandingOrderPOSTResponse processInternationalStandingOrder(CustomIStandingOrderPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		IStandingorderFoundationResource bankResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, IStandingorderFoundationResource.class);

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = null;

		String submissionId = null;
		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);

		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL)
			submissionId = UUID.randomUUID().toString();

		bankResponse.getData().setInternationalStandingOrderId(submissionId);
		bankResponse.getData().setCreationDateTime(customRequest.getCreatedOn());
		bankResponse.getData().setCharges(charges);
		bankResponse.setProcessExecutionStatus(processStatusEnum);

		bankResponse.getData().setMultiAuthorisation(multiAuthorisation);
		if (submissionId != null) {
			OBExternalStatus1Code status = calculateCMAStatus(processStatusEnum, multiAuthorisation, paymentStatusMap);
			bankResponse.getData().setStatus(status);
			bankResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			repository.save(bankResponse);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

		return bankResponse;
	}

	@Override
	public CustomIStandingOrderPOSTResponse retrieveStagedInternationalStandingOrder(
			CustomPaymentStageIdentifiers identifiers, Map<String, String> params) {
		IStandingorderFoundationResource bankResponse = null;

		try {
			bankResponse = repository
					.findOneByDataInternationalStandingOrderId(identifiers.getPaymentSubmissionId());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception
					.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}

		String initiationAmount = bankResponse.getData().getInitiation().getInstructedAmount().getAmount();
		if (reqAttributes.getMethodType().equals("GET")) {
			if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {

				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
		}
		return bankResponse;

	}
	private OBExternalStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		OBExternalStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}

}
