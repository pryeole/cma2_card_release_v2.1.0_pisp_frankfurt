package com.capgemini.psd2.account.statements.mongo.db.adapter.constants;

public class AccountStatementMongoDbAdapterConstants {

	public static final String REQUESTED_FROM_DATETIME = "fromStatementDateTime";

	public static final String REQUESTED_TO_DATETIME = "toStatementDateTime";
	
	public static final String REQUESTED_PAGE_NUMBER = "RequestedPageNumber";
	
	public static final String REQUESTED_PAGE_SIZE= "RequestedPageSize";
	
	public static final String LOCAL_FILE= "/2018-fifa-schedule.pdf";
	
	public static final String REQUESTED_STATEMENT= "statementId";
	
	public static final String REQUESTED_FROM_CONSENT_DATETIME = "RequestedFromConsentDateTime";
	
	public static final String REQUESTED_TO_CONSENT_DATETIME = "RequestedToConsentDateTime";
	
	public static final String CONSENT_EXPIRATION_DATETIME = "ConsentExpirationDateTime";
	
	public static final String REQUESTED_TXN_FILTER = "RequestedTxnFilter";
	
}
