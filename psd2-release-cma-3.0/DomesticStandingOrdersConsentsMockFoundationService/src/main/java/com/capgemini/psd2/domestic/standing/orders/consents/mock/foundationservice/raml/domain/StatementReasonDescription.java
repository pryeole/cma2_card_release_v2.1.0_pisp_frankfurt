package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Provides a description for the type of statement (For eg - Fee Statement, Account Statement)
 */
public enum StatementReasonDescription {
  
  ACCOUNT_STATEMENT("Account Statement"),
  
  FEE_STATEMENT("Fee Statement"),
  
  PRE_NOTIFIED_FEE_STATEMENT("Pre-notified Fee Statement"),
  
  PRE_NOTIFIED_INTEREST_STATEMENT("Pre-notified Interest Statement"),
  
  OVERLIMIT_ITEMS_STATEMENT("Overlimit Items Statement"),
  
  REGULAR_MONTHLY_STATEMENT("Regular Monthly Statement"),
  
  ANNUAL_SUMMARY_STATEMENT("Annual Summary Statement"),
  
  COMMERCIAL_MONTHLY_STATEMENT("Commercial Monthly Statement"),
  
  COMMERCIAL_SUMMARY_STATEMENT("Commercial Summary Statement");

  private String value;

  StatementReasonDescription(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static StatementReasonDescription fromValue(String text) {
    for (StatementReasonDescription b : StatementReasonDescription.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

