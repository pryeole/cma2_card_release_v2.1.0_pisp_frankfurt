package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.LocalDate;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The transaction information from the account of the end user \&quot;
 */
@ApiModel(description = "The transaction information from the account of the end user \"")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class Transaction   {
  @JsonProperty("transactionIdentificationNumber")
  private String transactionIdentificationNumber = null;

  @JsonProperty("financialEvent")
  private FinancialEvent financialEvent = null;

  @JsonProperty("creditDebitEventCode")
  private CreditDebitEventCode creditDebitEventCode = null;

  @JsonProperty("runDate")
  private LocalDate runDate = null;

  @JsonProperty("valueDate")
  private LocalDate valueDate = null;

  @JsonProperty("transactionDecription")
  private String transactionDecription = null;

  @JsonProperty("transactionLocation")
  private String transactionLocation = null;

  @JsonProperty("party")
  private Party20 party = null;

  @JsonProperty("counterPartyAccount")
  private CounterPartyAccount counterPartyAccount = null;

  @JsonProperty("impactedAccount")
  private ImpactedAccount impactedAccount = null;

  @JsonProperty("fxExchangeRate")
  private Double fxExchangeRate = null;

  @JsonProperty("balance")
  private Balance balance = null;

  public Transaction transactionIdentificationNumber(String transactionIdentificationNumber) {
    this.transactionIdentificationNumber = transactionIdentificationNumber;
    return this;
  }

  /**
   * An identifier that uniquely identifies the Transaction.
   * @return transactionIdentificationNumber
  **/
  @ApiModelProperty(required = true, value = "An identifier that uniquely identifies the Transaction.")
  @NotNull


  public String getTransactionIdentificationNumber() {
    return transactionIdentificationNumber;
  }

  public void setTransactionIdentificationNumber(String transactionIdentificationNumber) {
    this.transactionIdentificationNumber = transactionIdentificationNumber;
  }

  public Transaction financialEvent(FinancialEvent financialEvent) {
    this.financialEvent = financialEvent;
    return this;
  }

  /**
   * Get financialEvent
   * @return financialEvent
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public FinancialEvent getFinancialEvent() {
    return financialEvent;
  }

  public void setFinancialEvent(FinancialEvent financialEvent) {
    this.financialEvent = financialEvent;
  }

  public Transaction creditDebitEventCode(CreditDebitEventCode creditDebitEventCode) {
    this.creditDebitEventCode = creditDebitEventCode;
    return this;
  }

  /**
   * Get creditDebitEventCode
   * @return creditDebitEventCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public CreditDebitEventCode getCreditDebitEventCode() {
    return creditDebitEventCode;
  }

  public void setCreditDebitEventCode(CreditDebitEventCode creditDebitEventCode) {
    this.creditDebitEventCode = creditDebitEventCode;
  }

  public Transaction runDate(LocalDate runDate) {
    this.runDate = runDate;
    return this;
  }

  /**
   * The Run Date is the date that the transaction appeared on the account.  It is also known as the Post Date or Posting Date on some systems
   * @return runDate
  **/
  @ApiModelProperty(value = "The Run Date is the date that the transaction appeared on the account.  It is also known as the Post Date or Posting Date on some systems")

  @Valid

  public LocalDate getRunDate() {
    return runDate;
  }

  public void setRunDate(LocalDate runDate) {
    this.runDate = runDate;
  }

  public Transaction valueDate(LocalDate valueDate) {
    this.valueDate = valueDate;
    return this;
  }

  /**
   * The Value Date is the date that the transaction amount impacted on the account balance for the purposes of interest accrual and potentially funds availability
   * @return valueDate
  **/
  @ApiModelProperty(value = "The Value Date is the date that the transaction amount impacted on the account balance for the purposes of interest accrual and potentially funds availability")

  @Valid

  public LocalDate getValueDate() {
    return valueDate;
  }

  public void setValueDate(LocalDate valueDate) {
    this.valueDate = valueDate;
  }

  public Transaction transactionDecription(String transactionDecription) {
    this.transactionDecription = transactionDecription;
    return this;
  }

  /**
   * Description of the Payment  / Transaction
   * @return transactionDecription
  **/
  @ApiModelProperty(value = "Description of the Payment  / Transaction")


  public String getTransactionDecription() {
    return transactionDecription;
  }

  public void setTransactionDecription(String transactionDecription) {
    this.transactionDecription = transactionDecription;
  }

  public Transaction transactionLocation(String transactionLocation) {
    this.transactionLocation = transactionLocation;
    return this;
  }

  /**
   * Get transactionLocation
   * @return transactionLocation
  **/
  @ApiModelProperty(value = "")


  public String getTransactionLocation() {
    return transactionLocation;
  }

  public void setTransactionLocation(String transactionLocation) {
    this.transactionLocation = transactionLocation;
  }

  public Transaction party(Party20 party) {
    this.party = party;
    return this;
  }

  /**
   * Get party
   * @return party
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party20 getParty() {
    return party;
  }

  public void setParty(Party20 party) {
    this.party = party;
  }

  public Transaction counterPartyAccount(CounterPartyAccount counterPartyAccount) {
    this.counterPartyAccount = counterPartyAccount;
    return this;
  }

  /**
   * Get counterPartyAccount
   * @return counterPartyAccount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CounterPartyAccount getCounterPartyAccount() {
    return counterPartyAccount;
  }

  public void setCounterPartyAccount(CounterPartyAccount counterPartyAccount) {
    this.counterPartyAccount = counterPartyAccount;
  }

  public Transaction impactedAccount(ImpactedAccount impactedAccount) {
    this.impactedAccount = impactedAccount;
    return this;
  }

  /**
   * Get impactedAccount
   * @return impactedAccount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ImpactedAccount getImpactedAccount() {
    return impactedAccount;
  }

  public void setImpactedAccount(ImpactedAccount impactedAccount) {
    this.impactedAccount = impactedAccount;
  }

  public Transaction fxExchangeRate(Double fxExchangeRate) {
    this.fxExchangeRate = fxExchangeRate;
    return this;
  }

  /**
   * Get fxExchangeRate
   * @return fxExchangeRate
  **/
  @ApiModelProperty(value = "")


  public Double getFxExchangeRate() {
    return fxExchangeRate;
  }

  public void setFxExchangeRate(Double fxExchangeRate) {
    this.fxExchangeRate = fxExchangeRate;
  }

  public Transaction balance(Balance balance) {
    this.balance = balance;
    return this;
  }

  /**
   * Get balance
   * @return balance
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Balance getBalance() {
    return balance;
  }

  public void setBalance(Balance balance) {
    this.balance = balance;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction transaction = (Transaction) o;
    return Objects.equals(this.transactionIdentificationNumber, transaction.transactionIdentificationNumber) &&
        Objects.equals(this.financialEvent, transaction.financialEvent) &&
        Objects.equals(this.creditDebitEventCode, transaction.creditDebitEventCode) &&
        Objects.equals(this.runDate, transaction.runDate) &&
        Objects.equals(this.valueDate, transaction.valueDate) &&
        Objects.equals(this.transactionDecription, transaction.transactionDecription) &&
        Objects.equals(this.transactionLocation, transaction.transactionLocation) &&
        Objects.equals(this.party, transaction.party) &&
        Objects.equals(this.counterPartyAccount, transaction.counterPartyAccount) &&
        Objects.equals(this.impactedAccount, transaction.impactedAccount) &&
        Objects.equals(this.fxExchangeRate, transaction.fxExchangeRate) &&
        Objects.equals(this.balance, transaction.balance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionIdentificationNumber, financialEvent, creditDebitEventCode, runDate, valueDate, transactionDecription, transactionLocation, party, counterPartyAccount, impactedAccount, fxExchangeRate, balance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transaction {\n");
    
    sb.append("    transactionIdentificationNumber: ").append(toIndentedString(transactionIdentificationNumber)).append("\n");
    sb.append("    financialEvent: ").append(toIndentedString(financialEvent)).append("\n");
    sb.append("    creditDebitEventCode: ").append(toIndentedString(creditDebitEventCode)).append("\n");
    sb.append("    runDate: ").append(toIndentedString(runDate)).append("\n");
    sb.append("    valueDate: ").append(toIndentedString(valueDate)).append("\n");
    sb.append("    transactionDecription: ").append(toIndentedString(transactionDecription)).append("\n");
    sb.append("    transactionLocation: ").append(toIndentedString(transactionLocation)).append("\n");
    sb.append("    party: ").append(toIndentedString(party)).append("\n");
    sb.append("    counterPartyAccount: ").append(toIndentedString(counterPartyAccount)).append("\n");
    sb.append("    impactedAccount: ").append(toIndentedString(impactedAccount)).append("\n");
    sb.append("    fxExchangeRate: ").append(toIndentedString(fxExchangeRate)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

