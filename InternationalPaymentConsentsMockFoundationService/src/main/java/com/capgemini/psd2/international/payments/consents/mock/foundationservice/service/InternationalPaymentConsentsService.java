package com.capgemini.psd2.international.payments.consents.mock.foundationservice.service;

import com.capgemini.psd2.international.payments.consents.mock.foundationservice.domain.PaymentInstructionProposalInternational;

public interface InternationalPaymentConsentsService {
	
	public PaymentInstructionProposalInternational retrieveAccountInformation(String paymentInstructionProposalId) throws Exception;

	public PaymentInstructionProposalInternational createInternationalPaymentConsentsResource (PaymentInstructionProposalInternational paymentInstProposalReq) throws Exception;

	PaymentInstructionProposalInternational retrieveInternationalPaymentConsentsResource(
			String paymentInstructionProposalId) throws Exception;

}
