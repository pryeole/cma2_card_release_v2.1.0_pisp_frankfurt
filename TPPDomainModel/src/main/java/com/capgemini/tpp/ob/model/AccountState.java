package com.capgemini.tpp.ob.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Account state is used to retrieve and update the user&#39;s account state
 */
@ApiModel(description = "Account state is used to retrieve and update the user's account state")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class AccountState   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    ACCOUNTSTATE("urn:pingidentity:schemas:2.0:AccountState");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("accountDisabled")
  private Boolean accountDisabled = null;

  @JsonProperty("accountExpirationTime")
  private OffsetDateTime accountExpirationTime = null;

  @JsonProperty("secondsUntilAccountExpiration")
  private Integer secondsUntilAccountExpiration = null;

  @JsonProperty("passwordChangedTime")
  private OffsetDateTime passwordChangedTime = null;

  @JsonProperty("passwordExpirationWarnedTime")
  private OffsetDateTime passwordExpirationWarnedTime = null;

  @JsonProperty("secondsUntilPasswordExpiration")
  private Integer secondsUntilPasswordExpiration = null;

  @JsonProperty("authenticationFailureTimes")
  private List<OffsetDateTime> authenticationFailureTimes = null;

  @JsonProperty("secondsUntilAuthenticationFailureUnlock")
  private Integer secondsUntilAuthenticationFailureUnlock = null;

  @JsonProperty("remainingAuthenticationFailureCount")
  private Integer remainingAuthenticationFailureCount = null;

  @JsonProperty("lastLoginTime")
  private OffsetDateTime lastLoginTime = null;

  @JsonProperty("secondsUntilIdleLockout")
  private Integer secondsUntilIdleLockout = null;

  @JsonProperty("mustChangePassword")
  private Boolean mustChangePassword = null;

  @JsonProperty("secondsUntilPasswordResetLockout")
  private Integer secondsUntilPasswordResetLockout = null;

  @JsonProperty("graceLoginTimes")
  private List<OffsetDateTime> graceLoginTimes = null;

  @JsonProperty("remainingGraceLoginCount")
  private Integer remainingGraceLoginCount = null;

  @JsonProperty("passwordChangedByRequiredTime")
  private OffsetDateTime passwordChangedByRequiredTime = null;

  @JsonProperty("secondsUntilRequiredChangeTime")
  private Integer secondsUntilRequiredChangeTime = null;

  @JsonProperty("accountActivationTime")
  private OffsetDateTime accountActivationTime = null;

  @JsonProperty("accountUsabilityNotices")
  private List<AccountUsabilityIssue> accountUsabilityNotices = null;

  @JsonProperty("accountUsabilityWarnings")
  private List<AccountUsabilityIssue> accountUsabilityWarnings = null;

  @JsonProperty("accountUsabilityErrors")
  private List<AccountUsabilityIssue> accountUsabilityErrors = null;

  @JsonProperty("passwordHistory")
  private List<AccountStatePasswordHistory> passwordHistory = null;

  public AccountState schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public AccountState addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public AccountState accountDisabled(Boolean accountDisabled) {
    this.accountDisabled = accountDisabled;
    return this;
  }

   /**
   * True if the account is disabled, or false if not.  Set to null to clear.
   * @return accountDisabled
  **/
  @ApiModelProperty(value = "True if the account is disabled, or false if not.  Set to null to clear.")


  public Boolean getAccountDisabled() {
    return accountDisabled;
  }

  public void setAccountDisabled(Boolean accountDisabled) {
    this.accountDisabled = accountDisabled;
  }

  public AccountState accountExpirationTime(OffsetDateTime accountExpirationTime) {
    this.accountExpirationTime = accountExpirationTime;
    return this;
  }

   /**
   * Time of account expiration. Set to null to clear.
   * @return accountExpirationTime
  **/
  @ApiModelProperty(value = "Time of account expiration. Set to null to clear.")

  @Valid

  public OffsetDateTime getAccountExpirationTime() {
    return accountExpirationTime;
  }

  public void setAccountExpirationTime(OffsetDateTime accountExpirationTime) {
    this.accountExpirationTime = accountExpirationTime;
  }

  public AccountState secondsUntilAccountExpiration(Integer secondsUntilAccountExpiration) {
    this.secondsUntilAccountExpiration = secondsUntilAccountExpiration;
    return this;
  }

   /**
   * Seconds until account is expired.
   * @return secondsUntilAccountExpiration
  **/
  @ApiModelProperty(value = "Seconds until account is expired.")


  public Integer getSecondsUntilAccountExpiration() {
    return secondsUntilAccountExpiration;
  }

  public void setSecondsUntilAccountExpiration(Integer secondsUntilAccountExpiration) {
    this.secondsUntilAccountExpiration = secondsUntilAccountExpiration;
  }

  public AccountState passwordChangedTime(OffsetDateTime passwordChangedTime) {
    this.passwordChangedTime = passwordChangedTime;
    return this;
  }

   /**
   * Password changed time. Set to null to clear.
   * @return passwordChangedTime
  **/
  @ApiModelProperty(value = "Password changed time. Set to null to clear.")

  @Valid

  public OffsetDateTime getPasswordChangedTime() {
    return passwordChangedTime;
  }

  public void setPasswordChangedTime(OffsetDateTime passwordChangedTime) {
    this.passwordChangedTime = passwordChangedTime;
  }

  public AccountState passwordExpirationWarnedTime(OffsetDateTime passwordExpirationWarnedTime) {
    this.passwordExpirationWarnedTime = passwordExpirationWarnedTime;
    return this;
  }

   /**
   * Password expiration warned time. Set to null to clear.
   * @return passwordExpirationWarnedTime
  **/
  @ApiModelProperty(value = "Password expiration warned time. Set to null to clear.")

  @Valid

  public OffsetDateTime getPasswordExpirationWarnedTime() {
    return passwordExpirationWarnedTime;
  }

  public void setPasswordExpirationWarnedTime(OffsetDateTime passwordExpirationWarnedTime) {
    this.passwordExpirationWarnedTime = passwordExpirationWarnedTime;
  }

  public AccountState secondsUntilPasswordExpiration(Integer secondsUntilPasswordExpiration) {
    this.secondsUntilPasswordExpiration = secondsUntilPasswordExpiration;
    return this;
  }

   /**
   * Seconds until password will expire.
   * @return secondsUntilPasswordExpiration
  **/
  @ApiModelProperty(readOnly = true, value = "Seconds until password will expire.")


  public Integer getSecondsUntilPasswordExpiration() {
    return secondsUntilPasswordExpiration;
  }

  public void setSecondsUntilPasswordExpiration(Integer secondsUntilPasswordExpiration) {
    this.secondsUntilPasswordExpiration = secondsUntilPasswordExpiration;
  }

  public AccountState authenticationFailureTimes(List<OffsetDateTime> authenticationFailureTimes) {
    this.authenticationFailureTimes = authenticationFailureTimes;
    return this;
  }

  public AccountState addAuthenticationFailureTimesItem(OffsetDateTime authenticationFailureTimesItem) {
    if (this.authenticationFailureTimes == null) {
      this.authenticationFailureTimes = new ArrayList<OffsetDateTime>();
    }
    this.authenticationFailureTimes.add(authenticationFailureTimesItem);
    return this;
  }

   /**
   * The date and time of previous authentication failures.
   * @return authenticationFailureTimes
  **/
  @ApiModelProperty(value = "The date and time of previous authentication failures.")

  @Valid

  public List<OffsetDateTime> getAuthenticationFailureTimes() {
    return authenticationFailureTimes;
  }

  public void setAuthenticationFailureTimes(List<OffsetDateTime> authenticationFailureTimes) {
    this.authenticationFailureTimes = authenticationFailureTimes;
  }

  public AccountState secondsUntilAuthenticationFailureUnlock(Integer secondsUntilAuthenticationFailureUnlock) {
    this.secondsUntilAuthenticationFailureUnlock = secondsUntilAuthenticationFailureUnlock;
    return this;
  }

   /**
   * Seconds until authentication failure unlock.
   * @return secondsUntilAuthenticationFailureUnlock
  **/
  @ApiModelProperty(readOnly = true, value = "Seconds until authentication failure unlock.")


  public Integer getSecondsUntilAuthenticationFailureUnlock() {
    return secondsUntilAuthenticationFailureUnlock;
  }

  public void setSecondsUntilAuthenticationFailureUnlock(Integer secondsUntilAuthenticationFailureUnlock) {
    this.secondsUntilAuthenticationFailureUnlock = secondsUntilAuthenticationFailureUnlock;
  }

  public AccountState remainingAuthenticationFailureCount(Integer remainingAuthenticationFailureCount) {
    this.remainingAuthenticationFailureCount = remainingAuthenticationFailureCount;
    return this;
  }

   /**
   * Remaining authentication failure count.
   * @return remainingAuthenticationFailureCount
  **/
  @ApiModelProperty(readOnly = true, value = "Remaining authentication failure count.")


  public Integer getRemainingAuthenticationFailureCount() {
    return remainingAuthenticationFailureCount;
  }

  public void setRemainingAuthenticationFailureCount(Integer remainingAuthenticationFailureCount) {
    this.remainingAuthenticationFailureCount = remainingAuthenticationFailureCount;
  }

  public AccountState lastLoginTime(OffsetDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
    return this;
  }

   /**
   * Last login time. Set to null to clear.
   * @return lastLoginTime
  **/
  @ApiModelProperty(value = "Last login time. Set to null to clear.")

  @Valid

  public OffsetDateTime getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(OffsetDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public AccountState secondsUntilIdleLockout(Integer secondsUntilIdleLockout) {
    this.secondsUntilIdleLockout = secondsUntilIdleLockout;
    return this;
  }

   /**
   * Seconds until idle lockout.
   * @return secondsUntilIdleLockout
  **/
  @ApiModelProperty(readOnly = true, value = "Seconds until idle lockout.")


  public Integer getSecondsUntilIdleLockout() {
    return secondsUntilIdleLockout;
  }

  public void setSecondsUntilIdleLockout(Integer secondsUntilIdleLockout) {
    this.secondsUntilIdleLockout = secondsUntilIdleLockout;
  }

  public AccountState mustChangePassword(Boolean mustChangePassword) {
    this.mustChangePassword = mustChangePassword;
    return this;
  }

   /**
   * Must change password.
   * @return mustChangePassword
  **/
  @ApiModelProperty(readOnly = true, value = "Must change password.")


  public Boolean getMustChangePassword() {
    return mustChangePassword;
  }

  public void setMustChangePassword(Boolean mustChangePassword) {
    this.mustChangePassword = mustChangePassword;
  }

  public AccountState secondsUntilPasswordResetLockout(Integer secondsUntilPasswordResetLockout) {
    this.secondsUntilPasswordResetLockout = secondsUntilPasswordResetLockout;
    return this;
  }

   /**
   * Seconds until password reset lockout.
   * @return secondsUntilPasswordResetLockout
  **/
  @ApiModelProperty(readOnly = true, value = "Seconds until password reset lockout.")


  public Integer getSecondsUntilPasswordResetLockout() {
    return secondsUntilPasswordResetLockout;
  }

  public void setSecondsUntilPasswordResetLockout(Integer secondsUntilPasswordResetLockout) {
    this.secondsUntilPasswordResetLockout = secondsUntilPasswordResetLockout;
  }

  public AccountState graceLoginTimes(List<OffsetDateTime> graceLoginTimes) {
    this.graceLoginTimes = graceLoginTimes;
    return this;
  }

  public AccountState addGraceLoginTimesItem(OffsetDateTime graceLoginTimesItem) {
    if (this.graceLoginTimes == null) {
      this.graceLoginTimes = new ArrayList<OffsetDateTime>();
    }
    this.graceLoginTimes.add(graceLoginTimesItem);
    return this;
  }

   /**
   * Times of previous grace logins.
   * @return graceLoginTimes
  **/
  @ApiModelProperty(value = "Times of previous grace logins.")

  @Valid

  public List<OffsetDateTime> getGraceLoginTimes() {
    return graceLoginTimes;
  }

  public void setGraceLoginTimes(List<OffsetDateTime> graceLoginTimes) {
    this.graceLoginTimes = graceLoginTimes;
  }

  public AccountState remainingGraceLoginCount(Integer remainingGraceLoginCount) {
    this.remainingGraceLoginCount = remainingGraceLoginCount;
    return this;
  }

   /**
   * Remaining grace login count.
   * @return remainingGraceLoginCount
  **/
  @ApiModelProperty(readOnly = true, value = "Remaining grace login count.")


  public Integer getRemainingGraceLoginCount() {
    return remainingGraceLoginCount;
  }

  public void setRemainingGraceLoginCount(Integer remainingGraceLoginCount) {
    this.remainingGraceLoginCount = remainingGraceLoginCount;
  }

  public AccountState passwordChangedByRequiredTime(OffsetDateTime passwordChangedByRequiredTime) {
    this.passwordChangedByRequiredTime = passwordChangedByRequiredTime;
    return this;
  }

   /**
   * Password change by required time. Set to null to clear.
   * @return passwordChangedByRequiredTime
  **/
  @ApiModelProperty(value = "Password change by required time. Set to null to clear.")

  @Valid

  public OffsetDateTime getPasswordChangedByRequiredTime() {
    return passwordChangedByRequiredTime;
  }

  public void setPasswordChangedByRequiredTime(OffsetDateTime passwordChangedByRequiredTime) {
    this.passwordChangedByRequiredTime = passwordChangedByRequiredTime;
  }

  public AccountState secondsUntilRequiredChangeTime(Integer secondsUntilRequiredChangeTime) {
    this.secondsUntilRequiredChangeTime = secondsUntilRequiredChangeTime;
    return this;
  }

   /**
   * Seconds until require change time.
   * @return secondsUntilRequiredChangeTime
  **/
  @ApiModelProperty(readOnly = true, value = "Seconds until require change time.")


  public Integer getSecondsUntilRequiredChangeTime() {
    return secondsUntilRequiredChangeTime;
  }

  public void setSecondsUntilRequiredChangeTime(Integer secondsUntilRequiredChangeTime) {
    this.secondsUntilRequiredChangeTime = secondsUntilRequiredChangeTime;
  }

  public AccountState accountActivationTime(OffsetDateTime accountActivationTime) {
    this.accountActivationTime = accountActivationTime;
    return this;
  }

   /**
   * Time of account activation.  Set to null to clear.
   * @return accountActivationTime
  **/
  @ApiModelProperty(value = "Time of account activation.  Set to null to clear.")

  @Valid

  public OffsetDateTime getAccountActivationTime() {
    return accountActivationTime;
  }

  public void setAccountActivationTime(OffsetDateTime accountActivationTime) {
    this.accountActivationTime = accountActivationTime;
  }

  public AccountState accountUsabilityNotices(List<AccountUsabilityIssue> accountUsabilityNotices) {
    this.accountUsabilityNotices = accountUsabilityNotices;
    return this;
  }

  public AccountState addAccountUsabilityNoticesItem(AccountUsabilityIssue accountUsabilityNoticesItem) {
    if (this.accountUsabilityNotices == null) {
      this.accountUsabilityNotices = new ArrayList<AccountUsabilityIssue>();
    }
    this.accountUsabilityNotices.add(accountUsabilityNoticesItem);
    return this;
  }

   /**
   * Account usability notices.
   * @return accountUsabilityNotices
  **/
  @ApiModelProperty(value = "Account usability notices.")

  @Valid

  public List<AccountUsabilityIssue> getAccountUsabilityNotices() {
    return accountUsabilityNotices;
  }

  public void setAccountUsabilityNotices(List<AccountUsabilityIssue> accountUsabilityNotices) {
    this.accountUsabilityNotices = accountUsabilityNotices;
  }

  public AccountState accountUsabilityWarnings(List<AccountUsabilityIssue> accountUsabilityWarnings) {
    this.accountUsabilityWarnings = accountUsabilityWarnings;
    return this;
  }

  public AccountState addAccountUsabilityWarningsItem(AccountUsabilityIssue accountUsabilityWarningsItem) {
    if (this.accountUsabilityWarnings == null) {
      this.accountUsabilityWarnings = new ArrayList<AccountUsabilityIssue>();
    }
    this.accountUsabilityWarnings.add(accountUsabilityWarningsItem);
    return this;
  }

   /**
   * Account usability warnings.
   * @return accountUsabilityWarnings
  **/
  @ApiModelProperty(value = "Account usability warnings.")

  @Valid

  public List<AccountUsabilityIssue> getAccountUsabilityWarnings() {
    return accountUsabilityWarnings;
  }

  public void setAccountUsabilityWarnings(List<AccountUsabilityIssue> accountUsabilityWarnings) {
    this.accountUsabilityWarnings = accountUsabilityWarnings;
  }

  public AccountState accountUsabilityErrors(List<AccountUsabilityIssue> accountUsabilityErrors) {
    this.accountUsabilityErrors = accountUsabilityErrors;
    return this;
  }

  public AccountState addAccountUsabilityErrorsItem(AccountUsabilityIssue accountUsabilityErrorsItem) {
    if (this.accountUsabilityErrors == null) {
      this.accountUsabilityErrors = new ArrayList<AccountUsabilityIssue>();
    }
    this.accountUsabilityErrors.add(accountUsabilityErrorsItem);
    return this;
  }

   /**
   * Account usability errors.
   * @return accountUsabilityErrors
  **/
  @ApiModelProperty(value = "Account usability errors.")

  @Valid

  public List<AccountUsabilityIssue> getAccountUsabilityErrors() {
    return accountUsabilityErrors;
  }

  public void setAccountUsabilityErrors(List<AccountUsabilityIssue> accountUsabilityErrors) {
    this.accountUsabilityErrors = accountUsabilityErrors;
  }

  public AccountState passwordHistory(List<AccountStatePasswordHistory> passwordHistory) {
    this.passwordHistory = passwordHistory;
    return this;
  }

  public AccountState addPasswordHistoryItem(AccountStatePasswordHistory passwordHistoryItem) {
    if (this.passwordHistory == null) {
      this.passwordHistory = new ArrayList<AccountStatePasswordHistory>();
    }
    this.passwordHistory.add(passwordHistoryItem);
    return this;
  }

   /**
   * Password history. Set to null to clear.
   * @return passwordHistory
  **/
  @ApiModelProperty(value = "Password history. Set to null to clear.")

  @Valid

  public List<AccountStatePasswordHistory> getPasswordHistory() {
    return passwordHistory;
  }

  public void setPasswordHistory(List<AccountStatePasswordHistory> passwordHistory) {
    this.passwordHistory = passwordHistory;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountState accountState = (AccountState) o;
    return Objects.equals(this.schemas, accountState.schemas) &&
        Objects.equals(this.accountDisabled, accountState.accountDisabled) &&
        Objects.equals(this.accountExpirationTime, accountState.accountExpirationTime) &&
        Objects.equals(this.secondsUntilAccountExpiration, accountState.secondsUntilAccountExpiration) &&
        Objects.equals(this.passwordChangedTime, accountState.passwordChangedTime) &&
        Objects.equals(this.passwordExpirationWarnedTime, accountState.passwordExpirationWarnedTime) &&
        Objects.equals(this.secondsUntilPasswordExpiration, accountState.secondsUntilPasswordExpiration) &&
        Objects.equals(this.authenticationFailureTimes, accountState.authenticationFailureTimes) &&
        Objects.equals(this.secondsUntilAuthenticationFailureUnlock, accountState.secondsUntilAuthenticationFailureUnlock) &&
        Objects.equals(this.remainingAuthenticationFailureCount, accountState.remainingAuthenticationFailureCount) &&
        Objects.equals(this.lastLoginTime, accountState.lastLoginTime) &&
        Objects.equals(this.secondsUntilIdleLockout, accountState.secondsUntilIdleLockout) &&
        Objects.equals(this.mustChangePassword, accountState.mustChangePassword) &&
        Objects.equals(this.secondsUntilPasswordResetLockout, accountState.secondsUntilPasswordResetLockout) &&
        Objects.equals(this.graceLoginTimes, accountState.graceLoginTimes) &&
        Objects.equals(this.remainingGraceLoginCount, accountState.remainingGraceLoginCount) &&
        Objects.equals(this.passwordChangedByRequiredTime, accountState.passwordChangedByRequiredTime) &&
        Objects.equals(this.secondsUntilRequiredChangeTime, accountState.secondsUntilRequiredChangeTime) &&
        Objects.equals(this.accountActivationTime, accountState.accountActivationTime) &&
        Objects.equals(this.accountUsabilityNotices, accountState.accountUsabilityNotices) &&
        Objects.equals(this.accountUsabilityWarnings, accountState.accountUsabilityWarnings) &&
        Objects.equals(this.accountUsabilityErrors, accountState.accountUsabilityErrors) &&
        Objects.equals(this.passwordHistory, accountState.passwordHistory);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, accountDisabled, accountExpirationTime, secondsUntilAccountExpiration, passwordChangedTime, passwordExpirationWarnedTime, secondsUntilPasswordExpiration, authenticationFailureTimes, secondsUntilAuthenticationFailureUnlock, remainingAuthenticationFailureCount, lastLoginTime, secondsUntilIdleLockout, mustChangePassword, secondsUntilPasswordResetLockout, graceLoginTimes, remainingGraceLoginCount, passwordChangedByRequiredTime, secondsUntilRequiredChangeTime, accountActivationTime, accountUsabilityNotices, accountUsabilityWarnings, accountUsabilityErrors, passwordHistory);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountState {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    accountDisabled: ").append(toIndentedString(accountDisabled)).append("\n");
    sb.append("    accountExpirationTime: ").append(toIndentedString(accountExpirationTime)).append("\n");
    sb.append("    secondsUntilAccountExpiration: ").append(toIndentedString(secondsUntilAccountExpiration)).append("\n");
    sb.append("    passwordChangedTime: ").append(toIndentedString(passwordChangedTime)).append("\n");
    sb.append("    passwordExpirationWarnedTime: ").append(toIndentedString(passwordExpirationWarnedTime)).append("\n");
    sb.append("    secondsUntilPasswordExpiration: ").append(toIndentedString(secondsUntilPasswordExpiration)).append("\n");
    sb.append("    authenticationFailureTimes: ").append(toIndentedString(authenticationFailureTimes)).append("\n");
    sb.append("    secondsUntilAuthenticationFailureUnlock: ").append(toIndentedString(secondsUntilAuthenticationFailureUnlock)).append("\n");
    sb.append("    remainingAuthenticationFailureCount: ").append(toIndentedString(remainingAuthenticationFailureCount)).append("\n");
    sb.append("    lastLoginTime: ").append(toIndentedString(lastLoginTime)).append("\n");
    sb.append("    secondsUntilIdleLockout: ").append(toIndentedString(secondsUntilIdleLockout)).append("\n");
    sb.append("    mustChangePassword: ").append(toIndentedString(mustChangePassword)).append("\n");
    sb.append("    secondsUntilPasswordResetLockout: ").append(toIndentedString(secondsUntilPasswordResetLockout)).append("\n");
    sb.append("    graceLoginTimes: ").append(toIndentedString(graceLoginTimes)).append("\n");
    sb.append("    remainingGraceLoginCount: ").append(toIndentedString(remainingGraceLoginCount)).append("\n");
    sb.append("    passwordChangedByRequiredTime: ").append(toIndentedString(passwordChangedByRequiredTime)).append("\n");
    sb.append("    secondsUntilRequiredChangeTime: ").append(toIndentedString(secondsUntilRequiredChangeTime)).append("\n");
    sb.append("    accountActivationTime: ").append(toIndentedString(accountActivationTime)).append("\n");
    sb.append("    accountUsabilityNotices: ").append(toIndentedString(accountUsabilityNotices)).append("\n");
    sb.append("    accountUsabilityWarnings: ").append(toIndentedString(accountUsabilityWarnings)).append("\n");
    sb.append("    accountUsabilityErrors: ").append(toIndentedString(accountUsabilityErrors)).append("\n");
    sb.append("    passwordHistory: ").append(toIndentedString(passwordHistory)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

