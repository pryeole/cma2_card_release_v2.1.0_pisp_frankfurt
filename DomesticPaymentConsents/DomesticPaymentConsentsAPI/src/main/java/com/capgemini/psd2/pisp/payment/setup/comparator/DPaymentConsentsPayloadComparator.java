package com.capgemini.psd2.pisp.payment.setup.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DPaymentConsentsPayloadComparator implements Comparator<OBWriteDomesticConsentResponse1> {

	@Override
	public int compare(OBWriteDomesticConsentResponse1 response,
			OBWriteDomesticConsentResponse1 adaptedPaymentConsentsResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(response.getData().getInitiation().getInstructedAmount().getAmount()),Double
				.parseDouble(
						adaptedPaymentConsentsResponse.getData().getInitiation().getInstructedAmount().getAmount())) == 0) {
			adaptedPaymentConsentsResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(response.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (response.getData().getInitiation().equals(adaptedPaymentConsentsResponse.getData().getInitiation())
				&& response.getRisk().equals(adaptedPaymentConsentsResponse.getRisk()))
			value = 0;

		OBAuthorisation1 responseAuth = response.getData().getAuthorisation();
		OBAuthorisation1 adaptedResponseAuth = adaptedPaymentConsentsResponse.getData().getAuthorisation();

		if (!NullCheckUtils.isNullOrEmpty(responseAuth) && !NullCheckUtils.isNullOrEmpty(adaptedResponseAuth)
				&& value != 1) {
			if (responseAuth.equals(adaptedResponseAuth))
				value = 0;
			else
				value = 1;
		}

		return value;
	}

	public int comparePaymentDetails(CustomDPaymentConsentsPOSTResponse response,
			CustomDPaymentConsentsPOSTRequest request, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		String strPaymentConsentsRequest = JSONUtilities.getJSONOutPutFromObject(request);
		CustomDPaymentConsentsPOSTResponse adaptedPaymentConsentsResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), strPaymentConsentsRequest, CustomDPaymentConsentsPOSTResponse.class);

		if (!validateDebtorDetails(response.getData().getInitiation(),
				adaptedPaymentConsentsResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;

		compareAmount(response.getData().getInitiation().getInstructedAmount(),
				adaptedPaymentConsentsResponse.getData().getInitiation().getInstructedAmount());
		
		/*
		 * From CMA3 onwards, amount wont be treated equal if given as follows; eg:
		 * 1.00, 1.0 (both are treated different and idempotency check fails)
		 */
		/*
		 * Date: 13-Feb-2018 Changes are made for CR-10
		 */
		checkAndModifyEmptyFields(adaptedPaymentConsentsResponse, response);

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& (NullCheckUtils.isNullOrEmpty(adaptedPaymentConsentsResponse.getData().getAuthorisation()))) {
			return 1;
		}

		if (NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& !NullCheckUtils.isNullOrEmpty(adaptedPaymentConsentsResponse.getData().getAuthorisation())) {
			return 1;
		}

		int returnValue = compare(response, adaptedPaymentConsentsResponse);

		response.getData().getInitiation()
				.setRemittanceInformation(request.getData().getInitiation().getRemittanceInformation());
		response.getRisk().setDeliveryAddress(request.getRisk().getDeliveryAddress());

		// End of CR-10
		return returnValue;

	}

	private void checkAndModifyEmptyFields(CustomDPaymentConsentsPOSTResponse adaptedPaymentConsentsResponse,
			CustomDPaymentConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentConsentsResponse.getData().getInitiation(), response);
		checkAndModifyCreditorPostalAddressInformation(adaptedPaymentConsentsResponse.getData().getInitiation(),
				response);

		if (adaptedPaymentConsentsResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentConsentsResponse.getRisk().getDeliveryAddress(), response);
		}
		if (adaptedPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress() != null
				&& adaptedPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress(), response);
		}

	}

	private void checkAndModifyRemittanceInformation(OBDomestic1 obDomestic1,
			CustomDPaymentConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = obDomestic1.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obDomestic1.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyCreditorPostalAddressInformation(OBDomestic1 responseInitiation,
			CustomDPaymentConsentsPOSTResponse response) {

		OBPostalAddress6 crPostaladdInformation = responseInitiation.getCreditorPostalAddress();
		if (crPostaladdInformation != null && response.getData().getInitiation().getCreditorPostalAddress() == null
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressLine())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressType())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getSubDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getStreetName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getBuildingNumber())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getPostCode())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getTownName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountrySubDivision())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountry())) {

			responseInitiation.setCreditorPostalAddress(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress obRisk1DeliveryAddress,
			CustomDPaymentConsentsPOSTResponse response) {

		if (obRisk1DeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : obRisk1DeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			obRisk1DeliveryAddress.setAddressLine(addressLineList);
		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomDPaymentConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}

		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditorPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

	private boolean validateDebtorDetails(OBDomestic1 obDomestic1, OBDomestic1 obDomestic12,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {
			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				obDomestic1.getDebtorAccount().setName(null);

			// DebtorAgent is not present in CMA3
			return Objects.equals(obDomestic1.getDebtorAccount(), obDomestic12.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obDomestic12.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obDomestic1.getDebtorAccount() != null) {
			obDomestic1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	private void compareAmount(OBDomestic1InstructedAmount obDomestic1InstructedAmount,
			OBDomestic1InstructedAmount obDomestic1InstructedAmount2) {
		if (Double.compare(Double.parseDouble(obDomestic1InstructedAmount.getAmount()),
				Double.parseDouble(obDomestic1InstructedAmount2.getAmount())) == 0)
			obDomestic1InstructedAmount.setAmount(obDomestic1InstructedAmount2.getAmount());
	}

}
