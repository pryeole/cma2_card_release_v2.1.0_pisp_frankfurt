/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.capgemini.psd2.enums.ConsentStatusEnum;

/**
 * The Class ConsentTest.
 */
public class AispConsentTest {

	/**
	 * Test consent.
	 */
	@Test
	public void testConsent() {
		AispConsent consent = new AispConsent();
		consent.setTppCId("tpp123");
		consent.setPsuId("user123");
		consent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");
		consent.setEndDate("05-07-2017");
		consent.setStartDate("05-07-2017");
		consent.setStatus(ConsentStatusEnum.AUTHORISED);
		consent.setAccountRequestId("a1e590ad-73dc-453a-841e-2a2ef055e878");
		consent.setTransactionFromDateTime("10-10-2017");
		consent.setTransactionToDateTime("11-10-2017");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("22289");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("1022675");
		selectedAccounts.add(accountRequest1);
		consent.setAccountDetails(selectedAccounts);

		assertEquals("tpp123", consent.getTppCId());
		assertEquals("user123", consent.getPsuId());
		assertEquals("a1e590ad-73dc-453a-841e-2a2ef055e878", consent.getConsentId());
		assertEquals(selectedAccounts, consent.getAccountDetails());
		assertEquals("05-07-2017", consent.getEndDate());
		assertEquals("05-07-2017", consent.getStartDate());
		assertEquals(ConsentStatusEnum.AUTHORISED, consent.getStatus());
	    assertEquals("a1e590ad-73dc-453a-841e-2a2ef055e878", consent.getAccountRequestId());
	    assertEquals("10-10-2017", consent.getTransactionFromDateTime());
	    assertEquals("11-10-2017", consent.getTransactionToDateTime());
	}

}
