package com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client.InsertPreStagePaymentFoundationClient;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test.InsertPreStagePaymentFoundationServiceRetryTest.SpringConfig;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@ActiveProfiles("unit-test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpringConfig.class }, initializers = ConfigFileApplicationContextInitializer.class)
public class InsertPreStagePaymentFoundationServiceRetryTest {

	@Autowired
	private InsertPreStagePaymentFoundationClient insertPreStagePaymentFoundationClient;

	@Test(expected = RuntimeException.class)
	public void testInsertPreStagePaymentRetry() throws Exception {

		HttpHeaders headers = new HttpHeaders();
		RequestInfo reqInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		

		this.insertPreStagePaymentFoundationClient.restTransportForInsertPreStagePayment(reqInfo, paymentInstruction, ValidationPassed.class, headers);
		verify(insertPreStagePaymentFoundationClient, times(3)).restTransportForInsertPreStagePayment(anyObject(),anyObject(), anyObject(), anyObject());
		
	}
	
	@Test(expected = RuntimeException.class)
	public void testUpdatePreStagePaymentRetry() throws Exception {

		HttpHeaders headers = new HttpHeaders();
		RequestInfo reqInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		

		this.insertPreStagePaymentFoundationClient.restTransportForUpdatePreStagePayment(reqInfo, paymentInstruction, ValidationPassed.class, headers);
		verify(insertPreStagePaymentFoundationClient, times(3)).restTransportForUpdatePreStagePayment(anyObject(),anyObject(), anyObject(), anyObject());
		
	}
	
	@Test(expected = RuntimeException.class)
	public void testRetrievalPreStagePaymentRetry() throws Exception {

		HttpHeaders headers = new HttpHeaders();
		RequestInfo reqInfo = new RequestInfo();
	
	
		this.insertPreStagePaymentFoundationClient.restTransportForPaymentRetrieval(reqInfo, PaymentInstruction.class, headers);		
		verify(insertPreStagePaymentFoundationClient, times(3)).restTransportForPaymentRetrieval(anyObject(),anyObject(), anyObject());
		
           
		
	}
	
	
	@Configuration
	@EnableRetry
	@Profile("unit-test")
	@ComponentScan
	@EnableAutoConfiguration
	public static class SpringConfig {

		ValidationPassed validationPassed = new ValidationPassed();
		PaymentInstruction instructions = new PaymentInstruction();

		@Bean
		public InsertPreStagePaymentFoundationClient remoteCallService() throws Exception {
			InsertPreStagePaymentFoundationClient remoteService = mock(InsertPreStagePaymentFoundationClient.class);
            
			when(remoteService.restTransportForInsertPreStagePayment(anyObject(), anyObject(), anyObject(),anyObject()))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenReturn(validationPassed);
			
			when(remoteService.restTransportForUpdatePreStagePayment(anyObject(), anyObject(), anyObject(),anyObject()))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenReturn(validationPassed);
			
			
			when(remoteService.restTransportForPaymentRetrieval( anyObject(), anyObject(),anyObject()))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenThrow(new RuntimeException("Retry_Error_Code"))
							  .thenReturn(instructions);
			
			return remoteService;
		}

	}
}
