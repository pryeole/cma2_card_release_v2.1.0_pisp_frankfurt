package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;

public abstract class PaymentStagingService<T, U, V> {

	public abstract DomesticPaymentStagingAdapter locateDomesticStagingAdapter();

	public T retrieveDomesticPaymentConsentStaging(CustomPaymentStageIdentifiers customPaymentStageIdentifiers,
			Map<String, String> paramMap) {
		CustomDPaymentConsentsPOSTResponse domesticStageResponse = this.locateDomesticStagingAdapter()
				.retrieveStagedDomesticPaymentConsents(customPaymentStageIdentifiers, paramMap);
		T transformedResponse = transformDomesticPaymentStagingConsentData(domesticStageResponse);
		return transformedResponse;
	}

	public U retrieveDomesticPaymentPreAuthorizeStaging(CustomPaymentStageIdentifiers customPaymentStageIdentifiers,
			Map<String, String> paramMap) {
		CustomDPaymentConsentsPOSTResponse domesticStageResponse = this.locateDomesticStagingAdapter()
				.retrieveStagedDomesticPaymentConsents(customPaymentStageIdentifiers, paramMap);
		U transformedResponse = transformDomesticPaymentStagingPreAuthData(domesticStageResponse);
		return transformedResponse;
	}

	public V retrieveDomesticPaymentFraudStaging(CustomPaymentStageIdentifiers customPaymentStageIdentifiers,
			Map<String, String> paramMap) {
		CustomDPaymentConsentsPOSTResponse domesticStageResponse = this.locateDomesticStagingAdapter()
				.retrieveStagedDomesticPaymentConsents(customPaymentStageIdentifiers, paramMap);
		V transformedResponse = transformDomesticPaymentStagingFraudData(domesticStageResponse);
		return transformedResponse;
	}

	protected abstract T transformDomesticPaymentStagingConsentData(CustomDPaymentConsentsPOSTResponse domesticStageResponse);
	protected abstract U transformDomesticPaymentStagingPreAuthData(CustomDPaymentConsentsPOSTResponse domesticStageResponse);
	protected abstract V transformDomesticPaymentStagingFraudData(CustomDPaymentConsentsPOSTResponse domesticStageResponse);
}
