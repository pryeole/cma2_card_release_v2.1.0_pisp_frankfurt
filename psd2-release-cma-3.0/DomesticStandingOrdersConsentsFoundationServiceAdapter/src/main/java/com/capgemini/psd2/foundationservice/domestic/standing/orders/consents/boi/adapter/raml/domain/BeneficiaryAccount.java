package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Beneficiary Account object
 */
@ApiModel(description = "Beneficiary Account object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class BeneficiaryAccount   {
  @JsonProperty("accountName")
  private String accountName = null;

  @JsonProperty("accountNumber")
  private String accountNumber = null;

  @JsonProperty("parentNationalSortCodeNSCNumber")
  private String parentNationalSortCodeNSCNumber = null;

  @JsonProperty("internationalBankAccountNumberIBAN")
  private String internationalBankAccountNumberIBAN = null;

  @JsonProperty("swiftBankIdentifierCodeBIC")
  private String swiftBankIdentifierCodeBIC = null;

  @JsonProperty("ABARTNRoutingTransitNumber")
  private String abARTNRoutingTransitNumber = null;

  @JsonProperty("operationalOrganisationUnit")
  private OperationalOrganisationUnit operationalOrganisationUnit = null;

  public BeneficiaryAccount accountName(String accountName) {
    this.accountName = accountName;
    return this;
  }

  /**
   * Name of the account
   * @return accountName
  **/
  @ApiModelProperty(required = true, value = "Name of the account")
  @NotNull


  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public BeneficiaryAccount accountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }

  /**
   * The identifier of the Account. This will be unique within its Parent NSC, but will not be globally unique.
   * @return accountNumber
  **/
  @ApiModelProperty(value = "The identifier of the Account. This will be unique within its Parent NSC, but will not be globally unique.")


  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public BeneficiaryAccount parentNationalSortCodeNSCNumber(String parentNationalSortCodeNSCNumber) {
    this.parentNationalSortCodeNSCNumber = parentNationalSortCodeNSCNumber;
    return this;
  }

  /**
   * Parent NSC of the branch where account is held
   * @return parentNationalSortCodeNSCNumber
  **/
  @ApiModelProperty(required = true, value = "Parent NSC of the branch where account is held")
  @NotNull


  public String getParentNationalSortCodeNSCNumber() {
    return parentNationalSortCodeNSCNumber;
  }

  public void setParentNationalSortCodeNSCNumber(String parentNationalSortCodeNSCNumber) {
    this.parentNationalSortCodeNSCNumber = parentNationalSortCodeNSCNumber;
  }

  public BeneficiaryAccount internationalBankAccountNumberIBAN(String internationalBankAccountNumberIBAN) {
    this.internationalBankAccountNumberIBAN = internationalBankAccountNumberIBAN;
    return this;
  }

  /**
   * Get internationalBankAccountNumberIBAN
   * @return internationalBankAccountNumberIBAN
  **/
  @ApiModelProperty(value = "")


  public String getInternationalBankAccountNumberIBAN() {
    return internationalBankAccountNumberIBAN;
  }

  public void setInternationalBankAccountNumberIBAN(String internationalBankAccountNumberIBAN) {
    this.internationalBankAccountNumberIBAN = internationalBankAccountNumberIBAN;
  }

  public BeneficiaryAccount swiftBankIdentifierCodeBIC(String swiftBankIdentifierCodeBIC) {
    this.swiftBankIdentifierCodeBIC = swiftBankIdentifierCodeBIC;
    return this;
  }

  /**
   * Get swiftBankIdentifierCodeBIC
   * @return swiftBankIdentifierCodeBIC
  **/
  @ApiModelProperty(value = "")


  public String getSwiftBankIdentifierCodeBIC() {
    return swiftBankIdentifierCodeBIC;
  }

  public void setSwiftBankIdentifierCodeBIC(String swiftBankIdentifierCodeBIC) {
    this.swiftBankIdentifierCodeBIC = swiftBankIdentifierCodeBIC;
  }

  public BeneficiaryAccount abARTNRoutingTransitNumber(String abARTNRoutingTransitNumber) {
    this.abARTNRoutingTransitNumber = abARTNRoutingTransitNumber;
    return this;
  }

  /**
   * Bank Identifier for US Banks
   * @return abARTNRoutingTransitNumber
  **/
  @ApiModelProperty(value = "Bank Identifier for US Banks")


  public String getAbARTNRoutingTransitNumber() {
    return abARTNRoutingTransitNumber;
  }

  public void setAbARTNRoutingTransitNumber(String abARTNRoutingTransitNumber) {
    this.abARTNRoutingTransitNumber = abARTNRoutingTransitNumber;
  }

  public BeneficiaryAccount operationalOrganisationUnit(OperationalOrganisationUnit operationalOrganisationUnit) {
    this.operationalOrganisationUnit = operationalOrganisationUnit;
    return this;
  }

  /**
   * Get operationalOrganisationUnit
   * @return operationalOrganisationUnit
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OperationalOrganisationUnit getOperationalOrganisationUnit() {
    return operationalOrganisationUnit;
  }

  public void setOperationalOrganisationUnit(OperationalOrganisationUnit operationalOrganisationUnit) {
    this.operationalOrganisationUnit = operationalOrganisationUnit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BeneficiaryAccount beneficiaryAccount = (BeneficiaryAccount) o;
    return Objects.equals(this.accountName, beneficiaryAccount.accountName) &&
        Objects.equals(this.accountNumber, beneficiaryAccount.accountNumber) &&
        Objects.equals(this.parentNationalSortCodeNSCNumber, beneficiaryAccount.parentNationalSortCodeNSCNumber) &&
        Objects.equals(this.internationalBankAccountNumberIBAN, beneficiaryAccount.internationalBankAccountNumberIBAN) &&
        Objects.equals(this.swiftBankIdentifierCodeBIC, beneficiaryAccount.swiftBankIdentifierCodeBIC) &&
        Objects.equals(this.abARTNRoutingTransitNumber, beneficiaryAccount.abARTNRoutingTransitNumber) &&
        Objects.equals(this.operationalOrganisationUnit, beneficiaryAccount.operationalOrganisationUnit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountName, accountNumber, parentNationalSortCodeNSCNumber, internationalBankAccountNumberIBAN, swiftBankIdentifierCodeBIC, abARTNRoutingTransitNumber, operationalOrganisationUnit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BeneficiaryAccount {\n");
    
    sb.append("    accountName: ").append(toIndentedString(accountName)).append("\n");
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    parentNationalSortCodeNSCNumber: ").append(toIndentedString(parentNationalSortCodeNSCNumber)).append("\n");
    sb.append("    internationalBankAccountNumberIBAN: ").append(toIndentedString(internationalBankAccountNumberIBAN)).append("\n");
    sb.append("    swiftBankIdentifierCodeBIC: ").append(toIndentedString(swiftBankIdentifierCodeBIC)).append("\n");
    sb.append("    abARTNRoutingTransitNumber: ").append(toIndentedString(abARTNRoutingTransitNumber)).append("\n");
    sb.append("    operationalOrganisationUnit: ").append(toIndentedString(operationalOrganisationUnit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

