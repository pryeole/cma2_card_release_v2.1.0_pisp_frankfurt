package com.capgemini.psd2.pisp.domestic.standing.order.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;

public interface DomesticStandingOrderMultiAuthRepository extends MongoRepository<OBMultiAuthorisation1, String> {
}
