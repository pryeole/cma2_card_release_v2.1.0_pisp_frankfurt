package com.capgemini.psd2.mongo.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.util.ResourceUtils;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

@Configuration
@ConditionalOnExpression("'${spring.data.mongodb.host}' == 'mongodb.webservices.com' || '${spring.data.mongodb.host}' == 'mongodb1.webservices.com,mongodb2.webservices.com,mongodb3.webservices.com'")
public class MongoDBConfiguration {

	protected static final Logger LOGGER = LoggerFactory.getLogger(MongoDBConfiguration.class);

	@Value("${spring.data.mongodb.host:null}")
	private String hosts;

	@Value("${spring.data.mongodb.port:null}")
	private int port;

	@Value("${spring.data.mongodb.database:null}")
	private String database;

	@Value("${spring.data.mongodb.username:null}")
	private String username;

	@Value("${spring.data.mongodb.password:null}")
	private String password;

	@Value("${spring.data.mongodb.acknowledgement:1}")
	private int replicaSet;

	@Value("${spring.data.mongodb.timeout:2000}")
	private int replicaTimeOut;

	@Value("${spring.data.mongodb.keyAlias:null}")
	private String mongodbKeyAlias;

	@Value("${keyStoreName:null}")
	private String keyStoreFileName;

	@Value("${javax.net.ssl.keyStorePassword:null}")
	private String keyStorePassword;

	@Value("${javax.net.ssl.trustStore:null}")
	private String trustStoreName;

	@Value("${javax.net.ssl.trustStorePassword:null}")
	private String trustStorePassword;

	@Autowired
	private ApplicationContext appContext;

	@Bean
	@ConditionalOnExpression("'${spring.data.mongodb.host:true}' != 'true'")
	public MongoDbFactory mongoDbFactory() throws Exception {
		System.setProperty("javax.net.ssl.keyStore", "/classpath/" + keyStoreFileName);
		System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);

		System.setProperty("javax.net.ssl.trustStore", trustStoreName);
		System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
		SSLContext sslContext = null;
		if (mongodbKeyAlias.equals("null")) {
			sslContext = SSLContextBuilder.create()
					.loadKeyMaterial(ResourceUtils.getFile("classpath:" + keyStoreFileName),
							keyStorePassword.toCharArray(), keyStorePassword.toCharArray())
					.build();
		} else {
			sslContext = SSLContextBuilder.create()
					.loadKeyMaterial(ResourceUtils.getFile("classpath:" + keyStoreFileName),
							keyStorePassword.toCharArray(), keyStorePassword.toCharArray(),
							(aliases, socket) -> mongodbKeyAlias)
					.build();
		}
		MongoClientOptions.Builder builder = MongoClientOptions.builder();
		builder.sslEnabled(true).socketFactory(sslContext.getSocketFactory()).build();
		// ServerAddress addr1 = new ServerAddress(host, port);

		final List<ServerAddress> serverList = new ArrayList<>();
		String host[] = hosts.split(",");
		for (int i = 0; i < host.length; i++) {
			serverList.add(new ServerAddress(host[i], port));
		}

		MongoClientOptions sslOptions = builder.build();
		MongoClient mongoClient;
		if (username.equals("null") || password.equals("null")) {
			mongoClient = new MongoClient(serverList, sslOptions);
		} else {
			MongoCredential credential = MongoCredential.createPlainCredential(username, "$external",
					password.toCharArray());
			mongoClient = new MongoClient(serverList, Arrays.asList(credential), sslOptions);
		}
		return new SimpleMongoDbFactory(mongoClient, database);
	}

	@Bean
	@ConditionalOnExpression("'${spring.data.mongodb.host:true}' != 'true'")
	public MongoTemplate mongoTemplate() throws Exception {

		final MongoMappingContext mongoMappingContext = new MongoMappingContext();
		mongoMappingContext.setApplicationContext(appContext);
		
		MongoDbFactory mongoFactory = mongoDbFactory();
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoFactory);
		MappingMongoConverter mappingConverter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
		mappingConverter.setTypeMapper(new DefaultMongoTypeMapper(null));
		mappingConverter.afterPropertiesSet();

		MongoTemplate mongoTemplate = new PSD2MongoTemplate(mongoFactory, mappingConverter);

		mongoTemplate.setReadPreference(ReadPreference.secondaryPreferred());
		WriteConcern wcr = new WriteConcern(replicaSet, replicaTimeOut);
		mongoTemplate.setWriteConcern(wcr);
		return mongoTemplate;
	}

}