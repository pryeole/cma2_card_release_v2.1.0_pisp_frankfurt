package com.capgemini.psd2.pisp.test.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = {"com.capgemini.psd2"})
public class PaymentSetupITApplication {

	static ConfigurableApplicationContext context = null;
	public static void main(String[] args) {
		context=SpringApplication.run(PaymentSetupITApplication.class, args);
	}	
				
}
