/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.client.AccountSchedulePaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.constants.AccountSchedulePaymentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.delegate.AccountSchedulePaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOnceOffPaymentScheduleScheduleItemsresponse;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountSchedulePaymentsFoundationServiceAdapter.
 */
@Component
public class AccountSchedulePaymentsFoundationServiceAdapter implements AccountSchedulePaymentsAdapter {

	/** The single account balance base URL. */
	@Value("${foundationService.accountSchedulePaymentBaseURL}")
	private String accountScheduledPaymentBaseURL;
	
	@Value("${foundationService.accountSchedulePaymentEndURL}")
	private String accountScheduledPaymentendURL;


	/** The account balance foundation service delegate. */
	@Autowired
	private AccountSchedulePaymentsFoundationServiceDelegate accountSchedulePaymentsFoundationServiceDelegate;

	/** The account balance foundation service client. */
	@Autowired
	private AccountSchedulePaymentsFoundationServiceClient accountSchedulePaymentsFoundationServiceClient;

	@Override
	public PlatformAccountSchedulePaymentsResponse retrieveAccountSchedulePayments(
			AccountMapping accountMapping, Map<String, String> params) {
		
		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		} 
		
		if (params==null){
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(requestInfo,
				accountMapping, params);
		PartiesOnceOffPaymentScheduleScheduleItemsresponse response = null;
		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null
				&& !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
			params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_SUBTYPE, String.valueOf(accountDetails.getAccountSubType()));
			

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		
		if((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType()) && params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount")
				|| accountDetails.getAccountSubType().toString().contentEquals("Savings")){

			String finalURL = accountSchedulePaymentsFoundationServiceDelegate.getFoundationServiceURL(
					accountDetails.getAccountNSC(), accountDetails.getAccountNumber(),accountMapping.getPsuId(), accountScheduledPaymentBaseURL, accountScheduledPaymentendURL);
			requestInfo.setUrl(finalURL);
			
			MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String ,String>();
			queryParams.add(AccountSchedulePaymentsFoundationServiceConstants.PARENTNATIONALSORTCODENSCNUMBER, accountDetails.getAccountNSC());
			queryParams.add(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNTNUMBER, accountDetails.getAccountNumber());
			response = accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(requestInfo,PartiesOnceOffPaymentScheduleScheduleItemsresponse.class, queryParams, httpHeaders);	 
				 if(NullCheckUtils.isNullOrEmpty(response.getOneOffPaymentInstructionsList()))
						response = new PartiesOnceOffPaymentScheduleScheduleItemsresponse();
		}
		else{
			if(accountDetails.getAccountSubType().toString().contentEquals("CreditCard"))
					response = new PartiesOnceOffPaymentScheduleScheduleItemsresponse();
			
		}
		return accountSchedulePaymentsFoundationServiceDelegate.transformResponseFromFDToAPI(response, params);
	}
}
	
