package com.capgemini.psd2.pisp.file.payments.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.routing.FPaymentsAdapterFactory;
import com.capgemini.psd2.pisp.file.payments.routing.adapter.impl.FilePaymentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentsRoutingAdapterImplTest {

	@InjectMocks
	FilePaymentsRoutingAdapterImpl adapter;

	@Mock
	FPaymentsAdapterFactory factory;

	@Mock
	RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private FilePaymentsAdapter paymentConsentsAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testDownloadFilePaymentsReport() {
		Mockito.when(factory.getFilePaymentsStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.downloadFilePaymentsReport("8de525df-ed9c-4031-a8a9-c0f3887b0de2");
	}

	@Test
	public void testFindFilePaymentsResourceStatus() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Mockito.when(factory.getFilePaymentsStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.findFilePaymentsResourceStatus(stageIdentifiers);
	}

	@Test
	public void testProcessPayments() {

		CustomFilePaymentsPOSTRequest paymentConsentsRequest = new CustomFilePaymentsPOSTRequest();
		Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();
		Map<String, String> params = new HashMap<>();
		Mockito.when(factory.getFilePaymentsStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.processPayments(paymentConsentsRequest, paymentStatusMap, params);
		assertNotNull(adapter);
	}

	@Test
	public void testRetrieveStagedFilePayments() {

		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		Mockito.when(factory.getFilePaymentsStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.retrieveStagedFilePayments(customPaymentStageIdentifiers, params);
		assertNotNull(adapter);
	}

	@Test
	public void testGetFileRoutingAdapter() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Mockito.when(factory.getFilePaymentsStagingInstance(anyString())).thenReturn(null);
		adapter.findFilePaymentsResourceStatus(stageIdentifiers);
	}

	@Test
	public void testParams() {
		Map<String, String> params = null;
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		Token token = new Token();
		Map<String, String> seviceParams = new HashMap<String, String>();
		token.setSeviceParams(seviceParams );
		token.setClient_id("123");
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		FilePaymentsAdapter paymentConsentsAdapter = new FilePaymentsAdapter() {

			@Override
			public PaymentFileSubmitPOST201Response retrieveStagedFilePayments(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}
			
			@Override
			public PaymentFileSubmitPOST201Response processPayments(CustomFilePaymentsPOSTRequest paymentConsentsRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
			
			@Override
			public CustomFilePaymentsPOSTResponse findFilePaymentsResourceStatus(
					CustomPaymentStageIdentifiers stageIdentifiers) {
				return null;
			}
			
			@Override
			public PlatformFilePaymentsFileResponse downloadFilePaymentsReport(String paymentId) {
				return null;
			}
		};
		
		Mockito.when(factory.getFilePaymentsStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.retrieveStagedFilePayments(customPaymentStageIdentifiers, params);
		assertNotNull(adapter);
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		adapter = null;
		reqHeaderAtrributes = null;
	}

}
