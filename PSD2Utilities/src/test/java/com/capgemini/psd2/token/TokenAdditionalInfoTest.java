package com.capgemini.psd2.token;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class TokenAdditionalInfoTest {

	private TokenAdditionalInfo tokenAdditionalInfo;

	@Before
	public void setUp() throws Exception {
		tokenAdditionalInfo = new TokenAdditionalInfo();
		Map<String, List<Object>> claims = new HashMap<>();
		List<Object> claimsList = new ArrayList<>();
	    claimsList.add("ReadInformation");
		claims.put("claims", claimsList );
		tokenAdditionalInfo.setClaims(claims );
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("123456");
		tokenAdditionalInfo.setConsentTokenData(consentTokenData );
		tokenAdditionalInfo.setConsentType("AISP");
		tokenAdditionalInfo.setRequestId("123456");
		Set<String> scope = new HashSet<>();
		scope.add("accounts");
		tokenAdditionalInfo.setScope(scope );
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("brandId", "BOI");
		tokenAdditionalInfo.setSeviceParams(seviceParams );
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("AISP");
		tppInformation.setTppRoles(tppRoles );
		tokenAdditionalInfo.setTppInformation(tppInformation );
	}

	@Test
	public void test(){
		assertEquals("ReadInformation",tokenAdditionalInfo.getClaims().get("claims").get(0));
		assertEquals("123456",tokenAdditionalInfo.getConsentTokenData().getConsentId());
		assertEquals("AISP",tokenAdditionalInfo.getConsentType());
		assertEquals("123456",tokenAdditionalInfo.getRequestId());
		assertTrue(tokenAdditionalInfo.getScope().contains("accounts"));
		assertEquals("BOI",tokenAdditionalInfo.getSeviceParams().get("brandId"));
		assertTrue(tokenAdditionalInfo.getTppInformation().getTppRoles().contains("AISP"));
	}
}
