package com.capgemini.psd2.pisp.domestic.standing.order.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingorderFoundationResource;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderResponse1;

public class DomesticStandingOrderMongoDbAdapterImplTestMockData {
	
	public static CustomDStandingOrderPOSTRequest request;
	
	public static DStandingorderFoundationResource resource;
	
	public static CustomDStandingOrderPOSTRequest getRequest() {
		
		request = new CustomDStandingOrderPOSTRequest();
		
		request.data(new OBWriteDataDomesticStandingOrder1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("546.89");
		request.getData().getInitiation().getFirstPaymentAmount().setCurrency("EUR");
			
		
		return request;
		
	}
	
	public static DStandingorderFoundationResource getResource() {
		
		resource = new DStandingorderFoundationResource();
		 
		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("98.654");
		
		return resource;
		
	}
}
