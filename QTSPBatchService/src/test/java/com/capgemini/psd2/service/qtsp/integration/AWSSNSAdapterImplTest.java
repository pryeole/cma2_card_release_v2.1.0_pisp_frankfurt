package com.capgemini.psd2.service.qtsp.integration;

import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.amazonaws.Protocol;
import com.capgemini.psd2.model.qtsp.FailureInfo;
import com.capgemini.psd2.service.qtsp.config.AWSSNSConfig;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class AWSSNSAdapterImplTest {

	@InjectMocks
	private AWSSNSAdapterImpl awsSnsAdapterImpl;

	@Mock
	private QTSPServiceUtility utility;

	@Mock
	private AWSSNSConfig awsSNSConfig;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		when(awsSNSConfig.getAccessKey()).thenReturn("AKIAJRF5IWHUXG6WZ53Q");
		when(awsSNSConfig.getSecretKey()).thenReturn("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");
		when(awsSNSConfig.getMessage()).thenReturn("abc");
		when(awsSNSConfig.getNsAdd()).thenReturn("nsAddMsg");
		when(awsSNSConfig.getNsRemove()).thenReturn("nsRemoveMsg");
		when(awsSNSConfig.getPfAdd()).thenReturn("pfAddMsg");
		when(awsSNSConfig.getPfRemove()).thenReturn("pfRemoveMsg");
		when(awsSNSConfig.getRegion()).thenReturn("eu-west-1");
		when(awsSNSConfig.getTopicArn()).thenReturn("TopicArn");
	}

	@Test
	public void publishFailuresTest() {
		try {
			Field proxyPort = AWSSNSAdapterImpl.class.getDeclaredField("proxyPort");
			proxyPort.setAccessible(true);
			proxyPort.set(awsSnsAdapterImpl, "3128");

			Field protocol = AWSSNSAdapterImpl.class.getDeclaredField("protocol");
			protocol.setAccessible(true);
			protocol.set(awsSnsAdapterImpl, Protocol.HTTPS);

			Field proxyHost = AWSSNSAdapterImpl.class.getDeclaredField("proxyHost");
			proxyHost.setAccessible(true);
			proxyHost.set(awsSnsAdapterImpl, "proxy.webservices.com");

			FailureInfo failureInfo1 = new FailureInfo(QTSPStub.getCerts().get(0), "asdasd", "fsdfs",
					QTSPServiceUtility.PF_FAILURE_STAGE_ADD);
			FailureInfo failureInfo2 = new FailureInfo(QTSPStub.getCerts().get(0), "fdsfsdf", "sdfs",
					QTSPServiceUtility.PF_FAILURE_STAGE_REMOVE);
			FailureInfo failureInfo3 = new FailureInfo(QTSPStub.getCerts().get(0), "asrfdsf", "sdfdf",
					QTSPServiceUtility.NS_FAILURE_STAGE_ADD);
			FailureInfo failureInfo4 = new FailureInfo(QTSPStub.getCerts().get(0), "fsfsdfs", "grer",
					QTSPServiceUtility.NS_FAILURE_STAGE_REMOVE);
			FailureInfo failureInfo5 = new FailureInfo(QTSPStub.getCerts().get(0), "drgvdsf", "asad", "randomValue");
			List<FailureInfo> failures = new ArrayList<>();
			failures.add(failureInfo1);
			failures.add(failureInfo2);
			failures.add(failureInfo3);
			failures.add(failureInfo4);
			failures.add(failureInfo5);
			when(utility.getFailures()).thenReturn(failures);
			awsSnsAdapterImpl.publishFailures(utility.getFailures());
		} catch (Exception e) {
		}
	}
}
