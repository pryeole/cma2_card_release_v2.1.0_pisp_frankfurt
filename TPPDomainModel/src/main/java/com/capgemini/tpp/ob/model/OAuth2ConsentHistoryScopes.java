package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OAuth2ConsentHistoryScopes
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OAuth2ConsentHistoryScopes   {
  /**
   * The consent decision for the scope.
   */
  public enum ConsentEnum {
    GRANTED("granted"),
    
    DENIED("denied"),
    
    REVOKED("revoked");

    private String value;

    ConsentEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ConsentEnum fromValue(String text) {
      for (ConsentEnum b : ConsentEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("consent")
  private ConsentEnum consent = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("name")
  private String name = null;

  public OAuth2ConsentHistoryScopes consent(ConsentEnum consent) {
    this.consent = consent;
    return this;
  }

   /**
   * The consent decision for the scope.
   * @return consent
  **/
  @ApiModelProperty(value = "The consent decision for the scope.")


  public ConsentEnum getConsent() {
    return consent;
  }

  public void setConsent(ConsentEnum consent) {
    this.consent = consent;
  }

  public OAuth2ConsentHistoryScopes description(String description) {
    this.description = description;
    return this;
  }

   /**
   * The description of the scope.
   * @return description
  **/
  @ApiModelProperty(value = "The description of the scope.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public OAuth2ConsentHistoryScopes name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The name of the scope.
   * @return name
  **/
  @ApiModelProperty(value = "The name of the scope.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OAuth2ConsentHistoryScopes oauth2ConsentHistoryScopes = (OAuth2ConsentHistoryScopes) o;
    return Objects.equals(this.consent, oauth2ConsentHistoryScopes.consent) &&
        Objects.equals(this.description, oauth2ConsentHistoryScopes.description) &&
        Objects.equals(this.name, oauth2ConsentHistoryScopes.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(consent, description, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OAuth2ConsentHistoryScopes {\n");
    
    sb.append("    consent: ").append(toIndentedString(consent)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

