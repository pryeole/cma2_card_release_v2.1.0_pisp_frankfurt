package com.capgemini.psd2.pisp.payments.consent.test.mockdata;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public class DSOCPayloadComparatorTestMockData {

	public static CustomDStandingOrderConsentsPOSTResponse getResponse() {
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("20.0");
		response.getData().getInitiation().setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		response.getData().getInitiation().getRecurringPaymentAmount().setAmount("20.0");
		response.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		response.getData().getInitiation().getFinalPaymentAmount().setAmount("20.0");
		response.setRisk(new OBRisk1());

		return response;
	}
	
	public static CustomDStandingOrderConsentsPOSTResponse getAdaptedStandingOrderConsentsResponse() {
		CustomDStandingOrderConsentsPOSTResponse adaptedStandingOrderConsentsResponse = new CustomDStandingOrderConsentsPOSTResponse();
		adaptedStandingOrderConsentsResponse.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		adaptedStandingOrderConsentsResponse.getData().setAuthorisation(authorisation);
		adaptedStandingOrderConsentsResponse.getData().setInitiation(new OBDomesticStandingOrder1());
		adaptedStandingOrderConsentsResponse.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		adaptedStandingOrderConsentsResponse.getData().getInitiation().getFirstPaymentAmount().setAmount("20.0");
		adaptedStandingOrderConsentsResponse.getData().getInitiation().setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		adaptedStandingOrderConsentsResponse.getData().getInitiation().getRecurringPaymentAmount().setAmount("20.0");
		adaptedStandingOrderConsentsResponse.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		adaptedStandingOrderConsentsResponse.getData().getInitiation().getFinalPaymentAmount().setAmount("20.0");
		OBRisk1 risk = new OBRisk1();
		risk.setMerchantCategoryCode("BOI");
		adaptedStandingOrderConsentsResponse.setRisk(risk);
		
		return adaptedStandingOrderConsentsResponse;
		
	}

	public static CustomDStandingOrderConsentsPOSTResponse getStandingOrderConsentResponse() {
		CustomDStandingOrderConsentsPOSTResponse standingOrderConsentResponse = new CustomDStandingOrderConsentsPOSTResponse();
		standingOrderConsentResponse.data(new OBWriteDataDomesticStandingOrderConsentResponse1());
		standingOrderConsentResponse.getData().setAuthorisation(new OBAuthorisation1());
		standingOrderConsentResponse.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		standingOrderConsentResponse.getData().setInitiation(new OBDomesticStandingOrder1());
		standingOrderConsentResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		standingOrderConsentResponse.setRisk(new OBRisk1());
		
		return standingOrderConsentResponse;
	}

	public static CustomDStandingOrderConsentsPOSTRequest getStandingOrderRequest() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.setRisk(new OBRisk1());
		
		return request;
	}

	public static PaymentConsentsPlatformResource getResource() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("false");
		resource.setTppDebtorNameDetails("BOI");
		
		return resource;
	}
}
