package com.capgemini.psd2.service.qtsp.integration;

import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.qtsp.Items;
import com.capgemini.psd2.model.qtsp.PFError;
import com.capgemini.psd2.model.qtsp.PFResponse;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.qtsp.config.PFConfig;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;

@Lazy
@Component
public class PingFederateAdapterImpl implements PingFederateAdapter {

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	@Qualifier("qtspServiceRestClient")
	private RestClientSync restClientSync;

	@Autowired
	private QTSPServiceUtility utility;

	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	@Value("${api.pfDelayInMillis:0}")
	private String pfDelay;

	private static final Logger LOGGER = LoggerFactory.getLogger(PingFederateAdapterImpl.class);

	@Override
	public void importCAInPF(List<QTSPResource> newQTSPs) {

		Set<String> prefixes = new HashSet<>();
		if (utility.isUploadCertRequest())
			prefixes.add(reqHeaderAttribute.getTenantId());
		else
			prefixes.addAll(pfConfig.getPrefix().keySet());
		for (String key : prefixes) {
			HttpHeaders httpHeaders = populateHttpHeaders(key);
			String prefix = pfConfig.getPrefix().get(key);
			for (QTSPResource qtsp : newQTSPs) {
				try {
					addTrustedCAInPF(qtsp.getX509Certificate(), httpHeaders, prefix);
				} catch (Exception e) {
					LOGGER.error("Exception occured during \"Add Trusted CA in PF\" : {}", e.getMessage());
					try {
						PFError error = JSONUtilities.getObjectFromJSONString(e.getMessage(), PFError.class);
						if (!"cert_import_duplication_error".equals(error.getValidationErrors()[0].getErrorId())) {
							utility.logFailure(qtsp, prefix, pfConfig.getCaImportUrl(), key,
									QTSPServiceUtility.PF_FAILURE_STAGE_ADD);
							LOGGER.error("Failed to add Certificate with Fingerprint \"{}\" in PF, instance : \"{}\" ",
									qtsp.getCertificateFingerprint(), prefix + pfConfig.getCaImportUrl());
						} else
							LOGGER.info(
									"Certificate with Fingerprint \"{}\" is already present in PF, instance : \"{}\" ",
									qtsp.getCertificateFingerprint(), prefix + pfConfig.getCaImportUrl());
					} catch (Exception e1) {
						utility.logFailure(qtsp, prefix, pfConfig.getCaImportUrl(), key,
								QTSPServiceUtility.PF_FAILURE_STAGE_ADD);
						LOGGER.error("Could not parse PingFederate error, Exception : {}", e1.getMessage());
						LOGGER.error("Failed to add Certificate with Fingerprint \"{}\" in PF, instance : \"{}\" ",
								qtsp.getCertificateFingerprint(), prefix + pfConfig.getCaImportUrl());
					}

				}
			}
			replicatePFConfiguration(httpHeaders, prefix);
		}
	}

	private void addTrustedCAInPF(String cert, HttpHeaders httpHeaders, String prefix) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + pfConfig.getCaImportUrl());
		HashMap<String, String> map = new HashMap<>();
		map.put("fileData", QTSPServiceUtility.parseForPF(cert));
		QTSPServiceUtility.addDelay(pfDelay);
		restClientSync.callForPost(requestInfo, map, Map.class, httpHeaders);
	}

	@Override
	public void removeCAFromPF(List<QTSPResource> updatedQTSPs) {

		for (String key : pfConfig.getPrefix().keySet()) {
			HttpHeaders httpHeaders = populateHttpHeaders(key);
			String prefix = pfConfig.getPrefix().get(key);
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(prefix + pfConfig.getCaRemoveUrl());
			Items items = new Items();
			try {
				QTSPServiceUtility.addDelay(pfDelay);
				items = restClientSync.callForGet(requestInfo, Items.class, httpHeaders);
			} catch (Exception e) {
				LOGGER.error("\"Call For Get\" failed, Exception : {}", e.getMessage());
				processErrors(updatedQTSPs, prefix, key);
			}

			if (!items.getItems().isEmpty()) {
				Map<String, PFResponse> certs = new HashMap<>();
				items.getItems().forEach(pfResponse -> certs.put(pfResponse.getSha256Fingerprint(), pfResponse));
				remove(updatedQTSPs, certs, requestInfo, httpHeaders, prefix, key);
				replicatePFConfiguration(httpHeaders, prefix);
			} else {
				processErrors(updatedQTSPs, prefix, key);
			}
		}
	}

	@Override
	public void replicatePFConfiguration(HttpHeaders httpHeaders, String prefix) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + pfConfig.getReplicateUrl());
		try {
			QTSPServiceUtility.addDelay(pfDelay);
			restClientSync.callForPost(requestInfo, null, Map.class, httpHeaders);
		} catch (Exception e) {
			LOGGER.error("Failed to Replicate PF Configuration, Exception : {}", e.getMessage());
		}
	}

	private HttpHeaders populateHttpHeaders(String key) {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application", "json"));
		requestHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());
		String credentials = pfConfig.getAdminUser().get(key).concat(":").concat(pfConfig.getAdminPwd().get(key));
		requestHeaders.add("Authorization", "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes()));

		return requestHeaders;
	}

	private void processErrors(List<QTSPResource> updatedQTSPs, String prefix, String key) {
		for (QTSPResource qtsp : updatedQTSPs) {
			utility.logFailure(qtsp, prefix, pfConfig.getCaRemoveUrl(), key,
					QTSPServiceUtility.PF_FAILURE_STAGE_REMOVE);
			LOGGER.error("Failed to remove Certificate with Fingerprint \"{}\" from PF, instance : \"{}{}\" ",
					qtsp.getCertificateFingerprint(), prefix, pfConfig.getCaRemoveUrl());
		}
	}

	private void remove(List<QTSPResource> updatedQTSPs, Map<String, PFResponse> certs, RequestInfo requestInfo,
			HttpHeaders httpHeaders, String prefix, String key) {
		for (QTSPResource qtsp : updatedQTSPs) {
			PFResponse cert = certs.get(qtsp.getCertificateFingerprint().toUpperCase());
			if (null != cert) {
				requestInfo.setUrl(requestInfo.getUrl() + "/" + cert.getId());
				try {
					QTSPServiceUtility.addDelay(pfDelay);
					restClientSync.callForDelete(requestInfo, Map.class, httpHeaders);
				} catch (Exception e) {
					try {
						LOGGER.error("Exception occured during \"Delete\" : {}", e.getMessage());
						PFError error = JSONUtilities.getObjectFromJSONString(e.getMessage(), PFError.class);
						if (!"resource_not_found".equals(error.getResultId())) {
							utility.logFailure(qtsp, prefix, pfConfig.getCaRemoveUrl(), key,
									QTSPServiceUtility.PF_FAILURE_STAGE_REMOVE);
							LOGGER.error(
									"Failed to remove Certificate with Fingerprint \"{}\" from PF. Instance : \"{}{}\" ",
									qtsp.getCertificateFingerprint(), prefix, pfConfig.getCaRemoveUrl());
						} else {
							LOGGER.info(
									"Certificate with Fingerprint \"{}\" is not present in PF, instance : \"{}{}\" ",
									qtsp.getCertificateFingerprint(), prefix, pfConfig.getCaRemoveUrl());
						}
					} catch (Exception e1) {
						utility.logFailure(qtsp, prefix, pfConfig.getCaRemoveUrl(), key,
								QTSPServiceUtility.PF_FAILURE_STAGE_REMOVE);
						LOGGER.error("Could not parse PingFederate error, Exception : {}", e1.getMessage());
						LOGGER.error(
								"Failed to remove Certificate with Fingerprint \"{}\" from PF. Instance : \"{}{}\" ",
								qtsp.getCertificateFingerprint(), prefix, pfConfig.getCaRemoveUrl());
					}
				}
			} else {
				utility.logFailure(qtsp, prefix, pfConfig.getCaRemoveUrl(), key,
						QTSPServiceUtility.PF_FAILURE_STAGE_REMOVE);
				LOGGER.error("Failed to remove Certificate with Fingerprint \"{}\" from PF, instance: \"{}{}\" ",
						qtsp.getCertificateFingerprint(), prefix, pfConfig.getCaRemoveUrl());
			}
		}
	}

}
