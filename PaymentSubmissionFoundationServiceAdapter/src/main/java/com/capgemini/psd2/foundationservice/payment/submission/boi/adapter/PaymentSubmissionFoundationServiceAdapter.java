package com.capgemini.psd2.foundationservice.payment.submission.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.ValidationViolations;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.client.PaymentSubmissionFoundationServiceClient;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.constants.PaymentSubmissionFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.utility.PaymentSubmissionFoundationServiceUtility;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PaymentSubmissionFoundationServiceAdapter implements PaymentSubmissionExecutionAdapter {

	@Value("${foundationService.executePaymentBaseURL}")
	private String executePaymentBaseURL;

	@Autowired
	private PaymentSubmissionFoundationServiceUtility paymentSubmissionFoundationServiceUtility;

	@Autowired
	private PaymentSubmissionFoundationServiceClient paymentSubmissionFoundationServiceClient;
	
	@Autowired
	private AdapterUtility adapterUtility;
	
	@Override
	public PaymentSubmissionExecutionResponse executePaymentSubmission(CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest, Map<String, String> params) {
		ValidationPassed validationPassed = null;
		ValidationViolations validationViolations = null;
		String submissionID = null;
		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = paymentSubmissionFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSubmissionPOSTRequest, params);
		requestInfo.setUrl(executePaymentBaseURL);
		PaymentInstruction paymentInstruction = paymentSubmissionFoundationServiceUtility. transformRequestFromAPIToFS(paymentSubmissionPOSTRequest, params);

		try {
 			validationPassed = paymentSubmissionFoundationServiceClient.executePaymentSubmission(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
		} catch (AdapterException e) {
			validationViolations = (ValidationViolations) e.getFoundationError();
			if (NullCheckUtils.isNullOrEmpty(validationViolations)) {
				throw e;
			}			
			String errorCode = validationViolations.getValidationViolation().get(0).getErrorCode();
			String errorText = validationViolations.getValidationViolation().get(0).getErrorText();
			String throwableError = adapterUtility.getThrowableError().get(errorCode);
			
			if (!com.capgemini.psd2.utilities.NullCheckUtils.isNullOrEmpty(throwableError)) {
				e.getErrorInfo().setDetailErrorMessage(e.getErrorInfo().getErrorMessage());
				throw e;
			}			
			if (errorText.contains(PaymentSubmissionFoundationServiceConstants.DELIMITER)) {
				String[] splitResult = errorText.split(PaymentSubmissionFoundationServiceConstants.SPLIT_DELIMITER);
				submissionID = splitResult[0];
			}
			if (!NullCheckUtils.isNullOrEmpty(submissionID)) {
				PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
				paymentSubmissionExecutionResponse.setPaymentSubmissionId(submissionID);
				paymentSubmissionExecutionResponse.setPaymentSubmissionStatus(PaymentSubmissionFoundationServiceConstants.SUBMISSION_STATUS_REJECTED);
				return paymentSubmissionExecutionResponse;
			} else {
				throw AdapterException.populatePSD2Exception("Submission ID is not received from FS for Error code: " + errorCode, AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
		} catch (Exception e) {
			throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return paymentSubmissionFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
	}

}
