package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Party Basic Information object
 */
@ApiModel(description = "Party Basic Information object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PartyBasicInformation   {
  @JsonProperty("partySourceIdNumber")
  private String partySourceIdNumber = null;

  @JsonProperty("partyName")
  private String partyName = null;

  @JsonProperty("gcisCustomertype")
  private GcisCustomertype gcisCustomertype = null;

  @JsonProperty("partyActiveIndicator")
  private Boolean partyActiveIndicator = null;

  @JsonProperty("person")
  private PersonBasicInformation person = null;

  @JsonProperty("organisation")
  private Organisation organisation = null;

  public PartyBasicInformation partySourceIdNumber(String partySourceIdNumber) {
    this.partySourceIdNumber = partySourceIdNumber;
    return this;
  }

  /**
   * Get partySourceIdNumber
   * @return partySourceIdNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getPartySourceIdNumber() {
    return partySourceIdNumber;
  }

  public void setPartySourceIdNumber(String partySourceIdNumber) {
    this.partySourceIdNumber = partySourceIdNumber;
  }

  public PartyBasicInformation partyName(String partyName) {
    this.partyName = partyName;
    return this;
  }

  /**
   * This is the plain text representation of the name of the party (e.g. John Brooks, Vauxhall, Dublin University Hospital etc.).
   * @return partyName
  **/
  @ApiModelProperty(required = true, value = "This is the plain text representation of the name of the party (e.g. John Brooks, Vauxhall, Dublin University Hospital etc.).")
  @NotNull


  public String getPartyName() {
    return partyName;
  }

  public void setPartyName(String partyName) {
    this.partyName = partyName;
  }

  public PartyBasicInformation gcisCustomertype(GcisCustomertype gcisCustomertype) {
    this.gcisCustomertype = gcisCustomertype;
    return this;
  }

  /**
   * Get gcisCustomertype
   * @return gcisCustomertype
  **/
  @ApiModelProperty(value = "")

  @Valid

  public GcisCustomertype getGcisCustomertype() {
    return gcisCustomertype;
  }

  public void setGcisCustomertype(GcisCustomertype gcisCustomertype) {
    this.gcisCustomertype = gcisCustomertype;
  }

  public PartyBasicInformation partyActiveIndicator(Boolean partyActiveIndicator) {
    this.partyActiveIndicator = partyActiveIndicator;
    return this;
  }

  /**
   * Get partyActiveIndicator
   * @return partyActiveIndicator
  **/
  @ApiModelProperty(value = "")


  public Boolean isPartyActiveIndicator() {
    return partyActiveIndicator;
  }

  public void setPartyActiveIndicator(Boolean partyActiveIndicator) {
    this.partyActiveIndicator = partyActiveIndicator;
  }

  public PartyBasicInformation person(PersonBasicInformation person) {
    this.person = person;
    return this;
  }

  /**
   * Get person
   * @return person
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PersonBasicInformation getPerson() {
    return person;
  }

  public void setPerson(PersonBasicInformation person) {
    this.person = person;
  }

  public PartyBasicInformation organisation(Organisation organisation) {
    this.organisation = organisation;
    return this;
  }

  /**
   * Get organisation
   * @return organisation
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartyBasicInformation partyBasicInformation = (PartyBasicInformation) o;
    return Objects.equals(this.partySourceIdNumber, partyBasicInformation.partySourceIdNumber) &&
        Objects.equals(this.partyName, partyBasicInformation.partyName) &&
        Objects.equals(this.gcisCustomertype, partyBasicInformation.gcisCustomertype) &&
        Objects.equals(this.partyActiveIndicator, partyBasicInformation.partyActiveIndicator) &&
        Objects.equals(this.person, partyBasicInformation.person) &&
        Objects.equals(this.organisation, partyBasicInformation.organisation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(partySourceIdNumber, partyName, gcisCustomertype, partyActiveIndicator, person, organisation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartyBasicInformation {\n");
    
    sb.append("    partySourceIdNumber: ").append(toIndentedString(partySourceIdNumber)).append("\n");
    sb.append("    partyName: ").append(toIndentedString(partyName)).append("\n");
    sb.append("    gcisCustomertype: ").append(toIndentedString(gcisCustomertype)).append("\n");
    sb.append("    partyActiveIndicator: ").append(toIndentedString(partyActiveIndicator)).append("\n");
    sb.append("    person: ").append(toIndentedString(person)).append("\n");
    sb.append("    organisation: ").append(toIndentedString(organisation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

