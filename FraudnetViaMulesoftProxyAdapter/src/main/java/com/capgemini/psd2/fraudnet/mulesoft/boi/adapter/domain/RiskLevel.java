package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Event risk level.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum RiskLevel {

	LOW("LOW"),

	MEDIUM("MEDIUM"),

	HIGH("HIGH");

	private String value;

	RiskLevel(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static RiskLevel fromValue(String text) {
		for (RiskLevel b : RiskLevel.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
