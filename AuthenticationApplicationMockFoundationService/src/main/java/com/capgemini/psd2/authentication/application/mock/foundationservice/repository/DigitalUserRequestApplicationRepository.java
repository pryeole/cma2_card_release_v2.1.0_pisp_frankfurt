package com.capgemini.psd2.authentication.application.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.DigitalUserRequest;

public interface DigitalUserRequestApplicationRepository extends MongoRepository<DigitalUserRequest , String>{
	
	public DigitalUserRequest findBydigitalId(String digitalId);

}
