package com.capgemini.psd2.pisp.dsp.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("dsPaymentsRoutingAdapter")
public class DSPaymentsRoutingAdapterImpl implements DomesticScheduledPaymentsAdapter, ApplicationContextAware {

	private DomesticScheduledPaymentsAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSubmissionExecutionAdapter}")
	private String paymentSubmissionExecutionAdapter;

	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public GenericPaymentsResponse processDSPayments(CustomDSPaymentsPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		return getRoutingInstance(paymentSubmissionExecutionAdapter).processDSPayments(customRequest, paymentStatusMap,
				getHeaderParams(params));
	}

	@Override
	public CustomDSPaymentsPOSTResponse retrieveStagedDSPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {

		return getRoutingInstance(paymentSubmissionExecutionAdapter)
				.retrieveStagedDSPaymentsResponse(customPaymentStageIdentifiers, getHeaderParams(params));

	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	public DomesticScheduledPaymentsAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (DomesticScheduledPaymentsAdapter) applicationContext.getBean(adapterName);
		return beanInstance;
	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}

}
