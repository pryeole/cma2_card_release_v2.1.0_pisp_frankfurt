package com.capgemini.psd2.pisp.test.status;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.status.PaymentStatusEnum;


@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentStatusEnumTest {
	
	@Test
	public void testEnumSetterPositive(){
		PaymentStatusEnum.ACCEPTEDCUSTOMERPROFILE.getStatusId();
		assertEquals("AcceptedCustomerProfile", PaymentStatusEnum.ACCEPTEDCUSTOMERPROFILE.getStatusCode());	
		
	}
	
	@Test
	public void testFindStatusFromEnumValidValue(){
		PaymentStatusEnum.findStatusFromEnum("4");
	}
	
	@Test
	public void testFindStatusFromEnumInvalidValue(){
		assertEquals(null, PaymentStatusEnum.findStatusFromEnum("ABC"));
	}
	
	@Test
	public void testIsActiveEnumValidValue(){
		PaymentStatusEnum.isActive("4");
	
	}
	
	@Test
	public void testIsActiveEnumInvalidValue(){
		assertEquals(false, PaymentStatusEnum.isActive("ABC"));
	
	}

}
