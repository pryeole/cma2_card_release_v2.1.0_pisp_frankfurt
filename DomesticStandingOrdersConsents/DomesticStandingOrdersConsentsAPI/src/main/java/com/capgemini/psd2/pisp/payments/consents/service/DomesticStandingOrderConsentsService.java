package com.capgemini.psd2.pisp.payments.consents.service;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

public interface DomesticStandingOrderConsentsService {

	public CustomDStandingOrderConsentsPOSTResponse createDomesticStandingOrderConsentResource(CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest);
		
	public CustomDStandingOrderConsentsPOSTResponse retrieveDomesticStandingOrderConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest);
	
}
