package com.capgemini.psd2.pisp.payments.consent.test.mockdata;

import com.capgemini.psd2.pisp.domain.OBWriteDomesticStandingOrderConsent1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DStandingOrderConsentMockData {

	public static OBWriteDomesticStandingOrderConsent1 getPaymentConsentsPOSTRequest() {
		return new OBWriteDomesticStandingOrderConsent1();
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
