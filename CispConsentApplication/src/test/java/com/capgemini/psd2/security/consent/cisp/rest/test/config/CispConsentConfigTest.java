package com.capgemini.psd2.security.consent.cisp.rest.test.config;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.security.consent.cisp.config.CispConsentConfig;

public class CispConsentConfigTest {
	
	@InjectMocks
	private CispConsentConfig config;


	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test 
	public void fundsConfirmationConsentAdapterTest(){
		config.fundsConfirmationConsentAdapter();
		
	}
	
	@Test 
	public void buildCispCookieHandlerFilterTest(){
		config.buildCispCookieHandlerFilter();
		
	}
	
	@Test 
	public void buildCispBlockActionFilterTest(){
		config.buildCispBlockActionFilter();
		
	}
	
	@Test 
	public void cispConsentAuthorizationHelperTest(){
		config.cispConsentAuthorizationHelper();
		
	}
	
	@Test 
	public void blockCISPRefreshEventFilterTest(){
		config.blockCISPRefreshEventFilter();
		
	}
	
	@Test 
	public void cispFilterTest(){
		config.cispFilter();
		
	}
}
