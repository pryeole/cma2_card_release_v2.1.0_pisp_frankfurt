package com.capgemini.psd2.integration.service.impl.test;

import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.capgemini.psd2.integration.config.TokenTransformationConfigBean;

public class TokenTransformConfigBeanTest {
	
	@Test
	public void testBean(){
		TokenTransformationConfigBean bean = new TokenTransformationConfigBean();
		bean.setServiceparams(null);
		assertNull(bean.getServiceparams());
	}

}
