package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountInformationValidator")
@ConfigurationProperties("app")
public class AccountInformationValidatorImpl implements AISPCustomValidator<OBReadAccount2, OBReadAccount2> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadAccount2 validateRequestParams(OBReadAccount2 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadAccount2 obReadAccount2) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(obReadAccount2);
		executeAccountResponseCustomValidations(obReadAccount2);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

	private void executeAccountResponseSwaggerValidations(OBReadAccount2 obReadAccount2) {
		psd2Validator.validate(obReadAccount2);
		
	}
	
	private void executeAccountResponseCustomValidations(OBReadAccount2 obReadAccount2) {

		if (obReadAccount2 != null && obReadAccount2.getData() != null
				&& obReadAccount2.getData().getAccount() != null) {

			for (OBAccount2 obAccount2 : obReadAccount2.getData().getAccount()) {
				if(!NullCheckUtils.isNullOrEmpty(obAccount2)) {		
					commonAccountValidations.isValidCurrency(obAccount2.getCurrency());
					if (!NullCheckUtils.isNullOrEmpty(obAccount2.getAccount())) {
						for (OBCashAccount3 obCashAccount3 : obAccount2.getAccount()) {
							commonAccountValidations.validateSchemeNameWithIdentification(obCashAccount3.getSchemeName(),
									obCashAccount3.getIdentification());
						
							if (!NullCheckUtils.isNullOrEmpty(obCashAccount3.getSecondaryIdentification()) && (!NullCheckUtils.isNullOrEmpty(obCashAccount3.getSchemeName()))) {
								commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
									obCashAccount3.getSchemeName(), obCashAccount3.getSecondaryIdentification());
							}
						}
					}
				}
			}

		}
	}
	
}