package com.capgemini.psd2.payment.validation.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.validation.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.validation.mock.foundationservice.service.PaymentValidationService;


@RestController
@RequestMapping("/fs-payment-web-service/services/validatePayment")
public class PaymentValidationController {
	
	@Autowired
	private PaymentValidationService paymentValidationService; 
	
	@RequestMapping(method = RequestMethod.POST, consumes= {MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity<ValidationPassed> validatePaymentInstruction(
			@RequestBody PaymentInstruction paymentInstruction,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "client_secret") String clientSecret)
		
	{
		
		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {

			  throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_PMV);
			
		}
		
		if(NullCheckUtils.isNullOrEmpty(paymentInstruction.getPayment()) || NullCheckUtils.isNullOrEmpty(paymentInstruction.getEndToEndIdentification())){
			
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PMV);
		}
		
		ValidationPassed validationPassed =paymentValidationService.validatePaymentInstruction(paymentInstruction);
		
		return new ResponseEntity(validationPassed, HttpStatus.OK);
		
	}
	

}
