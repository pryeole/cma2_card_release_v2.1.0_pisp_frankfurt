package com.capgemini.psd2.pisp.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OBWriteDomesticConsentResponse1
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-05T13:53:27.174+05:30")

public class OBWriteDomesticConsentResponse1   {
  @JsonProperty("Data")
  private OBWriteDataDomesticConsentResponse1 data = null;

  @JsonProperty("Risk")
  private OBRisk1 risk = null;
  
  //Added manually
  
  @JsonProperty("Links")
  private PaymentSetupPOSTResponseLinks links = null;

  //Added manually
  @JsonProperty("Meta")
  private PaymentSetupPOSTResponseMeta meta = null;

  public OBWriteDomesticConsentResponse1 data(OBWriteDataDomesticConsentResponse1 data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   * @return data
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OBWriteDataDomesticConsentResponse1 getData() {
    return data;
  }

  public void setData(OBWriteDataDomesticConsentResponse1 data) {
    this.data = data;
  }

  public OBWriteDomesticConsentResponse1 risk(OBRisk1 risk) {
    this.risk = risk;
    return this;
  }

  /**
   * Get risk
   * @return risk
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OBRisk1 getRisk() {
    return risk;
  }

  public void setRisk(OBRisk1 risk) {
    this.risk = risk;
  }

  public OBWriteDomesticConsentResponse1 links(PaymentSetupPOSTResponseLinks links) {
	    this.links = links;
	    return this;
	  }

	   /**
	   * Get links
	   * @return links
	  **/
	  @ApiModelProperty(value = "")

	  @Valid

	  public PaymentSetupPOSTResponseLinks getLinks() {
	    return links;
	  }

	  public void setLinks(PaymentSetupPOSTResponseLinks links) {
	    this.links = links;
	  }

	  public OBWriteDomesticConsentResponse1 meta(PaymentSetupPOSTResponseMeta meta) {
	    this.meta = meta;
	    return this;
	  }

	   /**
	   * Get meta
	   * @return meta
	  **/
	  @ApiModelProperty(value = "")

	  @Valid

	  public PaymentSetupPOSTResponseMeta getMeta() {
	    return meta;
	  }

	  public void setMeta(PaymentSetupPOSTResponseMeta meta) {
	    this.meta = meta;
	  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBWriteDomesticConsentResponse1 obWriteDomesticConsentResponse1 = (OBWriteDomesticConsentResponse1) o;
    return Objects.equals(this.data, obWriteDomesticConsentResponse1.data) &&
        Objects.equals(this.risk, obWriteDomesticConsentResponse1.risk) &&
        Objects.equals(this.links, obWriteDomesticConsentResponse1.links) &&
        Objects.equals(this.meta, obWriteDomesticConsentResponse1.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, risk);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBWriteDomesticConsentResponse1 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    risk: ").append(toIndentedString(risk)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

