package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * GCIS customer classification separating Industrial and Consumer
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum GcisCustomertype {

	INDIVIDUAL("Individual"),

	COMMERCIAL("Commercial");

	private String value;

	GcisCustomertype(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static GcisCustomertype fromValue(String text) {
		for (GcisCustomertype b : GcisCustomertype.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
