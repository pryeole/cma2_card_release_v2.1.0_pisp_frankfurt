package com.capgemini.psd2.pisp.international.payments.test.mongo.db.adapter.impl.mockdata;

import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.IPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalResponse1;

public class IPaymentsMongoDbAdapterMockData {

	public static CustomIPaymentsPOSTRequest request;

	public static IPaymentsFoundationResource resource;

	public static CustomIPaymentsPOSTRequest getRequest() {

		request = new CustomIPaymentsPOSTRequest();
		request.setData(new OBWriteDataInternational1());
		request.getData().setInitiation(new OBInternational1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("123.45");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setChargeBearer(OBChargeBearerType1Code.FOLLOWINGSERVICELEVEL);
		request.getData().getInitiation().setExchangeRateInformation(new OBExchangeRate1());
		request.getData().getInitiation().getExchangeRateInformation().setContractIdentification("Identified");

		return request;
	}

	public static IPaymentsFoundationResource getResource() {
		resource = new IPaymentsFoundationResource();
		resource.setData(new OBWriteDataInternationalResponse1());
		resource.getData().setInitiation(new OBInternational1());
		resource.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		resource.getData().getInitiation().getInstructedAmount().setAmount("756.56");
		
		return resource;
	}

}
