/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.pisp.consent.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.pisp.consent.adapter.test.mock.data.PispConsentAdapterMockData;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.mongodb.WriteResult;


/**
 * The Class PispConsentAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PispConsentAdapterImplTest {

	//** The consent mapping repository. *//*
	@Mock
	private PispConsentMongoRepository pispConsentMongoRepository;
	
	@Mock
	private MongoTemplate mongoTemplate;

	//** The req header attributes. *//*
	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;

	//** The consent mapping adapter impl. *//*
	@InjectMocks
	PispConsentAdapterImpl pispConsentAdapterImpl;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}


	@Test 
	public void testCreateConsent(){
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(PispConsentAdapterMockData.getConsentMockData());
		PispConsent PispConsent=PispConsentAdapterMockData.getConsentMockData();
		pispConsentAdapterImpl.createConsent(PispConsent);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testConsentAlreadyExist(){
		when(pispConsentMongoRepository.findByPaymentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(PispConsentAdapterMockData.getConsentMockData());
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(PSD2Exception.class);
		PispConsent PispConsent=PispConsentAdapterMockData.getConsentMockData();
		pispConsentAdapterImpl.createConsent(PispConsent);
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails(){
		when(pispConsentMongoRepository.findByPaymentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(null);
		PispConsent PispConsent=PispConsentAdapterMockData.getConsentMockDataWithOutAccountDetails();
		pispConsentAdapterImpl.createConsent(PispConsent);
	}
	
	@Test
	public void testRetrieveConsentByPaymentRequestIdSuccessFlow(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByPaymentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(consent);
		assertEquals(consent.getPsuId(),pispConsentAdapterImpl.retrieveConsentByPaymentId("1234",ConsentStatusEnum.AWAITINGAUTHORISATION).getPsuId());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRetrieveConsentByPaymentRequestIdDataAccessResourceFailureException(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(pispConsentMongoRepository.findByPaymentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenThrow(DataAccessResourceFailureException.class);
		pispConsentAdapterImpl.retrieveConsentByPaymentId("1234",ConsentStatusEnum.AWAITINGAUTHORISATION);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateConsentStatusPSD2Exception(){
		Query query = new Query();
		Update update = new Update();
		when(mongoTemplate.updateFirst(query, update, PispConsent.class)).thenThrow(PSD2Exception.class);
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(PSD2Exception.class);
		pispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateConsentStatusDataResourceAccessExeption(){
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		Query query = new Query();
		Update update = new Update();
		when(mongoTemplate.updateFirst(query, update, PispConsent.class)).thenThrow(DataAccessResourceFailureException.class);
		when(pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		pispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

	@Test
	public void testUpdateConsentStatus(){
		Query query = new Query();
		Update update = new Update();
		boolean updateOfExisting = true;
		Object upsertedId = "";
		int n = 1;
		WriteResult value = new WriteResult(n, updateOfExisting, upsertedId);
		when(mongoTemplate.updateFirst(query, update, AispConsent.class)).thenReturn(value);
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		when(pispConsentMongoRepository.save(any(PispConsent.class))).thenReturn(consent);
		pispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}
	@Test(expected=PSD2Exception.class)
	@SuppressWarnings("unchecked")
	public void testRetrieveConsentByConsentIdStatus(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(null);
		pispConsentAdapterImpl.retrieveConsent("123455");
	}
	@Test(expected=PSD2Exception.class)
	@SuppressWarnings("unchecked")
	public void testRetrieveConsentByConsentIdException(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenThrow(PSD2Exception.class);
		pispConsentAdapterImpl.retrieveConsent("123455");
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testRetrieveConsentByConsentIdReturningConcent(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
	    PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		when(pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		pispConsentAdapterImpl.retrieveConsent("123455");
	}
	
	@Test
	public void testRetrieveConsentByPsuId() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		List<PispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(pispConsentMongoRepository.findByPsuIdAndCmaVersionIn(anyString(),any())).thenReturn(consentList);
		pispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", null);
	}
	
	@Test
	public void testRetrieveConsentByPsuIdAndStatus() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		List<PispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(pispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(consentList);
		pispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", ConsentStatusEnum.AUTHORISED);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentByPsuIdAndStatusException() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		PispConsent consent = PispConsentAdapterMockData.getConsentMockData();
		List<PispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(pispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenThrow(new DataAccessResourceFailureException("No Consents Found"));
		pispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", ConsentStatusEnum.AUTHORISED);
	}
	
	@Test
	public void updateConsentStatusWithResponseTest(){
		pispConsentAdapterImpl.updateConsentStatusWithResponse("1234", ConsentStatusEnum.AUTHORISED);
	}
	
	@Test
	public void retrieveConsentByPsuIdConsentStatusAndTenantIdTest(){
		pispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("32118465", ConsentStatusEnum.AUTHORISED, "BOIUK");
	}
	
	@Test
	public void retrieveConsentByPsuIdConsentStatusAndTenantId1Test(){
		pispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("32118465", null, "BOIUK");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveConsentByPaymentIdExceptionTest(){
		doThrow(new DataAccessResourceFailureException("")).when(pispConsentMongoRepository).findByPaymentIdAndCmaVersionIn(any(), any());
		pispConsentAdapterImpl.retrieveConsentByPaymentId("12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveConsentExceptionTest(){
		doThrow(new DataAccessResourceFailureException("")).when(pispConsentMongoRepository).findByConsentIdAndCmaVersionIn(any(), any());
		pispConsentAdapterImpl.retrieveConsent("12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveConsentByPsuIdConsentStatusAndTenantIdExceptionTest(){
		doThrow(new DataAccessResourceFailureException("")).when(pispConsentMongoRepository).findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(any(), any(), any(), any());
		pispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("12345",ConsentStatusEnum.AUTHORISED, "BOIUK");
	}
}