
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "activeOrganizationId"
})
public class CsAuth implements Serializable
{

    @JsonProperty("activeOrganizationId")
    private String activeOrganizationId;
    private static final long serialVersionUID = 1520921979228015200L;

    @JsonProperty("activeOrganizationId")
    public String getActiveOrganizationId() {
        return activeOrganizationId;
    }

    @JsonProperty("activeOrganizationId")
    public void setActiveOrganizationId(String activeOrganizationId) {
        this.activeOrganizationId = activeOrganizationId;
    }

}
