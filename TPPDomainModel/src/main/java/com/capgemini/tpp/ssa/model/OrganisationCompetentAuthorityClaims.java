package com.capgemini.tpp.ssa.model;

import java.util.ArrayList;

public class OrganisationCompetentAuthorityClaims {
	
	private ArrayList<Authorisation> authorisations;

	public ArrayList<Authorisation> getAuthorisations() {
		return this.authorisations;
	}

	public void setAuthorisations(ArrayList<Authorisation> authorisations) {
		this.authorisations = authorisations;
	}

	private String authority_id;

	public String getAuthority_id() {
		return this.authority_id;
	}

	public void setAuthority_id(String authority_id) {
		this.authority_id = authority_id;
	}

	private String registration_id;

	public String getRegistration_id() {
		return this.registration_id;
	}

	public void setRegistration_id(String registration_id) {
		this.registration_id = registration_id;
	}

	private String status;

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganisationCompetentAuthorityClaims other = (OrganisationCompetentAuthorityClaims) obj;
		if (authorisations == null) {
			if (other.authorisations != null)
				return false;
		} else if (!authorisations.equals(other.authorisations))
			return false;
		if (authority_id == null) {
			if (other.authority_id != null)
				return false;
		} else if (!authority_id.equals(other.authority_id))
			return false;
		if (registration_id == null) {
			if (other.registration_id != null)
				return false;
		} else if (!registration_id.equals(other.registration_id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
	
}
