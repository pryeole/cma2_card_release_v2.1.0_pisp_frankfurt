package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Standing Order Schedule Instruction object
 */
@ApiModel(description = "Standing Order Schedule Instruction object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-08T13:41:31.197+05:30")

public class StandingOrderScheduleInstruction   {
  @JsonProperty("paymentInstruction")
  private PaymentInstructionBasic paymentInstruction = null;

  @JsonProperty("standingOrderAmount")
  private Double standingOrderAmount = null;

  @JsonProperty("transactionCurrency")
  private Currency transactionCurrency = null;

  @JsonProperty("standingOrderFrequency")
  private StandingOrderFrequency standingOrderFrequency = null;

  @JsonProperty("schedule")
  private Schedule schedule = null;

  @JsonProperty("paymentInstructionToCounterPartyBasic")
  @Valid
  private List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic = new ArrayList<PaymentInstructionCounterpartyAccountBasic>();

  public StandingOrderScheduleInstruction paymentInstruction(PaymentInstructionBasic paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
    return this;
  }

  /**
   * Get paymentInstruction
   * @return paymentInstruction
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentInstructionBasic getPaymentInstruction() {
    return paymentInstruction;
  }

  public void setPaymentInstruction(PaymentInstructionBasic paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
  }

  public StandingOrderScheduleInstruction standingOrderAmount(Double standingOrderAmount) {
    this.standingOrderAmount = standingOrderAmount;
    return this;
  }

  /**
   * Get standingOrderAmount
   * @return standingOrderAmount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Double getStandingOrderAmount() {
    return standingOrderAmount;
  }

  public void setStandingOrderAmount(Double standingOrderAmount) {
    this.standingOrderAmount = standingOrderAmount;
  }

  public StandingOrderScheduleInstruction transactionCurrency(Currency transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * Get transactionCurrency
   * @return transactionCurrency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Currency getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(Currency transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }

  public StandingOrderScheduleInstruction standingOrderFrequency(StandingOrderFrequency standingOrderFrequency) {
    this.standingOrderFrequency = standingOrderFrequency;
    return this;
  }

  /**
   * Get standingOrderFrequency
   * @return standingOrderFrequency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public StandingOrderFrequency getStandingOrderFrequency() {
    return standingOrderFrequency;
  }

  public void setStandingOrderFrequency(StandingOrderFrequency standingOrderFrequency) {
    this.standingOrderFrequency = standingOrderFrequency;
  }

  public StandingOrderScheduleInstruction schedule(Schedule schedule) {
    this.schedule = schedule;
    return this;
  }

  /**
   * Get schedule
   * @return schedule
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Schedule getSchedule() {
    return schedule;
  }

  public void setSchedule(Schedule schedule) {
    this.schedule = schedule;
  }

  public StandingOrderScheduleInstruction paymentInstructionToCounterPartyBasic(List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic) {
    this.paymentInstructionToCounterPartyBasic = paymentInstructionToCounterPartyBasic;
    return this;
  }

  public StandingOrderScheduleInstruction addPaymentInstructionToCounterPartyBasicItem(PaymentInstructionCounterpartyAccountBasic paymentInstructionToCounterPartyBasicItem) {
    this.paymentInstructionToCounterPartyBasic.add(paymentInstructionToCounterPartyBasicItem);
    return this;
  }

  /**
   * Get paymentInstructionToCounterPartyBasic
   * @return paymentInstructionToCounterPartyBasic
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<PaymentInstructionCounterpartyAccountBasic> getPaymentInstructionToCounterPartyBasic() {
    return paymentInstructionToCounterPartyBasic;
  }

  public void setPaymentInstructionToCounterPartyBasic(List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic) {
    this.paymentInstructionToCounterPartyBasic = paymentInstructionToCounterPartyBasic;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StandingOrderScheduleInstruction standingOrderScheduleInstruction = (StandingOrderScheduleInstruction) o;
    return Objects.equals(this.paymentInstruction, standingOrderScheduleInstruction.paymentInstruction) &&
        Objects.equals(this.standingOrderAmount, standingOrderScheduleInstruction.standingOrderAmount) &&
        Objects.equals(this.transactionCurrency, standingOrderScheduleInstruction.transactionCurrency) &&
        Objects.equals(this.standingOrderFrequency, standingOrderScheduleInstruction.standingOrderFrequency) &&
        Objects.equals(this.schedule, standingOrderScheduleInstruction.schedule) &&
        Objects.equals(this.paymentInstructionToCounterPartyBasic, standingOrderScheduleInstruction.paymentInstructionToCounterPartyBasic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstruction, standingOrderAmount, transactionCurrency, standingOrderFrequency, schedule, paymentInstructionToCounterPartyBasic);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StandingOrderScheduleInstruction {\n");
    
    sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
    sb.append("    standingOrderAmount: ").append(toIndentedString(standingOrderAmount)).append("\n");
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("    standingOrderFrequency: ").append(toIndentedString(standingOrderFrequency)).append("\n");
    sb.append("    schedule: ").append(toIndentedString(schedule)).append("\n");
    sb.append("    paymentInstructionToCounterPartyBasic: ").append(toIndentedString(paymentInstructionToCounterPartyBasic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

