package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.constants;

public class PispScaConsentFoundationServiceConstants {
	
	public static final String CHANNEL_CODE = "X-API-CHANNEL-CODE";
	public static final String CORRELATION_REQ_HEADER = "X-CORRELATION-ID";
	public static final String USER_ID = "X-BOI-USER";
	public static final String PARTY_SOURCE_ID = "x-api-party-source-id-number";
	public static final String TRANSACTION_ID = "x-api-transaction-id";
	public static final String DOMESTIC_ST_ORD = "DomesticStandingOrdersPayments";
	public static final String DOMESTIC_PROCESS_CODE = "PPA_DPIPVP_019";
	public static final String SCHEDULED_PROCESS_CODE = "PPA_SPIPVP_019";
	public static final String STANDING_PROCESS_CODE = "PPA_SOPIPVP_019";
	public static final String VALIDATION_VIOLATIONS = "validationViolations";
	public static final String CORRELATION_ID = "correlationMuleReqHeader";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR1 = "401";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR2 = "429";
	public static final String APPLICATION = "application";
	public static final String DATE_FORMAT ="yyyy-MM-dd'T'HH:mm:ssXXX";
	public static final String VERSION_V1 ="v1.0";
	public static final String PARTY_NAME = "BOL Customer Id";
	public static final String UK_OBIE_SORTCODEACCOUNTNUMBER = "UK.OBIE.SortCodeAccountNumber";
	public static final String UK_OBIE_IBAN = "UK.OBIE.IBAN";
	public static final String SORTCODEACCOUNTNUMBER = "SortCodeAccountNumber";
	public static final String IBAN = "IBAN";
}
