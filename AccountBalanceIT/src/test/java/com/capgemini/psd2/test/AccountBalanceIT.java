package com.capgemini.psd2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.domain.Balance;
import com.capgemini.psd2.aisp.domain.Balance.CreditDebitIndicatorEnum;
import com.capgemini.psd2.aisp.domain.Balance.TypeEnum;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Data5Amount;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.config.AccountBalanceITConfig;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AccountBalanceITConfig.class)
@WebIntegrationTest
public class AccountBalanceIT {

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	@Value("${app.accountBalanceURL}")
	private String accountBalanceURL;

	@Value("${app.firstAccountId}")
	private String firstAccountId;
	@Value("${app.secondAccountId}")
	private String secondAccountId;
	@Value("${app.thirdAccountId}")
	private String thirdAccountId;
	@Value("${app.fourthAccountId}")
	private String fourthAccountId;
	@Value("${app.fifthAccountId}")
	private String fifthAccountId;

	@Value("${app.firstRefToken}")
	private String firstRefToken;
	@Value("${app.secondRefToken}")
	private String secondRefToken;
	@Value("${app.invalidRefToken}")
	private String invalidRefToken;
	@Value("${app.expiredToken}")
	private String expiredToken;

	@Value("${app.mHeaderValue}")
	private String mHeaderValue;

	@Autowired
	protected RestClientSyncImpl restClientSync;
	@Autowired
	protected RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Conditions  covered
	 * 1. AISP is able to send a Account Balance Request through GET method.
	 * 3. Account Balance Request always contains valid AccountId
	 * 6. AccountId cannot be Null or blank.
	 * 9. AccountId length should be upto 40 characters.
	 * 11. Valid Authorization header is sent in the request
	 * 20.A GET accountBalance response came with all required mandatory fields named as Data, Links and Meta Data
	 * 26. Data cannot be null or blank.
	 * 27. Data field came with Balance array that always contains AccountId, Amount, CreditDebitIndicator, DateTime and Type fields and maybe CreditLine field.
	 * 28. AccountId cannot be Null or blank.
	 * 30. AccountId only contains the same values as sent in the request.
	 * 32. AccountId length should be upto 40 characters.
	 * 34. Amount is an array containing Amount and Currency as mandatory fields.
	 * 35. Amount cannot be Null or blank.
	 * 39. Amount can be start with -ve sign.
	 * 45 Currency cannot be Null or blank.
	 * 53. CreditDebtIndicator cannot be Null or blank.
	 * 54. CreditDebtIndicator only allows Values as "Credit" or "Debit"
	 * 55. If amount is zero or greater than zero then CreditDebtIndicator field should be "Credit"
	 * 56. If amount is less than zero then CreditDebtIndicator field should be "Debit"
	 * 59. Type cannot be Null or blank.
	 * 60. Type only allows specific values as ClosingAvailable, ClosingBooked, Expected, ForwardAvailable, Information, InterimAvailable, InterimBooked, OpeningAvailable, OpeningBooked and PreviouslyClosedBooked
	 * 61.Links cannot be null or blank.
	 * 62. Links is an arrays that may be containing first, last, next and prev fields where self field is mandatory.
	 * 63. Self field cannot be null or blank.
	 * 65. Meta cannot be null or blank
	 * 66. Meta only contain total-pages field that can be optional.
	 * 67. If Total-pages populate then it only contains integer values upto 32 characters.
	 */
	@Test
	public void testRetrieveAccountBalance(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		headers.add("Accept", "application/json");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		BalancesGETResponse balanceResponse = restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		assertNotNull(balanceResponse);

		Links links = balanceResponse.getLinks();
		assertNotNull(links);
		assertNotNull(links.getSelf());
		assertNull(links.getFirst());
		assertNull(links.getLast());
		assertNull(links.getNext());
		assertNull(links.getPrev());

		assertNotNull(balanceResponse.getMeta());
		assertNotNull(balanceResponse.getMeta().getTotalPages());
		Integer totalPages = balanceResponse.getMeta().getTotalPages();
		if(String.valueOf(totalPages).length() <= 32 )
			assertTrue(String.valueOf(totalPages).length() <= 32);
		else
			assertFalse(String.valueOf(totalPages).length() <= 32);

		List<Balance> balances = balanceResponse.getData().getBalance();
		assertTrue(balances.size() > 0);

		for (Balance balancesGETResponseData : balances) {

			assertNotNull(balancesGETResponseData.getAccountId());
			assertEquals(firstAccountId, balancesGETResponseData.getAccountId());
			assertTrue(balancesGETResponseData.getAccountId().length() <= 40);
			//Null Value of CreditDebitIndicatorEnum
			CreditDebitIndicatorEnum creditDebitIndicator = balancesGETResponseData.getCreditDebitIndicator();
			assertNotNull(creditDebitIndicator);
			for(CreditDebitIndicatorEnum creditDebitIndicatorType:CreditDebitIndicatorEnum.values())
			{
				if(creditDebitIndicatorType.equals(creditDebitIndicator)){
					assertEquals(creditDebitIndicatorType, creditDebitIndicator);
				}
			}

			TypeEnum typeEnum=balancesGETResponseData.getType();
			assertNotNull(typeEnum);
			for(TypeEnum enumValue:TypeEnum.values())
			{
				if(enumValue.equals(typeEnum)){
					assertEquals(enumValue, typeEnum);
				}
			}

			Data5Amount amount = balancesGETResponseData.getAmount();
			assertNotNull(amount);
			assertNotNull(amount.getAmount());
			Double amt = Double.parseDouble(amount.getAmount());
			if(BigDecimal.valueOf(amt).compareTo(BigDecimal.ZERO) == 0 || BigDecimal.valueOf(amt).compareTo(BigDecimal.ZERO) > 0 )
				assertEquals(CreditDebitIndicatorEnum.CREDIT, creditDebitIndicator);
			if(BigDecimal.valueOf(amt).compareTo(BigDecimal.ZERO) < 0)
				assertEquals(CreditDebitIndicatorEnum.DEBIT, creditDebitIndicator);
			assertNotNull(amount.getCurrency());
		}

	}

	/**
	 * 14. Permission is not granted into Authorization code to view the balance
	 */
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountBalanceDifferentPermission(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+secondAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", secondRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception ce){
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), ce.getErrorInfo().getStatusCode());
			throw ce;
		}
	}

	/**
	 *  AccountId is not in token
	 */
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountBalanceAccountIdNotInToken(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+secondAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception ce){
			assertEquals("For the given account,user is not authorized to access the requested resource.",ce.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), ce.getErrorInfo().getStatusCode());
			throw ce;
		}
	}
	/**
	 * 46. Currency is Null or blank.
	 */
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountBalanceNullCurrencyInData(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+thirdAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception e){
			assertNotNull(e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}
	/**
	 * 5. Account Balance Request contains invalid AccountId
	 * 10. AccountId length is greater than 40 characters.
	 */
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountBalanceAccountIdInvalid(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+fourthAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception e){
			assertEquals("Validation error found with the provided input",e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.BAD_REQUEST.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}
	/**
	 *12. Invalid Authorization code is sent in the request
	 */

	@Test(expected = PSD2Exception.class)
	public void testInvalidToken(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", invalidRefToken);
		headers.add("x-fapi-financial-id",mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertNotNull(e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}


	/**
	 * 15 .Authorization header is null or blank and sent in the request
	 */
	@Test(expected = HttpClientErrorException.class)
	public void testNoAuthorizationToken() {
		String url =  edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", mHeaderValue);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restTemplate.exchange(url, HttpMethod.GET, requestEntity,BalancesGETResponse.class );
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED,e.getStatusCode());
			throw e;
		}
	}

	/**
	 * 7. AccountId is Null or blank.
	 */

	@Test(expected = HttpClientErrorException.class)
	public void testNullAccountId() {
		String acctId=null;
		String url =  edgeserverURL+accountBalanceURL+"/accounts/"+acctId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try {
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, BalancesGETResponse.class);
		} catch (HttpClientErrorException e) {
			assertTrue(e.getResponseBodyAsString().contains("No permissions given for requested account"));
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			throw e;
		}
	}
	/**
	 * 17.Value of x-fapifinancial-id is missing in the request header.
	 */

	@Test(expected = PSD2Exception.class)
	public void testMainHeaderNotPassed() {
		String acctId=null;
		String url =  edgeserverURL+accountBalanceURL+"/accounts/"+acctId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("Mandatory header missing.",e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.BAD_REQUEST.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}
	/**
	 * 2. AISP send a request other than GET method to collect Account Balance.(via POST method)
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountBalancePost(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForPost(requestInfo,null, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception e){
			assertNotNull(e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}
	/**
	 * 4. Account Balance Request contains valid AccountId but not exists into database.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountBalanceNoDataInDatabase(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+fifthAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception e){
			assertNotNull(e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.NOT_FOUND.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}
	/**
	 * 51. Currency value exists either of EUR or GBP.
	 */

	@Test
	public void testCurrency(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		headers.add("Accept", "application/json");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		BalancesGETResponse balanceResponse = restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		assertNotNull(balanceResponse);
		List<Balance> balances = balanceResponse.getData().getBalance();
		for (Balance balancesGETResponseData : balances) {
			Data5Amount amount = balancesGETResponseData.getAmount();
			assertNotNull(amount);
			assertNotNull(amount.getCurrency());
			if("GBP".equals(amount.getCurrency()))
				assertEquals("GBP", amount.getCurrency());
			else if("EUR".equals(amount.getCurrency()))
				assertEquals("EUR", amount.getCurrency());
			else {
				assertNotEquals("GBP", amount.getCurrency());
				assertNotEquals("EUR", amount.getCurrency());
			}
		}
	}

	/**
	 * 16. Submitted request can contain optional headers named as x-fapi-customer-ip-address, x-fapi-customer-last-logged-time and x-fapi-interaction-id where x-fapi-financial-id is mandatory.
	 * 18. An optional header Accept sent with the value as application/json.
	 * 21.In successful response, x-fapi-interaction-id must come into header.
	 * 24.If x-fapi-interaction-id is absent in the request header then it is always sent in the response header by the ASASP.
	 * 25. In successful response, Content-type header should be populate with value as application/json
	 */
	@Test
	public void testInteractionHeaderInResponseAndContentType(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		headers.add("Accept", "application/json");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity ,String.class);
		assertNotNull(response.getHeaders().get("x-fapi-interaction-id"));
		assertNotNull(response.getHeaders().get("Content-Type"));
		assertTrue(response.getHeaders().get("Content-Type").get(0).contains("application/json"));
	}
    /**
     * 22. Value of x-fapi-interaction-id in response is always same if x-fapi-interaction-id sent in the request.
     */
	@Test
	public void testInteractionHeaderSameInResponse(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id", mHeaderValue);
		headers.add("Accept", "application/json");
		headers.add("x-fapi-interaction-id",mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity ,String.class);
		assertEquals(mHeaderValue,response.getHeaders().get("x-fapi-interaction-id").get(0));
	}

	/**
	 * 23. If the value of x-fapi-interaction-id is absent or blank in the request then its value must sent by the ASASP
	 */
	@Test
	public void testInteractionHeaderBlank(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id",mHeaderValue);
		headers.add("x-fapi-interaction-id"," ");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity ,String.class);
		assertNotNull(response.getHeaders().get("x-fapi-interaction-id"));
	}

	/**
	 * 13. Expired Authorization code is sent in the request
	 */
	@Test(expected = PSD2Exception.class)
	public void testExpiredAuthorizationToken(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", expiredToken);
		headers.add("x-fapi-financial-id",mHeaderValue);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try{
			restClientSync.callForGet(requestInfo, BalancesGETResponse.class, headers);
		}
		catch(PSD2Exception e){
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}
	/**
	 * Accept Header is passed with value other than application/json
	 */
	@Test(expected = HttpClientErrorException.class)
	public void testAcceptHeaderWithDifferentValue(){
		String url = edgeserverURL+accountBalanceURL+"/accounts/"+firstAccountId+"/balances";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", firstRefToken);
		headers.add("x-fapi-financial-id",mHeaderValue);
		headers.add("Accept", "text");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try{
			restTemplate.exchange(url, HttpMethod.GET, requestEntity ,String.class);
		}
		catch(HttpClientErrorException e){
			assertEquals(HttpStatus.NOT_ACCEPTABLE, e.getStatusCode());
			throw e;
		}
	}
	
	

}



