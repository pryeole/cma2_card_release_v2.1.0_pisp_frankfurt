package com.capgemini.psd2.aisp.validation.adapter;

public interface AISPCustomValidator<T, V> {

	// ValidateAccountsRequest
	public T validateRequestParams(T t);

	// ValidateAccountsResponse
	public boolean validateResponseParams(V v);
	
	// validateAccountRequestId
	public void validateUniqueId(String consentId);

}
