package com.capgemini.psd2.validator.enums;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Roles {
	AISP("accounts"), PISP("payments"), CBPII("fundsconfirmations");

	private String role;

	Roles(String s) {
		this.role = s;
	}

	public String getRole() {
		return role;
	}
	
	public static List<String> getRoleList(){
		return Stream.of(Roles.values()).map(Roles::getRole).collect(Collectors.toList());
	}
}
