package com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain;

import java.util.List;

public class AccountIdentificationMapping {
	private List<IdntificationMapping> identificationMappingList;

	public List<IdntificationMapping> getIdentificationMappingList() {
		return identificationMappingList;
	}

	public void setIdentificationMappingList(List<IdntificationMapping> identificationMappingList) {
		this.identificationMappingList = identificationMappingList;
	}
}
