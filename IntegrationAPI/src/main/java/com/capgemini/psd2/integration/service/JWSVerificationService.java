package com.capgemini.psd2.integration.service;

public interface JWSVerificationService {

	public void validateSignature(String body,String clientId);
}

