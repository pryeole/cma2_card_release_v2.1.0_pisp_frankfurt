package com.capgemini.psd2.aisp.platform.domain;

public class PlatformAccountSatementsFileResponse {
	
	private String fileName;
	
	private byte[] fileByteArray;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getFileByteArray() {
		return fileByteArray;
	}

	public void setFileByteArray(byte[] fileByteArray) {
		this.fileByteArray = fileByteArray;
	}
	
}
