package com.capgemini.psd2.security.exceptions;

import org.springframework.security.core.AuthenticationException;

import com.capgemini.psd2.exceptions.ErrorInfo;

public class PSD2AuthenticationException extends AuthenticationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ErrorInfo errorInfo = null;
	
	
	public PSD2AuthenticationException(String message,ErrorInfo errorInfo){
		super(message);
		this.errorInfo = errorInfo;
	}
	
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}
	

	public static PSD2AuthenticationException populateAuthenticationFailedException(SCAConsentErrorCodeEnum errorCodeEnum,String detailedErrorMessage){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailedErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		PSD2AuthenticationException authenticationException = new PSD2AuthenticationException(detailedErrorMessage,errorInfo);
		return authenticationException;
	}
	
	public static PSD2AuthenticationException populateAuthenticationFailedException(SCAConsentErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		PSD2AuthenticationException authenticationException = new PSD2AuthenticationException(errorCodeEnum.getDetailErrorMessage(),errorInfo);
		authenticationException.errorInfo = errorInfo;
		return authenticationException;
	}	
	
	public static PSD2AuthenticationException populateAuthenticationFailedException(ErrorInfo errorInfo){
		PSD2AuthenticationException authenticationException = new PSD2AuthenticationException(errorInfo.getDetailErrorMessage(),errorInfo);
		return authenticationException;
	}	
	
}