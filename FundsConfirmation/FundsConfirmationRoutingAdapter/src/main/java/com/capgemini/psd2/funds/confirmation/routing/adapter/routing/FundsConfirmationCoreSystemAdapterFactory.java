package com.capgemini.psd2.funds.confirmation.routing.adapter.routing;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;

@Component
public class FundsConfirmationCoreSystemAdapterFactory
		implements ApplicationContextAware, FundsConfirmationAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public FundsConfirmationAdapter getAdapterInstance(String adapterName) {
		return (FundsConfirmationAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

}
