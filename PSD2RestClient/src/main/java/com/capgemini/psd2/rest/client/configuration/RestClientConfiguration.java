/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandlerImpl;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class RestClientConfiguration.
 */
@Configuration
public class RestClientConfiguration {

    /**
     * Rest template.
     *
     * @return the rest template
     */
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    /**
     * Rest client sync.
     *
     * @return the rest client sync
     */
    @Bean
    @Primary
    public RestClientSync restClientSync(){
    	return new RestClientSyncImpl(exceptionHandler());
    }
    
    /**
     * Exception handler.
     *
     * @return the exception handler
     */
    @Bean
    public ExceptionHandler exceptionHandler(){
    	return new ExceptionHandlerImpl();
    }
}
