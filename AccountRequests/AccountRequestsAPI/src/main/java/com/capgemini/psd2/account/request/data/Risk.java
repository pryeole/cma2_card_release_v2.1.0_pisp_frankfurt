package com.capgemini.psd2.account.request.data;

public class Risk {
	
	private String riskData;

	public String getRiskData() {
		return riskData;
	}

	public void setRiskData(String riskData) {
		this.riskData = riskData;
	}

}
