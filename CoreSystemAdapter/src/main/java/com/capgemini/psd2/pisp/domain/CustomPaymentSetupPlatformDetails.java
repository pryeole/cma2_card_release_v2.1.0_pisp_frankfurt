package com.capgemini.psd2.pisp.domain;

import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;

public class CustomPaymentSetupPlatformDetails {

	private String setupCmaVersion;
	private PaymentTypeEnum paymentType;
	private String endToEndIdentification;
	private String tppDebtorDetails;
	private String tppDebtorNameDetails;
	private String instructionIdentification;
	private String fileHash;
	
	// Add specific payment details here

	/**
	 * @return the endToEndIdentification
	 */
	public String getEndToEndIdentification() {
		return endToEndIdentification;
	}

	/**
	 * @param endToEndIdentification
	 *            the endToEndIdentification to set
	 */
	public void setEndToEndIdentification(String endToEndIdentification) {
		this.endToEndIdentification = endToEndIdentification;
	}

	/**
	 * @return the tppDebtorDetails
	 */
	public String getTppDebtorDetails() {
		return tppDebtorDetails;
	}

	/**
	 * @param tppDebtorDetails
	 *            the tppDebtorDetails to set
	 */
	public void setTppDebtorDetails(String tppDebtorDetails) {
		this.tppDebtorDetails = tppDebtorDetails;
	}

	/**
	 * @return the tppDebtorNameDetails
	 */
	public String getTppDebtorNameDetails() {
		return tppDebtorNameDetails;
	}

	/**
	 * @param tppDebtorNameDetails
	 *            the tppDebtorNameDetails to set
	 */
	public void setTppDebtorNameDetails(String tppDebtorNameDetails) {
		this.tppDebtorNameDetails = tppDebtorNameDetails;
	}

	/**
	 * @return the instructionIdentification
	 */
	public String getInstructionIdentification() {
		return instructionIdentification;
	}

	/**
	 * @param instructionIdentification
	 *            the instructionIdentification to set
	 */
	public void setInstructionIdentification(String instructionIdentification) {
		this.instructionIdentification = instructionIdentification;
	}

	/**
	 * @return the setupCmaVersion
	 */
	public String getSetupCmaVersion() {
		return setupCmaVersion;
	}

	/**
	 * @param setupCmaVersion
	 *            the setupCmaVersion to set
	 */
	public void setSetupCmaVersion(String setupCmaVersion) {
		this.setupCmaVersion = setupCmaVersion;
	}

	/**
	 * @return the paymentType
	 */
	public PaymentTypeEnum getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(PaymentTypeEnum paymentType) {
		this.paymentType = paymentType;
	}

	
	/**
	 * @return the fileHash
	 */
	public String getFileHash() {
		return fileHash;
	}

	/**
	 * @param fileHash for fileHash to set
	 */
	public void setFileHash(String fileHash) {
		this.fileHash = fileHash;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomPaymentSetupPlatformDetails [setupCmaVersion=" + setupCmaVersion + ", paymentType=" + paymentType
				+ ", endToEndIdentification=" + endToEndIdentification + ", tppDebtorDetails=" + tppDebtorDetails
				+ ", tppDebtorNameDetails=" + tppDebtorNameDetails + ", instructionIdentification="
				+ instructionIdentification + ", fileHash=" + fileHash + "]";
	}

}
