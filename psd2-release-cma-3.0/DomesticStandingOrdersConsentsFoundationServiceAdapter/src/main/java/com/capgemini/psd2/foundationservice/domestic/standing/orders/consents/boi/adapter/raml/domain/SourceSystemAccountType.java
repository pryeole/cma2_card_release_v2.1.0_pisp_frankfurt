package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets sourceSystemAccountType
 */
public enum SourceSystemAccountType {
  
  CURRENT_ACCOUNT("Current Account"),
  
  SAVINGS_ACCOUNT("Savings Account"),
  
  CREDIT_CARD("Credit Card");

  private String value;

  SourceSystemAccountType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static SourceSystemAccountType fromValue(String text) {
    for (SourceSystemAccountType b : SourceSystemAccountType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

