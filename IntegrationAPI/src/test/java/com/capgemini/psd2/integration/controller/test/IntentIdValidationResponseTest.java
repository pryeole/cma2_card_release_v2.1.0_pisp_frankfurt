package com.capgemini.psd2.integration.controller.test;


import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.capgemini.psd2.integration.dtos.IntentIdValidationResponse;

public class IntentIdValidationResponseTest {
	
	@Test
	public void testBean(){
		IntentIdValidationResponse validationResponse = new IntentIdValidationResponse();
		validationResponse.setIntentType(null);
		assertNull(validationResponse.getIntentType());
		
	}

}
