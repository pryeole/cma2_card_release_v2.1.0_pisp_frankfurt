package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OAuth2ConsentScopes
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OAuth2ConsentScopes   {
  /**
   * The consent decision for the scope.
   */
  public enum ConsentEnum {
    GRANTED("granted"),
    
    DENIED("denied");

    private String value;

    ConsentEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ConsentEnum fromValue(String text) {
      for (ConsentEnum b : ConsentEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("consent")
  private ConsentEnum consent = null;

  @JsonProperty("consentPromptText")
  private String consentPromptText = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("name")
  private String name = null;

  public OAuth2ConsentScopes consent(ConsentEnum consent) {
    this.consent = consent;
    return this;
  }

   /**
   * The consent decision for the scope.
   * @return consent
  **/
  @ApiModelProperty(value = "The consent decision for the scope.")


  public ConsentEnum getConsent() {
    return consent;
  }

  public void setConsent(ConsentEnum consent) {
    this.consent = consent;
  }

  public OAuth2ConsentScopes consentPromptText(String consentPromptText) {
    this.consentPromptText = consentPromptText;
    return this;
  }

   /**
   * The consent prompt text for the scope.
   * @return consentPromptText
  **/
  @ApiModelProperty(value = "The consent prompt text for the scope.")


  public String getConsentPromptText() {
    return consentPromptText;
  }

  public void setConsentPromptText(String consentPromptText) {
    this.consentPromptText = consentPromptText;
  }

  public OAuth2ConsentScopes description(String description) {
    this.description = description;
    return this;
  }

   /**
   * A description of this scope that is intended for end-users.
   * @return description
  **/
  @ApiModelProperty(value = "A description of this scope that is intended for end-users.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public OAuth2ConsentScopes name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The name of the scope.
   * @return name
  **/
  @ApiModelProperty(value = "The name of the scope.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OAuth2ConsentScopes oauth2ConsentScopes = (OAuth2ConsentScopes) o;
    return Objects.equals(this.consent, oauth2ConsentScopes.consent) &&
        Objects.equals(this.consentPromptText, oauth2ConsentScopes.consentPromptText) &&
        Objects.equals(this.description, oauth2ConsentScopes.description) &&
        Objects.equals(this.name, oauth2ConsentScopes.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(consent, consentPromptText, description, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OAuth2ConsentScopes {\n");
    
    sb.append("    consent: ").append(toIndentedString(consent)).append("\n");
    sb.append("    consentPromptText: ").append(toIndentedString(consentPromptText)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

