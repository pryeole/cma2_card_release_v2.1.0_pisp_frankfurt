package com.capgemini.psd2.funds.confirmation.test.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.utilities.FundsConfirmationValidationUtility;

public class FundsConfirmationValidationUtilityTest {

	@InjectMocks
	private FundsConfirmationValidationUtility utility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	// Test with not null value
	@Test
	public void testIsEmptyNotNull() {
		String str = "Test";
		assertNotEquals(true, FundsConfirmationValidationUtility.isEmpty(str));
	}

	// Test with null value

	@Test
	public void testIsEmptyNull() {
		String str = null;
		assertEquals(true, FundsConfirmationValidationUtility.isEmpty(str));

	}

	// Test with empty string

	@Test
	public void testIsEmptyString() {
		String str = "";
		assertEquals(true, FundsConfirmationValidationUtility.isEmpty(str));

	}

	@Test
	public void isValidCurrency_validCurrencyCode() {
		Assert.assertTrue("ValidCurrency", utility.isValidCurrency("EUR"));
	}

	@Test(expected = PSD2Exception.class)
	public void isValidCurrency_emptyCurrencyCode() {
		utility.isValidCurrency(null);
	}
}
