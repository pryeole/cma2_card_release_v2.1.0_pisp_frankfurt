(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/error-notice.html',
    '<div class="alert col-xs-12 col-sm-12 col-md-12" role="alert" aria-atomic="true" ng-class="errorCssClass">\n' +
    '    <p class="message" ng-if="errorMsgText.length">\n' +
    '        <span class="alert-type" ng-bind="alertText"></span>\n' +
    '        <span ng-bind="errorMsgText"> </span>\n' +
    '        <ng-template ng-if="errorData.correlationId">\n' +
    '            <span class="alert-type" ng-bind="corRelationText"> </span>\n' +
    '            <span ng-bind="corRelationMsgText"></span>\n' +
    '        </ng-template>\n' +
    '    </p>  \n' +
    '    <p ng-if="isErrorMsgTextDescription" ng-bind-html="errorMsgTextDescription"> </p>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/loading-spinner.html',
    '<div class="block-ui-overlay"></div>\n' +
    '<div class="block-ui-message-container" aria-live="assertive" aria-atomic="true">\n' +
    '	<div class="block-ui-message" ng-class="$_blockUiMessageClass">\n' +
    '		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
    '	</div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/login.html',
    '<!--header start-->\n' +
    '<header>\n' +
    '    <div class="header-container">\n' +
    '        <div class="container">\n' +
    '            <img class="logo" ng-src={{logoPath}} title="{{logoAltLabel}}" alt="{{logoAltLabel}}" ng-if="logoAltLabel.length" />\n' +
    '            <div class="portal-details">\n' +
    '                <h1 ng-bind="accountAccessText"> </h1>\n' +
    '                <h2 ng-bind="thirdPartyText"></h2>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</header>\n' +
    '<!--header end-->\n' +
    '\n' +
    '<!--section start-->\n' +
    '\n' +
    '\n' +
    '<div class="main-container clearfix">\n' +
    '    <div class="login-container" ng-show="loginHeaderText.length">\n' +
    '        <h3 ng-bind="loginHeaderText"></h3>\n' +
    '        <error-notice ng-if="alertData" error-data="alertData"></error-notice>\n' +
    '        <error-notice ng-if="maintenanceAlertData" error-data="maintenanceAlertData"></error-notice>\n' +
    '        <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '        <form class="form-container" id="loginForm" method="POST" action="login" autocomplete="off">\n' +
    '            <label class="sr-only" aria-hidden="true" for="preventpassword">Prevent to open The Default Browser Password remember popup.</label>\n' +
    '            <input type="text" id="preventpassword" style="display:none" />\n' +
    '            <div class="form-group">\n' +
    '                <label for="username" ng-bind="userText"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="text" ng-keypress="filterValue($event)" maxlength="8" class="form-control" placeholder="{{userPlaceHolderLabel}}" ng-model="userId" name="username" id="username" autocomplete="off" />\n' +
    '                    <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)" aria-label="{{userTooltipLbl}}" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{userTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="form-group" ng-hide="sandboxFlag">\n' +
    '                <label for="pwd" ng-bind="passwordText"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="password" maxlength="6" class="form-control" ng-model="passcode" placeholder="{{passwordPlaceHolderLabel}}" name="password" id="pwd" ng-keypress="filterValue($event)" autocomplete="off">\n' +
    '                    <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)"  aria-label="{{passwordTooltipLbl}}" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{passwordTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <p><strong ng-bind="noteTitleText"></strong><span ng-bind="loginNoteDescText"></span></p>\n' +
    '\n' +
    '            <a href="{{loginHelpUrl}}" class="need-help"  target="_blank">\n' +
    '				<span ng-bind="needHelpText" title="{{tooltipTt}}"></span>\n' +
    '                <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '			</a>\n' +
    '            <div class="button-container"><button type="button" class="btn btn-primary pull-right" ng-click="loginAction()" ng-bind="continueText"></button>\n' +
    '                <button type="button" class="btn btn-secondary" ng-click="backToThirdParty()" ng-bind="backtoTPPText"></button>\n' +
    '\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '    <div class="info-container" ng-show="furtherText.length">\n' +
    '        <h4 ng-bind-html="furtherText"></h4>\n' +
    '        <h5 ng-bind="boikeycodeText"></h5>\n' +
    '\n' +
    '        <img class="pull-right" alt="{{keyImageAlt}}" ng-src="{{cdnBaseURL}}/sca-ui/img/phone.png" />\n' +
    '        <p><span ng-bind-html="boikeycodeAppText"></span></p>\n' +
    '\n' +
    '\n' +
    '        <p><span ng-bind-html="boikeycodeHelpText"></span>\n' +
    '        </p>\n' +
    '        <h5 ng-bind="registreKeyCodeText"></h5>\n' +
    '        <ul>\n' +
    '            <li><strong ng-bind="step1Text"></strong> <span ng-bind="step1DescText"></span></li>\n' +
    '            <li><strong ng-bind="step2Text"></strong> <span ng-bind="step2DescText"></span> </li>\n' +
    '            <li><strong ng-bind="step3Text"></strong> <span ng-bind-html="step3DescText"></span></li>\n' +
    '        </ul>\n' +
    '        <p><strong ng-bind="noteInfoText"></strong><span ng-bind="noteDescText"></span></p>\n' +
    '        <a href="{{login365Url}}"  target="_blank">\n' +
    '		 <span ng-bind="loginLinkText" title="{{tooltipTt}}"></span>\n' +
    '          <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '		</a>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!--section end-->\n' +
    '\n' +
    '\n' +
    '<!--footer start-->\n' +
    '<footer>\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12">\n' +
    '                <ul class="footer-links">\n' +
    '                    <li>\n' +
    '                        <a href="{{aboutUsUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="aboutUsText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                   </li>\n' +
    '					 <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                   <li>\n' +
    '                        <a href="{{privacyPolicyUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="cookieText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '					  <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                     <li>\n' +
    '                        <a href="{{tncUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="tncText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '					<li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                   <li>\n' +
    '                        <a href="{{helpUrl}}" target=\'_blank\'>\n' +
    '                            <span ng-bind="helpText" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <p class="regulatory-statement" ng-bind="regulatoryText"></p>\n' +
    '    </div>\n' +
    '</footer>\n' +
    '\n' +
    '<!--footer end -->\n' +
    '<!-- <model-pop-up modal="modelPopUpConf"></model-pop-up>   -->\n' +
    ' <div ui-view="modal" autoscroll="false"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/loginBol-footer.html',
    '<!--footer start-->\n' +
    '<footer role="contentinfo">\n' +
    '    <div class="row footer-container">\n' +
    '        <div class="col-md-6 col-lg-7 boi-footer-text">\n' +
    '            <p ng-init="REGULATORY_TEXT=(\'FOOTER.REGULATORY_LABEL\'| translate)"ng-bind="REGULATORY_TEXT |uppercase"></p>\n' +
    '        </div>\n' +
    '        <div class="col-md-6 col-lg-5 boi-footer-nav">\n' +
    '            <ul class="footer-navigation">\n' +
    '                <li>\n' +
    '                    <a href="{{aboutUsUrl}}" target=\'_blank\'>\n' +
    '                        <span ng-bind="aboutUsText" title="{{tooltipTt}}"></span>\n' +
    '                        <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li>\n' +
    '                    <a href="{{privacyPolicyUrl}}" target=\'_blank\'>\n' +
    '                        <span ng-bind="cookieText" title="{{tooltipTt}}"></span>\n' +
    '                        <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                    </a> \n' +
    '                </li>\n' +
    '                <li>\n' +
    '                    <a href="{{tncUrl}}" target=\'_blank\'>\n' +
    '                        <span ng-bind="tncText" title="{{tooltipTt}}"></span>\n' +
    '                        <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                    </a> \n' +
    '                </li>\n' +
    '                <li>\n' +
    '                    <a href="{{helpUrl}}" target=\'_blank\'>\n' +
    '                       <span ng-bind="helpText" title="{{tooltipTt}}"></span>\n' +
    '                       <span class="sr-only" aria-label="{{tooltipTt}}"></span>                        \n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</footer> \n' +
    '<!--footer end -->');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/loginBol.html',
    '<!--header start-->\n' +
    '<header>\n' +
    '    <div class="header-container">\n' +
    '        <div class="container">\n' +
    '            <img class="logo" ng-src={{logoPath}} title="{{logoAltLabel}}" alt="{{logoAltLabel}}" ng-if="logoAltLabel.length" />\n' +
    '            <div class="portal-details">\n' +
    '                <h1 ng-bind="accountAccessText"> </h1>\n' +
    '                <h2 ng-bind="thirdPartyText"></h2>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</header>\n' +
    '<!--header end-->\n' +
    '\n' +
    '<!--section start-->\n' +
    '\n' +
    '\n' +
    '<div class="main-container clearfix">\n' +
    '    <div class="login-container" ng-show="loginHeaderText.length">\n' +
    '        <h3 ng-bind="loginHeaderText"></h3>\n' +
    '        <error-notice ng-if="alertData" error-data="alertData"></error-notice>\n' +
    '        <error-notice ng-if="maintenanceAlertData" error-data="maintenanceAlertData"></error-notice>\n' +
    '        <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '        <form class="form-container" id="loginForm" method="POST" action="login" autocomplete="off">\n' +
    '            <label class="sr-only" aria-hidden="true" for="preventpassword">Prevent to open The Default Browser Password remember popup.</label>\n' +
    '            <input type="text" id="preventpassword" style="display:none" />\n' +
    '            <div class="form-group">\n' +
    '                <label for="username" ng-bind="userText"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="text" ng-keypress="filterValue($event, true)" maxlength="8" class="form-control" placeholder="{{userPlaceHolderLabel}}" ng-model="userId" name="username" id="username" autocomplete="off" />\n' +
    '                    <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)" aria-label="{{userTooltipLbl}}" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{userTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="form-group">\n' +
    '                <label for="pwd" ng-bind="passwordText"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="password" maxlength="6" class="form-control" ng-model="passcode" placeholder="{{passwordPlaceHolderLabel}}" name="password" id="pwd" ng-keypress="filterValue($event, false)" autocomplete="off">\n' +
    '                    <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)"  aria-label="{{passwordTooltipLbl}}" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{passwordTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="form-group pass-phrase-group">\n' +
    '                <label for="pass-phrase" ng-bind="phraseLabel"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="text" maxlength="40" class="form-control" ng-model="passPhrase" placeholder="{{phrasePlaceholderLabel}}" name="phrase" id="pass-phrase" ng-keypress="filterValue($event, true)" autocomplete="off">\n' +
    '                    <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)"  aria-label="{{passPhraseTooltipMsg}}" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{passPhraseTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '\n' +
    '            <!-- <p><strong ng-bind="noteTitleText"></strong><span ng-bind="loginNoteDescText"></span></p> -->\n' +
    '\n' +
    '            <a href="{{loginHelpUrl}}" class="need-help"  target="_blank">\n' +
    '				<span ng-bind="needHelpText" title="{{tooltipTt}}"></span>\n' +
    '                <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '			</a>\n' +
    '            <div class="button-container"><button type="button" class="btn btn-primary pull-right" ng-click="loginAction()" ng-bind="continueText"></button>\n' +
    '                <button type="button" class="btn btn-secondary" ng-click="backToThirdParty()" ng-bind="backtoTPPText"></button>\n' +
    '\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '    <div class="info-container" ng-show="furtherText.length">\n' +
    '        <h4 ng-bind-html="furtherText"></h4>\n' +
    '        <h5 ng-bind="boikeycodeText"></h5>\n' +
    '\n' +
    '        <img class="pull-right" alt="{{keyImageAlt}}" ng-src="{{cdnBaseURL}}/sca-ui/img/phone.png" />\n' +
    '        <p><span ng-bind-html="boikeycodeAppText"></span></p>\n' +
    '\n' +
    '\n' +
    '        <p><span ng-bind-html="boikeycodeHelpText"></span>\n' +
    '        </p>\n' +
    '        <h5 ng-bind="registreKeyCodeText"></h5>\n' +
    '        <ul>\n' +
    '            <li><strong ng-bind="step1Text"></strong> <span ng-bind="step1DescText"></span></li>\n' +
    '            <li><strong ng-bind="step2Text"></strong> <span ng-bind="step2DescText"></span> </li>\n' +
    '            <li><strong ng-bind="step3Text"></strong> <span ng-bind-html="step3DescText"></span></li>\n' +
    '        </ul>\n' +
    '        <p><strong ng-bind="noteInfoText"></strong><span ng-bind="noteDescText"></span></p>\n' +
    '        <a href="{{login365Url}}"  target="_blank">\n' +
    '		 <span ng-bind="loginLinkText" title="{{tooltipTt}}"></span>\n' +
    '          <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '		</a>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!--section end-->\n' +
    '\n' +
    '<!--footer start-->\n' +
    '<login-bol-footer></login-bol-footer>\n' +
    '<!--footer end -->\n' +
    ' <redirect-popup modal="modelPopUpConf"></redirect-popup> \n' +
    ' <!--<div ui-view="modal" autoscroll="false"></div>-->\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal-1.html',
    '<div class="modal-body pop-confirm-body lightbox1" role="dialog" style="">\n' +
    '    <span class="sr-only" ng-bind="modelPopUpConf.lightbox1PopUpDisplayed"></span>\n' +
    '    <button type="button" class="close" title="{{modelPopUpConf.lightbox1CloseImageAlt}}" ng-click="cancelBtnClicked()" autofocus>\n' +
    '        <span class="sr-only" ng-bind="modelPopUpConf.lightbox1CloseImageAlt"></span>\n' +
    '    </button>\n' +
    '    <div class="confirm-msg">\n' +
    '        <p ng-bind="modelPopUpConf.lightBox1Description"></p>\n' +
    '    </div>\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section footer-lightbox1">\n' +
    '        <div class="btn-container">\n' +
    '            <button class="btn btn-lightbox1 btn-left" ng-click="cancelBtnClicked()">\n' +
    '                <span ng-bind="modelPopUpConf.lightBox1HaveBoiKeycodeApp"></span>\n' +
    '                <span class="sr-only" aria-label="{{lightbox1ClosePopUp}}"></span>\n' +
    '            </button>\n' +
    '            <button class="btn btn-lightbox1 text-left" ng-click="goToModal2()">\n' +
    '                <span ng-bind="modelPopUpConf.lightBox1IdontHaveBoiKeycodeApp"></span>\n' +
    '                <span class="sr-only" aria-label="{{openPopUp}}"></span>\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal-2.html',
    '<div class="modal-body  lightbox2" aria-live="polite">\n' +
    '    <span class="sr-only" ng-bind="modelPopUpConf.lightbox1PopUpDisplayed"></span>\n' +
    '    <button type="button" class="close" title="{{modelPopUpConf.lightbox1CloseImageAlt}}" ng-click="cancelBtnClicked()" autofocus>\n' +
    '        <span class="sr-only" ng-bind="modelPopUpConf.lightbox1CloseImageAlt"></span>\n' +
    '    </button>\n' +
    '    <div class="confirm-msg">\n' +
    '        <p ng-bind="modelPopUpConf.lightBox2Description"></p>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="modal-footer  button-section xs-button-section footer-lightbox2">\n' +
    '        <div class="box-lightbox2">\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.optionLabel1"></span>\n' +
    '            <p ng-bind="modelPopUpConf.lightBox2IdontWantToContinue"></p>\n' +
    '            <button role="button" class="btn btn-lightbox2 " ng-click="goToTPP()">\n' +
    '                <span ng-bind="modelPopUpConf.lightBox2ReturnToThirdParty"></span>\n' +
    '                <span class="sr-only" aria-label="{{redirectToTpp}}"></span>\n' +
    '            </button>\n' +
    '                   </div>\n' +
    '        <div class="box-lightbox2 ">\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.optionLabel2 "></span>\n' +
    '            <p ng-bind="modelPopUpConf.lightBox2IwantToRegisterKeycodeApp"></p>\n' +
    '            <button role="button" class="btn btn-lightbox2 " ng-click="goToModal3()">\n' +
    '                <span ng-bind="modelPopUpConf.lightBox2RegisterKeycodeApp"></span>\n' +
    '                <span class="sr-only" aria-label="{{openRegisterWindow}}"></span>\n' +
    '            </button>\n' +
    '                    </div>\n' +
    '        <div class="box-lightbox2 ">\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.optionLabel3 "></span>\n' +
    '            <p ng-bind="modelPopUpConf.lightBox2IhavePreviouslyRegisterKeycodeApp"></p>\n' +
    '            <button role="button" class="btn btn-lightbox2 " ng-click="cancelBtnClicked()">\n' +
    '                <span ng-bind="modelPopUpConf.lightBox2ContinueWithLogin"></span>\n' +
    '                <span class="sr-only" aria-label="{{lightbox2ClosePopUp}}"></span>\n' +
    '            </button>\n' +
    '                   </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal-3.html',
    '<div class="modal-body pop-confirm-body lightbox3" aria-live="polite">\n' +
    '    <span class="sr-only" ng-bind="modelPopUpConf.lightbox1PopUpDisplayed"></span>\n' +
    '    <button  role="button" type="button" class="close" title="{{modelPopUpConf.lightbox1CloseImageAlt}}" ng-click="cancelBtnClicked()" autofocus>\n' +
    '        <span class="sr-only" ng-bind="modelPopUpConf.lightbox1CloseImageAlt"></span>\n' +
    '    </button>\n' +
    '    <div class="confirm-msg">\n' +
    '        <p style="display:none;" ng-bind-html="modelPopUpConf.lightBox3Description1"> </p>\n' +
    '        <p class="text-center-lightbox3" ng-bind-html="modelPopUpConf.lightBox3Description1"></p>\n' +
    '        <p class="text-center-lightbox3" ng-bind-html="modelPopUpConf.lightBox3Description2"></p>\n' +
    '        <p class="text-center-lightbox3" ng-bind="modelPopUpConf.lightBox3Description3"></p>\n' +
    '    </div>\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section footer-lightbox3">\n' +
    '        <button type="button" class="btn btn-lightbox3 btn-left" ng-click="goToModal2()">\n' +
    '            <span ng-bind="modelPopUpConf.lightBox3BackToOptions"></span>\n' +
    '            <span class="sr-only" aria-label="{{backToOption}}"></span>\n' +
    '        </button>\n' +
    '        <button type="button" class="btn btn-lightbox3" ng-click="goTo365()">\n' +
    '            <span ng-bind="modelPopUpConf.lightBox3LoginTo365Online"></span>\n' +
    '            <span class="sr-only" aria-label="{{redirectToLogin}}"></span>\n' +
    '        </button>\n' +
    '           </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal.html',
    ' <model-pop-up modal="modelPopUpConf" cdn-base-url="cdnBaseURL"></model-pop-up> ');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modalPopUp.html',
    '<div class="content ">\n' +
    '    <div class="content" ui-view="modal" autoscroll="false"></div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/redirectPopup.html',
    '<div class="backtothirdparty-popup">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-logo"></div>\n' +
    '        <div class="success-modal-header">\n' +
    '            <span id="modal_title" ng-bind="modal.modelpopupTitle"></span>\n' +
    '        </div>\n' +
    '        <div class="success-modal-body">\n' +
    '            <p ng-bind="modal.modelpopupBodyContent1"> </p>\n' +
    '            <p ng-bind="modal.modelpopupBodyContent2"> </p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/session-out-popup.html',
    '<div class="modal-body pop-confirm-body lightbox1">\n' +
    '  <!--   <button type="button " class="close " aria-hidden="true " ng-click="cancelBtnClicked() "><img class="close " ng-src ="{{close}}"></button>  -->\n' +
    '    <div class="confirm-msg">        \n' +
    '        <p class="text-center" ng-bind="sessionDescText"> </p>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section footer-lightbox1">\n' +
    '        <div class="text-center">\n' +
    '            <button role="button" class="btn btn-lightbox1" ng-click="redirectTPP()" ng-bind="sessionPopupBtnText">\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();
