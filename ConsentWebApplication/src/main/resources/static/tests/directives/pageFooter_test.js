"use strict";
describe("ConsentApp.pageFooter_test", function() {
    beforeEach(module("consentTemplates", "consentApp"));
    var compile, scope, element, allAccounts;

    beforeEach(inject(function($rootScope, $compile) {
        scope = $rootScope.$new();
        element = angular.element("<page-footer></page-footer>");
        $compile(element)(scope);
        scope.$digest();
    }));

    describe("test template", function() {
        it("should have CSS classes in template", function() {
            expect(element.hasClass("footer-bg")).toBeTruthy();
        });
    });
});