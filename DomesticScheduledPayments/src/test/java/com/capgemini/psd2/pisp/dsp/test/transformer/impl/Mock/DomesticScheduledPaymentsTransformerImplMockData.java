package com.capgemini.psd2.pisp.dsp.test.transformer.impl.Mock;

import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledResponse1;

public class DomesticScheduledPaymentsTransformerImplMockData {

	public OBWriteDataDomesticScheduledResponse1 getOBWriteDataDomesticScheduledResponse1(){
		OBWriteDataDomesticScheduledResponse1 data = new OBWriteDataDomesticScheduledResponse1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		
		
				//Setting Creditor Agent
				OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
				OBPostalAddress6 postalAddress = new OBPostalAddress6();
				creditorAgent.setSchemeName("BICFI");
				creditorAgent.setIdentification("Testing Scheme");
				creditorAgent.setName("Testing Creditor Agent Block");
				creditorAgent.setPostalAddress(postalAddress);
						
				//Setting Creditor Account
				OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
				creditorAccount.setSchemeName("IBAN");
				creditorAccount.setIdentification("GB29NWBK60161331926819");
				creditorAccount.setName("Creditor Account");
				creditorAccount.setSecondaryIdentification("123");
				
				OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
				creditorAccount.setSchemeName("PAN");
				creditorAccount.setIdentification("123456789");
				creditorAccount.setName("Debtor Account");
				creditorAccount.setSecondaryIdentification("123");
				
				initiation.setCreditorAccount(creditorAccount);
				initiation.setDebtorAccount(debtorAccount);
								
				data.setInitiation(initiation);
				data.setCreationDateTime("2018-05-09T11:10:12.064Z");
				data.setStatusUpdateDateTime("2018-05-09T11:10:12.064Z");
				
				return data;
	}
	
}
