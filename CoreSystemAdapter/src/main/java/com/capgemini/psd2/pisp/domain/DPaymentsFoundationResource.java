package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "paymentSubmissionFoundationResources")
public class DPaymentsFoundationResource extends PaymentDomesticSubmitPOST201Response{
	@Id
	private String id;

	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
