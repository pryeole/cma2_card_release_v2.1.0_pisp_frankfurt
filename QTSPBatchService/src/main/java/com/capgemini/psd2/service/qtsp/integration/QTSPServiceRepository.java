package com.capgemini.psd2.service.qtsp.integration;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.model.qtsp.QTSPResource;

public interface QTSPServiceRepository extends MongoRepository<QTSPResource, String> {

}