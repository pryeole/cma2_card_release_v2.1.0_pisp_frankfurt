/*package com.capgemini.psd2.security.saas.test.model;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.identitymanagement.models.UserAuthorities;
import com.capgemini.psd2.security.models.JwtToken;
import com.capgemini.psd2.security.models.UserContext;
import com.capgemini.psd2.security.saas.factories.JwtTokenFactory;
import com.capgemini.psd2.security.saas.model.JwtSettings;

public class JwtTokenFactoryTest {

	@Test
	public void createAccessJwtTokenTest() {
		String signingKey = "0123456789abcdef";
		int tokenExpirationTime = 15;
		int refreshTokenExpTime = 60;
		String tokenIssuer = "http://SaaS.com";
		JwtSettings settings = new JwtSettings();
		settings.setTokenSigningKey(signingKey);
		settings.setTokenIssuer(tokenIssuer);
		settings.setTokenExpirationTime(tokenExpirationTime);
		settings.setRefreshTokenExpTime(refreshTokenExpTime);
		
		String userName = "testUser";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new UserAuthorities(AdapterSecurityConstants.USER_AUTHORITY));
		UserContext userContext = UserContext.create(userName,authorities);
		JwtTokenFactory factory = new JwtTokenFactory(settings);
	     JwtToken token = factory.createAccessJwtToken(userContext);
		assertNotNull(token);
	}

}
*/