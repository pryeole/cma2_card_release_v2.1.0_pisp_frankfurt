package com.capgemini.psd2.fraudsystem.domain;

import com.capgemini.psd2.aisp.domain.OBAccount2;

public class AccountWithFraudSystemResponse extends OBAccount2 {
	Object fraudResponse;

	public Object getFraudResponse() {
		return fraudResponse;
	}

	public void setFraudResponse(Object fraudResponse) {
		this.fraudResponse = fraudResponse;
	}

}
