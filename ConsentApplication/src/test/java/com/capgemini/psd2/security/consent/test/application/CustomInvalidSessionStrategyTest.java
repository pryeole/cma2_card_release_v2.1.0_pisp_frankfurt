package com.capgemini.psd2.security.consent.test.application;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomInvalidSessionStrategyTest {
	
	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private HttpServletResponse httpServletResponse;
	
/*	@InjectMocks
	private CustomInvalidSessionStrategy customInvalidSessionStrategy;
*/	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testOnInvalidSessionDetected() throws IOException, ServletException{
		when(httpServletRequest.getRequestURI()).thenReturn("openbanking-consent/healthjsp");
		RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
		when(httpServletRequest.getRequestDispatcher(anyString())).thenReturn(requestDispatcher );
		//customInvalidSessionStrategy.onInvalidSessionDetected(httpServletRequest, httpServletResponse);
	}
}
