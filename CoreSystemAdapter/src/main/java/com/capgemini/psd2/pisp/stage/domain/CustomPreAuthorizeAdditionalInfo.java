package com.capgemini.psd2.pisp.stage.domain;

public class CustomPreAuthorizeAdditionalInfo {
	private String payerCurrency;
	private String payerJurisdiction;
	private String accountNumber;
	private String accountNSC;
	private String accountBic;
	private String accountIban;
	private String accountPan;
	
	/**
	 * @return the payerCurrency
	 */
	public String getPayerCurrency() {
		return payerCurrency;
	}
	/**
	 * @param payerCurrency the payerCurrency to set
	 */
	public void setPayerCurrency(String payerCurrency) {
		this.payerCurrency = payerCurrency;
	}
	/**
	 * @return the payerJurisdiction
	 */
	public String getPayerJurisdiction() {
		return payerJurisdiction;
	}
	/**
	 * @param payerJurisdiction the payerJurisdiction to set
	 */
	public void setPayerJurisdiction(String payerJurisdiction) {
		this.payerJurisdiction = payerJurisdiction;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the accountNSC
	 */
	public String getAccountNSC() {
		return accountNSC;
	}
	/**
	 * @param accountNSC the accountNSC to set
	 */
	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}
	/**
	 * @return the accountBic
	 */
	public String getAccountBic() {
		return accountBic;
	}
	/**
	 * @param accountBic the accountBic to set
	 */
	public void setAccountBic(String accountBic) {
		this.accountBic = accountBic;
	}
	/**
	 * @return the accountIban
	 */
	public String getAccountIban() {
		return accountIban;
	}
	/**
	 * @param accountIban the accountIban to set
	 */
	public void setAccountIban(String accountIban) {
		this.accountIban = accountIban;
	}
	/**
	 * @return the accountPan
	 */
	public String getAccountPan() {
		return accountPan;
	}
	/**
	 * @param accountPan the accountPan to set
	 */
	public void setAccountPan(String accountPan) {
		this.accountPan = accountPan;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomPreAuthorizeAdditionalInfo [payerCurrency=" + payerCurrency + ", payerJurisdiction="
				+ payerJurisdiction + ", accountNumber=" + accountNumber + ", accountNSC=" + accountNSC
				+ ", accountBic=" + accountBic + ", accountIban=" + accountIban + ", accountPan=" + accountPan + "]";
	}
}
