package com.capgemini.psd2.customer.account.profile.mock.foundationservice.service;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.ChannelProfile;

public  interface CustomerAccountInformationService {
	public ChannelProfile  retrieveCustomerAccountInformation(String id) throws Exception;

}
