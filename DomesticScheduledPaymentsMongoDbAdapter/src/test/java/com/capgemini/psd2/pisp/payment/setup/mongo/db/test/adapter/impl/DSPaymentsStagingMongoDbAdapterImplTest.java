package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.DSPaymentsStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DSPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPaymentsStagingMongoDbAdapterImplTest {

	@Mock
	private DSPaymentsFoundationRepository repository;

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private DSPaymentsStagingMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessDSPaymentsMultiAuthorised() {

		CustomDSPaymentsPOSTRequest request = DSPaymentsStagingMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(repository.save(any(CustomDSPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processDSPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDSPaymentsAwaitingFurtherAuthorisation() {

		CustomDSPaymentsPOSTRequest request = DSPaymentsStagingMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(repository.save(any(CustomDSPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processDSPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDSPaymentsRejectedAuthorisation() {

		CustomDSPaymentsPOSTRequest request = DSPaymentsStagingMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(repository.save(any(CustomDSPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processDSPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDSPaymentsNoAuthorisation() {

		CustomDSPaymentsPOSTRequest request = DSPaymentsStagingMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.FAIL;
		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();

		OBMultiAuthorisation1 multiAuthorisation = null;

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(repository.save(any(CustomDSPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processDSPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDSPaymentsNullSubmissionId() {

		CustomDSPaymentsPOSTRequest request = DSPaymentsStagingMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(repository.save(any(CustomDSPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processDSPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessDSPaymentsDataAccessResourceFailureException() {

		CustomDSPaymentsPOSTRequest request = DSPaymentsStagingMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(repository.save(any(CustomDSPaymentsPOSTResponse.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		adapter.processDSPayments(request, new HashMap<>(), new HashMap<>());
	}
	
	@Test
	public void testRetrieveStagedDSPaymentsResponse() {
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();
		
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(repository.findOneByDataDomesticScheduledPaymentId(anyString())).thenReturn(response);
		
		adapter.retrieveStagedDSPaymentsResponse(customPaymentStageIdentifiers,  new HashMap<>());
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDSPaymentsResponseException() {
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomDSPaymentsPOSTResponse response = DSPaymentsStagingMongoDbAdapterImplTestMockData.getResponse();
		
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(repository.findOneByDataDomesticScheduledPaymentId(anyString())).thenReturn(response);
		
		adapter.retrieveStagedDSPaymentsResponse(customPaymentStageIdentifiers,  new HashMap<>());
		
	}

	@After
	public void tearDown() {
		utility = null;
		reqAttributes = null;
		repository = null;
		adapter = null;
	}

}
