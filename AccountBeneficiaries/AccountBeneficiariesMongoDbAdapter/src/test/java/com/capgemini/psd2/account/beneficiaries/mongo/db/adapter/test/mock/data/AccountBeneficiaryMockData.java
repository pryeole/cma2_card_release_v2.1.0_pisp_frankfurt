package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBeneficiaryCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

public class AccountBeneficiaryMockData {
	
	public static List<AccountBeneficiaryCMA2> getMockAccountDetails()
	{
		return new ArrayList<AccountBeneficiaryCMA2>();
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static OBReadBeneficiary2 getMockExpectedAccountBeneficiaryResponse()
	{
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		AccountBeneficiaryCMA2 mockAccountBeneficiary = new AccountBeneficiaryCMA2();
		
		mockAccountBeneficiary.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		
		List<OBBeneficiary2> beneficiaryList = new ArrayList<>();
		beneficiaryList.add(mockAccountBeneficiary);
		obReadBeneficiary2Data.setBeneficiary(beneficiaryList);
		obReadBeneficiary2.setData(obReadBeneficiary2Data);
		return obReadBeneficiary2;
	}
	

}
