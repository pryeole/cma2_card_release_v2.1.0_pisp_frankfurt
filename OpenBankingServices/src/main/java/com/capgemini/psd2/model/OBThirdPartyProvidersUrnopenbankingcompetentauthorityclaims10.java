package com.capgemini.psd2.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Claims sourced from a competent authority - can be applied to either TPPs to ASPSP
 */
@ApiModel(description = "Claims sourced from a competent authority - can be applied to either TPPs to ASPSP")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")
@JsonInclude(Include.NON_NULL)
public class OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10   {
  @JsonProperty("Authorisations")
  private List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations = null;

  @JsonProperty("AuthorityId")
  private String authorityId = null;

  @JsonProperty("MemberState")
  private String memberState = null;

  @JsonProperty("RegistrationId")
  private String registrationId = null;

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 authorisations(List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations) {
    this.authorisations = authorisations;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 addAuthorisationsItem(OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations authorisationsItem) {
    if (this.authorisations == null) {
      this.authorisations = new ArrayList<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations>();
    }
    this.authorisations.add(authorisationsItem);
    return this;
  }

   /**
   * Home or Passported Authorisations
   * @return authorisations
  **/
  @ApiModelProperty(value = "Home or Passported Authorisations")

  @Valid

  public List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> getAuthorisations() {
    return authorisations;
  }

  public void setAuthorisations(List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations) {
    this.authorisations = authorisations;
  }

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 authorityId(String authorityId) {
    this.authorityId = authorityId;
    return this;
  }

   /**
   * The ID of the accrediting authority.
   * @return authorityId
  **/
  @ApiModelProperty(value = "The ID of the accrediting authority.")


  public String getAuthorityId() {
    return authorityId;
  }

  public void setAuthorityId(String authorityId) {
    this.authorityId = authorityId;
  }

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 memberState(String memberState) {
    this.memberState = memberState;
    return this;
  }

   /**
   * Member State of the Authority
   * @return memberState
  **/
  @ApiModelProperty(value = "Member State of the Authority")


  public String getMemberState() {
    return memberState;
  }

  public void setMemberState(String memberState) {
    this.memberState = memberState;
  }

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 registrationId(String registrationId) {
    this.registrationId = registrationId;
    return this;
  }

   /**
   * Registration Id with the principle authority
   * @return registrationId
  **/
  @ApiModelProperty(value = "Registration Id with the principle authority")


  public String getRegistrationId() {
    return registrationId;
  }

  public void setRegistrationId(String registrationId) {
    this.registrationId = registrationId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 = (OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10) o;
    return Objects.equals(this.authorisations, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10.authorisations) &&
        Objects.equals(this.authorityId, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10.authorityId) &&
        Objects.equals(this.memberState, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10.memberState) &&
        Objects.equals(this.registrationId, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10.registrationId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authorisations, authorityId, memberState, registrationId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 {\n");
    
    sb.append("    authorisations: ").append(toIndentedString(authorisations)).append("\n");
    sb.append("    authorityId: ").append(toIndentedString(authorityId)).append("\n");
    sb.append("    memberState: ").append(toIndentedString(memberState)).append("\n");
    sb.append("    registrationId: ").append(toIndentedString(registrationId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

