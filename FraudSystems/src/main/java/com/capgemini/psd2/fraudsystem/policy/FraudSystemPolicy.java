package com.capgemini.psd2.fraudsystem.policy;

@FunctionalInterface
public interface FraudSystemPolicy {

	public <T> T applyPolicy(T fraudSystemRequest, T fraudSystemResponse);

}
