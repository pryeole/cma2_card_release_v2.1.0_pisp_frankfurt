package com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;

public interface FPaymentsDataRepository extends MongoRepository<CustomFilePaymentsPOSTResponse, String>{
	
	 public CustomFilePaymentsPOSTResponse findOneByDataFilePaymentId(String filePaymentId );	

	 public CustomFilePaymentsPOSTResponse findOneByDataConsentId(String consentId );	

}
