package com.capgemini.psd2.aisp.validation.test.adapter.impl;


import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
import com.capgemini.psd2.aisp.domain.OBBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.aisp.domain.OBCashAccount4;
import com.capgemini.psd2.aisp.domain.OBCurrencyExchange5;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1;
import com.capgemini.psd2.aisp.domain.OBPostalAddress6;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.domain.OBTransaction3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountTransactionsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionValidatorTest {
	

	@InjectMocks
	AccountTransactionsValidatorImpl accountTransactionsValidatorImpl;
	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private PSD2Validator psd2Validator;
	
	 OBTransaction3 data = new OBTransaction3();
	 OBActiveOrHistoricCurrencyAndAmount amount = new OBActiveOrHistoricCurrencyAndAmount();
	 OBTransactionCashBalance balance = new OBTransactionCashBalance();
	 OBActiveOrHistoricCurrencyAndAmount amount2 = new OBActiveOrHistoricCurrencyAndAmount();
	 OBBankTransactionCodeStructure1 bankTransactionCode = new OBBankTransactionCodeStructure1();
	 OBTransaction3ProprietaryBankTransactionCode proprietaryBankTransactionCode = new OBTransaction3ProprietaryBankTransactionCode();
	 OBMerchantDetails1 merchantDetails = new OBMerchantDetails1();
		

		public  OBReadTransaction3 getAccountTransactionsGETResponse() {
			OBReadTransaction3 resp = new OBReadTransaction3();
			List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
			data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
			data.setTransactionId("123");
			data.setTransactionReference("Ref123");
			OBCashAccount4 obCashAccount4 = new OBCashAccount4();
			obCashAccount4.setIdentification("asdfasdf");
			obCashAccount4.setSecondaryIdentification("asdfas");
			obCashAccount4.setSchemeName("asdfasd");
			data.setCreditorAccount(obCashAccount4);
			OBActiveOrHistoricCurrencyAndAmount chargeAmount= new OBActiveOrHistoricCurrencyAndAmount();
			chargeAmount.setAmount("asfd");
			chargeAmount.setCurrency("asdf");
			data.setChargeAmount(chargeAmount);
			OBCurrencyExchange5  currencyExchange= new OBCurrencyExchange5();
			currencyExchange.setContractIdentification("afsdf");
			currencyExchange.setExchangeRate(new BigDecimal("123123"));
			currencyExchange.setQuotationDate("2017-04-05T10:43:07+00:00");
			currencyExchange.setSourceCurrency("asdf");
			currencyExchange.setTargetCurrency("asd");
			currencyExchange.setUnitCurrency("asdfas");
			//instructedAmount
			currencyExchange.setInstructedAmount(chargeAmount);
			data.setCurrencyExchange(currencyExchange);
			data.setDebtorAccount(obCashAccount4);
			OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
			creditorAgent.setIdentification("asdf");
			creditorAgent.setName("asdfas");
			creditorAgent.setSchemeName("asfd");
			OBPostalAddress6  postalAddress =new OBPostalAddress6();
			postalAddress.setCountry("IND");
			creditorAgent.setPostalAddress(postalAddress);
			data.setCreditorAgent(creditorAgent);
			data.setDebtorAgent(creditorAgent);
			amount.setAmount("10.00");
			amount.setCurrency("GBP");
			data.setAmount(amount);
			data.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
			data.setStatus(OBEntryStatus1Code.BOOKED);
			data.setBookingDateTime("2017-04-05T10:43:07+00:00");
			data.setValueDateTime("2017-04-05T10:45:22+00:00");
			data.setTransactionInformation("Cash from Aubrey");
			data.setAddressLine("XYZ address Line");
			bankTransactionCode.setCode("ReceivedCreditTransfer");
			bankTransactionCode.setSubCode("DomesticCreditTransfer");
			data.setBankTransactionCode(bankTransactionCode);
			proprietaryBankTransactionCode.setCode("Transfer");
			proprietaryBankTransactionCode.setIssuer("AlphaBank");
			data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
			balance.setAmount(amount2);
			amount2.setAmount("230.00");
			amount2.setCurrency("GBP");
			balance.setCreditDebitIndicator(
					OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
			balance.setType(OBBalanceType1Code.INTERIMBOOKED);
			data.setBalance(balance);
			data.setMerchantDetails(merchantDetails);
			merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
			merchantDetails.setMerchantName("MerchantXYZ");
			dataList.add(data);
			OBReadTransaction3Data data3 = new OBReadTransaction3Data();
			data3.setTransaction(dataList);
			resp.setData(data3);
			Links links = new Links();
			links.setFirst("1");
			links.setLast("10");
			links.setNext("5");
			links.setPrev("3");
			links.setSelf("4");
			resp.setLinks(links);
			Meta meta = new Meta();
			meta.setFirstAvailableDateTime("2017-04-05T10:43:07+00:00");
			meta.setLastAvailableDateTime("2017-04-05T10:43:07+00:00");
			meta.setTotalPages(25);
			resp.setMeta(meta);
			
			return resp;
		}
	
		@Test
		public void validateResponseParamsTest() {
			Field reqValidationEnabled = null;
				try {
					reqValidationEnabled = AccountTransactionsValidatorImpl.class.getDeclaredField("resValidationEnabled");
					reqValidationEnabled.setAccessible(true);
					reqValidationEnabled.set(accountTransactionsValidatorImpl, true);
				} catch (NoSuchFieldException | SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				assertTrue(accountTransactionsValidatorImpl.validateResponseParams(getAccountTransactionsGETResponse()));
		}
		
		@Test
		public void validateResponseParamsTestNullParam() {
			Field reqValidationEnabled = null;
				try {
					reqValidationEnabled = AccountTransactionsValidatorImpl.class.getDeclaredField("resValidationEnabled");
					reqValidationEnabled.setAccessible(true);
					reqValidationEnabled.set(accountTransactionsValidatorImpl, true);
				} catch (NoSuchFieldException | SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				assertTrue(accountTransactionsValidatorImpl.validateResponseParams(null));
		}
		@Test
		public void validateResponseParamsResValidationEnabledFalseTest() {
			Field reqValidationEnabled = null;
				try {
					reqValidationEnabled = AccountTransactionsValidatorImpl.class.getDeclaredField("resValidationEnabled");
					reqValidationEnabled.setAccessible(true);
					reqValidationEnabled.set(accountTransactionsValidatorImpl, false);
				} catch (NoSuchFieldException | SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				assertTrue(accountTransactionsValidatorImpl.validateResponseParams(getAccountTransactionsGETResponse()));
		}
		
		@Test(expected=PSD2Exception.class)
		public void validateUniqueIdTestForNullParam()
		{
			accountTransactionsValidatorImpl.validateUniqueId(null);
		}
		@Test
		public void validateUniqueIdTest()
		{
			Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("asd");  
			accountTransactionsValidatorImpl.validateUniqueId("asdf");
		}
		@Test
		public void validateRequestParamsTest()
		{
			assertTrue(accountTransactionsValidatorImpl.validateRequestParams(getAccountTransactionsGETResponse()).getData().getTransaction().get(0).getTransactionId().equals("123"));
		}
		
		
		
}
