package com.capgemini.psd2.adapter.security.custom.domain;

import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomAuthenticationServicePostRequest {
	
	@JsonProperty("digitalUser")
	private DigitalUser13 digitalUser;
	
	@JsonProperty("fsHeaders")
	private String headers;
	
	@JsonProperty("channelType")
	private String channelType;
	
	public DigitalUser13 getDigitalUser() {
		return digitalUser;
	}

	public void setDigitalUser(DigitalUser13 digitalUser) {
		this.digitalUser = digitalUser;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
	
}
