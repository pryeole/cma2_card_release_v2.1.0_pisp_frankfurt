package com.capgemini.psd2.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.Data;
import com.capgemini.psd2.aisp.domain.Data.PermissionsEnum;

public class AccountRequestITTestMockData {

	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockData() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00.050");
		data.setTransactionFromDateTime("2017-05-03T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockDataInvalidPermissions() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		permissions.add(PermissionsEnum.READTRANSACTIONSDETAIL);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00.050");
		data.setTransactionFromDateTime("2017-05-03T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static String getAccountRequestsParamatersMockDataInvalidRequest() {
		Map< String, String> map = new HashMap<>();
		map.put("error", "500");		
		return map.toString();
	}
	
	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockDataInvalidExpirationDateTime() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockDataInvalidToAndFromDateTime() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00.050");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00.00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00.00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	
	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockDataPastExpirationDateTime() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2016-05-02T00:00:00.050");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00.00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00.00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static AccountRequestPOSTRequest getAccountRequestsParamatersMockDataToBeforeFromDateTime() {
		AccountRequestPOSTRequest accountRequestsParammeters = new AccountRequestPOSTRequest();
		Data data = new Data();

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00.050");
		data.setTransactionFromDateTime("2017-05-03T00:00:00.050");
		data.setTransactionToDateTime("2016-05-03T00:00:00.050");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}

}
