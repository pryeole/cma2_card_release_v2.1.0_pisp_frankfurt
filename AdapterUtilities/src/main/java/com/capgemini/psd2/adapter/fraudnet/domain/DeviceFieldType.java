package com.capgemini.psd2.adapter.fraudnet.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The type of device metadata. JSC - This type should be used to pass the
 * string collected from the JSC or mobile collector(s). The collector data is
 * added as a single long string. There can only be one JSC type. HDIM - This
 * type should be used to pass the string collected from the Hosted Device
 * Manager(HDM) service. The collected data is added as a single long string.
 * There can only be one HDIM type. Cookie - This type should be used to pass
 * any stable portion of the user&#39;s cookie that does not change from visit
 * to visit. Each Cookie will be passed in this repeating block as name/ value
 * pairs. There can be more than one Cookie type. Header - This type should be
 * used to pass any HTTP header information as initially communicated by the
 * customer&#39;s device. Each Header will be passed in this repeating block as
 * name/ value pairs. There can be more than one Header type.
 */

public enum DeviceFieldType {

	JSC("JSC"),

	HDIM("HDIM"),

	COOKIE("Cookie"),

	HEADER("Header");

	private String value;

	DeviceFieldType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static DeviceFieldType fromValue(String text) {
		for (DeviceFieldType b : DeviceFieldType.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
