package com.capgemini.psd2.fraudsystem.request.handler;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadAccount2;

public interface FraudSystemRequestHandler {
	public void captureDeviceEventInfo(Map<String, Object> params);
	public OBReadAccount2 captureContactInfo(Map<String, Object> params);
	public void captureFinanacialAccountsInfo(OBReadAccount2 custInfo, Map<String, Object> params);
	public void captureTransferInfo(Map<String, Object> params);
} 