package com.capgemini.psd2.security.saas.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.SCAAuthenticationRetrieveFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.SCAAuthenticationServiceFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.security.saas.service.SaaSAppService;
import com.capgemini.psd2.security.saas.utility.SaaSAppUtility;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxConfig;

@Service
public class SaaSAppServiceImpl implements SaaSAppService{
	
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SCAAuthenticationServiceFoundationServiceAdapter authenticationPOSTAdapter;
	
	@Autowired
	private SCAAuthenticationRetrieveFoundationServiceAdapter authenticationGETAdapter;
	
	@Autowired
	private SaaSAppUtility saaSAppUtility;
	
	@Autowired
	private SandboxConfig sandboxConfig;
	
	@Override
	public CustomAuthenticationServiceGetResponse authenticateDOB(CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest) {
		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse = new CustomAuthenticationServicePostResponse();
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		try {
			if(Boolean.valueOf(sandboxConfig.isSandboxEnabled())){
				if (NullCheckUtils.isNullOrEmpty(customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier())) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName is blank or invalid");
				}
			}else{
				if (NullCheckUtils.isNullOrEmpty(customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier())
						|| NullCheckUtils.isNullOrEmpty(customAuthenticationServicePostRequest.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed().get(0).getValue())
						/*|| !saaSAppUtility.validUserAndPassword(customDOBPostRequest.getUserid())*/) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
				}
			}
			AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
					requestHeaderAttributes.getIntentId(), ConsentStatusEnum.AUTHORISED);
			if (consent != null && !consent.getPsuId().equalsIgnoreCase(customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier())) {
				throw PSD2AuthenticationException.populateAuthenticationFailedException(
						SCAConsentErrorCodeEnum.UN_AUTHORIZED_USER_REFRESH_TOKEN_FLOW);
			}
			requestHeaderAttributes.setPsuId(customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier());
			
			Map<String, String> params = new HashMap<String, String>();
			//params.put(AdapterSecurityConstants.CHANNELID_HEADER, "B365");
			params.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
			params.put(AdapterSecurityConstants.USER_HEADER, customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier());
			params.put(AdapterSecurityConstants.CHANNELID_HEADER, requestHeaderAttributes.getChannelId().toUpperCase());
			//loginResponse = authenticationAdapter.authenticate(customDOBPostRequest, params);
			customAuthenticationServicePostResponse= authenticationPOSTAdapter.retrieveAuthenticationService(customAuthenticationServicePostRequest, params);
			
			if(!customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator()){
				customAuthenticationServiceGetResponse = retrievePINandListOfDevices(customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier());
				customAuthenticationServiceGetResponse.setSessionInitiationFailureIndicator(customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator());
			}else{
				customAuthenticationServiceGetResponse.setSessionInitiationFailureIndicator(customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator());
				AuthenticationParameters authenticationParameters = new AuthenticationParameters();
				authenticationParameters.setDigitalUser(customAuthenticationServicePostResponse.getLoginResponse().getDigitalUser());
				customAuthenticationServiceGetResponse.setAuthenticationParameters(authenticationParameters);
			}
			
			
			
			//to be added after list of devices
			/*SimpleDateFormat sdf = new SimpleDateFormat(PFConstants.DATE_FORMAT);
			String currentDate = sdf.format(new Date());
			saaSAppUtility.dropOff(username, currentDate);*/
			
		} catch (PSD2AuthenticationException e) {
			throw e;
		}
		catch(PSD2Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getErrorInfo());
		}
		catch(Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getMessage(),SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return customAuthenticationServiceGetResponse;
	}
	
	@Override
	public CustomAuthenticationServiceGetResponse retrievePINandListOfDevices(String username){
		
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		try {
			if(Boolean.valueOf(sandboxConfig.isSandboxEnabled())){
				if (NullCheckUtils.isNullOrEmpty(username)) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName is blank or invalid");
				}
			}else{
				if (NullCheckUtils.isNullOrEmpty(username)
						/*|| !saaSAppUtility.validUserAndPassword(username,dob)*/) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
				}
			}
			
			requestHeaderAttributes.setPsuId(username);
			
			Map<String, String> params = new HashMap<String, String>();
			params.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
			params.put(AdapterSecurityConstants.USER_HEADER, username);
			params.put(AdapterSecurityConstants.CHANNELID_HEADER, requestHeaderAttributes.getChannelId().toUpperCase());
			params.put("channel-code", "B365");
			customAuthenticationServiceGetResponse= authenticationGETAdapter.getAuthenticate(params);
		} catch (PSD2AuthenticationException e) {
			throw e;
		}
		catch(PSD2Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getErrorInfo());
		}
		catch(Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getMessage(),SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return customAuthenticationServiceGetResponse;
		
	}
	
	@Override
	public CustomAuthenticationServicePostResponse submitPINDetailsandSelectedDevices(CustomAuthenticationServicePostRequest postRequest){
		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse = new CustomAuthenticationServicePostResponse();
		try {
			if(Boolean.valueOf(sandboxConfig.isSandboxEnabled())){
				if (NullCheckUtils.isNullOrEmpty(postRequest.getDigitalUser().getDigitalUserIdentifier())) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName is blank or invalid");
				}
			}else{
				if (NullCheckUtils.isNullOrEmpty(postRequest.getDigitalUser().getDigitalUserIdentifier())
						/*|| !saaSAppUtility.validUserAndPassword(username,dob)*/) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
				}
			}
			
			requestHeaderAttributes.setPsuId(postRequest.getDigitalUser().getDigitalUserIdentifier());
			
			Map<String, String> params = new HashMap<String, String>();
			//params.put(AdapterSecurityConstants.CHANNELID_HEADER, "B365");
			params.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
			params.put(AdapterSecurityConstants.USER_HEADER, postRequest.getDigitalUser().getDigitalUserIdentifier());
			params.put(AdapterSecurityConstants.CHANNELID_HEADER, requestHeaderAttributes.getChannelId().toUpperCase());
			//loginResponse = authenticationAdapter.authenticate(customDOBPostRequest, params);
			customAuthenticationServicePostResponse= authenticationPOSTAdapter.retrieveAuthenticationService(postRequest, params);
			
			//to be added after list of devices
			SimpleDateFormat sdf = new SimpleDateFormat(PFConstants.DATE_FORMAT);
			String currentDate = sdf.format(new Date());
			//customAuthenticationServicePostResponse.setDropOffRef(saaSAppUtility.dropOff(postRequest.getDigitalUser().getDigitalUserIdentifier(), currentDate));
			
		} catch (PSD2AuthenticationException e) {
			throw e;
		}
		catch(PSD2Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getErrorInfo());
		}
		catch(Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getMessage(),SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return customAuthenticationServicePostResponse;
		
	}
}
