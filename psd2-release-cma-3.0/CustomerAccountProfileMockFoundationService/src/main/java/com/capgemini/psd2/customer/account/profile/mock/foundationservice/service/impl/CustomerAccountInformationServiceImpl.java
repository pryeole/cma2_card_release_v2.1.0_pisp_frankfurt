package com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.ChannelProfile;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.repository.CustomerAccountInformationRepository;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.CustomerAccountInformationService;

@Service
public class CustomerAccountInformationServiceImpl implements CustomerAccountInformationService {
	  @Autowired
		CustomerAccountInformationRepository customerAccountInformationRepository;
	@Override
	public ChannelProfile retrieveCustomerAccountInformation(String id) throws Exception {
	
    ChannelProfile channelProfile = customerAccountInformationRepository.findById(id);
		
		
		if(channelProfile == null)
		{
			throw new RecordNotFoundException("Not Found :"+id);
		}
		return channelProfile;

	}

}
