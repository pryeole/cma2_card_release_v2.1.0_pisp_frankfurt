package com.capgemini.psd2.pisp.payment.setup.test.controller;

//Test Class
import static org.junit.Assert.assertNotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.controller.InternationalPaymentConsentsApiController;
import com.capgemini.psd2.pisp.payment.setup.service.InternationalPaymentConsentsService;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentConsentsApiControllerTest {


	@Mock
	private InternationalPaymentConsentsService service;

	@InjectMocks
	private InternationalPaymentConsentsApiController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testObjectMapper() {
		controller.getObjectMapper();
	}

	@Test
	public void testRequest() {
		controller.getRequest();
	}

	@Test
	public void testCreateInternationalPaymentConsentsResource() throws Exception {
		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		OBWriteInternationalConsent1 consent = new OBWriteInternationalConsent1();

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";
		Mockito.when(service.createInternationalPaymentConsentsResource(request)).thenReturn(response);

		controller.createInternationalPaymentConsents(consent, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);
	}

	@Test
	public void testRetrieveInternationalPaymentConsentsResource() throws Exception {
		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		PaymentRetrieveGetRequest retrieveRequest = new PaymentRetrieveGetRequest();

		retrieveRequest.setConsentId("123456");
		assertNotNull("ConsentId should not be null", retrieveRequest.getConsentId());
		Mockito.when(service.retrieveInternationalPaymentConsentsResource(retrieveRequest)).thenReturn(response);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String consentId = "123456";

		controller.getInternationalPaymentConsentsConsentId(consentId, xFapiFinancialId, authorization, null, null,
				null, null);
	}

	@After
	public void tearDown() {
		service = null;
		controller = null;
	}
}