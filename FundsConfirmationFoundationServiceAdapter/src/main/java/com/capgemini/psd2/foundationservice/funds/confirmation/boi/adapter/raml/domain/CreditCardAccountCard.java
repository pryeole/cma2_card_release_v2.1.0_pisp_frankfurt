package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * CreditCardAccountCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T14:38:09.445+05:30")

public class CreditCardAccountCard   {
  @JsonProperty("customerReference")
  private String customerReference = null;

  @JsonProperty("maskedCardPANNumber")
  private String maskedCardPANNumber = null;

  /**
   * Gets or Sets primaryCardIndicator
   */
  public enum PrimaryCardIndicatorEnum {
    Y("Y"),
    
    N("N");

    private String value;

    PrimaryCardIndicatorEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PrimaryCardIndicatorEnum fromValue(String text) {
      for (PrimaryCardIndicatorEnum b : PrimaryCardIndicatorEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("primaryCardIndicator")
  private PrimaryCardIndicatorEnum primaryCardIndicator = null;

  public CreditCardAccountCard customerReference(String customerReference) {
    this.customerReference = customerReference;
    return this;
  }

  /**
   * Get customerReference
   * @return customerReference
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCustomerReference() {
    return customerReference;
  }

  public void setCustomerReference(String customerReference) {
    this.customerReference = customerReference;
  }

  public CreditCardAccountCard maskedCardPANNumber(String maskedCardPANNumber) {
    this.maskedCardPANNumber = maskedCardPANNumber;
    return this;
  }

  /**
   * Get maskedCardPANNumber
   * @return maskedCardPANNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getMaskedCardPANNumber() {
    return maskedCardPANNumber;
  }

  public void setMaskedCardPANNumber(String maskedCardPANNumber) {
    this.maskedCardPANNumber = maskedCardPANNumber;
  }

  public CreditCardAccountCard primaryCardIndicator(PrimaryCardIndicatorEnum primaryCardIndicator) {
    this.primaryCardIndicator = primaryCardIndicator;
    return this;
  }

  /**
   * Get primaryCardIndicator
   * @return primaryCardIndicator
  **/
  @ApiModelProperty(value = "")


  public PrimaryCardIndicatorEnum getPrimaryCardIndicator() {
    return primaryCardIndicator;
  }

  public void setPrimaryCardIndicator(PrimaryCardIndicatorEnum primaryCardIndicator) {
    this.primaryCardIndicator = primaryCardIndicator;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreditCardAccountCard creditCardAccountCard = (CreditCardAccountCard) o;
    return Objects.equals(this.customerReference, creditCardAccountCard.customerReference) &&
        Objects.equals(this.maskedCardPANNumber, creditCardAccountCard.maskedCardPANNumber) &&
        Objects.equals(this.primaryCardIndicator, creditCardAccountCard.primaryCardIndicator);
  }

  @Override
  public int hashCode() {
    return Objects.hash(customerReference, maskedCardPANNumber, primaryCardIndicator);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreditCardAccountCard {\n");
    
    sb.append("    customerReference: ").append(toIndentedString(customerReference)).append("\n");
    sb.append("    maskedCardPANNumber: ").append(toIndentedString(maskedCardPANNumber)).append("\n");
    sb.append("    primaryCardIndicator: ").append(toIndentedString(primaryCardIndicator)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

