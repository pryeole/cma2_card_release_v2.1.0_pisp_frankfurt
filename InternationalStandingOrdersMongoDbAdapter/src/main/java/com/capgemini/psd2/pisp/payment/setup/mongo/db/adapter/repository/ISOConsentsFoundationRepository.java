package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;

public interface ISOConsentsFoundationRepository extends MongoRepository<CustomIStandingOrderConsentsPOSTResponse, String>  {
	public CustomIStandingOrderConsentsPOSTResponse findOneByDataConsentId(String consentId);
}
