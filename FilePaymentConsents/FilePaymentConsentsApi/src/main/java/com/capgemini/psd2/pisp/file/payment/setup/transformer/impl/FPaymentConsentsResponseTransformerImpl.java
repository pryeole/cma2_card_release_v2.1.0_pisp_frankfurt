package com.capgemini.psd2.pisp.file.payment.setup.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FPaymentConsentsResponseTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class FPaymentConsentsResponseTransformerImpl implements FPaymentConsentsResponseTransformer {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomFilePaymentConsentsPOSTResponse filePaymentConsentsResponseTransformer(
			CustomFilePaymentConsentsPOSTResponse fileConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {

		/*
		 * These fields are expected to already be populated in fileSetupTppResponse
		 * from File Stage Service : Charges, CutOffDateTime, ExpectedExecutionDateTime,
		 * ExpectedSettlementDateTime
		 */

		/* Fields from platform Resource */
		fileConsentsTppResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
		String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
				.valueOf(paymentSetupPlatformResource.getStatus().toUpperCase()).getCompatibleStatus();
		fileConsentsTppResponse.getData().setStatus(OBExternalConsentStatus2Code.fromValue(setupCompatibleStatus));
		fileConsentsTppResponse.getData().setStatusUpdateDateTime(
				NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResource.getStatusUpdateDateTime())
						? paymentSetupPlatformResource.getUpdatedAt()
						: paymentSetupPlatformResource.getStatusUpdateDateTime());

		/*
		 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
		 * to send in response. debtorFlag would be set as Null DebtorAgent is not
		 * available in CMA3
		 */

		String debtorAccountScheme = fileConsentsTppResponse.getData().getInitiation().getDebtorAccount() != null
				? fileConsentsTppResponse.getData().getInitiation().getDebtorAccount().getSchemeName()
				: null;

		if (debtorAccountScheme != null
				&& PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
			fileConsentsTppResponse.getData().getInitiation().getDebtorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
		}

		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()))
			fileConsentsTppResponse.getData().getInitiation().setDebtorAccount(null);

		/*
		 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
		 * TPP.
		 */
		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& !Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			fileConsentsTppResponse.getData().getInitiation().getDebtorAccount().setName(null);

		if (fileConsentsTppResponse.getLinks() == null)
			fileConsentsTppResponse.setLinks(new PaymentSetupPOSTResponseLinks());

		fileConsentsTppResponse.getLinks().setSelf(PispUtilities.populateLinks(
				paymentSetupPlatformResource.getPaymentConsentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (fileConsentsTppResponse.getMeta() == null)
			fileConsentsTppResponse.setMeta(new PaymentSetupPOSTResponseMeta());

		return fileConsentsTppResponse;
	}

	@Override
	public CustomFilePaymentSetupPOSTRequest paymentConsentRequestTransformer(
			CustomFilePaymentSetupPOSTRequest paymentConsentRequest) {

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getInitiation().getRequestedExecutionDateTime()))
			paymentConsentRequest.getData().getInitiation()
					.setRequestedExecutionDateTime(pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getInitiation().getRequestedExecutionDateTime()));

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation()) && !NullCheckUtils
				.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime())) {
			paymentConsentRequest.getData().getAuthorisation()
					.setCompletionDateTime(pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime()));
		}
		return paymentConsentRequest;
	}

}
