package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "standingOrderConsentFoundationResources")
public class CustomDStandingOrderConsentsPOSTResponse extends PaymentDomesticStandingOrder201Response {

	@Id
	@JsonIgnore
	private String id;
	@JsonIgnore
	private Object fraudScore;
	@JsonIgnore
	private OBTransactionIndividualStatus1Code paymentSubmissionStatus;

	private ProcessConsentStatusEnum consentProcessStatus;

	@JsonIgnore
	public OBTransactionIndividualStatus1Code getPaymentSubmissionStatus() {
		return paymentSubmissionStatus;
	}

	public void setPaymentSubmissionStatus(OBTransactionIndividualStatus1Code paymentSubmissionStatus) {
		this.paymentSubmissionStatus = paymentSubmissionStatus;
	}

	@JsonIgnore
	public ProcessConsentStatusEnum getConsentProcessStatus() {
		return consentProcessStatus;
	}

	public void setConsentProcessStatus(ProcessConsentStatusEnum consentProcessStatus) {
		this.consentProcessStatus = consentProcessStatus;
	}

	public String getPaymentSubmissionStatusUpdateDateTime() {
		return paymentSubmissionStatusUpdateDateTime;
	}

	public void setPaymentSubmissionStatusUpdateDateTime(String paymentSubmissionStatusUpdateDateTime) {
		this.paymentSubmissionStatusUpdateDateTime = paymentSubmissionStatusUpdateDateTime;
	}

	private String paymentSubmissionStatusUpdateDateTime;

	@JsonIgnore
	public Object getFraudScore() {
		return fraudScore;
	}

	public void setFraudScore(Object fraudScore) {
		this.fraudScore = fraudScore;
	}

	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
