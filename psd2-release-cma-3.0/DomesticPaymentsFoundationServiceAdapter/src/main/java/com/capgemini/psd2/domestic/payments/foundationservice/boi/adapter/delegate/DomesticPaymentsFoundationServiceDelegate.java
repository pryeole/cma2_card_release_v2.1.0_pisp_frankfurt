package com.capgemini.psd2.domestic.payments.foundationservice.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.domestic.payments.foundationservice.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.domestic.payments.foundationservice.boi.adapter.transformer.DomesticPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;
@Component
public class DomesticPaymentsFoundationServiceDelegate {

	@Autowired
	private DomesticPaymentsFoundationServiceTransformer domesticPaymentsFoundationServiceTransformer;
	
	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;
	
	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;
	
	@Value("${foundationService.partySourceId:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceId;
	
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;
	
	@Value("${foundationService.domesticPaymentSubmissionSetupVersion}")
	private String domesticPaymentSubmissionSetupVersion;
	

	
	public HttpHeaders createPaymentRequestHeaders(CustomPaymentStageIdentifiers customStageIdentifiers,RequestInfo requestInfo ,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(sourcesystem, "PSD2API");
		httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		if (customStageIdentifiers.getPaymentSetupVersion().equalsIgnoreCase("1.0")) {
			httpHeaders.add(systemApiVersion, "1.1");
		} else {
			httpHeaders.add(systemApiVersion, "3.0");
		}
		return httpHeaders;
	}
	
	public String getPaymentFoundationServiceURL(String domesticPaymentBaseURL,String version,String DomesticPaymentId){
		return domesticPaymentBaseURL + "/" + "v1.0"+ "/" + "domestic/payment-instructions" + "/" +  DomesticPaymentId;
	}

	public HttpHeaders createPaymentRequestHeadersPost(RequestInfo requestInfo, Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add(partySourceId, "");
		httpHeaders.add(sourcesystem, "PSD2API");
		
		return httpHeaders;
	}

	public String postPaymentFoundationServiceURL(String domesticPaymentSubmissionBaseURL) {
		
		return domesticPaymentSubmissionBaseURL+ "/" + "v" + domesticPaymentSubmissionSetupVersion + "/" + "domestic/payment-instructions";
	}

	public PaymentInstructionProposalComposite transformDomesticSubmissionResponseFromAPIToFDForInsert(
			CustomDPaymentsPOSTRequest domesticPaymentsRequest, Map<String, String> params) {
		
		return domesticPaymentsFoundationServiceTransformer.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest,params);
	}

	public PaymentDomesticSubmitPOST201Response transformDomesticConsentResponseFromFDToAPIForInsert(
			PaymentInstructionProposalComposite paymentInstructionProposalCompositeResponse,
			Map<String, String> params) {
		
		return domesticPaymentsFoundationServiceTransformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalCompositeResponse,params);
	}

	

}
