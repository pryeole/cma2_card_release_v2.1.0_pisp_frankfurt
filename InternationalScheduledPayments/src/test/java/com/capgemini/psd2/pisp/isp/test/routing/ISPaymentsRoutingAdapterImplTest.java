package com.capgemini.psd2.pisp.isp.test.routing;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.isp.routing.ISPaymentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPaymentsRoutingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private ISPaymentsRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testprocessISPayments() {
		CustomISPaymentsPOSTRequest request = new CustomISPaymentsPOSTRequest();
		Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();

		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		InternationalScheduledPaymentsAdapter paymentsAdapter = new InternationalScheduledPaymentsAdapter() {

			@Override
			public CustomISPaymentsPOSTResponse retrieveStagedISPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentsResponse processISPayments(CustomISPaymentsPOSTRequest customRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(paymentsAdapter);
		adapter.processISPayments(request, paymentStatusMap, params);
	}

	@Test
	public void testRetrieveStagedISPaymentsResponse() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		InternationalScheduledPaymentsAdapter paymentsAdapter = new InternationalScheduledPaymentsAdapter() {

			@Override
			public CustomISPaymentsPOSTResponse retrieveStagedISPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentsResponse processISPayments(CustomISPaymentsPOSTRequest customRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(paymentsAdapter);
		adapter.retrieveStagedISPaymentsResponse(stageIdentifiers, params);
	}

	@Test
	public void testRetrieveStagedISPaymentsResponseNullParams() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = null;
		InternationalScheduledPaymentsAdapter paymentsAdapter = new InternationalScheduledPaymentsAdapter() {

			@Override
			public CustomISPaymentsPOSTResponse retrieveStagedISPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentsResponse processISPayments(CustomISPaymentsPOSTRequest customRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(paymentsAdapter);
		adapter.retrieveStagedISPaymentsResponse(stageIdentifiers, params);
		assertNull(params);
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		applicationContext = null;
		adapter = null;
	}

}
