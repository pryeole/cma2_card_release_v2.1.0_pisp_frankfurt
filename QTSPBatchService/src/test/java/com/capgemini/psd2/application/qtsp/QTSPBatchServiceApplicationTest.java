package com.capgemini.psd2.application.qtsp;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPBatchServiceApplicationTest {

	@InjectMocks
	private QTSPBatchServiceApplication qtspBatchServiceApp;

	@Mock
	private RestClientSync restClientSync;

	@Mock
	private EurekaClient eurekaClient;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void performTest_success() {
		Application app = new Application("qtsp");
		when(eurekaClient.getApplication(any(String.class))).thenReturn(app);
		when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class)))
		.thenReturn("SUCCESS");
		qtspBatchServiceApp.perform();
	}

	@Test
	public void performTest_null() {
		when(eurekaClient.getApplication("qtspService")).thenReturn(null);
		qtspBatchServiceApp.perform();
	}
}
