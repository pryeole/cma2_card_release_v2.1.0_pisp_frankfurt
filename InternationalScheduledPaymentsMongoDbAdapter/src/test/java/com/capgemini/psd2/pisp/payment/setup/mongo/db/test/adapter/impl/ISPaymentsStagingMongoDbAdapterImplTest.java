package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.ISPaymentsStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPaymentsStagingMongoDbAdapterImplTest {

	@Mock
	private ISPaymentsFoundationRepository repository;

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private ISPaymentsStagingMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessISPaymentsMultiAuthorised() {
		CustomISPaymentsPOSTRequest request = ISPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(CustomISPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processISPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessISPaymentsAwaitingFurtherAuthorisation() {
		CustomISPaymentsPOSTRequest request = ISPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);

		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(CustomISPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processISPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessISPaymentsAuthRejected() {

		CustomISPaymentsPOSTRequest request = ISPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(CustomISPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processISPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessISPaymentsNullAuthorisation() {
		CustomISPaymentsPOSTRequest request = ISPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.FAIL;

		OBMultiAuthorisation1 multiAuthorisation = null;

		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(CustomISPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processISPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessISPaymentsNullSubmissionId() {
		CustomISPaymentsPOSTRequest request = ISPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = null;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(CustomISPaymentsPOSTResponse.class))).thenReturn(response);

		adapter.processISPayments(request, new HashMap<>(), new HashMap<>());
		assertNull(processStatus);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessISPayments_DataAccessResourceFailureException() {
		CustomISPaymentsPOSTRequest request = ISPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(CustomISPaymentsPOSTResponse.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		adapter.processISPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testRetrieveStagedISPaymentsResponse() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomISPaymentsPOSTResponse response = ISPaymentsMongoDbAdapterMockData.getResponse();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);

		when(repository.findOneByDataInternationalScheduledPaymentId(anyString())).thenReturn(response);

		adapter.retrieveStagedISPaymentsResponse(stageIdentifiers, new HashMap<>());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedISPaymentsResponse_Exception() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomISPaymentsPOSTResponse response = ISPaymentsMongoDbAdapterMockData.getResponse();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);

		when(repository.findOneByDataInternationalScheduledPaymentId(anyString())).thenReturn(response);

		adapter.retrieveStagedISPaymentsResponse(stageIdentifiers, new HashMap<>());
	}
	
	@Test
	public void createstage_Test_CIExRate() {

		CustomISPaymentsPOSTRequest Request = new CustomISPaymentsPOSTRequest();
		OBWriteDataInternationalScheduled1 data = new OBWriteDataInternationalScheduled1();
		Request.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);

		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		adapter.processISPayments(Request, new HashMap<>(), new HashMap<>());

	}
	
	@Test
	public void createstage_Test_CIExRate_NonMatching() {

		CustomISPaymentsPOSTRequest Request = new CustomISPaymentsPOSTRequest();
		OBWriteDataInternationalScheduled1 data = new OBWriteDataInternationalScheduled1();
		Request.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		adapter.processISPayments(Request, new HashMap<>(), new HashMap<>());
	}


	@After
	public void tearDown() {
		repository = null;
		utility = null;
		reqAttributes = null;
		adapter = null;
	}
}
