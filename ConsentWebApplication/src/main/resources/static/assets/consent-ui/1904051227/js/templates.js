(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/account-access.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-12 account-heading">\n' +
    '            <h3 class="accountSel" ng-bind="accountSelText"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '\n' +
    '    <!-- page level errors STARTS-->\n' +
    '    <div class="row error-data" ng-if="errorData">\n' +
    '        <div class="col-md-12">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="row mobile-section-btn" ng-if="returnTppbtn">\n' +
    '        <div class="col-md-12 btn-section">\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="returnToThirdparty()" aria-label="{{returntotppbtn}}"\n' +
    '                ng-bind="returntotppbtn"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page level errors ENDS-->\n' +
    '\n' +
    '    <!--Hiding contents on page level error-->\n' +
    '    <div ng-hide="errorData">\n' +
    '        <!-- page description STARTS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12 page-title-description">\n' +
    '                <p class="page-desc">\n' +
    '                    <span ng-bind="preUserTitleText"></span>\n' +
    '                    <span class="tpp-name" ng-bind="tppInfo"></span>\n' +
    '                    <span ng-bind="postUserTitleText"></span>\n' +
    '                </p>\n' +
    '                <p class="acct-description" ng-bind-html="accountSelDescriptionText"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row redirction-btn">\n' +
    '            <div class="col-md-12">\n' +
    '                <a href ng-click="scrollRedirect($event,\'requested-transaction\')" id="view-permission" class="permission-btn pull-left">\n' +
    '                    <span ng-bind="viewReqstPerBtn"></span>\n' +
    '                    <span aria-label="{{viewRequestedButtonTitle}}"></span>\n' +
    '                </a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '        <!-- page description ENDS-->\n' +
    '        <!--list of accounts table ENDS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12">\n' +
    '                <div class="table-header-container">\n' +
    '                    <h4 class="table-header" ng-bind="selAccountText"></h4>\n' +
    '                    <button type="button" ng-show="!isAllAccountSelected" role="button" class="btn btn-default  accountselectionBtn" ng-disabled="!selectAllAccBtnEnabled"\n' +
    '                        ng-click=\'accountToggleAll(true,$event)\' ng-model="isAllAccountSelected" ng-bind="selectAllBtn"></button>\n' +
    '                    <button type="button" ng-show="isAllAccountSelected" role="button" class="btn btn-default  accountselectionBtn" ng-click=\'accountToggleAll(false,$event)\'\n' +
    '                        ng-attr-aria-hidden="{{isAllAccountSelected?\'false\':\'true\'}}" ng-bind="selectNoneBtn"></button>\n' +
    '                </div>\n' +
    '                <div class="info-restricted-acct" id="find-link">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind="notSeeingText"></span>\n' +
    '                        <a class="find-link" href ng-click="scrollRedirect($event,\'important-info-section\')">\n' +
    '                            <span ng-bind-html="findOutWhyText"></span>\n' +
    '                            <span aria-label="{{findOutWhyTitle}}"></span>\n' +
    '                        </a>\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '                <div class="account-Details-section">\n' +
    '                    <!--desktop view STARTS-->\n' +
    '                    <div class="table-view ">\n' +
    '                        <table class="table table-responsive" id="table-data">\n' +
    '                            <caption class="sr-only" ng-bind="selAccountTableCaptionText"></caption>\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col">\n' +
    '                                        <div class="checkbox">\n' +
    '                                            <input type="checkbox" id="checkall" ng-disabled="!selectAllAccBtnEnabled" ng-click="accountToggleAll(isAllAccountSelected,$event)"\n' +
    '                                                ng-model="isAllAccountSelected" />\n' +
    '                                            <label for="checkall" class="text-o" ng-bind="selAllText"> </label>\n' +
    '                                        </div>\n' +
    '                                    </th>\n' +
    '                                    <th scope="col" ng-bind="nickNameText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctNoText"> </th>\n' +
    '                                    <th scope="col" ng-bind="currencyText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctTypeText"></th>\n' +
    '                                    <th scope="col" ng-bind="balanceText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse" ng-class-even="{even:true}" ng-class-odd="{odd:true}"\n' +
    '                                    ng-class="{\'acct-disable\' : acct.accountDisabled }">\n' +
    '                                    <td>\n' +
    '                                        <div class="checkbox" ng-show="!acct.accountDisabled">\n' +
    '                                            <input type="checkbox" ng-click="selectedAccountList()" ng-model="acct.selected" aria-label="{{acct.Account.Identification | maskNumber:maskAccountNumberLength}}"\n' +
    '                                                id="{{acct.HashedValue}}" />\n' +
    '                                            <label class="text-o" for="{{acct.HashedValue}}" ng-bind="checkboxScrLabel"></label>\n' +
    '                                        </div>\n' +
    '                                        <div class="disable-account" ng-show="acct.accountDisabled">\n' +
    '                                            <a href="javascript:void(0)" role="button" ng-click="openJointAcctPopup()" ng-attr-aria-label="{{acctNoText }} {{acct.Account.Identification  | maskNumber:maskAccountNumberLength}} {{ disabledPopoverScrMsg}}">\n' +
    '                                                <i class="fa fa-question-circle"></i>\n' +
    '                                            </a>\n' +
    '                                        </div>\n' +
    '                                    </td>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountSubType"></td>\n' +
    '                                    <td ng-bind=""></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <!--desktop view ENDS-->\n' +
    '\n' +
    '                </div>\n' +
    '                <div ng-if="AccountsByPage[currentPage].length === pageSize" class="not-seeing-info">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind="notSeeingText"></span>\n' +
    '                        <a class="find-link" href ng-click="scrollRedirect($event,\'important-info-section\')">\n' +
    '                            <span ng-bind-html="findOutWhyText"></span>\n' +
    '                            <span aria-label="{{findOutWhyTitle}}"></span>\n' +
    '                        </a>\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '                <acct-pagination acct-page="pageSize" max-size="maxPaginationSize" force-ellipse="true" rotate="false" current-page="currentPage"\n' +
    '                    filtered-accounts="allAccounts" accounts-by-page="AccountsByPage" ng-show="allAccounts.length > pageSize"></acct-pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <!--requested transaction access STARTS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12 access-date-header">\n' +
    '                <h4 class="access-date-title" ng-init="ACCESS_DATE=(\'AISP.ACCOUNT_SELECTION_PAGE.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER\'|translate)"\n' +
    '                    ng-bind="ACCESS_DATE"> </h4>\n' +
    '            </div>\n' +
    '            <div class="col-md-12 transaction-details-container">\n' +
    '                <div class="col-md-6 col-sm-12 access-date-col">\n' +
    '                    <div class="access-date-label">\n' +
    '                        <span class="boi-input" ng-bind="fromText"> </span>\n' +
    '                    </div>\n' +
    '                    <div class="access-date-value">\n' +
    '                        <span class="boi-input" ng-bind="fromDate? (fromDate | date: \'dd MMMM yyyy\') : (\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL\'| translate)">\n' +
    '                        </span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 col-sm-12 access-date-col access-date-col-right">\n' +
    '                    <div class="access-date-label">\n' +
    '                        <span class="boi-input" ng-bind="toText"></span>\n' +
    '                    </div>\n' +
    '                    <div class="access-date-value">\n' +
    '                        <span class="boi-input" ng-bind="tillDate? (tillDate | date: \'dd MMMM yyyy\') : (\'AISP.REVIEW_CONFIRM_PAGE.TRANSACTION_PERMISSION.NO_CONSENT_DATE_LABEL\'| translate)">\n' +
    '                        </span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--requested transaction access ENDS-->\n' +
    '\n' +
    '        <!--consent validity STARTS-->\n' +
    '        <div class="row consent-validity-row">\n' +
    '            <div class="">\n' +
    '                <div class="box-container consent-validity-section">\n' +
    '                    <span class="boi-input" ng-bind="expiryDate ? (preConsentValidityText+(expiryDate | date: \'dd MMMM yyyy\')+postConsentValidityText) : noConsentValidityText"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <!--consent validity ENDS-->\n' +
    '\n' +
    '        <!-- button section STARTS-->\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <!-- important info section STARTS-->\n' +
    '        <div class="col-md-4 col-lg-4 pull-right">\n' +
    '            <a href ng-click="scrollRedirect($event,\'find-link\')" class="back-to-top-btn">\n' +
    '                <span ng-bind="backToTopBtn"></span>\n' +
    '                <span aria-label="{{backToTopBtnNotSeeingAcctTitle}}"></span>\n' +
    '                <i class="fa fa-chevron-up"></i>\n' +
    '            </a>\n' +
    '        </div>\n' +
    '\n' +
    '        <div class="row important-subinfo" id="important-info-section">\n' +
    '            <h4 class="access-date-title" ng-bind="infoMainHeading"></h4>\n' +
    '            <div class="sub-imp-info" ng-repeat="subHdata in subHeadingInfo" ng-if="!$last">\n' +
    '                <h5 class="subheading" ng-bind="subHdata.heading" ng-if="subHdata.heading !== \'\'"></h5>\n' +
    '                <p class="subinfo-para" ng-repeat="supPara in subHdata.para track by $index" ng-bind-html="supPara" ng-if="supPara"></p>\n' +
    '                <ul class="subinfo-list">\n' +
    '                    <li ng-repeat="subList in subHdata.list track by $index" ng-bind-html="subList" ng-if="subList"></li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!-- important info section ENDS-->\n' +
    '\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <!--list of permissions STARTS-->\n' +
    '        <div class="row permission-list-wrap">\n' +
    '            <div class="">\n' +
    '                <div class="row boi-box-header">\n' +
    '                    <div class="col-md-8 col-lg-8">\n' +
    '                        <h4 class="access-date-title" ng-init="PERMISSION_TITLE=(\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_HEADER\'|translate)"\n' +
    '                            ng-bind="PERMISSION_TITLE"> </h4>\n' +
    '                    </div>\n' +
    '                    <div id="requested-transaction" class="col-md-4 col-lg-4">\n' +
    '                        <a href ng-click="scrollRedirect($event,\'view-permission\')" class="back-to-top-btn">\n' +
    '                            <span ng-bind="backToTopBtn"></span>\n' +
    '                            <span aria-label="{{backToTopBtnVewRequestPermissionTitle}}"></span>\n' +
    '                            <i class="fa fa-chevron-up"></i>\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row box-container">\n' +
    '                    <ul class="permission-list">\n' +
    '                        <li ng-repeat="i in [0,1,2,3,4]" ng-switch="(\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+pl+ \'_ELEMENTS\' | uppercase | translate).length">\n' +
    '                            <div ng-switch-when="0">\n' +
    '                                <span class="list-group-item consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.\'+pl| uppercase)|translate)"\n' +
    '                                    ng-bind="perm_list"> </span>\n' +
    '                            </div>\n' +
    '                            <div ng-switch-default>\n' +
    '                                <div ng-if="permissionExists[i]">\n' +
    '                                    <custom-uib-accordian status="status" heading="i" pl="permissionListData"></custom-uib-accordian>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                    <a href ng-click="scrollRedirect($event,\'view-permission\')" class="back-to-top-btn">\n' +
    '                        <span ng-bind="backToTopBtn"></span>\n' +
    '                        <span aria-label="{{backToTopBtnVewRequestPermissionTitle}}"></span>\n' +
    '                        <i class="fa fa-chevron-up"></i>\n' +
    '                    </a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--list of permissions ENDS-->\n' +
    '</section>\n' +
    '\n' +
    '<!--Page pop up START-->\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<!--Page pop up END-->\n' +
    '\n' +
    '<!--Page footer START-->\n' +
    '<div class="boi-aisp-consent-footer" ng-class="{\'footerAddMargin\':!isDataFound, \'footerAddMarginWhenError\':errorMessage && !callFromTppButton}">\n' +
    '    <aisp-page-footer></aisp-page-footer>\n' +
    '</div>\n' +
    '<!--Page footer END-->\n' +
    '\n' +
    '<!-- Fixed button section START-->\n' +
    '<div class="row content-container mobile-section-btn boi-fixed-btn-container" ng-hide="isDataFound">\n' +
    '    <div class="container">\n' +
    '        <div class="col-md-12 error-box-wrap" ng-if="errorMessage">\n' +
    '            <error-notice error-data="errorMessage"></error-notice>\n' +
    '        </div>\n' +
    '        <div class="col-md-12 fixed-button-wrap">\n' +
    '            <button ng-hide="isDataFound" class="btn btn-primary pull-right" ng-disabled="(!selAccounts.length) && (!selAccounts.account)"\n' +
    '                ng-click="allowSubmission()" ng-bind="authoriseAcctAcessBtn" aria-label="{{authoriseAccessActionText}}">\n' +
    '            </button>\n' +
    '            <a ng-if="retry" ng-href="{{retry}}" class="btn btn-primary pull-right" ng-bind="rtrBtn"> </a>\n' +
    '            <span ng-hide="isAccSelected" class="account-select-info-text" ng-bind="AccountSelectInfoText"></span>\n' +
    '            <span ng-show="isAccSelected" class="account-selected-info-text">\n' +
    '                <span class="account-select-info-left" ng-bind="AccountSelectedInfoText"></span>\n' +
    '                <span class="account-select-info-right" ng-bind="AccountSelectCounterText"></span>\n' +
    '            </span>\n' +
    '            <button class="btn btn-secondary pull-left cancelBtn" ng-click="openDenyModal()" aria-label="{{cancelBtnPopText}}" ng-bind="denyBtn">\n' +
    '            </button>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!-- Fixed button section ENDS-->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/acct-header.html',
    '<!--header start-->\n' +
    '<header role="banner">\n' +
    ' <div class="header-container">\n' +
    '    <div class=" container header-inner-container">\n' +
    '        <a href="#" class="logo-container">\n' +
    '                <img class="logo" ng-src={{logoUrl}} title="{{bankLogoImgAlt}}" alt="{{bankLogoImgAlt}}" ng-if="bankLogoImgAlt.length"/>        \n' +
    '        </a>\n' +
    '        <div class="portal-details">\n' +
    '            <h1 ng-bind="accountAccessText"> </h1>\n' +
    '            <h2 ng-bind="thirdPartyText"></h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    ' </div>\n' +
    '</header>\n' +
    '<!--header end-->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/acct-pagination.html',
    '<div role="navigation" class="pagination-section"  aria-label="{{\'AISP.ACCOUNT_SELECTION_PAGE.PAGINATION.SELECT_ACCOUNT_PAGINATION_SCREENREADER_LABEL\'|translate}}">\n' +
    '    <ul uib-pagination ng-class="{\'border-chrome45\': applyChromeBorder}" \n' +
    '        total-items="totalAccounts" ng-model="currentPage" \n' +
    '        template-url="views/pagination.html" \n' +
    '        items-per-page="acctPage" max-size="maxSize" \n' +
    '        force-ellipses="forceEllipse" rotate="false"\n' +
    '       >\n' +
    '    </ul>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/aisp-authorisation-renewal.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-12 account-heading">\n' +
    '            <h3 class="accountSel" ng-bind="accountRenewalText"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '\n' +
    '    <!-- page error STARTS-->\n' +
    '    <div class="row" ng-if="errorData">\n' +
    '        <div class="col-md-12 renewal-error">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page error ENDS-->\n' +
    '    <div class="row mobile-section-btn" ng-if="returnTppbtn">\n' +
    '        <div class="col-md-12 btn-section">\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="returnToThirdparty()" aria-label="{{cancelBtnPopText}}"\n' +
    '                ng-bind="returntotppbtn"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <!--Hiding contents on page level error-->\n' +
    '    <div ng-hide="errorData">\n' +
    '        <!-- page description STARTS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12">\n' +
    '                <p class="page-description renewal-info">\n' +
    '                    <span class="acct-description" ng-bind-html="instructionTextPart1"></span>\n' +
    '                    <span class="tpp-name" ng-bind="tppInfo"></span>\n' +
    '                    <span class="acct-description" ng-bind-html="instructionTextPart2"></span>\n' +
    '                    <span class="tpp-name" ng-bind="tppInfo"></span>\n' +
    '                    <span class="acct-description" ng-bind-html="instructionTextPart3"></span>\n' +
    '                    <span class="acct-desc-topspace" ng-if="instructionTextPart4 !== \'\'">\n' +
    '                        <span class="acct-description" ng-bind-html="instructionTextPart4"></span>\n' +
    '                        <span class="tpp-name text-capitalize" ng-bind="tppInfo"></span>\n' +
    '                        <span class="acct-description" ng-bind-html="instructionTextPart5"></span>\n' +
    '                    </span>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row redirction-btn">\n' +
    '            <div class="col-md-12">\n' +
    '                <a href ng-click="scrollRedirect($event,\'requested-transaction\')" id="view-permission" class="permission-btn pull-left">\n' +
    '                    <span ng-bind="viewReqstPerBtn"></span>\n' +
    '                    <span aria-label="{{viewRequestedButtonTitle}}"></span>\n' +
    '                </a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '        <!-- page description ENDS-->\n' +
    '\n' +
    '        <!--confirm selected accounts table STARTS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12">\n' +
    '                <div class="table-header-container">\n' +
    '                    <h4 class="table-header" ng-bind="selectedAccountText"> </h4>\n' +
    '                </div>\n' +
    '                <div class="details-container" ng-class="selectedAcctTableData.length > totalNumberVisibleRows? \'review-card-details\': \'\' ">\n' +
    '                    <div class="account-table pisp-account-review-table">\n' +
    '                        <table class="table table-responsive">\n' +
    '                            <caption class="sr-only" ng-bind="selectedRenewAuthAccountTableCaptionText"></caption>\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col" ng-bind="nickNameText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctNoText"> </th>\n' +
    '                                    <th scope="col" ng-bind="currencyText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctTypeText"></th>\n' +
    '                                    <th scope="col" ng-bind="balanceText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage] " ng-class="$odd ? \'even\' : \'odd\'">\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountSubType"></td>\n' +
    '                                    <td ng-bind=""></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '                <acct-pagination acct-page="pageSize" max-size="maxPaginationSize" force-ellipse="true" rotate="false" current-page="currentPage"\n' +
    '                    filtered-accounts="allAccounts" accounts-by-page="AccountsByPage" ng-show="allAccounts.length > pageSize"></acct-pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--confirm selected accounts table ENDS-->\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '        <!--requested transaction access STARTS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12 access-date-header">\n' +
    '                <h4 class="access-date-title" ng-bind="transactionAccessDateHeader"> </h4>\n' +
    '            </div>\n' +
    '            <div class="col-md-12 transaction-details-container">\n' +
    '                <div class="col-md-6 col-sm-12 access-date-col">\n' +
    '                    <div class="access-date-label">\n' +
    '                        <span class="boi-input" ng-bind="fromText"> </span>\n' +
    '                    </div>\n' +
    '                    <div class="access-date-value">\n' +
    '                        <span class="boi-input" ng-bind="fromDate? (fromDate | date: \'dd MMMM yyyy\') : noConsentLabel">\n' +
    '                        </span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 col-sm-12 access-date-col access-date-col-right">\n' +
    '                    <div class="access-date-label">\n' +
    '                        <span class="boi-input" ng-bind="toText"></span>\n' +
    '                    </div>\n' +
    '                    <div class="access-date-value">\n' +
    '                        <span class="boi-input" ng-bind="tillDate? (tillDate | date: \'dd MMMM yyyy\') : noConsentLabel">\n' +
    '                        </span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--requested transaction access ENDS-->\n' +
    '\n' +
    '        <!--consent validity STARTS-->\n' +
    '        <div class="row consent-validity-row">\n' +
    '            <div class="box-container consent-validity-section">\n' +
    '                <span class="boi-input" ng-if="expiryDate" ng-bind="preTillDateLabel">&nbsp;</span>\n' +
    '                <span class="boi-input date-data" ng-bind="expiryDate ? (expiryDate | date:\'dd MMMM yyyy\') : expiryInvalidDateLabel"></span>\n' +
    '                <span class="boi-input" ng-if="expiryDate" ng-bind="postTillDateLabel"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--consent validity ENDS-->\n' +
    '\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '        <!-- important info section STARTS-->\n' +
    '\n' +
    '        <div class="row important-subinfo">\n' +
    '            <h4 class="access-date-title" ng-bind="infoMainHeading"></h4>\n' +
    '            <div class="sub-imp-info" ng-repeat="subHdata in subHeadingInfo" ng-if="!$last">\n' +
    '                <h5 class="subheading" ng-bind="subHdata.heading" ng-if="subHdata.heading !== \'\'"></h5>\n' +
    '                <p class="subinfo-para" ng-repeat="supPara in subHdata.para track by $index" ng-bind-html="supPara" ng-if="supPara"></p>\n' +
    '                <ul class="subinfo-list">\n' +
    '                    <li ng-repeat="subList in subHdata.list track by $index" ng-bind-html="subList" ng-if="subList"></li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!-- important info section ENDS-->\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '        <!--list of permissions STARTS-->\n' +
    '        <div class="row permission-list-wrap">\n' +
    '            <div class="">\n' +
    '                <div class="row boi-box-header">\n' +
    '                    <div class="col-md-8 col-lg-8">\n' +
    '                        <h4 class="access-date-title" ng-bind="permissionsListHeaderText"> </h4>\n' +
    '                    </div>\n' +
    '                    <div id="requested-transaction" class="col-md-4 col-lg-4">\n' +
    '                        <a href ng-click="scrollRedirect($event,\'view-permission\')" class="back-to-top-btn">\n' +
    '                            <span ng-bind="backToTopBtn"></span>\n' +
    '                            <span aria-label="{{backToTopBtnNotSeeingAcctTitle}}"></span>\n' +
    '                            <i class="fa fa-chevron-up"></i>\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row box-container">\n' +
    '                    <ul class="permission-list">\n' +
    '                        <li ng-repeat="i in [0,1,2,3,4]" ng-switch="(\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+pl+ \'_ELEMENTS\' | uppercase | translate).length">\n' +
    '                            <div ng-switch-when="0">\n' +
    '                                <span class="list-group-item consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.\'+pl| uppercase)|translate)"\n' +
    '                                    ng-bind="perm_list"> </span>\n' +
    '                            </div>\n' +
    '                            <div ng-switch-default>\n' +
    '                                <div ng-if="permissionExists[i]">\n' +
    '                                    <custom-uib-accordian status="status" heading="i" pl="permissionListData"></custom-uib-accordian>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                    <a href ng-click="scrollRedirect($event,\'view-permission\')" class="back-to-top-btn">\n' +
    '                        <span ng-bind="backToTopBtn"> </span>\n' +
    '                        <span aria-label="{{backToTopBtnNotSeeingAcctTitle}}"></span>\n' +
    '                        <i class="fa fa-chevron-up"></i>\n' +
    '                    </a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<!--list of permissions ENDS-->\n' +
    '<!--Page modal START-->\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<!--Page modal END-->\n' +
    '\n' +
    '<!--Page footer START-->\n' +
    '<div class="boi-pisp-footer boi-renwal-footer" ng-class="{\'footerAddMargin\':!isDataFound, \'footerAddMarginWhenError\':errorMessage && !callFromTppButton, \'footerRenewalError\': errorData}">\n' +
    '    <aisp-page-footer></aisp-page-footer>\n' +
    '</div>\n' +
    '<!--Page footer END-->\n' +
    '\n' +
    '<!-- button section STARTS-->\n' +
    '<div class="row content-container mobile-section-btn boi-fixed-btn-container" ng-hide="errorData">\n' +
    '    <div class="container">\n' +
    '        <div class="col-md-12 error-box-wrap" ng-if="errorMessage">\n' +
    '            <error-notice error-data="errorMessage"></error-notice>\n' +
    '        </div>\n' +
    '        <div class="col-md-12 fixed-button-wrap">\n' +
    '            <button role="button" class="btn btn-primary pull-right" ng-click="allowSubmission()" ng-hide="errorData"> \n' +
    '                <sapn ng-bind="allowAuthorisationButtonLabel"></sapn>\n' +
    '                <span aria-label="{{allowBtnScrlabel}}"></span>\n' +
    '            </button>\n' +
    '            <button role="button" class="btn btn-secondary pull-left cancelBtn" ng-click="openModal()" aria-label="{{\'AISP.AUTHORISATION_RENEWAL_PAGE.BUTTONS.PRESS_DENY_SCREENREADER_LABEL\'| translate}}"\n' +
    '                ng-bind="denyAuthorisationButtonLabel">\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!-- button section ENDS-->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/aisp-footer.html',
    '<!--footer start-->\n' +
    '<footer role="contentinfo">\n' +
    '    <div class="row footer-container">\n' +
    '        <div class="col-md-6 col-lg-7 boi-footer-text">\n' +
    '            <p ng-init="REGULATORY_TEXT=(\'AISP.FOOTER.REGULATORY_LABEL\'| translate)"ng-bind="REGULATORY_TEXT |uppercase"></p>\n' +
    '        </div>\n' +
    '        <div class="col-md-6 col-lg-5 boi-footer-nav">\n' +
    '            <ul class="footer-navigation">\n' +
    '                <li>\n' +
    '                    <a href="{{aboutUsUrl}}" target=\'_blank\'>\n' +
    '                        <span ng-bind="aboutUsText" title="{{tooltipTt}}"></span>\n' +
    '                        <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li>\n' +
    '                    <a href="{{privacyPolicyUrl}}" target=\'_blank\'>\n' +
    '                        <span ng-bind="cookieText" title="{{tooltipTt}}"></span>\n' +
    '                        <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                    </a> \n' +
    '                </li>\n' +
    '                <li>\n' +
    '                    <a href="{{tncUrl}}" target=\'_blank\'>\n' +
    '                        <span ng-bind="tncText" title="{{tooltipTt}}"></span>\n' +
    '                        <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                    </a> \n' +
    '                </li>\n' +
    '                <li>\n' +
    '                    <a href="{{helpUrl}}" target=\'_blank\'>\n' +
    '                       <span ng-bind="helpText" title="{{tooltipTt}}"></span>\n' +
    '                       <span class="sr-only" aria-label="{{tooltipTt}}"></span>                        \n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</footer> \n' +
    '<!--footer end -->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/cancel-confirm-popup.html',
    '<div class="modal-header pop-confirm-header title-header">\n' +
    '    <button type="button" aria-expanded="false" aria-label="close" role="button" class="close" ng-click="cancelNo()" data-dismiss="modal">\n' +
    '         <i class="fa fa-close"></i> \n' +
    '    </button>\n' +
    '    <h2 class="boi-widget"> {{\'CANCEL_POPUP_HEADER\' | translate}} </h2>         \n' +
    '</div>\n' +
    '<div class="modal-body pop-confirm-body">\n' +
    '    <div class="confirm-msg">\n' +
    '        <p translate="CANCEL_POPUP_BODY.0" class="boi-input text-center"><p>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<div class="modal-footer pop-confirm-footer button-section">\n' +
    '        <button class="btn btn-primary pull-right col-xs-12 col-sm-1 col-md-1 secondary" ng-click="canecelYes()"> {{\'YES_BUTTON\' | translate}}  </button>\n' +
    '        <button type="button" class="btn btn-secondary col-xs-12 col-sm-1 col-md-1 primary" ng-click="cancelNo()" data-dismiss="modal"> {{\'NO_BUTTON\' | translate}} </button>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/cisp-account.html',
    '<section class="container main-container">\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-12 page-header">\n' +
    '            <h2 class="text-capitalize header-title-txt-clr">{{\'BANK_TITLE\' | translate}}</h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container" ng-if="errorMsg">\n' +
    '        <div class="col-xs-10 col-sm-10 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row shadow-container">\n' +
    '                <div class="col-xs-12 col-sm-12 col-md-12 content tpp-info">\n' +
    '                    <h3 class="text-capitalize" ng-class "infoMsg?\'text-info\':\'text-danger\'">{{errorMsg | translate}}</h3>\n' +
    '                    <h5 ng-if="correlationId" class="text-capitalize text-danger">{{ \'ERROR_REFERENCE_LABEL\' | translate}} <strong> {{correlationId}}</strong></h5>\n' +
    '                    <a ng-if="aispUrl" ng-href="{{aispUrl}}" class="go-back pull-right"><strong class="text-capitalize">{{ \'GO_BACK_TO\' | translate}} <u> {{aispName}} </u></strong></a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row shadow-container ">\n' +
    '                <h5 class="main-header"> {{ \'PRE_USER_TITLE_TEXT\' | translate }} {{ tppInfo }} {{ \'POST_USER_TITLE_TEXT\' | translate }}</h5>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row title-header acct-header-txt-clr acct-header-bg hidden-md hidden-lg hidden-sm">\n' +
    '                <h2 class=""> {{\'SEL_ACCOUNT_TITLE\' | translate}} </h2>\n' +
    '            </div>\n' +
    '            <div class="row shadow-container account-select">\n' +
    '                <div class="account-search-box">\n' +
    '                    <label class="sr-only" for="srch-term">{{\'SEARCH\' | translate}}</label>\n' +
    '                    <input type="text" class="form-control" value="search" placeholder="Search" name="srch-term" id="srch-term" ng-model="searchText" ng-change="search()">\n' +
    '                    <button class="btn btn-default btn-primary-bg btn-primary-txt-clr search-icon glyphicon glyphicon-search"><span class="sr-only">search</span></button>\n' +
    '                </div>\n' +
    '\n' +
    '                <div class="account-Details-section">\n' +
    '                    <div class="table-view hidden-xs">\n' +
    '                        <table class="table table-responsive">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th><label class="text-o">radio</label></th>\n' +
    '                                    <th scope="col"> {{\'NICK_NAME\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_NUMBER\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'CURRENCY\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_TYPE\' | translate}} </th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse">\n' +
    '                                    <td>\n' +
    '                                        <div class="radio">\n' +
    '                                            <input type="radio" ng-model="selAccounts.account" ng-value="acct" name="singleSelectAccount" id="{{acct.accountNumber}}" value="acct" />\n' +
    '                                            <label class="text-o" for="{{acct.accountNumber}}">table data</label>\n' +
    '                                        </div>\n' +
    '                                    </td>\n' +
    '                                    <td>{{acct.nickname}}</td>\n' +
    '                                    <td>{{acct.accountNumber}}</td>\n' +
    '                                    <td>{{acct.currency}}</td>\n' +
    '                                    <td>{{acct.accountType}}</td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="div-view hidden-md hidden-lg hidden-sm">\n' +
    '                        <div class="col-xs-12 acc-cont" ng-repeat="acct in AccountsByPage[currentPage]  | orderBy:columnToOrder:reverse">\n' +
    '                            <div class="radio">\n' +
    '                                <input type="radio" ng-model="selAccounts.account" ng-value="acct" name="singleSelectAccountMobile" id="{{acct.accountNumber}}_id" value="acct" />\n' +
    '                                <label for="{{acct.accountNumber}}_id"> {{acct.accountNumber}}  </label>\n' +
    '                            </div>\n' +
    '                            <p ng-show="acct.selected"> {{\'NICK_NAME\' | translate}} : {{acct.nickname}} </p>\n' +
    '                            <p ng-show="acct.selected"> {{\'CURRENCY\' | translate}} : {{acct.currency}} </p>\n' +
    '                            <p ng-show="acct.selected"> {{\'ACCOUNT_TYPE\' | translate}} : {{acct.accountType}} </p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <acct-pagination acct-page="pageSize" max-size="maxPaginationSize" force-ellipse="true" rotate="false" current-page="currentPage" filtered-accounts="filteredAccounts" accounts-by-page="AccountsByPage"></acct-pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="row content-container" ng-if="selAccounts.length || selAccounts.account">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 nopadding">\n' +
    '            <div class="row title-header acct-header-txt-clr acct-header-bg">\n' +
    '                <h2 class=""> {{\'SELECT_ACCOUNT_TITLE\' | translate}} </h2>\n' +
    '            </div>\n' +
    '            <div class="row shadow-container ">\n' +
    '                <div class="selected-account-Details-section">\n' +
    '                    <div class="table-view-selected-account hidden-xs">\n' +
    '                        <table class="table table-responsive">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col"> {{\'NICK_NAME\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_NUMBER\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'CURRENCY\' | translate}} </th>\n' +
    '                                    <th scope="col"> {{\'ACCOUNT_TYPE\' | translate}} </th>\n' +
    '                                    <th scope="col" class="sr-only">remove</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <!--	<tr ng-repeat="acct in psuAcct2">-->\n' +
    '                                <tr ng-repeat="acct in selAccounts track by $index">\n' +
    '                                    {{ $index }}\n' +
    '                                    <td>{{acct.nickname}}</td>\n' +
    '                                    <td>{{acct.accountNumber}}</td>\n' +
    '                                    <td>{{acct.currency}}</td>\n' +
    '                                    <td>{{acct.accountType}}</td>\n' +
    '                                    <td>\n' +
    '                                        <a href="#" class="glyphicon glyphicon-remove remove-icon" ng-click="removeAccount(acct,$event)"></a>\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="div-view-selected-account hidden-md hidden-lg hidden-sm">\n' +
    '                        <div class="col-xs-12 acc-cont" ng-repeat="acct in selAccounts track by $index">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <p> {{\'NICK_NAME\' | translate}} : {{acct.nickname}} </p>\n' +
    '                                <p> {{\'ACCOUNT_NUMBER\' | translate}} : {{acct.accountNumber}} </p>\n' +
    '                                <p> {{\'CURRENCY\' | translate}} : {{acct.currency}} </p>\n' +
    '                                <p> {{\'ACCOUNT_TYPE\' | translate}} : {{acct.accountType}} </p>\n' +
    '                                <button class="btn-secondary-bg btn-secondary-txt-clr btn-secondary-border-clr btn btn-secondary btn-inline text-upper-case col-xs-12" ng-click="removeAccount(acct,$event)"> {{\'DELETE_BUTTON\' | translate}}  </button>\n' +
    '                            </div>\n' +
    '\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row content-container">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 text-right nopadding btn-area">\n' +
    '            <button class="btn-primary-bg btn-primary-txt-clr btn-primary-border-clr btn btn-primary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 secondary" ng-disabled="(!selAccounts.length) && (!selAccounts.account)" ng-click="goPreview()"> {{\'NEXT\' | translate}}  </button>\n' +
    '            <button class="btn-secondary-bg btn-secondary-txt-clr btn-secondary-border-clr btn btn-secondary pull-right text-upper-case col-xs-12 col-sm-1 col-md-1 primary"> {{\'CANCEL\' | translate}} </button>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/customUibAccordian.html',
    '<uib-accordion>\n' +
    '        <div uib-accordion-group class="permission-data" is-open="status.open" ng-keypress="toggleDisable($event)">\n' +
    '            <uib-accordion-heading>\n' +
    '                <i class="fa" ng-class="{\'fa-minus-circle\': status.open, \'fa-plus-circle\': !status.open}"></i>\n' +
    '                <span ng-if="heading===0" ng-attr-aria-expanded="{{status.open?\'true\':\'false\'}}" class="consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.HEADING1\'  | uppercase)|translate)" ng-bind="perm_list"> </span>\n' +
    '                <span ng-if="heading===1" ng-attr-aria-expanded="{{status.open?\'true\':\'false\'}}" class="consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.HEADING2\'  | uppercase)|translate)" ng-bind="perm_list">  </span>\n' +
    '                <span ng-if="heading===2" ng-attr-aria-expanded="{{status.open?\'true\':\'false\'}}" class="consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.HEADING3\'  | uppercase)|translate)" ng-bind="perm_list">  </span>\n' +
    '                <span ng-if="heading===3" ng-attr-aria-expanded="{{status.open?\'true\':\'false\'}}" class="consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.HEADING4\'  | uppercase)|translate)" ng-bind="perm_list">  </span>\n' +
    '                <span ng-if="heading===4" ng-attr-aria-expanded="{{status.open?\'true\':\'false\'}}" class="consent-header boi-accname" ng-init="perm_list=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_LABEL.HEADING5\'  | uppercase)|translate)" ng-bind="perm_list">  </span>\n' +
    '                <!-- <i class="pull-right fa" ng-class="{\'fa-chevron-up\': status.open, \'fa-chevron-down\': !status.open}"></i> -->\n' +
    '            </uib-accordion-heading>\n' +
    '    \n' +
    '            \n' +
    '            <div ng-repeat="p in pl">\n' +
    '                <div ng-if="heading===0">\n' +
    '                    <div ng-if="p===\'ReadAccountsBasic\' || p===\'ReadAccountsDetail\' || p===\'ReadBalances\' || p===\'ReadPAN\'   ">\n' +
    '                        <span  id="collapsAccrd" class="hidden-md hidden-lg hidden-sm boi-input-sm" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                        <span id="collapsAccrd" class="hidden-xs boi-input-placeholder" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '    \n' +
    '                <div ng-if="heading===1">\n' +
    '                    <div ng-if="p===\'ReadBeneficiariesBasic\' || p===\'ReadBeneficiariesDetail\' || p===\'ReadStandingOrdersBasic\' || p===\'ReadStandingOrdersDetail\' || p===\'ReadDirectDebits\' || p===\'ReadScheduledPaymentsBasic\' || p===\'ReadScheduledPaymentsDetail\' ">\n' +
    '                        <span id="collapsAccrd" class="hidden-md hidden-lg hidden-sm boi-input-sm" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                        <span id="collapsAccrd" class="hidden-xs boi-input-placeholder" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '    \n' +
    '                <div ng-if="heading===2">\n' +
    '                    <div ng-if="p===\'AllReadTransactionsBasic\' || p===\'ReadTransactionsCreditsBasic\' || p===\'ReadTransactionsDebitsBasic\' || p===\'AllReadTransactionsDetail\' || p===\'ReadTransactionsCreditsDetail\' || p===\'ReadTransactionsDebitsDetail\'   ">\n' +
    '                        <span id="collapsAccrd" class="hidden-md hidden-lg hidden-sm boi-input-sm" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                        <span id="collapsAccrd" class="hidden-xs boi-input-placeholder" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '    \n' +
    '                <div ng-if="heading===3">\n' +
    '                    <div ng-if="p===\'ReadStatementsBasic\' || p===\'ReadStatementsDetail\'">\n' +
    '                        <span id="collapsAccrd" class="hidden-md hidden-lg hidden-sm boi-input-sm" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                        <span id="collapsAccrd" class="hidden-xs boi-input-placeholder" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '    \n' +
    '                <div ng-if="heading===4">\n' +
    '                    <div ng-if="p===\'ReadProducts\'">\n' +
    '                        <span id="collapsAccrd" class="hidden-md hidden-lg hidden-sm boi-input-sm" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                        <span id="collapsAccrd" class="hidden-xs boi-input-placeholder" ng-init="pl_elm=((\'AISP.REVIEW_CONFIRM_PAGE.PERMISSIONS.PERMISSION_DESCRIPTIONS.\'+p+\'_ELEMENTS\' | uppercase)|translate)"\n' +
    '                            ng-bind="pl_elm"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </uib-accordion>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/error-notice.html',
    '<div class="alert page-alert" role="alert" aria-atomic="true" ng-class="errorCssClass">\n' +
    '    <p>\n' +
    '        <span role="alert" class="alert-prefix" ng-bind="alertText"></span>\n' +
    '        <span role="alert" class="alert-suffix" ng-bind-html="errorMsgText"></span>\n' +
    '        <ng-template ng-if="errorData.correlationId">\n' +
    '            <span class="alert-prefix" ng-bind="corRelationText"> </span>\n' +
    '            <span class="alert-suffix" ng-bind="corRelationMsgText"></span>\n' +
    '        </ng-template>\n' +
    '    </p>\n' +
    '    <p ng-if="isErrorMsgTextDescription" ng-bind-html="errorMsgTextDescription"> </p>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/funds-check.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container cisp-section" ng-hide="sessiontimeoutflag" ng-class="{\'cispsectionmargin\': isDataFound}">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div id="payment-section" class="col-md-12 account-heading">\n' +
    '            <h3 ng-bind="accountSelText"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '\n' +
    '    <!-- page errors STARTS-->\n' +
    '    <div class="row error-data content-container" ng-if="errorData">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-12 nopadding">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row" ng-if="returnTppbtn">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-12">\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="returnToThirdparty()" aria-label="{{cancelBtnPopText}}"\n' +
    '                ng-bind="returntotppbtn"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page errors ENDS-->\n' +
    '\n' +
    '    <div ng-hide="isDataFound">\n' +
    '        <!-- page description STARTS-->\n' +
    '        <div class="row page-desc-blk">\n' +
    '            <div class="col-md-12 page-title-description">\n' +
    '                <p class="page-desc">\n' +
    '                    <span ng-bind="preUserTitleText"></span>\n' +
    '                    <span class="tpp-name" ng-bind="tppInfo"></span>\n' +
    '                    <span ng-bind="postUserTitleText"></span>\n' +
    '                </p>\n' +
    '                <p class="acct-description" ng-bind-html="accountSelDescriptionText"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!-- page description ENDS-->\n' +
    '\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <div class="row content-container payment-details-row">\n' +
    '            <!--<div class="col-md-12">-->\n' +
    '            <div class="table-header-container">\n' +
    '                <h4 class="access-date-title" ng-bind="acctDetailText"> </h4>\n' +
    '            </div>\n' +
    '            <!-- Account details section Start -->\n' +
    '            <div class="row payment-details-wrap" ng-if="ibanNsc">\n' +
    '                <div class="col-md-6 payment-details-col payment-details-col-left">\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="acctNameText"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="accountName"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 payment-details-col payment-details-col-right">\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="ibanNscText"></div>\n' +
    '                        <div class="payment-section-value">\n' +
    '                            <span ng-bind="accountNo"></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row payment-details-wrap" ng-if="pan">\n' +
    '                <div class="col-md-6 payment-details-col payment-details-col-left">\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="acctNameText"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="accountName"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 payment-details-col payment-details-col-right">\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="panText"></div>\n' +
    '                        <div class="payment-section-value">\n' +
    '                            <span ng-bind="accountNo"></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <!-- Account details section End -->\n' +
    '        </div>\n' +
    '\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <div class="row" id="transfer-limit-section">\n' +
    '            <div class="col-md-8">\n' +
    '                <h3 ng-bind="consentExpirationHeaderText" class="access-date-title transfer-limit-heading"></h3>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="col-md-12">\n' +
    '                <p class="consent-validity-section" ng-if="expiryDate">\n' +
    '                    <span ng-bind="consentExpirationDesc1Text"></span>\n' +
    '                    <span ng-bind="expiryDate | date: \'dd MMMM yyyy\'"></span>\n' +
    '                    <span ng-bind-html="consentExpirationDesc2Text"></span>\n' +
    '                </p>\n' +
    '                <p class="consent-validity-section" ng-if="expiryDate == \'\' || expiryDate == null || expiryDate == undefined">\n' +
    '                    <span ng-bind-html="consentExpirationDescWithoutDate"></span>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <div class="col-md-4 col-lg-4 pull-right">\n' +
    '            <a href ng-click="scrollRedirect($event,\'payment-section\')" class="back-to-top-btn">\n' +
    '                <span ng-bind="backToTopBtn"></span>\n' +
    '                <span aria-label="{{backToTopBtnForFundCheck}}"></span>\n' +
    '                <i class="fa fa-chevron-up"></i>\n' +
    '            </a>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '\n' +
    '            <div class="col-md-8 col-sm-8 col-xs-8">\n' +
    '                <h3 ng-bind="cutoffTimeHeaderText" class="access-date-title cutoff-limit-heading">\n' +
    '                </h3>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="col-md-12 col-sm-12 col-xs-12 please-note-section">\n' +
    '                <p>\n' +
    '                    <span ng-bind="cutoffTimeDesc1Text"></span>\n' +
    '                    <span ng-bind="tppName"></span>\n' +
    '                    <span ng-bind="cutoffTimeDesc2Text"></span>\n' +
    '                    <span ng-bind="cutoffTimeDesc3Text"></span>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<div class="boi-cisp-footer" ng-class="{\'footerAddMargin\':!isDataFound, \'footerAddMarginWhenError\':errorMessage}">\n' +
    '    <aisp-page-footer></aisp-page-footer>\n' +
    '</div>\n' +
    '<!-- button section STARTS-->\n' +
    '<div class="row content-container mobile-section-btn boi-fixed-btn-container" ng-hide="isDataFound">\n' +
    '\n' +
    '    <div class="container">\n' +
    '        <div class="col-md-12 fixed-confirmation-box">\n' +
    '            <div class="confirmation-box-wrap" ng-class="[{confirmationBoxCheck : confirmThirdParty}]">\n' +
    '                <label ng-bind="confirmationForThirdParty" for="confirmationCheck"></label>\n' +
    '                <div class="confirmThirdParty-checkbox">\n' +
    '                    <input id="confirmationCheck" type="checkbox" name="confirm-third-party" ng-model="confirmThirdParty">\n' +
    '                    <span class="custom-check"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <div class="col-md-12 error-box-wrap" ng-if="errorMessage">\n' +
    '            <error-notice error-data="errorMessage"></error-notice>\n' +
    '        </div>\n' +
    '\n' +
    '        <div class="col-md-12 fixed-button-wrap">\n' +
    '            <button ng-hide="isDataFound" class="btn btn-primary pull-right" ng-disabled="!confirmThirdParty" ng-click="authorisePaymentDetails()"\n' +
    '                ng-bind="contBtn" aria-controls="LiveRegion"> </button>\n' +
    '            <button class="btn btn-secondary pull-left cancelBtn" ng-click="openCancelModal()" aria-label="{{\'CISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL\'| translate}}"\n' +
    '                ng-bind="cancelBtn"> </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!-- button section ENDS-->');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/loading-spinner.html',
    '<div class="block-ui-overlay"></div>\n' +
    '<div class="block-ui-message-container" aria-live="assertive" aria-atomic="true">\n' +
    '	<div class="block-ui-message" ng-class="$_blockUiMessageClass">\n' +
    '		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
    '	</div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/modalPopUp.html',
    '<div role="dialog" aria-modal="true">\n' +
    '    <div ng-if="modal.modelpopupType == \'cancelpopup\'">\n' +
    '        <div role="document">\n' +
    '            <div class="modal-header">\n' +
    '                <span class="sr-only" ng-bind="modal.cancelRequestScrLabel"></span>\n' +
    '                <span class="boi-widget title-text" id="modal_title" ng-bind="modal.modelpopupTitle"></span>\n' +
    '                <a autofocus tabindex="0" type="button" role="button" class="btn-close pull-right" ng-click="cancelBtnClicked()">\n' +
    '                    <span class="sr-only" ng-bind="modal.closebtn"></span>\n' +
    '                    <i class="fa fa-close close-cancel-popup-btn" aria-hidden="true" title="{{modal.closebtn}}"></i>\n' +
    '                </a>\n' +
    '            </div>\n' +
    '            <div class="modal-body pop-confirm-body">\n' +
    '                <div class="confirm-msg">\n' +
    '                    <p class="text-center" ng-bind="modal.modelpopupBodyContent"> </p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '                <div class="text-center">\n' +
    '                    <button role="button" ng-show="modal.btn.okbtn.visible" class="btn btn-primary pull-right" ng-click="okBtnClicked()">\n' +
    '                        <span ng-bind="modal.btn.okbtn.label"></span>\n' +
    '                        <span aria-label="{{modal.yesBtnScrLabel}}"></span>\n' +
    '                    </button>\n' +
    '                    <button role="button" ng-show="modal.btn.cancelbtn.visible" class="btn btn-secondary pull-left" ng-click="cancelBtnClicked()" data-dismiss="modal">\n' +
    '                        <span ng-bind="modal.btn.cancelbtn.label"></span>\n' +
    '                        <span class="sr-only" aria-label="{{modal.noBtnScrLabel}}"></span>\n' +
    '                    </button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div ng-if="modal.modelpopupType == \'sessionTimeOutpopup\'">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="modal.sessionTimeoutScrLabel"></span>\n' +
    '            <!-- <button type="button " class="close " aria-hidden="true "  ng-click="sessionOutClicked() ">&times;</button>-->\n' +
    '            <span class="title-text" id="modal_title" ng-bind=" modal.modelpopupTitle"> </span>\n' +
    '        </div>\n' +
    '        <div class="modal-body pop-confirm-body">\n' +
    '            <div class="confirm-msg">\n' +
    '                <p class="text-center" ng-bind="modal.modelpopupBodyContent"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '            <div class="text-center">\n' +
    '                <button role="button" ng-show="modal.btn.okbtn.visible " class="btn btn-primary pull-right" ng-click="okBtnClicked()" ng-bind="modal.btn.okbtn.label" autofocus> </button>\n' +
    '                <button role="button" ng-show="modal.btn.cancelbtn.visible" class="btn btn-secondary pull-left" ng-click="cancelBtnClicked()"\n' +
    '                    data-dismiss="modal">\n' +
    '                    <span ng-bind=" modal.btn.cancelbtn.label"></span>\n' +
    '                    <span class="sr-only" aria-label="{{modal.noBtnScrLabel}}"></span>\n' +
    '                </button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div ng-if="modal.modelpopupType == \'denypopup\'">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="modal.cancelRequestScrLabel"></span>\n' +
    '            <span class="title-text" id="modal_title" ng-bind="modal.modelpopupTitle"></span>\n' +
    '            <a tabindex="0" type="button" role="button" class="btn-close pull-right close-deny-modal" ng-click="cancelBtnClicked()" autofocus>\n' +
    '                <span class="sr-only" ng-bind="modal.closebtn"></span>\n' +
    '                <i class="fa fa-close" aria-hidden="true" title="{{modal.closebtn}}"></i>\n' +
    '            </a>\n' +
    '        </div>\n' +
    '        <div class="modal-body pop-confirm-body">\n' +
    '            <div class="confirm-msg">\n' +
    '                <p class="text-center" ng-bind="modal.modelpopupBodyContent"> </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '            <div class="text-center">\n' +
    '                <button role="button" ng-show="modal.btn.okbtn.visible" class="btn btn-primary pull-right" ng-click="okBtnClicked()">\n' +
    '                    <span ng-bind=" modal.btn.okbtn.label"></span>\n' +
    '                    <span class="sr-only" aria-label="{{modal.yesBtnScrLabel}}"></span>\n' +
    '                </button>\n' +
    '                <button role="button" ng-show="modal.btn.cancelbtn.visible" class="btn btn-secondary pull-left" ng-click="cancelBtnClicked()" data-dismiss="modal">\n' +
    '                    <span ng-bind="modal.btn.cancelbtn.label"></span>\n' +
    '                    <span class="sr-only" aria-label="{{modal.noBtnScrLabel}}"></span>\n' +
    '                </button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<div aria-modal="true" role="dialog">\n' +
    '    <div ng-if="modal.modelpopupType == \'authorisepopup\'">\n' +
    '        <div class="authorisepopup">\n' +
    '            <div role="document">\n' +
    '                <span class="sr-only" ng-bind="modal.authoriseModalOpenScrLabel"></span>\n' +
    '                <div class="modal-logo" ng-bind="modal.modalBankLogoImgAlt"></div>\n' +
    '                <div class="success-modal-header">\n' +
    '                    <span id="modal_title" ng-bind="modal.modelpopupTitle"></span>\n' +
    '                </div>\n' +
    '                <div class="success-modal-body">\n' +
    '                    <p ng-bind="modal.modelpopupBodyContent1"> </p>\n' +
    '                    <p class="model-note" ng-if="modal.modelpopupBodyContent2" ng-bind="modal.modelpopupBodyContent2"> </p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div ng-if="modal.modelpopupType == \'jointAccInfo\'" class="red-modal">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="modal.openjointActScrLabel"></span>\n' +
    '            <!-- <button type="button " class="close " aria-hidden="true "  ng-click="sessionOutClicked() ">&times;</button>-->\n' +
    '            <span class="title-text" id="modal_title" ng-bind="modal.modelpopupTitle"> </span>\n' +
    '        </div>\n' +
    '        <div class="modal-body pop-confirm-body">\n' +
    '            <div class="confirm-msg">\n' +
    '                <p class="text-center" ng-bind-html="modal.modelpopupBodyContent"></p>\n' +
    '                <p class="find-out-more">\n' +
    '                    <a href=\'{{modal.jointAcctHelpLink}}\' target=\'_blank\'>\n' +
    '                        <span ng-bind="modal.jointAcctFindOutLink" title="{{modal.jointActLinkLabel}}"></span>\n' +
    '                        <span class="sr-only" ng-bind="modal.jointActLinkLabel"></span>\n' +
    '                    </a>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '            <div class="text-center">\n' +
    '                <button role="button" ng-show="modal.btn.okbtn.visible " class="btn btn-primary" ng-click="cancelBtnClicked()" ng-bind="modal.btn.okbtn.label"\n' +
    '                    aria-lebel="" autofocus> </button>\n' +
    '                <span class="sr-only" aria-label="{{modal.noBtnScrLabel}}"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/pagination.html',
    '<li role="menuitem" ng-if="::boundaryLinks" ng-class="{boi_widget_sm: noPrevious()||ngDisabled}" class="pagination-first">\n' +
    '    <a role="button" href="/first" ng-click="selectPage(1, $event)" ng-disabled="noPrevious()||ngDisabled" uib-tabindex-toggle>{{::getText(\'first\')}}</a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-if="::directionLinks" ng-class="{boi_widget_sm_disable: noPrevious()||ngDisabled,boi_widget_sm_active: !noPrevious()&&!ngDisabled}"\n' +
    '    class="pagination-prev">\n' +
    '    <a role="button" href="/previous" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()||ngDisabled" aria-label="Previous"\n' +
    '        uib-tabindex-toggle>\n' +
    '        <i class="fa fa-chevron-left" aria-hidden="true"></i>\n' +
    '    </a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-repeat="page in pages track by $index" ng-class="{boi_widget_sm_blue: page.active,boi_widget_sm: !ngDisabled&&!page.active}"\n' +
    '    class="pagination-page">\n' +
    '    <a role="button" href="/selectpage" ng-click="selectPage(page.number, $event)" ng-disabled="ngDisabled&&!page.active" id="{{page.text}}"\n' +
    '        uib-tabindex-toggle ng-bind="page.text"></a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-if="::directionLinks" ng-class="{boi_widget_sm_disable: noNext()||ngDisabled,boi_widget_sm_active: !noNext()&&!ngDisabled}"\n' +
    '    class="pagination-next">\n' +
    '    <a role="button" href="/next" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()||ngDisabled" aria-label="Next"\n' +
    '        uib-tabindex-toggle>\n' +
    '        <i class="fa fa-chevron-right" aria-hidden="true"></i>\n' +
    '    </a>\n' +
    '</li>\n' +
    '<li role="menuitem" ng-if="::boundaryLinks" ng-class="{boi_widget_sm: noNext()||ngDisabled}" class="pagination-last">\n' +
    '    <a role="button" href="/last" ng-click="selectPage(totalPages, $event)" ng-disabled="noNext()||ngDisabled" uib-tabindex-toggle>{{::getText(\'last\')}}</a>\n' +
    '</li>');
}]);
})();

(function(module) {
try {
  module = angular.module('consentPartials');
} catch (e) {
  module = angular.module('consentPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/pisp-account.html',
    '<page-header></page-header>\n' +
    '<section class="container main-container pisp-section" ng-hide="sessiontimeoutflag">\n' +
    '    <!-- page title STARTS-->\n' +
    '    <div class="row">\n' +
    '        <div id="payment-section" class="col-md-12 account-heading">\n' +
    '            <h3 ng-bind="accountSelText"></h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page title ENDS-->\n' +
    '\n' +
    '    <!-- page errors STARTS-->\n' +
    '    <div class="row error-data content-container" ng-if="errorData">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-12 nopadding">\n' +
    '            <error-notice error-data="errorData"></error-notice>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row" ng-if="returnTppbtn">\n' +
    '        <div class="col-xs-12 col-sm-12 col-md-12">\n' +
    '            <button role="button" class="btn btn-secondary pull-left" ng-click="returnToThirdparty()" aria-label="{{cancelBtnPopText}}"\n' +
    '                ng-bind="returntotppbtn"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- page errors ENDS-->\n' +
    '    \n' +
    '    <!--Hiding contents on page level error-->\n' +
    '    <div ng-hide="isDataFound || errorData">\n' +
    '        <!-- page description STARTS-->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12 page-title-description">\n' +
    '                <p ng-if="!isAccPreSelected && psuAcct.Data.Account.length !== 1" class="main-header page-description" ng-bind-html="paymentCheckTextForAccounts"></p>\n' +
    '                <p ng-if="isAccPreSelected || psuAcct.Data.Account.length === 1" class="main-header page-description" ng-bind-html="paymentCheckText"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!-- page description ENDS-->\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <!-- Payee details table starts-->\n' +
    '        <div class="row content-container payment-details-row">\n' +
    '            <!--<div class="col-md-12">-->\n' +
    '            <div class="table-header-container">\n' +
    '                <h2 class="access-date-title" ng-bind="paymentInfoText"> </h2>\n' +
    '            </div>\n' +
    '            <!-- New payment details section Start -->\n' +
    '            <div class="row payment-details-wrap">\n' +
    '                <div class="col-md-6 payment-details-col payment-details-col-left">\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="paymtIntByText"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="payInstBy?payInstBy:noDataText"></div>\n' +
    '                    </div>\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="paymentBicNsc"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="bic?bic:noDataText"></div>\n' +
    '                    </div>\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="PayAmountText"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="amount?(amount | isoCurrency:currency):noDataText"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 payment-details-col payment-details-col-right">\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="payNameText"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="payeeName?payeeName:noDataText"></div>\n' +
    '                    </div>\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="paymentIbanAcc"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="accountNo?accountNo:noDataText"></div>\n' +
    '                    </div>\n' +
    '                    <div class="payment-inner-row">\n' +
    '                        <div class="payment-section-label" ng-bind="payRefText"></div>\n' +
    '                        <div class="payment-section-value" ng-bind="payeeReference?payeeReference:noDataText"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="schedule-payment-details" ng-if="schedulePayment">\n' +
    '                <div class="info-restricted-acct">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind-html="futureDatePaymentText"></span>\n' +
    '                </div>\n' +
    '                <div class="row payment-details-wrap">\n' +
    '                    <div class="col-md-6 payment-details-col payment-details-col-left">\n' +
    '                        <div class="payment-inner-row">\n' +
    '                            <div class="payment-section-label" ng-bind="paymentDateText"></div>\n' +
    '                            <div ng-if="paymentScheduleDate" class="payment-section-value" ng-bind="paymentScheduleDate | date: \'dd-MM-yyyy\'"></div>\n' +
    '                            <div ng-if="!paymentScheduleDate" class="payment-section-value" ng-bind="noDataText"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="schedule-payment-details" ng-if="standingPayment">\n' +
    '                <div class="info-restricted-acct">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind-html="standingOrderPaymentText"></span>\n' +
    '                </div>\n' +
    '                <div class="row payment-details-wrap">\n' +
    '                    <div class="col-md-6 payment-details-col payment-details-col-left">\n' +
    '                        <div class="payment-inner-row">\n' +
    '                            <div class="payment-section-label" ng-bind="firstPaymentText"></div>\n' +
    '                            <div ng-if="firstPaymentDate" class="payment-section-value" ng-bind="firstPaymentDate | date: \'dd-MM-yyyy\'"></div>\n' +
    '                            <div ng-if="!firstPaymentDate" class="payment-section-value" ng-bind="noDataText"></div>\n' +
    '                        </div>\n' +
    '\n' +
    '                        <div class="payment-inner-row">\n' +
    '                            <div class="payment-section-label" ng-bind="lastPaymentText"></div>\n' +
    '                            <div ng-if="finalPaymentDate" class="payment-section-value" ng-bind="finalPaymentDate | date: \'dd-MM-yyyy\'"></div>\n' +
    '                            <div ng-if="!finalPaymentDate" class="payment-section-value" ng-bind="noDataText"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-md-6 payment-details-col payment-details-col-right">\n' +
    '                        <div class="payment-inner-row">\n' +
    '                            <div class="payment-section-label" ng-bind="paymentFrequencyText"></div>\n' +
    '                            <div class="payment-section-value" ng-bind="paymentFrequency?paymentFrequency:noDataText"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!-- Payee details table ENDS-->\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <!-- NEW SELECT ACCT SECTION STARTED -->\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12" ng-if="!isAccPreSelected && psuAcct.Data.Account.length !== 1">\n' +
    '                <div class="table-header-container">\n' +
    '                    <h2 class="table-header" ng-bind="selAccountText"></h2>\n' +
    '                </div>\n' +
    '                <div class="info-restricted-acct" id="find-link">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind="notSeeingText"></span>\n' +
    '                        <a class="find-link" href ng-click="scrollRedirect($event,\'important-info-section\')">\n' +
    '                            <span ng-bind="findoutWhyLink"></span>\n' +
    '                            <span aria-label="{{findoutWhyScrText}}"></span>\n' +
    '                        </a>\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '                <div class="account-Details-section">\n' +
    '                    <!--desktop view STARTS-->\n' +
    '                    <div class="table-view">\n' +
    '                        <table class="table table-responsive" id="table-data">\n' +
    '                            <caption class="sr-only" ng-bind="selAccountTableCaptionText"></caption>\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col">\n' +
    '                                        <span aria-hidden="true" class="sr-only" ng-bind="nickNameText"></span>\n' +
    '                                    </th>\n' +
    '                                    <th scope="col" ng-bind="nickNameText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctNoText"> </th>\n' +
    '                                    <th scope="col" ng-bind="currencyText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctTypeText"></th>\n' +
    '                                    <th scope="col" ng-bind="balanceText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage] ">\n' +
    '                                    <td>\n' +
    '                                        <label class="radio-container" for="{{acct.HashedValue}}">\n' +
    '                                            <span aria-hidden="true" class="sr-only" ng-bind="nickNameText"></span>\n' +
    '                                            <input type="radio" name="acct-select" aria-label="{{acct.Account.Identification | maskNumber:maskAccountNumberLength}}"\n' +
    '                                                ng-model="acct.selected" ng-click="selectAccount(acct)" id="{{acct.HashedValue}}">\n' +
    '                                            <span class="radio-selected"></span>\n' +
    '                                        </label>\n' +
    '                                    </td>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountSubType"></td>\n' +
    '                                    <td ng-bind=""></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <!--desktop view ENDS-->\n' +
    '\n' +
    '                </div>\n' +
    '                <div ng-if="AccountsByPage[currentPage].length === pageSize" class="not-seeing-info">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind="notSeeingText"></span>\n' +
    '                        <a class="find-link" href ng-click="scrollRedirect($event,\'important-info-section\')">\n' +
    '                            <span ng-bind="findoutWhyLink"></span>\n' +
    '                            <span aria-label="{{findoutWhyScrText}}"></span>\n' +
    '                        </a>\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '\n' +
    '                <acct-pagination acct-page="pageSize" max-size="maxPaginationSize" force-ellipse="true" rotate="false" current-page="currentPage"\n' +
    '                    filtered-accounts="accData" accounts-by-page="AccountsByPage" ng-show="accData.length > pageSize">\n' +
    '                </acct-pagination>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="col-md-12" ng-if="isAccPreSelected || psuAcct.Data.Account.length === 1">\n' +
    '                <div id="selected-payment-section" class="table-header-container">\n' +
    '                    <h4 class="table-header" ng-bind="paymentAcctTxt"></h4>\n' +
    '                </div>\n' +
    '                <div class="info-restricted-acct">\n' +
    '                    <p>\n' +
    '                        <span ng-if="psuAcct.Data.Account.length === 1 && !isAccPreSelected" ng-bind="paymentAcctIntroOneAcct"></span>\n' +
    '                        <span ng-if="isAccPreSelected" ng-bind-html="paymentIntroTxtPreSelect"></span>\n' +
    '                        <span ng-if="isAccPreSelected" ng-bind-html="tppInfo"></span>\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '                <div class="account-Details-section">\n' +
    '                    <!--desktop view STARTS-->\n' +
    '                    <div class="table-view">\n' +
    '                        <table class="table table-responsive" id="table-data">\n' +
    '                            <caption class="sr-only" ng-bind="selAccountTableCaptionText"></caption>\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th scope="col">\n' +
    '                                        <span aria-hidden="true" class="sr-only" ng-bind="nickNameText"></span>\n' +
    '                                    </th>\n' +
    '                                    <th scope="col" ng-bind="nickNameText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctNoText"> </th>\n' +
    '                                    <th scope="col" ng-bind="currencyText"></th>\n' +
    '                                    <th scope="col" ng-bind="acctTypeText"></th>\n' +
    '                                    <th scope="col" ng-bind="balanceText"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="acct in AccountsByPage[currentPage] ">\n' +
    '                                    <td></td>\n' +
    '                                    <td ng-bind="acct.Nickname"></td>\n' +
    '                                    <td ng-bind="acct.Account.Identification | maskNumber:maskAccountNumberLength"></td>\n' +
    '                                    <td ng-bind="acct.Currency"></td>\n' +
    '                                    <td ng-bind="acct.AccountSubType"></td>\n' +
    '                                    <td ng-bind=""></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <!--desktop view ENDS-->\n' +
    '\n' +
    '                </div>\n' +
    '                <div class="info-restricted-acct cutoff-transfer-link" id="find-link">\n' +
    '                    <span>\n' +
    '                        <i class="fa fa-question-circle questn-icon"></i>\n' +
    '                        <span class="not-seeing" ng-bind-html="infoOfCutoffAndTransLimit"></span>\n' +
    '                        <a class="find-link" href ng-click="scrollRedirect($event,\'important-info-section\')">\n' +
    '                            <span ng-bind="infoOfCutoffAndTransLimitLinkTxt"></span>\n' +
    '                            <span aria-label="{{transferLmtScreenReadTxt}}"></span>\n' +
    '                        </a>\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <hr class="dotted-line"></hr>\n' +
    '\n' +
    '        <div class="row restrict-account-import">\n' +
    '            <div class="col-md-8" id="important-info-section">\n' +
    '                <h2 ng-bind="importantSectionHeading" class="access-date-title transfer-limit-heading"></h2>\n' +
    '            </div>\n' +
    '            <div class="col-md-4 col-lg-4">\n' +
    '                <a href ng-if="!isAccPreSelected && psuAcct.Data.Account.length > 1" ng-click="scrollRedirect($event,\'payment-section\')"\n' +
    '                    class="back-to-top-btn">\n' +
    '                    <span ng-bind="backToTopBtn"></span>\n' +
    '                    <span aria-label="{{backToTopBtnLbl}}"></span>\n' +
    '                    <i class="fa fa-chevron-up"></i>\n' +
    '                </a>\n' +
    '                <a href ng-if="isAccPreSelected || psuAcct.Data.Account.length === 1" ng-click="scrollRedirect($event,\'selected-payment-section\')"\n' +
    '                    class="back-to-top-btn">\n' +
    '                    <span ng-bind="backToTopBtn"></span>\n' +
    '                    <span aria-label="{{backToTopBtnLblForPreAcc}}"></span>\n' +
    '                    <i class="fa fa-chevron-up"></i>\n' +
    '                </a>\n' +
    '            </div>\n' +
    '            <div class="col-md-8">\n' +
    '                <h2 ng-bind="restrictedAccountHeaderText" class="restricted-account-heading transfer-limit-heading"></h2>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="col-md-12">\n' +
    '                <p ng-bind-html="restrictedAccountDesc1Text">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-8">\n' +
    '                <h2 ng-bind="transferLimitHeaderText" class="restricted-account-heading transfer-limit-heading"></h2>\n' +
    '            </div>\n' +
    '            <div class="col-md-12 transfer-limit-section-PISP">\n' +
    '                <p ng-bind-html="transferLimitDesc1Text">\n' +
    '                </p>\n' +
    '                <p ng-bind-html="transferLimitDesc2Text">\n' +
    '                </p>\n' +
    '                <p ng-bind-html="transferLimitDesc3Text">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <div class="row cutoff-times-dec">\n' +
    '            <div class="col-md-12">\n' +
    '                <h2 ng-bind="cutoffTimeHeaderText" class="restricted-account-heading cutoff-limit-heading">\n' +
    '                </h2>\n' +
    '                <p ng-bind="cutoffTimeDesc1Text">\n' +
    '                </p>\n' +
    '                <p ng-bind="cutoffTimeDesc2Text">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<!--Page modal START-->\n' +
    '<model-pop-up modal="modelPopUpConf"></model-pop-up>\n' +
    '<!--Page modal END-->\n' +
    '\n' +
    '<!--Page footer START-->\n' +
    '<div class="boi-pisp-footer" ng-class="{\'footerAddMargin\':!isDataFound, \'footerAddMarginWhenError\':errorMessage && !returnToThirdparty}">\n' +
    '    <aisp-page-footer></aisp-page-footer>\n' +
    '</div>\n' +
    '<!--Page footer END-->\n' +
    '\n' +
    '<!-- button section STARTS-->\n' +
    '<div class="row content-container mobile-section-btn boi-fixed-btn-container" ng-hide="isDataFound">\n' +
    '    <div class="container">\n' +
    '        <div class="col-md-12 error-box-wrap" ng-if="errorMessage">\n' +
    '            <error-notice error-data="errorMessage"></error-notice>\n' +
    '        </div>\n' +
    '        <div class="col-md-12 fixed-button-wrap">\n' +
    '            <button ng-hide="isDataFound" class="btn btn-primary pull-right" ng-disabled="stopCnfirm" ng-click="createPayment()" ng-bind="contBtn"\n' +
    '                aria-controls="LiveRegion"> </button>\n' +
    '            <a ng-if="retry" ng-href="{{retry}}" class="btn btn-primary pull-right" ng-bind="rtrBtn"> </a>\n' +
    '            <span ng-hide="!stopCnfirm" class="account-select-info-text" ng-bind="AccountSelectInfoText"></span>\n' +
    '            <button role="button" class="btn btn-secondary pull-left cancelBtn" ng-click="openCancelModal()">\n' +
    '                <span ng-bind="cancelBtn"></span>\n' +
    '                <span aria-label="{{\'PISP.ACCOUNT_SELECTION_PAGE.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL\'| translate}}"></span>\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!-- button section ENDS-->');
}]);
})();
