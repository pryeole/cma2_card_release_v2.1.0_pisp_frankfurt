package com.capgemini.psd2.pisp.ispc.test.routing;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.ispc.routing.ISPConentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPConentsRoutingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private ISPConentsRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testSetApplicationContext() {
		adapter.setApplicationContext(new ApplicationContextMock());
	}

	@Test
	public void testProcessInternationalScheduledPaymentConsents() {

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		InternationalScheduledPaymentStagingAdapter stagingAdapter = new InternationalScheduledPaymentStagingAdapter() {

			@Override
			public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(
					CustomISPConsentsPOSTRequest internationalScheduledPaymentRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(stagingAdapter);
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.AUTHORISED);
	}

	@Test
	public void testRetrieveStagedInternationalScheduledPaymentConsents() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		InternationalScheduledPaymentStagingAdapter stagingAdapter = new InternationalScheduledPaymentStagingAdapter() {

			@Override
			public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(
					CustomISPConsentsPOSTRequest internationalScheduledPaymentRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(stagingAdapter);
		adapter.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);

	}

	@Test
	public void testRetrieveStagedInternationalScheduledPaymentConsentsNullParams() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = null;

		InternationalScheduledPaymentStagingAdapter stagingAdapter = new InternationalScheduledPaymentStagingAdapter() {

			@Override
			public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(
					CustomISPConsentsPOSTRequest internationalScheduledPaymentRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(stagingAdapter);
		adapter.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
		assertNull(params);

	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		applicationContext = null;
		adapter = null;
	}

}
