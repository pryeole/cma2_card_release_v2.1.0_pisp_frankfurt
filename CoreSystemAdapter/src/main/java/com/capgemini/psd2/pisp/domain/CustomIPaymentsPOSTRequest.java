package com.capgemini.psd2.pisp.domain;

public class CustomIPaymentsPOSTRequest extends OBWriteInternational1{
	private String createdOn;
	private Object fraudSystemResponse;
	
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public Object getFraudSystemResponse() {
		return fraudSystemResponse;
	}
	public void setFraudSystemResponse(Object fraudSystemResponse) {
		this.fraudSystemResponse = fraudSystemResponse;
	}
	@Override
	public String toString() {
		return "CustomIPaymentsPOSTRequest [createdOn=" + createdOn + ", fraudSystemResponse=" + fraudSystemResponse
				+ "]";
	}
	

}
