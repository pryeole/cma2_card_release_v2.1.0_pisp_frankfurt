package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service.DomesticStandingOrdersConsentsService;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
@RequestMapping("/group-payments/p/payments-service")
public class DomesticStandingOrdersConsentsFoundationController {
	
	@Autowired
	private DomesticStandingOrdersConsentsService domesticStandingOrdersConsentsService;
	
	@Autowired
	private ValidationUtility validationUtility;
	
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/{payment-instruction-proposal-id}", method = RequestMethod.GET, produces = {
MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public StandingOrderInstructionProposal domesticPaymentConsentGet(
			@PathVariable("payment-instruction-proposal-id") String paymentInstructionProposalId,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

		if (null == channelcode || null == sourceUserReqHeader || null == transactionId || null == sourceSystemReqHeader) {
			throw new MissingAuthenticationHeaderException("Header Missing");
		}

		if (paymentInstructionProposalId == null) {

			throw new InvalidParameterRequestException("Bad request");
		}
		System.out.println("transactionId========"+transactionId);
		validationUtility.validateErrorCode(transactionId);
		return domesticStandingOrdersConsentsService.retrieveAccountInformation(paymentInstructionProposalId);

	}
	
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<StandingOrderInstructionProposal> domesticPaymentConsentPost(@RequestBody StandingOrderInstructionProposal standingOrderInstructionProposalReq,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId)throws Exception {

		if (null == channelcode  || null == sourceSystemReqHeader || null == sourceUserReqHeader
				|| null == transactionId) {
			throw new MissingAuthenticationHeaderException("Header Missing");
		}
		
		
		if(null==standingOrderInstructionProposalReq) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);		
		}
		System.out.println("standingOrderInstructionProposalReq.getReference()========"+standingOrderInstructionProposalReq.getReference());	
		validationUtility.validateMockBusinessValidationsForSchedulePayment(standingOrderInstructionProposalReq.getReference());
		StandingOrderInstructionProposal standingOrderInstructionProposalResponse = null;
		try {
			standingOrderInstructionProposalResponse = domesticStandingOrdersConsentsService.createDomesticStandingOrdersConsentsResource(standingOrderInstructionProposalReq);

		} catch (RecordNotFoundException e) {
			e.printStackTrace();
			}
		
		if (null == standingOrderInstructionProposalResponse) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}
		
		return new ResponseEntity<>(standingOrderInstructionProposalResponse, HttpStatus.CREATED);

	} 



}
