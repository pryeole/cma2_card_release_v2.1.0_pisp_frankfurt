"use strict";
describe("ConsentApp.AispAccountCtrl_test", function() {
    beforeEach(module("consentApp"));

    var $controller,
        $scope,
        mockConsentService,
        q,
        deferred,
        config,
        AccountsByPage,
        allAccounts,
        eventObj,
        psuAccounts,
        tppInfo,
        acctReq,
        accountService,
        fundsCheckService,
        $translate,
        translateDeferred,
        blockUI,
        windowObj = { location: { href: "" } },
        errorData = { errorCode: "999", correlationId: null },
        defaultErrorData = { errorCode: "800", correlationId: null };


    beforeEach(function() {
        mockConsentService = {
            cancelRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            sessionLogout: function() { /**/ }
        };
        module(function($provide) {
            $provide.value("$window", windowObj);
        });
    });

    beforeEach(inject(function($controller, $rootScope, ConsentService, $q, _$window_) {
        jasmine.getFixtures().fixturesPath = "base/tests/fixtures/";
        $scope = $rootScope.$new();
        q = $q;
        windowObj = _$window_;
        translateDeferred = q.defer();
        $translate = jasmine.createSpy("$translate").and.returnValue(translateDeferred.promise);
        blockUI = jasmine.createSpyObj("blockUI", ["stop"]);

        var controller = $controller("AispAccountCtrl", { $scope: $scope, ConsentService: mockConsentService, $window: windowObj, $translate: $translate, blockUI: blockUI });
        config = {
            accTypeChecking: "Checking",
            accTypeCredit: "CreditCard",
            minConsentPeriod: 1,
            maxConsentPeriod: 180,
            minTransHistoryPeriod: 1,
            maxTransHistoryPeriod: 36,
            lang: "en",
            defaultCssTheme: "default-theme.css",
            logoPath: "img/logo.png",
            aispConsent: "AISP",
            pispConsent: "PISP",
            cispConsent: "CISP",
            searchOnAccounts: false,
            pageSize: 10,
            maxPaginationSize: 4,
            maskAccountNumberLength: 4,
            visibleSelectedAccounts: 3
        };
        AccountsByPage = [null, [{ "userId": "1234", "accountNumber": "10203345", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "John", "accountNSC": " SC802001", "selected": false }, { "userId": "1235", "accountNumber": "10203346", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "mike", "accountNSC": " SC802001", "selected": false }, { "userId": "1236", "accountNumber": "10203347", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "jtma", "accountNSC": " SC802001", "selected": false }, { "userId": "1237", "accountNumber": "10203348", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "harry", "accountNSC": " SC802001", "selected": false }, { "userId": "1238", "accountNumber": "10203349", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "harry123", "accountNSC": " SC802001", "selected": false }, { "userId": "1239", "accountNumber": "10203350", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "marry", "accountNSC": " SC802001", "selected": false }, { "userId": "1241", "accountNumber": "10203351", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "marry1", "accountNSC": " SC802001", "selected": false }, { "userId": "1242", "accountNumber": "10203352", "accountName": "John Doe", "accountType": "Saving", "currency": "eur", "nickname": "Mac", "accountNSC": " SC802001", "selected": false }, { "userId": "1243", "accountNumber": "10203353", "accountName": "John Doe", "accountType": "Current", "currency": "eur", "nickname": "John", "accountNSC": " SC802001", "selected": false }, { "userId": "1244", "accountNumber": "10203354", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "John Doe", "accountNSC": " SC802001", "selected": false }],
            [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": false }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": false }]
        ];
        allAccounts = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:47" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:48" }];
        tppInfo = "Moneywise";
        eventObj = jasmine.createSpyObj("eventObj", ["preventDefault"]);
        psuAccounts = [{ "userId": "1234", "accountNumber": "10203345", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "John", "accountNSC": " SC802001" }, { "userId": "1235", "accountNumber": "10203346", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "mike", "accountNSC": " SC802001" }, { "userId": "1236", "accountNumber": "10203347", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "jtma", "accountNSC": " SC802001" }, { "userId": "1237", "accountNumber": "10203348", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "harry", "accountNSC": " SC802001" }, { "userId": "1238", "accountNumber": "10203349", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "harry123", "accountNSC": " SC802001" }, { "userId": "1239", "accountNumber": "10203350", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "marry", "accountNSC": " SC802001" }, { "userId": "1241", "accountNumber": "10203351", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "marry1", "accountNSC": " SC802001" }, { "userId": "1242", "accountNumber": "10203352", "accountName": "John Doe", "accountType": "Saving", "currency": "eur", "nickname": "Mac", "accountNSC": " SC802001" }, { "userId": "1243", "accountNumber": "10203353", "accountName": "John Doe", "accountType": "Current", "currency": "eur", "nickname": "John", "accountNSC": " SC802001" }, { "userId": "1244", "accountNumber": "10203354", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "John Doe", "accountNSC": " SC802001" }, { "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001" }];
        angular.forEach(psuAccounts, function(value) {
            value.selected = false;
        });
        acctReq = { "Data": { "AccountRequestId": "03be465a-0555-4bb4-8fa2-d22e131986ce", "Status": "AwaitingAuthorisation", "CreationDateTime": "2017-07-07T16:48:07.159", "Permissions": ["ReadAccountsBasic", "ReadAccountsDetail", "ReadBalances", "ReadBeneficiariesBasic", "ReadBeneficiariesDetail", "ReadDirectDebits", "ReadStandingOrdersBasic", "ReadStandingOrdersDetail", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadTransactionsDetail"], "ExpirationDateTime": "2017-05-02T00:00:00-00:00", "TransactionFromDateTime": "2017-05-03T00:00:00-00:00", "TransactionToDateTime": "2017-12-03T00:00:00-00:00" }, "Risk": null };
        loadFixtures("index-aisp.html");
    }));

    describe("test init method", function() {
        it("initiate happy flow", function() {
            inject(function(config) {
                spyOn($scope, "isFromReview");
                spyOn($scope, "pagination");
                spyOn($scope, "isSelectedAll");
                spyOn($scope, "createCustomErr");
                var error = { "errorCode": 999 };
                $scope.init();
                expect($scope.errorData).toBe(null);
                expect($scope.retry).toBe(null);
                expect($scope.pageSize).toBe(config.pageSize);
                expect($scope.maxPaginationSize).toBe(config.maxPaginationSize);
                expect($scope.searchOnAccounts).toBe(config.searchOnAccounts);
                expect($scope.maskAccountNumberLength).toBe(config.maskAccountNumberLength);
                expect($scope.AccountsByPage).toEqual({});
                expect($scope.accountsData.length).toBe(0);
                expect($scope.selAccounts.length).toBe(0);
                expect($scope.searchText).toBe("");
                expect($scope.currentPage).toBe(1);
                expect($scope.isAllAccountSelected).toBe(false);
                expect($scope.filteredAccts.length).toBe(0);
                expect($scope.createCustomErr).toHaveBeenCalledTimes(0);
                expect($scope.accReq).toEqual(angular.fromJson(acctReq));
                expect($scope.allAccounts).toEqual(angular.fromJson(psuAccounts));
                expect($scope.tppInfo).toBe(tppInfo);
                expect($scope.tillDate).toBe($scope.accReq.TransactionToDateTime);
            });
        });

        it("initiate error flow", function() {
            appendSetFixtures('<input type="hidden" id="error" name="error" value="{&quot;errorCode&quot;:999}" />');
            spyOn($scope, "createCustomErr");
            $scope.init();
            expect($scope.createCustomErr).toHaveBeenCalled();
        });

        it("initiate no data flow", function() {
            loadFixtures("index-no-data-aisp.html");
            spyOn($scope, "createCustomErr");
            $scope.init();
            expect($scope.allAccounts.length).toBe(0);
        });

        it("current page watch flow", function() {
            spyOn($scope, "isSelectedAll");
            spyOn($scope, "isFromReview").and.returnValue(true);
            $scope.init();
            $scope.$digest();
            $scope.currentPage = 2;
            $scope.$digest();
            expect($scope.isSelectedAll).toHaveBeenCalled();
        });
    });

    describe("test createCustomErr", function() {
        it("createCustomErr", function() {
            expect($scope.createCustomErr).toBeDefined();
            $scope.createCustomErr();
        });
    });

    describe("test pagination", function() {
        it("pagination", function() {
            inject(function(AcctService) {
                spyOn(AcctService, "paged").and.returnValue(AccountsByPage);
                expect($scope.pagination).toBeDefined();
                $scope.pagination();
                expect($scope.AccountsByPage).toEqual(AccountsByPage);
                expect($scope.accountsData).toEqual(AccountsByPage[1]);
            });
        });
    });

    describe("test toggleAll", function() {
        it("accountToggleAll", function() {
            $scope.AccountsByPage = AccountsByPage;
            spyOn($scope, "selectedAccountList").and.callFake(function() { /**/ });
            expect($scope.accountToggleAll).toBeDefined();
            $scope.currentPage = 1;
            $scope.accountToggleAll(true);
            var items = $scope.AccountsByPage[$scope.currentPage];
            for (var index = 0; index < items.length; index++) {
                expect(items[index].selected).toBe(true);
            }
            expect($scope.selectedAccountList).toHaveBeenCalledTimes(1);
        });
    });

    describe("test all accounts selected", function() {
        it("isSelectedAll", function() {
            $scope.AccountsByPage = AccountsByPage;
            expect($scope.isSelectedAll).toBeDefined();

            $scope.currentPage = 2;
            var items = $scope.AccountsByPage[$scope.currentPage];
            for (var index = 0; index < items.length; index++) {
                items[index].selected = true;
            }
            $scope.isSelectedAll();
            expect($scope.isAllAccountSelected).toBe(true);
            items[1].selected = false;
            $scope.isSelectedAll();
            expect($scope.isAllAccountSelected).toBe(false);

        });
    });

    describe("test remove account", function() {
        it("removeAccount", function() {
            spyOn($scope, "selectedAccountList");
            var account = {};
            $scope.removeAccount(account, eventObj);
            expect(account.selected).toBe(false);
            expect($scope.selectedAccountList).toHaveBeenCalledTimes(1);
            expect(eventObj.preventDefault).toHaveBeenCalled();
        });
    });

    describe("test selected account", function() {
        it("selectedAccountList", function() {
            spyOn($scope, "isSelectedAll");
            $scope.allAccounts = allAccounts;
            $scope.selectedAccountList();
            expect($scope.selAccounts).toEqual(allAccounts);
            expect($scope.isSelectedAll).toHaveBeenCalledTimes(1);
        });
    });

    xdescribe("test search", function() {
        it("search", function() {
            inject(function(AcctService) {
                var searchResults = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:47" }];

                spyOn(AcctService, "searched").and.returnValue(searchResults);
                spyOn($scope, "pagination");
                $scope.allAccounts = allAccounts;
                $scope.searchText = "";
                $scope.search();
                expect($scope.filteredAccounts).toEqual(allAccounts);
            });

        });
    });
    describe("test goPreview", function() {
        it("goPreview", function() {
            inject(function($state) {
                var selectedAccounts = [{}, {}];
                $scope.selAccounts = selectedAccounts;
                $scope.accReq = {};
                spyOn($state, "go");
                $scope.goPreview();
                expect($scope.accReq["SelectedAccounts"]).toBeDefined();
                expect($scope.accReq["SelectedAccounts"]).toEqual(selectedAccounts);
                expect($state.go).toHaveBeenCalledWith("aispReview", {
                    "accountRequestDetails": {
                        "accountReqData": {
                            "SelectedAccounts": [{}, {}]
                        }
                    }
                });
            });
        });
    });

    describe("test cancelSubmission happy flow", function() {
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);

                deferred.resolve({ status: 200, data: { redirectUri: "redirect-to" } });
                $scope.$digest();
                expect(windowObj.location.href).toEqual("redirect-to");
            });
        });
    });

    describe("test cancelSubmission error flow", function() {
        it("cancelSubmission branch if", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: "999" } }, headers: function() { return null } });
                $scope.$digest();
                expect($scope.errorData).toEqual(errorData);
            });
        });
        it("cancelSubmission branch 800", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: "800" } }, headers: function() { return null } });
                $scope.$digest();
                expect($scope.errorData).toEqual(defaultErrorData);
            });
        });
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                spyOn($scope, "openSessionOutModal");
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "url" }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.openSessionOutModal).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("test sessionSubmission", function() {
        it("sessionSubmission", function() {
            $scope.sessionSubmission();
            expect(windowObj.location.href).toBeUndefined();
        });
    });
    describe("test openModal", function() {
        it("test openModal happy flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                CANCEL_POPUP_HEADER: "RESOLVED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "RESOLVED_CANCEL_POPUP_BODY",
                NO_BUTTON: "RESOLVED_NO_BUTTON",
                YES_BUTTON: "RESOLVED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("RESOLVED_NO_BUTTON");
        });

        it("test openModal error flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                CANCEL_POPUP_HEADER: "REJECTED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "REJECTED_CANCEL_POPUP_BODY",
                NO_BUTTON: "REJECTED_NO_BUTTON",
                YES_BUTTON: "REJECTED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("REJECTED_NO_BUTTON");
        });
    });
    describe("test openSessionOutModal", function() {
        it("test openSessionOutModal happy flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");
            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                SESSION_TIMEOUT_POPUP_HEADER: "RESOLVED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "RESOLVED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "RESOLVED_OK_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_OK_BUTTON");
        });

        it("test openSessionOutModal error flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");

            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                SESSION_TIMEOUT_POPUP_HEADER: "REJECTED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "REJECTED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "REJECTED_OK_BUTTON"
            });
            $scope.$digest();
            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_OK_BUTTON");

        });
    });
    describe("test isFromReview", function() {
        it("isFromReview: selected accounts exist", function() {
            inject(function($state) {
                var selectedAccounts = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:47" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:48" }];

                $scope.allAccounts = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": false, "$$hashKey": "object:47" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": false, "$$hashKey": "object:48" }];

                $state.params = ($state.params) ? $state.params : {};
                $state.params.selectedAccountDetails = {
                    accountReqData: {
                        SelectedAccounts: selectedAccounts
                    }
                };

                $scope.isFromReview();

                for (var index = 0; index < $scope.allAccounts.length; index++) {
                    expect($scope.allAccounts[index].selected).toBe(true);
                }
            });
        });

        it("isFromReview: selected accounts do not exist", function() {
            inject(function($state) {
                var selectedAccounts = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:47" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:48" }];

                $scope.allAccounts = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": false, "$$hashKey": "object:47" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": false, "$$hashKey": "object:48" }];

                $state.params = ($state.params) ? $state.params : {};
                $state.params.selectedAccountDetails = null;

                $scope.isFromReview();

                for (var index = 0; index < $scope.allAccounts.length; index++) {
                    expect($scope.allAccounts[index].selected).toBe(false);
                }
            });
        });
    });
});