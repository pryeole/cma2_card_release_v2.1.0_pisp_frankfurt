package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadBalance1;

public class PlatformAccountBalanceResponse {

	// CMA2 response
	private OBReadBalance1 oBReadBalance1;

	// Additional Information
	private Map<String, String> additionalInformation;

	
	public OBReadBalance1 getoBReadBalance1() {
		return oBReadBalance1;
	}

	public void setoBReadBalance1(OBReadBalance1 oBReadBalance1) {
		this.oBReadBalance1 = oBReadBalance1;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}
