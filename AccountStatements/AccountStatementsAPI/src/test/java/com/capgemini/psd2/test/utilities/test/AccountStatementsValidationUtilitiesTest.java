package com.capgemini.psd2.test.utilities.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.statements.utilities.AccountStatementValidationUtilities;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(Parameterized.class)
public class AccountStatementsValidationUtilitiesTest {
	
	@InjectMocks
	static AccountStatementValidationUtilities utility = new AccountStatementValidationUtilities();

	private String inputID, expectedOutput;

	Integer inputPageNo,inputPageSize = 5;

	public AccountStatementsValidationUtilitiesTest(String inputID, Integer inputPageNo, String expectedOutput) {
		super();
		this.inputID = inputID;
		this.inputPageNo = inputPageNo;
		this.expectedOutput = expectedOutput;
	}

	@Before
	public void setUp() throws Exception {
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		specificErrorMessageMap.put("no_account_id_found","No Account id found in the request");
		specificErrorMessageMap.put("validation_error","Validation error found with the provided input");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Parameters
	public static List<Object[]> conditionsToTest() {
		String validID = "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
		Integer validPageNo = 2;
		String above40ID = "269c3ff5-d7f8-419b-a3b9-7136c5b4611e-13344bff-1242565XXXXXXXX";
		Integer invalidPageNo = -2;

		Object expectedScenarios[][] = {
				// Test Scenario 1) When ID is null
				{ null, validPageNo, "No Account id found in the request" },

				// Test Scenario 2) When ID is blank
				{ "", validPageNo, "No Account id found in the request" },

				// Test Scenario 3) When ID is greater than 40 characters.
				{ above40ID, validPageNo, "Validation error found with the provided input" },

				// Test Scenario 4) When pageNo is invalid
				{ validID, invalidPageNo, "Validation error found with the provided input" },

				// Test Scenario 5) When pageNo is null - success case
				{ validID, null, "Passed" },

				// Test Scenario 6) When pageNo is valid - success case
				{ validID, validPageNo, "Passed" } };

		return (List<Object[]>) Arrays.asList(expectedScenarios);

	}
	
	@Test
	public void retrieveTransactionTestWithDiffParams() {
		try {
			ReflectionTestUtils.setField(utility, "minPageSize", "5");
			ReflectionTestUtils.setField(utility, "maxPageSize", "25");
			utility.validateParams(inputPageNo,inputPageSize);
			
		} catch (PSD2Exception e) {
			assertEquals(expectedOutput, e.getOBErrorResponse1().getErrors().get(0).getMessage());
		}
	}
	
	@Test
	public void retrieveTransactionTestWithDiffParams1() {
		try {
			ReflectionTestUtils.setField(utility, "minPageSize", "5");
			ReflectionTestUtils.setField(utility, "maxPageSize", "25");
			utility.validateParams(inputPageNo,3);
			
		} catch (PSD2Exception e) {
			assertEquals("Validation error found with the provided input", e.getOBErrorResponse1().getErrors().get(0).getMessage());
		}
	}

	@Test
	public void isDateComparisonPassedTest() {
	
			utility.isDateComparisonPassed("2017-05-03T00:00:00", "2017-05-03T00:00:00");
	
	}
	
	@Test(expected = NullPointerException.class)
	public void noDateComparisonPassedTest() {
	
			utility.isDateComparisonPassed(null, null);
	
	}
	
	@Test
	public void testvalidateDateSuccess() {
		utility.validateDate("2017-05-03T00:00:00", "2017-05-03T00:00:00");
	}
	
	@Test(expected = PSD2Exception.class)
	public void testvalidateDateException() {
		utility.validateDate("2018-05-03T00:00:00", "2017-05-03T00:00:00");
	}
	
	@Test(expected = Exception.class)
	public void noDateComparisonfailTest() {
	
			utility.isDateComparisonPassed("1234","1234");
	
	}
}
