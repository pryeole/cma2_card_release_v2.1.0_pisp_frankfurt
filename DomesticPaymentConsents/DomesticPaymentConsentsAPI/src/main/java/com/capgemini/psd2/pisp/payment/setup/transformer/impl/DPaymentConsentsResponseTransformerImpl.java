package com.capgemini.psd2.pisp.payment.setup.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("domesticPaymentConsentTransformer")
public class DPaymentConsentsResponseTransformerImpl
		implements PaymentConsentsTransformer<CustomDPaymentConsentsPOSTResponse, CustomDPaymentConsentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomDPaymentConsentsPOSTResponse paymentConsentsResponseTransformer(
			CustomDPaymentConsentsPOSTResponse domesticConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {

		/*
		 * These fields are expected to already be populated in domesticSetupTppResponse
		 * from Domestic Stage Service : Charges, CutOffDateTime,
		 * ExpectedExecutionDateTime, ExpectedSettlementDateTime
		 */

		/* Fields from platform Resource */
		domesticConsentsTppResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
		String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
				.valueOf(paymentSetupPlatformResource.getStatus().toUpperCase()).getCompatibleStatus();
		domesticConsentsTppResponse.getData().setStatus(OBExternalConsentStatus1Code.fromValue(setupCompatibleStatus));
		domesticConsentsTppResponse.getData().setStatusUpdateDateTime(
				NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResource.getStatusUpdateDateTime())
						? paymentSetupPlatformResource.getUpdatedAt()
						: paymentSetupPlatformResource.getStatusUpdateDateTime());

		/*
		 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
		 * to send in response. debtorFlag would be set as Null DebtorAgent is not
		 * available in CMA3
		 */
		String creditorAccountScheme = domesticConsentsTppResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			domesticConsentsTppResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		String debtorAccountScheme = domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount() != null
				? domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount().getSchemeName()
				: null;

		if (debtorAccountScheme != null
				&& PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
			domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
		}

		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()))
			domesticConsentsTppResponse.getData().getInitiation().setDebtorAccount(null);

		/*
		 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
		 * TPP.
		 */
		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& !Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount().setName(null);

		if (domesticConsentsTppResponse.getLinks() == null)
			domesticConsentsTppResponse.setLinks(new PaymentSetupPOSTResponseLinks());

		domesticConsentsTppResponse.getLinks().setSelf(PispUtilities.populateLinks(
				paymentSetupPlatformResource.getPaymentConsentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (domesticConsentsTppResponse.getMeta() == null)
			domesticConsentsTppResponse.setMeta(new PaymentSetupPOSTResponseMeta());

		return domesticConsentsTppResponse;
	}

	@Override
	public CustomDPaymentConsentsPOSTRequest paymentConsentRequestTransformer(
			CustomDPaymentConsentsPOSTRequest paymentConsentRequest) {
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation())) {
			paymentConsentRequest.getData().getAuthorisation()
					.setCompletionDateTime(pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime()));
		}
		return paymentConsentRequest;
	}

}