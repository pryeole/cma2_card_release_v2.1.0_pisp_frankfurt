package com.capgemini.psd2.rest.client.helpers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public final class HeaderHelper {

	private HeaderHelper(){
		
	}
	
	public static HttpHeaders populateJSONContentTypeHeader() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application", "json"));
		return requestHeaders;
	}

}
