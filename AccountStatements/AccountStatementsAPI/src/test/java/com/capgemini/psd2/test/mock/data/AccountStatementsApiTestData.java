package com.capgemini.psd2.test.mock.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.capgemini.psd2.account.statements.mongo.db.adapter.constants.AccountStatementMongoDbAdapterConstants;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3Data;
import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadStatement1Data;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder3;
import com.capgemini.psd2.aisp.domain.OBStatement1;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount ;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
//import com.capgemini.psd2.aisp.domain.OBStatement1Amount1;
//import com.capgemini.psd2.aisp.domain.OBStatement1Amount2;
import com.capgemini.psd2.aisp.domain.OBStatementBenefit1;
import com.capgemini.psd2.aisp.domain.OBStatementDateTime1 ;
import com.capgemini.psd2.aisp.domain.OBStatementFee1;
import com.capgemini.psd2.aisp.domain.OBStatementInterest1 ;
import com.capgemini.psd2.aisp.domain.OBStatementRate1 ;
import com.capgemini.psd2.aisp.domain.OBStatementValue1 ;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance ;
//import com.capgemini.psd2.aisp.domain.OBTransactionBalanceAmount;
import com.capgemini.psd2.aisp.domain.OBBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBExternalStatementType1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1 ;
import com.capgemini.psd2.aisp.domain.OBTransaction3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountStatementsApiTestData {
	static OBStatement1 data = new OBStatement1();
	static OBActiveOrHistoricCurrencyAndAmount amount = new OBActiveOrHistoricCurrencyAndAmount();
	static OBStatementBenefit1  benefit = new OBStatementBenefit1 ();
	static OBStatementDateTime1  dateTime = new OBStatementDateTime1 ();
	static OBStatementFee1  fee = new OBStatementFee1 ();
	static OBActiveOrHistoricCurrencyAndAmount amount1 = new OBActiveOrHistoricCurrencyAndAmount();
	static OBActiveOrHistoricCurrencyAndAmount amount2 = new OBActiveOrHistoricCurrencyAndAmount();
	static OBStatementInterest1  intrest = new OBStatementInterest1 ();
	static OBStatementRate1  rate = new OBStatementRate1 ();
	static OBStatementValue1  value = new OBStatementValue1 ();
	static OBTransaction3 transdata = new OBTransaction3();
	static OBActiveOrHistoricCurrencyAndAmount transamount = new OBActiveOrHistoricCurrencyAndAmount();
	static OBTransactionCashBalance  balance = new OBTransactionCashBalance ();
	static OBActiveOrHistoricCurrencyAndAmount  transamount2 = new OBActiveOrHistoricCurrencyAndAmount ();
	static OBBankTransactionCodeStructure1 bankTransactionCode = new OBBankTransactionCodeStructure1();
	static OBTransaction3ProprietaryBankTransactionCode  proprietaryBankTransactionCode = new OBTransaction3ProprietaryBankTransactionCode ();
	static OBMerchantDetails1  merchantDetails = new OBMerchantDetails1 ();
	
	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}
	
	

	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}
	
	public static String getTestBankID() {
		return "TestBankID";
	}
	
	public static PlatformAccountSatementsFileResponse getMockFileResponse(){
		PlatformAccountSatementsFileResponse response = new PlatformAccountSatementsFileResponse();
		response.setFileName("file");
		byte[] byteArray = null;
		try{
			Resource resource = new ClassPathResource("/2018-fifa-schedule.pdf");
			File file = resource.getFile();
			byteArray = FileUtils.readFileToByteArray(file);
		}catch(Exception e){
			
		}
		response.setFileByteArray(byteArray);
		return response;
	}
	
	public static PlatformAccountStatementsResponse getMockPlatformAccountStatementsResponseWithLinksMeta() {
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		OBReadStatement1Data oBReadStatement1Data = new OBReadStatement1Data();

		List<OBStatement1> statementList = new ArrayList<>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("269c3ff5-d7f8-419b-a3b9-7136");
		data.setStatementReference("gdqwe67384");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		data.setEndDateTime("2018-09-01T00:00:00+00:00");
		data.setStartDateTime("2016-10-01T00:00:00+00:00");
		
		amount.setAmount("10.00");
		amount.setCurrency("GBP");

		benefit.setAmount(amount);
		benefit.setType("CASHBACK");

		dateTime.setDateTime("2018-07-23T10:29:22.765Z");
		dateTime.setType("DIRECTDEBITDUE");
		List<String> statementDescription = new ArrayList<String>();
		data.setStatementDescription(statementDescription);

		fee.setType("CASHADVANCE");
		fee.setCreditDebitIndicator(OBStatementFee1.CreditDebitIndicatorEnum.DEBIT);
		fee.setAmount(amount1);
		amount1.setAmount("10.00");
		amount1.setCurrency("GBP");

		intrest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.DEBIT);
		intrest.setType("BALANCETRANSFER");
		intrest.setAmount(amount2);
		amount2.setAmount("10.00");
		amount2.setCurrency("GBP");

		rate.setRate("3.5");
		rate.setType("ANNUALBALANCETRANSFERAFTERPROMO");
	
		value.setValue(3536);
		value.setType("AIRMILESPOINTSBALANCE");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
	
		statementList.add(data);

		oBReadStatement1Data.setStatement(statementList);
		OBReadStatement1 obReadStatement1 = new OBReadStatement1();
		obReadStatement1.setData(oBReadStatement1Data);

		Links links = new Links();
		obReadStatement1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadStatement1.setMeta(meta);

		platformAccountStatementsResponse.setoBReadStatement1(obReadStatement1);

		return platformAccountStatementsResponse;

	}
	
	public static Object getMockExpectedAccountStatementsResponseWithLinksMeta() {
		
		OBReadStatement1Data oBReadStatement1Data = new OBReadStatement1Data();

		List<OBStatement1> statementList = new ArrayList<>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("269c3ff5-d7f8-419b-a3b9-7136");
		data.setStatementReference("gdqwe67384");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		data.setEndDateTime("2018-09-01T00:00:00+00:00");
		data.setStartDateTime("2016-10-01T00:00:00+00:00");
		
		amount.setAmount("10.00");
		amount.setCurrency("GBP");

		benefit.setAmount(amount);
		benefit.setType("CASHBACK");

		dateTime.setDateTime("2018-07-23T10:29:22.765Z");
		dateTime.setType("DIRECTDEBITDUE");
		List<String> statementDescription = new ArrayList<String>();
		data.setStatementDescription(statementDescription);

		fee.setType("CASHADVANCE");
		fee.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.OBStatementFee1.CreditDebitIndicatorEnum.DEBIT);
		fee.setAmount(amount1);
		amount1.setAmount("10.00");
		amount1.setCurrency("GBP");

		intrest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.DEBIT);
		intrest.setType("BALANCETRANSFER");
		intrest.setAmount(amount2);
		amount2.setAmount("10.00");
		amount2.setCurrency("GBP");

		rate.setRate("3.5");
		rate.setType("ANNUALBALANCETRANSFERAFTERPROMO");
	
		value.setValue(3536);
		value.setType("AIRMILESPOINTSBALANCE");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
	
		statementList.add(data);

		oBReadStatement1Data.setStatement(statementList);
		OBReadStatement1 obReadStatement1 = new OBReadStatement1();
		obReadStatement1.setData(oBReadStatement1Data);

		Links links = new Links();
		obReadStatement1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadStatement1.setMeta(meta);

		return obReadStatement1;

	}
	
	public static List<Object> getMockClaimsList() {
		List<Object> claimList = new ArrayList<Object>();
		claimList.add("ReadTransactionsCredits");
		claimList.add("ReadTransactionsDebits");
		return claimList;
	}
	

	
	public static PlatformAccountStatementsResponse getAccountStatementsGETResponseForService() {
		OBReadStatement1 resp = new OBReadStatement1();
		List<OBStatement1> dataList = new ArrayList<OBStatement1>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("269c3ff5-d7f8-419b-a3b9-7136");
		data.setStatementReference("gdqwe67384");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		data.setEndDateTime("2018-09-01T00:00:00+00:00");
		data.setStartDateTime("2016-10-01T00:00:00+00:00");
		
		amount.setAmount("10.00");
		amount.setCurrency("GBP");

		benefit.setAmount(amount);
		benefit.setType("CASHBACK");

		dateTime.setDateTime("2018-07-23T10:29:22.765Z");
		dateTime.setType("DIRECTDEBITDUE");
		List<String> statementDescription = new ArrayList<String>();
		data.setStatementDescription(statementDescription);

		fee.setType("CASHADVANCE");
		fee.setCreditDebitIndicator(OBStatementFee1.CreditDebitIndicatorEnum.DEBIT);
		fee.setAmount(amount1);
		amount1.setAmount("10.00");
		amount1.setCurrency("GBP");

		intrest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.DEBIT);
		intrest.setType("BALANCETRANSFER");
		intrest.setAmount(amount2);
		amount2.setAmount("10.00");
		amount2.setCurrency("GBP");

		rate.setRate("3.5");
		rate.setType("ANNUALBALANCETRANSFERAFTERPROMO");
	
		value.setValue(3536);
		value.setType("AIRMILESPOINTSBALANCE");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		dataList.add(data);
		OBReadStatement1Data data3 = new OBReadStatement1Data();
		data3.setStatement(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		
		PlatformAccountStatementsResponse platResp=new PlatformAccountStatementsResponse();
		platResp.setoBReadStatement1(resp);
		return platResp;	
}
	
	public static PlatformAccountStatementsResponse getAccountStatementsGETResponseForServiceTesting() {
		OBReadStatement1 resp = new OBReadStatement1();
		List<OBStatement1> dataList = new ArrayList<OBStatement1>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("269c3ff5-d7f8-419b-a3b9-7136");
		data.setStatementReference("gdqwe67384");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		data.setEndDateTime("2018-09-01T00:00:00+00:00");
		data.setStartDateTime("2016-10-01T00:00:00+00:00");
		
		amount.setAmount("10.00");
		amount.setCurrency("GBP");

		benefit.setAmount(amount);
		benefit.setType("CASHBACK");

		dateTime.setDateTime("2018-07-23T10:29:22.765Z");
		dateTime.setType("DIRECTDEBITDUE");
		List<String> statementDescription = new ArrayList<String>();
		data.setStatementDescription(statementDescription);

		fee.setType("CASHADVANCE");
		fee.setCreditDebitIndicator(OBStatementFee1.CreditDebitIndicatorEnum.DEBIT);
		fee.setAmount(amount1);
		amount1.setAmount("10.00");
		amount1.setCurrency("GBP");

		intrest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.DEBIT);
		intrest.setType("BALANCETRANSFER");
		intrest.setAmount(amount2);
		amount2.setAmount("10.00");
		amount2.setCurrency("GBP");

		rate.setRate("3.5");
		rate.setType("ANNUALBALANCETRANSFERAFTERPROMO");

		value.setValue(3536);
		value.setType("AIRMILESPOINTSBALANCE");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		dataList.add(data);
		OBReadStatement1Data data3 = new OBReadStatement1Data();
		data3.setStatement(dataList);
		//resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountStatementsResponse platResp=new PlatformAccountStatementsResponse();
		platResp.setoBReadStatement1(resp);
		return platResp;	
	
	}
	
	public static OBReadTransaction3 getAccountTransactionsGETResponse() {
		
		
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		transdata.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		transdata.setTransactionId("123");
		transdata.setTransactionReference("Ref123");
		transamount.setAmount("10.00");
		transamount.setCurrency("GBP");
		transdata.setAmount(transamount);
		transdata.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		transdata.setStatus(OBEntryStatus1Code.BOOKED);
		transdata.setBookingDateTime("2017-04-05T10:43:07+00:00");
		transdata.setValueDateTime("2017-04-05T10:45:22+00:00");
		transdata.setTransactionInformation("Cash from Aubrey");
		transdata.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		transdata.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		transdata.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(transamount2);
		transamount2.setAmount("230.00");
		transamount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		transdata.setBalance(balance);
		transdata.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(transdata);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		
		return resp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseForService() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		transdata.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		transdata.setTransactionId("123");
		transdata.setTransactionReference("Ref123");
		transamount.setAmount("10.00");
		transamount.setCurrency("GBP");
		transdata.setAmount(transamount);
		transdata.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		transdata.setStatus(OBEntryStatus1Code.BOOKED);
		transdata.setBookingDateTime("2017-04-05T10:43:07+00:00");
		transdata.setValueDateTime("2017-04-05T10:45:22+00:00");
		transdata.setTransactionInformation("Cash from Aubrey");
		transdata.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		transdata.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		transdata.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(transamount2);
		transamount2.setAmount("230.00");
		transamount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		transdata.setBalance(balance);
		transdata.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(transdata);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction3(resp);
		return platResp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseForServiceTesting() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		transdata.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		transdata.setTransactionId("123");
		transdata.setTransactionReference("Ref123");
		transamount.setAmount("10.00");
		transamount.setCurrency("GBP");
		transdata.setAmount(transamount);
		transdata.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		transdata.setStatus(OBEntryStatus1Code.BOOKED);
		transdata.setBookingDateTime("2017-04-05T10:43:07+00:00");
		transdata.setValueDateTime("2017-04-05T10:45:22+00:00");
		transdata.setTransactionInformation("Cash from Aubrey");
		transdata.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		transdata.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		transdata.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(transamount2);
		transamount2.setAmount("230.00");
		transamount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		transdata.setBalance(balance);
		transdata.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(transdata);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		//resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction3(resp);
		return platResp;
	}

	public static Token getMockTokenWithALLFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}
	public static  Set<Object> getMockTokenWithALLFilterTransaction() {
		Set<Object> claims = new HashSet<Object>();
		claims.add("READTRANSACTIONSBASIC");
		claims.add("READTRANSACTIONSDETAIL");
		return claims;
	}

	public static Token getMockTokenWithCreditFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithDebitFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static PlatformAccountStatementsResponse getAccountStatementsGETResponseWithNullLinks() {
	
		OBReadStatement1 resp = new OBReadStatement1();
		List<OBStatement1> dataList = new ArrayList<OBStatement1>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("269c3ff5-d7f8-419b-a3b9-7136");
		data.setStatementReference("gdqwe67384");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		data.setEndDateTime("2018-09-01T00:00:00+00:00");
		data.setStartDateTime("2016-10-01T00:00:00+00:00");
		
		amount.setAmount("10.00");
		amount.setCurrency("GBP");

		benefit.setAmount(amount);
		benefit.setType("CASHBACK");

		dateTime.setDateTime("2018-07-23T10:29:22.765Z");
		dateTime.setType("DIRECTDEBITDUE");
		List<String> statementDescription = new ArrayList<String>();
		data.setStatementDescription(statementDescription);

		fee.setType("CASHADVANCE");
		fee.setCreditDebitIndicator(OBStatementFee1.CreditDebitIndicatorEnum.DEBIT);
		fee.setAmount(amount1);
		amount1.setAmount("10.00");
		amount1.setCurrency("GBP");

		intrest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.DEBIT);
		intrest.setType("BALANCETRANSFER");
		intrest.setAmount(amount2);
		amount2.setAmount("10.00");
		amount2.setCurrency("GBP");

		rate.setRate("3.5");
		rate.setType("ANNUALBALANCETRANSFERAFTERPROMO");

		value.setValue(3536);
		value.setType("AIRMILESPOINTSBALANCE");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		dataList.add(data);
		OBReadStatement1Data data3 = new OBReadStatement1Data();
		data3.setStatement(dataList);
		resp.setData(data3);
		Links links = new Links();
		resp.setLinks(links);
		PlatformAccountStatementsResponse platResp=new PlatformAccountStatementsResponse();
		platResp.setoBReadStatement1(resp);
		return platResp;
		
	}
	
	public static PlatformAccountStatementsResponse getAccountStatementsGETResponseWithNullLinksObject() {

		OBReadStatement1 resp = new OBReadStatement1();
		List<OBStatement1> dataList = new ArrayList<OBStatement1>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("269c3ff5-d7f8-419b-a3b9-7136");
		data.setStatementReference("gdqwe67384");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		data.setEndDateTime("2018-09-01T00:00:00+00:00");
		data.setStartDateTime("2016-10-01T00:00:00+00:00");
		
		amount.setAmount("10.00");
		amount.setCurrency("GBP");

		benefit.setAmount(amount);
		benefit.setType("CASHBACK");

		dateTime.setDateTime("2018-07-23T10:29:22.765Z");
		dateTime.setType("DIRECTDEBITDUE");
		List<String> statementDescription = new ArrayList<String>();
		data.setStatementDescription(statementDescription);

		fee.setType("CASHADVANCE");
		fee.setCreditDebitIndicator(OBStatementFee1.CreditDebitIndicatorEnum.DEBIT);
		fee.setAmount(amount1);
		amount1.setAmount("10.00");
		amount1.setCurrency("GBP");

		intrest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.DEBIT);
		intrest.setType("BALANCETRANSFER");
		intrest.setAmount(amount2);
		amount2.setAmount("10.00");
		amount2.setCurrency("GBP");

		rate.setRate("3.5");
		rate.setType("ANNUALBALANCETRANSFERAFTERPROMO");

		value.setValue(3536);
		value.setType("AIRMILESPOINTSBALANCE");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		dataList.add(data);
		OBReadStatement1Data data3 = new OBReadStatement1Data();
		data3.setStatement(dataList);
		resp.setData(data3);
		PlatformAccountStatementsResponse platResp=new PlatformAccountStatementsResponse();
		platResp.setoBReadStatement1(resp);
		return platResp;
	
	}
	

		public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullLinks() {
			OBReadTransaction3 resp = new OBReadTransaction3();
			List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
			transdata.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
			transdata.setTransactionId("123");
			transdata.setTransactionReference("Ref123");
			transamount.setAmount("10.00");
			transamount.setCurrency("GBP");
			transdata.setAmount(transamount);
			transdata.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
			transdata.setStatus(OBEntryStatus1Code.BOOKED);
			transdata.setBookingDateTime("2017-04-05T10:43:07+00:00");
			transdata.setValueDateTime("2017-04-05T10:45:22+00:00");
			transdata.setTransactionInformation("Cash from Aubrey");
			transdata.setAddressLine("XYZ address Line");
			bankTransactionCode.setCode("ReceivedCreditTransfer");
			bankTransactionCode.setSubCode("DomesticCreditTransfer");
			transdata.setBankTransactionCode(bankTransactionCode);
			proprietaryBankTransactionCode.setCode("Transfer");
			proprietaryBankTransactionCode.setIssuer("AlphaBank");
			transdata.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
			balance.setAmount(transamount2);
			transamount2.setAmount("230.00");
			transamount2.setCurrency("GBP");
			balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
			balance.setType(OBBalanceType1Code.INTERIMBOOKED);
			transdata.setBalance(balance);
			transdata.setMerchantDetails(merchantDetails);
			merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
			merchantDetails.setMerchantName("MerchantXYZ");
			dataList.add(transdata);
			OBReadTransaction3Data data3 = new OBReadTransaction3Data();
			data3.setTransaction(dataList);
			resp.setData(data3);
			Links links = new Links();
			resp.setLinks(links);
			PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
			transResp.setoBReadTransaction3(resp);
			return transResp;
		}

		public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullLinksObject() {
			OBReadTransaction3 resp = new OBReadTransaction3();
			List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
			transdata.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
			transdata.setTransactionId("123");
			transdata.setTransactionReference("Ref123");
			transamount.setAmount("10.00");
			transamount.setCurrency("GBP");
			transdata.setAmount(transamount);
			transdata.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
			transdata.setStatus(OBEntryStatus1Code.BOOKED);
			transdata.setBookingDateTime("2017-04-05T10:43:07+00:00");
			transdata.setValueDateTime("2017-04-05T10:45:22+00:00");
			transdata.setTransactionInformation("Cash from Aubrey");
			transdata.setAddressLine("XYZ address Line");
			bankTransactionCode.setCode("ReceivedCreditTransfer");
			bankTransactionCode.setSubCode("DomesticCreditTransfer");
			transdata.setBankTransactionCode(bankTransactionCode);
			proprietaryBankTransactionCode.setCode("Transfer");
			proprietaryBankTransactionCode.setIssuer("AlphaBank");
			transdata.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
			balance.setAmount(transamount2);
			transamount2.setAmount("230.00");
			transamount2.setCurrency("GBP");
			balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
			balance.setType(OBBalanceType1Code.INTERIMBOOKED);
			transdata.setBalance(balance);
			transdata.setMerchantDetails(merchantDetails);
			merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
			merchantDetails.setMerchantName("MerchantXYZ");
			dataList.add(transdata);
			OBReadTransaction3Data data3 = new OBReadTransaction3Data();
			data3.setTransaction(dataList);
			resp.setData(data3);
			PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
			transResp.setoBReadTransaction3(resp);
			return transResp;
		}

		public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullDataObject() {
			OBReadTransaction3 resp = null;
			List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
			transdata.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
			transdata.setTransactionId("123");
			transdata.setTransactionReference("Ref123");
			transamount.setAmount("10.00");
			transamount.setCurrency("GBP");
			transdata.setAmount(transamount);
			transdata.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
			transdata.setStatus(OBEntryStatus1Code.BOOKED);
			transdata.setBookingDateTime("2017-04-05T10:43:07+00:00");
			transdata.setValueDateTime("2017-04-05T10:45:22+00:00");
			transdata.setTransactionInformation("Cash from Aubrey");
			transdata.setAddressLine("XYZ address Line");
			bankTransactionCode.setCode("ReceivedCreditTransfer");
			bankTransactionCode.setSubCode("DomesticCreditTransfer");
			transdata.setBankTransactionCode(bankTransactionCode);
			proprietaryBankTransactionCode.setCode("Transfer");
			proprietaryBankTransactionCode.setIssuer("AlphaBank");
			transdata.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
			balance.setAmount(transamount2);
			transamount2.setAmount("230.00");
			transamount2.setCurrency("GBP");
			balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
			balance.setType(OBBalanceType1Code.INTERIMBOOKED);
			transdata.setBalance(balance);
			transdata.setMerchantDetails(merchantDetails);
			merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
			merchantDetails.setMerchantName("MerchantXYZ");
			dataList.add(transdata);
			OBReadTransaction3Data data3 = new OBReadTransaction3Data();
			data3.setTransaction(dataList);
			resp.setData(data3);
			PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
			transResp.setoBReadTransaction3(resp);
			return transResp;
		}
	
	public static AispConsent getAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2017-05-03T00:00:00");
		aispConsent.setTransactionToDateTime("2017-12-03T00:00:00");
		return aispConsent;
	}
	
	public static AispConsent getAispConsent2() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2015-01-02T00:00:00");
		aispConsent.setTransactionToDateTime("2017-01-02T00:00:00");
		return aispConsent;
	}
	
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}
	
	
}