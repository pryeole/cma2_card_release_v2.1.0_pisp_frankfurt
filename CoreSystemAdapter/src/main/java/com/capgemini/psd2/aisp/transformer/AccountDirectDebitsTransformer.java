package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;

public interface AccountDirectDebitsTransformer {
	public <T> PlatformAccountDirectDebitsResponse transformAccountDirectDebits(T source, Map<String, String> params);
}
