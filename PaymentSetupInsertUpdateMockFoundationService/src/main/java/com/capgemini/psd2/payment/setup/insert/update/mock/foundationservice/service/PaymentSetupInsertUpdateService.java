package com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.service;

import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.domain.PaymentInstruction;

public  interface PaymentSetupInsertUpdateService {
	public ValidationPassed stagePaymentInsertion(PaymentInstruction paymentInstruction) throws Exception;

}
