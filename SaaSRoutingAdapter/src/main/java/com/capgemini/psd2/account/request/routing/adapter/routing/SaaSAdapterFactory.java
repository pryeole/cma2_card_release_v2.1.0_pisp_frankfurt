package com.capgemini.psd2.account.request.routing.adapter.routing;

import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;

public interface SaaSAdapterFactory {

	public AuthenticationAdapter getAdapterInstance(String coreSystemName); 
}
