package com.capgemini.psd2.controller.qtsp;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.UploadQTSPService;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class UploadQTSPControllerTest {

	@InjectMocks
	private UploadQTSPController uploadQTSPController;

	@Mock
	private UploadQTSPService uploadQTSPService;

	@Mock
	private QTSPServiceUtility utility;

	@Mock
	private RequestHeaderAttributes reqHeaderAttribute;

	private QTSPResource qtsp;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		qtsp = QTSPStub.getCerts().get(0);
		utility.setUploadCertRequest(true);
	}

	@Test
	public void uploadCertificateTest() {
		when(uploadQTSPService.validateRequestBody(qtsp)).thenReturn(true);
		when(uploadQTSPService.uploadQTSP(qtsp)).thenReturn(true);
		uploadQTSPController.uploadCertificate(qtsp, "BOI");
	}

	@Test
	public void uploadCertificateTest_validationFailure() {
		when(uploadQTSPService.validateRequestBody(qtsp)).thenReturn(false);
		uploadQTSPController.uploadCertificate(qtsp, "BOI");
	}

	@Test
	public void uploadCertificateTest_uploadFailure() {
		when(uploadQTSPService.validateRequestBody(qtsp)).thenReturn(true);
		when(uploadQTSPService.uploadQTSP(qtsp)).thenReturn(false);
		uploadQTSPController.uploadCertificate(qtsp, "BOI");
	}

}
