package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing;

import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;

public interface IPaymentConsentsAdapterFactory {

	public InternationalPaymentStagingAdapter getInternationalPaymentSetupStagingInstance(String coreSystemName);

}
