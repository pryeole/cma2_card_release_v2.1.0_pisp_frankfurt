/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.pisp.consent.adapter.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;

/**
 * The Interface PispConsentMongoRepository.
 */
public interface PispConsentMongoRepository extends MongoRepository<PispConsent, String> {
	
	/**
	 * Find by consent id.
	 *
	 * @param consentId the consent id
	 * @return the pispConsent
	 */
	public PispConsent findByConsentIdAndCmaVersionIn(String consentId,List<String> cmaVersion);

	public PispConsent findByPaymentIdAndStatusAndCmaVersionIn(String paymentId,ConsentStatusEnum status, List<String> cmaVersion);
	
	public List<PispConsent> findByPsuIdAndStatusAndCmaVersionIn(String psuId, ConsentStatusEnum status, List<String> cmaVersion);

	public List<PispConsent> findByPsuIdAndCmaVersionIn(String psuId,List<String> cmaVersion );
	
	public PispConsent findByPaymentIdAndCmaVersionIn(String paymentId, List<String> cmaVersion);
	
	public List<PispConsent> findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(String psuId, ConsentStatusEnum status, String tenantid, List<String> cmaVersion);

	public List<PispConsent> findByPsuIdAndTenantIdAndCmaVersionIn(String psuId, String tenantId , List<String> cmaVersion);
}
