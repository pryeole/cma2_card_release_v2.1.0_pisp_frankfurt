package com.capgemini.psd2.pisp.domestic.payments.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domestic.payments.controller.DomesticPaymentsController;
import com.capgemini.psd2.pisp.domestic.payments.service.DomesticPaymentsService;
import com.capgemini.psd2.pisp.domestic.payments.test.mock.data.DomesticPaymentSubmissionPOSTRequestResponseMockData;
import com.capgemini.psd2.pisp.validation.adapter.impl.DomesticPaymentValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentsControllerTest {

	private MockMvc mockMvc;

	PSD2Exception obPSD2Exception;
	@Mock
	private DomesticPaymentsService service;
	@Mock
	private DomesticPaymentValidatorImpl validator;

	@Mock
	private OBPSD2ExceptionUtility util;

	@InjectMocks
	private DomesticPaymentsController controller;
	

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
		Map<String, String> map = new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");

		Map<String, String> map1 = new HashMap<>();
		map1.put("signature_missing", "drtyrty");
		
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(map1);
		
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateDomesticPaymentSubmissionResource() throws Exception {
		PaymentDomesticSubmitPOST201Response response = new PaymentDomesticSubmitPOST201Response();
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();
		request.setData(data);
		request.setRisk(risk);
		request.getData();
		request.getRisk();
		//when(validator.validateDomesticPaymentsPOSTRequest(anyObject())).thenReturn(true);
		when(service.createDomesticPaymentsResource(anyObject())).thenReturn(response);
		this.mockMvc
				.perform(post("/domestic-payments").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(request)))
				.andExpect(status().isCreated());
	}

	@Test
	public void testExceptionCreateDomesticPaymentSubmissionResource() throws Exception {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		//when(validator.validateDomesticPaymentsPOSTRequest(anyObject())).thenReturn(true);
		when(service.createDomesticPaymentsResource(anyObject()))
				.thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		this.mockMvc
				.perform(post("/domestic-payments").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(request)))
				.andExpect(status().isBadRequest());
	}
	

	@Test
	public void testExceptionCreateDomesticPaymentSubmissionResource_statesException() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		//when(validator.validateDomesticPaymentsPOSTRequest(anyObject())).thenReturn(true);
		when(service.createDomesticPaymentsResource(anyObject()))
				.thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		try {
			this.mockMvc
					.perform(post("/domestic-payments").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
							.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(request)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid Request"));
		}
	}
	

	@Test
	public void testRetrieveDomesticPaymentPaymentSubmissionResource() throws Exception {
		PaymentDomesticSubmitPOST201Response submissionResponse = new PaymentDomesticSubmitPOST201Response();
		DPaymentsRetrieveGetRequest submissionRequest = new DPaymentsRetrieveGetRequest();
		//when(validator.validateDomesticPaymentsGETRequest(anyObject())).thenReturn(true);
		when(service.retrieveDomesticPaymentsResource(anyObject())).thenReturn(submissionResponse);
	    submissionRequest.setDomesticPaymentId("123");
		this.mockMvc
				.perform(get("/domestic-payments/{DomesticPaymentId}",123)
						.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(submissionRequest)))
				.andExpect(status().isOk());
	}

	@Test
	public void testExceptionRetrieveDomesticPaymentPaymentSubmissionResource() throws Exception {
		DPaymentsRetrieveGetRequest submissionRequest = new DPaymentsRetrieveGetRequest();
		//when(validator.validateDomesticPaymentsGETRequest(anyObject())).thenReturn(true);
		when(service.retrieveDomesticPaymentsResource(anyObject()))
				.thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		submissionRequest.setDomesticPaymentId("123");
		this.mockMvc
		          .perform(get("/domestic-payments/{DomesticPaymentId}",123)
						.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(submissionRequest)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testExceptionRetrieveDomesticPaymentPaymentSubmissionResource_statesException() {
		DPaymentsRetrieveGetRequest submissionRequest = new DPaymentsRetrieveGetRequest();
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		submissionRequest.setDomesticPaymentId("123");
		//when(validator.validateDomesticPaymentsGETRequest(anyObject())).thenReturn(true);
		when(service.retrieveDomesticPaymentsResource(anyObject()))
				.thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID));
		try {
			this.mockMvc
					.perform(get("/domestic-payments/{DomesticPaymentId}", 123)
							.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
							.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(submissionRequest)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request/DomesticPaymentId"));
		}
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveDomesticPaymentResource_OBPSD2Exception() {

		DPaymentsRetrieveGetRequest paymentRetrieveRequest = new DPaymentsRetrieveGetRequest();
		paymentRetrieveRequest.setDomesticPaymentId("58923");
		String PaymentId = "34576";
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(service.retrieveDomesticPaymentsResource(anyObject()))
				.thenThrow(PSD2Exception.class);
		try {
			this.mockMvc
					.perform(get("/domestic-payments/{DomesticPaymentId}", 58923)
							.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(
									DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(paymentRetrieveRequest)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request/DomesticPaymentId"));
		}
		controller.retrieveDomesticPaymentPaymentSubmissionResource(PaymentId);
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticPaymentResource_OBPSD2Exception() {

		CustomDPaymentsPOSTRequest paymentRequest = new CustomDPaymentsPOSTRequest();
		PaymentDomesticSubmitPOST201Response response = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		
		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
	
		response.setData(data);
		Mockito.when(service.createDomesticPaymentsResource(anyObject()))
			.thenThrow(PSD2Exception.class);

		try {
			this.mockMvc
					.perform(post("/domestic-payments").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
							.content(DomesticPaymentSubmissionPOSTRequestResponseMockData.asJsonString(response)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid Request"));
		}
		controller.createDomesticPaymentSubmissionResource(paymentRequest);
	}
	
	@After
	public void tearDown() {
		service = null;
		//validator = null;
		controller = null;
	}

}
