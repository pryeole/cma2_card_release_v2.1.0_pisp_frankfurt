package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Descriptive code for the product category.  If ProductType - \"Other\" is chosen, the object OtherProductType must be populated with name, and description.
 */
public enum OBExternalProductType1Code {
  
  BUSINESSCURRENTACCOUNT("BusinessCurrentAccount"),
  
  COMMERCIALCREDITCARD("CommercialCreditCard"),
  
  OTHER("Other"),
  
  PERSONALCURRENTACCOUNT("PersonalCurrentAccount"),
  
  SMELOAN("SMELoan");

  private String value;

  OBExternalProductType1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalProductType1Code fromValue(String text) {
    for (OBExternalProductType1Code b : OBExternalProductType1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

