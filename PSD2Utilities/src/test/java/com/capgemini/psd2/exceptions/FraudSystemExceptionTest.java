package com.capgemini.psd2.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class FraudSystemExceptionTest {
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testConstructor(){
		String message = "abcd";
		ErrorInfo errorInfo = new ErrorInfo();
		Map<String,String> params = new HashMap<>();
		FraudSystemException exception = new FraudSystemException(message,errorInfo,params);
		exception.getParams();
	}
}
