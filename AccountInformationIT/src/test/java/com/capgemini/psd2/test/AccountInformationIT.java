/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum;
import com.capgemini.psd2.config.AccountInformationITConfig;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.google.common.base.CharMatcher;


/**Scenarios Covered with corresponding functions:
 *Function : SuccesstestRetrieveAccountInformation:
 * 		AISP is able to send a Account information Request through GET method.
 * 		Account info  Request always contains valid AccountId
 * 		AccountId cannot be Null or blank.
 * 		AccountId length should be upto 40 characters.
 * 		Valid Authorization header is sent in the request
 * 		Submitted request can contain optional headers named as x-fapi-customer-ip-address, x-fapi-customer-last-logged-time and x-fapi-interaction-id where x-fapi-financial-id is mandatory.
 * 		Identification length should be upto 34 characters.
 * 		Name length should be upto 70 characters.
 *		If populated then SecondaryIdentification length should be upto 34 characters.
 *		AccountId cannot be Null or blank.
 *		AccountId only contains the same values as sent in the request.
 *		AccountId length should be upto 40 characters.
 *		Currency cannot be Null or blank.
 *		Currency only allows Capital Alphabetic Values
 *		Currency length must be 3 character.
 *		If populated then Nickname length should be upto 70 characters.
 *		If populated then it is an arrays that must be containing Identification and SchemeName fields.
 *		Meta only contain total-pages field that can be optional.
 *		If Total-pages populate then it only contains integer values upto 32 characters.

 *Function : InvalidAccIdGivenTest:
 *		Account info Request contains invalid AccountId (doesn't exist in database)

 *Function : NullAccIdGivenTest:
 *		AccountId is Null or blank.

 *Function : NoAuthTokenGivenTest:
 *		Authorization header is null or blank and sent in the request

 *Function : InvalidAuthTokenGivenTest:
 *		Invalid Authorization code is sent in the request

 *Function : ValidEnumsTest:
 *		SchemeName only allows either of the values as BBAN and IBAN
 *		Currency value exists either of EUR or GBP.
 *		SchemeName only allows either of the values as BICFI or UKSortCode

 *Function : OnePermissionGivenInToken:
 *		Only ReadAccountsBasic permission is sent in the Authorization code

 *Function : PermissionInvalidGivenInToken:
 *		Permission is not granted into Authorization code to view the account information

 *Function : onlyMandateFieldsTest:
 *		A GET accountInformation response came with all required mandatory fields named as Data, Links and Meta Data
 *		Data cannot be null or blank.
 *		Identification cannot be Null or blank.
 *		SchemeName cannot be Null or blank.
 *		Links cannot be null or blank.
 *		Links is an arrays that may be containing first, last, next and prev fields where self field is mandatory.
 *		Self field cannot be null or blank.
 *		Meta cannot be null or blank

 *Function : nonMandateFieldsTest:
 *		Data field came with Account array that always contains AccountId and Currency and may be contains Account, Nickname and Servicer fields.
 *		Account field is optional and if populated then it is an arrays that always contains Identification, SchemeName fields and may be Name, SecondaryIdentification fields.
 *		SecondaryIdentification is optional and if populated then it can be Null or blank.

 *Function : accIdMoreThan40Test:
 *		AccountId length is greater than 40 characters.

 *Function : fapiMandatoryHeaderMissingTest:
 *		Value of x-fapifinancial-id is missing in the request header.

 *Function : PostRequestTest:
 *		AISP send a request other than GET method to collect Account Information

 *Function : AcceptFieldInHeaderTest:
 *		An optional field Accept sent with the value as application/json.
 *		If optional field Accept sent and its values is other than application/json.

 *Function : xfapiInteractionIdTest:
 *		In successful response, x-fapi-interaction-id must come into header.
 *		Value of x-fapi-interaction-id in response is always same if x-fapi-interaction-id sent in the request.
 *		If the value of x-fapi-interaction-id is absent or blank in the request then its value must sent by the ASASP.

 *Function : ResponseHeaderFieldsTest:
 *		In successful response, Content-type header should be populate with value as application/json
 
 *Function : testExpiredAuthorizationToken:
 *		Expired Authorization code is sent in the request
 **/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AccountInformationITConfig.class)
@WebIntegrationTest
public class AccountInformationIT {

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	@Value("${app.accountId1}")
	private String accountId1;
	@Value("${app.accountId2}")
	private String accountId2;
	@Value("${app.accountId3}")
	private String accountId3;
	@Value("${app.accountId4}")
	private String accountId4;
	@Value("${app.invalidAccountId}")
	private String invalidAccountId;
	@Value("${app.accountInformationURL}")
	private String accountInformationURL;
	@Value("${app.refToken}")
	private String refToken;
	@Value("${app.invalidRefToken}")
	private String invalidRefToken;
	@Value("${app.P2RefToken}")
	private String P2RefToken;
	@Value("${app.P3RefToken}")
	private String P3RefToken;
	@Value("${app.expiredToken}")
	private String expiredToken;

	@Autowired
	protected RestClientSyncImpl restClientSync;

	@Autowired
	protected RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
	}

//--------------------------- Single Account Information IT -------------------------- 	
	//valid Permissions : #"ReadAccountsBasic","ReadAccountsDetail","ReadBalances"
	/** 		AISP is able to send a Account information Request through GET method.
	 * 		Account info  Request always contains valid AccountId
	 * 		AccountId cannot be Null or blank.
	 * 		AccountId length should be upto 40 characters.
	 * 		Valid Authorization header is sent in the request
	 * 		Submitted request can contain optional headers named as x-fapi-customer-ip-address, x-fapi-customer-last-logged-time and x-fapi-interaction-id where x-fapi-financial-id is mandatory.
	 * 		Identification length should be upto 34 characters.
	 * 		Name length should be upto 70 characters.
	 *		If populated then SecondaryIdentification length should be upto 34 characters.
	 *		AccountId cannot be Null or blank.
	 *		AccountId only contains the same values as sent in the request.
	 *		AccountId length should be upto 40 characters.
	 *		Currency cannot be Null or blank.
	 *		Currency only allows Capital Alphabetic Values
	 *		Currency length must be 3 character.
	 *		If populated then Nickname length should be upto 70 characters.
	 *		If populated then it is an arrays that must be containing Identification and SchemeName fields.
	 *		Meta only contain total-pages field that can be optional.
	 *		If Total-pages populate then it only contains integer values upto 32 characters.*/
	@Test
	public void successRetrieveAccountInformationTest() {
		String url = edgeserverURL /*+accountInformationURL */+"/accounts/" + accountId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				AccountGETResponse.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		AccountGETResponse accountGETResponse = response.getBody();
		assertNotNull(accountGETResponse);
		assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
		List<Account> accounts = accountGETResponse.getData().getAccount();
		for (Account accountGETResponseData : accounts) {
			assertEquals(accountId1, accountGETResponseData.getAccountId());
			assertTrue(accountGETResponseData.getAccountId().length() <= 40);
			assertNotNull(accountGETResponseData.getCurrency());
			assertTrue(CharMatcher.JAVA_UPPER_CASE.matchesAllOf(accountGETResponseData.getCurrency()));
			assertNotNull(accountGETResponseData.getNickname());
			assertTrue(accountGETResponseData.getNickname().length() <= 70);
			assertNotNull(accountGETResponseData.getAccount());
			assertNotNull(accountGETResponseData.getAccount().getSchemeName());
			assertNotNull(accountGETResponseData.getAccount().getIdentification());
			assertTrue(accountGETResponseData.getAccount().getIdentification().length() <= 34);
			assertNotNull(accountGETResponseData.getServicer());
			assertNotNull(accountGETResponseData.getServicer().getIdentification());
			assertNotNull(accountGETResponseData.getServicer().getSchemeName());
		}
		assertNotNull(accountGETResponse.getLinks());
		assertNotNull(accountGETResponse.getLinks().getSelf());
		assertNotNull(accountGETResponse.getMeta());
		assertTrue(accountGETResponse.getMeta().getTotalPages().toString().length() <= 32);
	}

	// AccountID is not valid //401 UNAUTHORIZED
	/** Account info Request contains invalid AccountId (doesn't exist in database)*/	
	@Test(expected = PSD2Exception.class)
	public void invalidAccIdGivenTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + invalidAccountId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("For the given account,user is not authorized to access the requested resource.",
					e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	// AccountID is not present, //404 NOT_FOUND
	/**AccountId is Null or blank.*/
	@Test(expected = HttpClientErrorException.class)
	public void nullAccIdGivenTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/"+ null;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			throw e;
		}
	}

	// Auth code is absent //401 UNAUTHORIZED
	/**Authorization header is null or blank and sent in the request*/
	@Test(expected = HttpClientErrorException.class)
	public void noAuthTokenGivenTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			throw e;
		}
	}

	// Auth code is not valid //401 UNAUTHORIZED
	/**Invalid Authorization code is sent in the request*/
	@Test(expected = PSD2Exception.class)
	public void invalidAuthTokenGivenTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", invalidRefToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertNotNull(e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	/**		SchemeName only allows either of the values as BBAN and IBAN
	 *		Currency value exists either of EUR or GBP.
	 *		SchemeName only allows either of the values as BICFI or UKSortCode*/
	@Test
	public void validEnumsTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
				headers);
		assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
		List<Account> accounts = accountGETResponse.getData().getAccount();
		for (Account accountGETResponseData : accounts) {
			com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum accntIdentSchemeName = accountGETResponseData.getAccount().getSchemeName();
			com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum servicerSchemeName = accountGETResponseData
					.getServicer().getSchemeName();

			Boolean flag = Boolean.FALSE;
			for (com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum schemeNameType : com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.values()) {
				if (schemeNameType.equals(accntIdentSchemeName)) {
					assertEquals(schemeNameType, accntIdentSchemeName);
					flag = Boolean.TRUE;
				}
			}
			assertEquals(Boolean.TRUE, flag);
			flag = Boolean.FALSE;
			for (com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum schemeNameType : com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum
					.values()) {
				if (schemeNameType.equals(servicerSchemeName)) {
					assertEquals(schemeNameType, servicerSchemeName);
					flag = Boolean.TRUE;
				}
			}
			// Currency check
			assertEquals(Boolean.TRUE, flag);
			flag = (accountGETResponseData.getCurrency().equals("EUR")
					|| accountGETResponseData.getCurrency().equals("GBP")) ? Boolean.TRUE : Boolean.FALSE;
			assertEquals(Boolean.TRUE, flag);
		}
	}

	// Permission ReadAccountsBasic
	/**Only ReadAccountsBasic permission is sent in the Authorization code*/
	@Test
	public void onePermissionGivenInTokenTest() {
		String url = edgeserverURL /*+accountInformationURL */+"/accounts/" + accountId3;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", P2RefToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
				headers);
		assertNotNull(accountGETResponse);
		assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
		List<Account> accounts = accountGETResponse.getData().getAccount();
		for (Account accountGETResponseData : accounts) {
			assertNotNull(accountGETResponseData.getAccountId());
			assertNotNull(accountGETResponseData.getCurrency());
			assertNotNull(accountGETResponseData.getNickname());

			assertNull(accountGETResponseData.getAccount());
			assertNull(accountGETResponseData.getServicer());

		}
		assertNotNull(accountGETResponse.getLinks());
		assertNotNull(accountGETResponse.getLinks().getSelf());
	}

	// InvalidPermission : ReadBalances //403 FORBIDDEN
	/**Permission is not granted into Authorization code to view the account information*/
	@Test(expected = PSD2Exception.class)
	public void permissionInvalidGivenInTokenTest() {
		String url = edgeserverURL /*+accountInformationURL */+"/accounts/" + accountId3;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", P3RefToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}

	}

	// Strict Mandate fields Test
	/**		A GET accountInformation response came with all required mandatory fields named as Data, Links and Meta Data
	 *		Data cannot be null or blank.
	 *		Identification cannot be Null or blank.
	 *		SchemeName cannot be Null or blank.
	 *		Links cannot be null or blank.
	 *		Links is an arrays that may be containing first, last, next and prev fields where self field is mandatory.
	 *		Self field cannot be null or blank.
	 *		Meta cannot be null or blank*/
	@Test
	public void onlyMandateFieldsTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId2;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
				headers);
		assertNotNull(accountGETResponse);
		assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
		List<Account> accounts = accountGETResponse.getData().getAccount();
		for (Account accountGETResponseData : accounts) {
			assertNotNull(accountGETResponseData.getAccountId());
			assertNotNull(accountGETResponseData.getCurrency());

			assertNotNull(accountGETResponseData.getAccount());
			assertNotNull(accountGETResponseData.getAccount().getIdentification());
			assertNotNull(accountGETResponseData.getAccount().getSchemeName());

			assertNotNull(accountGETResponseData.getServicer());
		}
		assertNotNull(accountGETResponse.getMeta());
		assertNotNull(accountGETResponse.getLinks());
		assertNotNull(accountGETResponse.getLinks().getSelf());
		assertNull(accountGETResponse.getLinks().getFirst());
		assertNull(accountGETResponse.getLinks().getLast());
		assertNull(accountGETResponse.getLinks().getNext());
		assertNull(accountGETResponse.getLinks().getPrev());

	}

	// Mandate-NonMandateFieldsTest-2
	/**		Data field came with Account array that always contains AccountId and Currency and may be contains Account, Nickname and Servicer fields.
	 *		Account field is optional and if populated then it is an arrays that always contains Identification, SchemeName fields and may be Name, SecondaryIdentification fields.
	 *		SecondaryIdentification is optional and if populated then it can be Null or blank.*/
	@Test
	public void nonMandateFieldsTest() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId2;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
				headers);
		assertNotNull(accountGETResponse);
		assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
		
		List<Account> accounts = accountGETResponse.getData().getAccount();
		for (Account accountGETResponseData : accounts) {
			assertNotNull(accountGETResponseData.getAccountId());
			assertNotNull(accountGETResponseData.getCurrency());
			assertNotNull(accountGETResponseData.getAccount());
			assertNotNull(accountGETResponseData.getAccount().getIdentification());
			assertNotNull(accountGETResponseData.getAccount().getSchemeName());
			assertNotNull(accountGETResponseData.getServicer());
			assertNotNull(accountGETResponseData.getServicer().getIdentification());
			assertNotNull(accountGETResponseData.getServicer().getSchemeName());

			assertNull(accountGETResponseData.getAccount().getSecondaryIdentification());
		}
		assertNotNull(accountGETResponse.getLinks());
		assertNotNull(accountGETResponse.getLinks().getSelf());
	}

	// AccountId more than 40 characters Test //400 BAD_REQUEST
	/**AccountId length is greater than 40 characters.*/
	@Test(expected = PSD2Exception.class)
	public void accIdMoreThan40Test() {
		String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId4;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}


	// fapi-financial-id mandatory header Test //400 BAD_REQUEST
		/**Value of x-fapifinancial-id is missing in the request header.*/
		@Test(expected = HttpClientErrorException.class)
		public void fapiMandatoryHeaderMissingTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				HttpEntity<?> requestEntity = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
			} catch (HttpClientErrorException e) {
				assertTrue(e.getResponseBodyAsString().contains("x-fapi-financial-id is missing in headers."));
				assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
				throw e;
			}
		}

		// Post Request Sent Test, 405 METHOD_NOT_ALLOWED
		/**AISP send a request other than GET method to collect Account Information*/
		@Test(expected = HttpClientErrorException.class)
		public void postRequestTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);

			try {
				HttpEntity<?> requestEntity = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.POST, requestEntity, Object.class);
			} catch (HttpClientErrorException e) {
				assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
				throw e;
			}

		}

		/**		An optional field Accept sent with the value as application/json.
		 *		If optional field Accept sent and its values is other than application/json.*/
		@Test(expected = HttpClientErrorException.class)
		public void acceptFieldInHeaderTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			headers.add("Accept", "application/json");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertEquals(HttpStatus.OK, response.getStatusCode());
			try {
				headers.remove("Accept");
				headers.add("Accept", "text");
				HttpEntity<?> requestEntity2 = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.GET, requestEntity2, Object.class);
			} catch (HttpClientErrorException e) {
				assertEquals(HttpStatus.NOT_ACCEPTABLE, e.getStatusCode());
				assertTrue(response.getHeaders().get("Content-Type").toString().contains(("application/json")));
				throw e;
			}
		}


		/**		In successful response, x-fapi-interaction-id must come into header.
		 *		Value of x-fapi-interaction-id in response is always same if x-fapi-interaction-id sent in the request.
		 *		If the value of x-fapi-interaction-id is absent or blank in the request then its value must sent by the ASASP.*/
		@Test
		public void xfapiInteractionIdTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			headers.add("x-fapi-interaction-id", "xyz123");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);

			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertTrue(response.getHeaders().get("x-fapi-interaction-id").contains("xyz123"));

			headers.remove("x-fapi-interaction-id");
			HttpEntity<?> requestEntity2 = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response2 = restTemplate.exchange(url, HttpMethod.GET, requestEntity2,
					AccountGETResponse.class);
			assertNotNull(response2.getHeaders().get("x-fapi-interaction-id"));
			assertTrue(!(response2.getHeaders().get("x-fapi-interaction-id").contains("xyz123")));
		}

		// Response Header Fields Test
		/**In successful response, Content-type header should be populate with value as application/json*/
		@Test
		public void responseHeaderFieldsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertNotNull(response.getHeaders().get("Content-Type"));
			assertTrue(response.getHeaders().get("Content-Type").toString().contains(("application/json")));

		}
		
		/**Expired Authorization code is sent in the request*/	
		@Test(expected = PSD2Exception.class)
		public void expiredAuthorizationTokenTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts/" + accountId1;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization",expiredToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
			} catch (PSD2Exception e) {
				assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
				throw e;
			}
		}
		
//--------------------------- Multiple Account Information IT --------------------------
		
		// fapi-financial-id mandatory header Test //400 BAD_REQUEST
		/**Value of x-fapifinancial-id is missing in the request header.*/
		@Test(expected = HttpClientErrorException.class)
		public void fapiMandatoryHeaderMissingMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				HttpEntity<?> requestEntity = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
			} catch (HttpClientErrorException e) {
				assertTrue(e.getResponseBodyAsString().contains("x-fapi-financial-id is missing in headers."));
				assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
				throw e;
			}
		}

		// Post Request Sent Test, 405 METHOD_NOT_ALLOWED
		/**AISP send a request other than GET method to collect Account Information*/
		@Test(expected = HttpClientErrorException.class)
		public void putRequestMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts" ;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);

			try {
				HttpEntity<?> requestEntity = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Object.class);
			} catch (HttpClientErrorException e) {
				assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
				throw e;
			}

		}

		/**		An optional field Accept sent with the value as application/json.
		 *		If optional field Accept sent and its values is other than application/json.*/
		@Test(expected = HttpClientErrorException.class)
		public void acceptFieldInHeaderMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts" ;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			headers.add("Accept", "application/json");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertEquals(HttpStatus.OK, response.getStatusCode());
			try {
				headers.remove("Accept");
				headers.add("Accept", "text");
				HttpEntity<?> requestEntity2 = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.GET, requestEntity2, Object.class);
			} catch (HttpClientErrorException e) {
				assertEquals(HttpStatus.NOT_ACCEPTABLE, e.getStatusCode());
				assertTrue(response.getHeaders().get("Content-Type").toString().contains(("application/json")));
				throw e;
			}
		}


		/**		In successful response, x-fapi-interaction-id must come into header.
		 *		Value of x-fapi-interaction-id in response is always same if x-fapi-interaction-id sent in the request.
		 *		If the value of x-fapi-interaction-id is absent or blank in the request then its value must sent by the ASASP.*/
		@Test
		public void xfapiInteractionIdMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts" ;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			headers.add("x-fapi-interaction-id", "xyz123");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);

			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertTrue(response.getHeaders().get("x-fapi-interaction-id").contains("xyz123"));

			headers.remove("x-fapi-interaction-id");
			HttpEntity<?> requestEntity2 = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response2 = restTemplate.exchange(url, HttpMethod.GET, requestEntity2,
					AccountGETResponse.class);
			assertNotNull(response2.getHeaders().get("x-fapi-interaction-id"));
			assertTrue(!(response2.getHeaders().get("x-fapi-interaction-id").contains("xyz123")));
		}

		// Response Header Fields Test
		/**In successful response, Content-type header should be populate with value as application/json*/
		@Test
		public void responseHeaderFieldsMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertNotNull(response.getHeaders().get("Content-Type"));
			assertTrue(response.getHeaders().get("Content-Type").toString().contains(("application/json")));

		}
		
		/**Expired Authorization code is sent in the request*/	
		@Test(expected = PSD2Exception.class)
		public void expiredAuthorizationTokenMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization",expiredToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
			} catch (PSD2Exception e) {
				assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
				throw e;
			}
		}

		// Auth code is absent //401 UNAUTHORIZED
		/**Authorization header is null or blank and sent in the request*/
		@Test(expected = HttpClientErrorException.class)
		public void noAuthTokenGivenMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				HttpEntity<?> requestEntity = new HttpEntity<>(headers);
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
			} catch (HttpClientErrorException e) {
				assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
				throw e;
			}
		}

		// Auth code is not valid //401 UNAUTHORIZED
		/**Invalid Authorization code is sent in the request*/
		@Test(expected = PSD2Exception.class)
		public void invalidAuthTokenGivenMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", invalidRefToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
			} catch (PSD2Exception e) {
				assertNotNull(e.getErrorInfo().getErrorMessage());
				assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
				throw e;
			}
		}
		
		@Test
		public void onlyMandateFieldsMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts" ;
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
					headers);
			assertNotNull(accountGETResponse);
			assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
			List<Account> accounts = accountGETResponse.getData().getAccount();
			for (Account accountGETResponseData : accounts) {
				assertNotNull(accountGETResponseData.getAccountId());
				assertNotNull(accountGETResponseData.getCurrency());

				assertNotNull(accountGETResponseData.getAccount());
				assertNotNull(accountGETResponseData.getAccount().getIdentification());
				assertNotNull(accountGETResponseData.getAccount().getSchemeName());

				assertNotNull(accountGETResponseData.getServicer());
			}
			assertNotNull(accountGETResponse.getMeta());
			assertNotNull(accountGETResponse.getLinks());
			assertNotNull(accountGETResponse.getLinks().getSelf());
			assertNull(accountGETResponse.getLinks().getFirst());
			assertNull(accountGETResponse.getLinks().getLast());
			assertNull(accountGETResponse.getLinks().getNext());
			assertNull(accountGETResponse.getLinks().getPrev());

		}

		// Mandate-NonMandateFieldsTest-2
		@Test
		public void nonMandateFieldsMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
					headers);
			assertNotNull(accountGETResponse);
			assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
			List<Account> accounts = accountGETResponse.getData().getAccount();
			for (Account accountGETResponseData : accounts) {
				assertNotNull(accountGETResponseData.getAccountId());
				assertNotNull(accountGETResponseData.getCurrency());
				assertNotNull(accountGETResponseData.getAccount());
				assertNotNull(accountGETResponseData.getAccount().getIdentification());
				assertNotNull(accountGETResponseData.getAccount().getSchemeName());
				assertNotNull(accountGETResponseData.getServicer());
				assertNotNull(accountGETResponseData.getServicer().getIdentification());
				assertNotNull(accountGETResponseData.getServicer().getSchemeName());

				assertNull(accountGETResponseData.getAccount().getSecondaryIdentification());
			}
			assertNotNull(accountGETResponse.getLinks());
			assertNotNull(accountGETResponse.getLinks().getSelf());
		}

		// Permission ReadAccountsBasic
		/**Only ReadAccountsBasic permission is sent in the Authorization code*/
		@Test
		public void onePermissionGivenInTokenMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+"/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", P2RefToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
					headers);
			assertNotNull(accountGETResponse);
			assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
			List<Account> accounts = accountGETResponse.getData().getAccount();
			for (Account accountGETResponseData : accounts) {
				assertNotNull(accountGETResponseData.getAccountId());
				assertNotNull(accountGETResponseData.getCurrency());
				assertNotNull(accountGETResponseData.getNickname());

				assertNull(accountGETResponseData.getAccount());
				assertNull(accountGETResponseData.getServicer());

			}
			assertNotNull(accountGETResponse.getLinks());
			assertNotNull(accountGETResponse.getLinks().getSelf());
		}

		/**Permission is not granted into Authorization code to view the account information*/
		@Test(expected = PSD2Exception.class)
		public void permissionInvalidGivenInTokenMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+"/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", P3RefToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			try {
				restClientSync.callForGet(requestInfo, AccountGETResponse.class, headers);
			} catch (PSD2Exception e) {
				assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
				throw e;
			}

		}
		
		/**		SchemeName only allows either of the values as BBAN and IBAN
		 *		Currency value exists either of EUR or GBP.
		 *		SchemeName only allows either of the values as BICFI or UKSortCode*/
		@Test
		public void validEnumsMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+ "/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(url);
			AccountGETResponse accountGETResponse = restClientSync.callForGet(requestInfo, AccountGETResponse.class,
					headers);
			assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
			List<Account> accounts = accountGETResponse.getData().getAccount();
			for (Account accountGETResponseData : accounts) {
				SchemeNameEnum accntIdentSchemeName = accountGETResponseData.getAccount().getSchemeName();
				com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum servicerSchemeName = accountGETResponseData
						.getServicer().getSchemeName();

				Boolean flag = Boolean.FALSE;
				for (SchemeNameEnum schemeNameType : SchemeNameEnum.values()) {
					if (schemeNameType.equals(accntIdentSchemeName)) {
						assertEquals(schemeNameType, accntIdentSchemeName);
						flag = Boolean.TRUE;
					}
				}
				assertEquals(Boolean.TRUE, flag);
				flag = Boolean.FALSE;
				for (com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum schemeNameType : com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum
						.values()) {
					if (schemeNameType.equals(servicerSchemeName)) {
						assertEquals(schemeNameType, servicerSchemeName);
						flag = Boolean.TRUE;
					}
				}
				// Currency check
				assertEquals(Boolean.TRUE, flag);
				flag = (accountGETResponseData.getCurrency().equals("EUR")
						|| accountGETResponseData.getCurrency().equals("GBP")) ? Boolean.TRUE : Boolean.FALSE;
				assertEquals(Boolean.TRUE, flag);
			}
		}

		//valid Permissions : #"ReadAccountsBasic","ReadAccountsDetail","ReadBalances"
		/** 		AISP is able to send a Account information Request through GET method.
		 * 		Account info  Request always contains valid AccountId
		 * 		AccountId cannot be Null or blank.
		 * 		AccountId length should be upto 40 characters.
		 * 		Valid Authorization header is sent in the request
		 * 		Submitted request can contain optional headers named as x-fapi-customer-ip-address, x-fapi-customer-last-logged-time and x-fapi-interaction-id where x-fapi-financial-id is mandatory.
		 * 		Identification length should be upto 34 characters.
		 * 		Name length should be upto 70 characters.
		 *		If populated then SecondaryIdentification length should be upto 34 characters.
		 *		AccountId cannot be Null or blank.
		 *		AccountId only contains the same values as sent in the request.
		 *		AccountId length should be upto 40 characters.
		 *		Currency cannot be Null or blank.
		 *		Currency only allows Capital Alphabetic Values
		 *		Currency length must be 3 character.
		 *		If populated then Nickname length should be upto 70 characters.
		 *		If populated then it is an arrays that must be containing Identification and SchemeName fields.
		 *		Meta only contain total-pages field that can be optional.
		 *		If Total-pages populate then it only contains integer values upto 32 characters.*/
		@Test
		public void successRetrieveAccountInformationMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+"/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertEquals(HttpStatus.OK, response.getStatusCode());
			AccountGETResponse accountGETResponse = response.getBody();
			assertNotNull(accountGETResponse);
			assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
			List<Account> accounts = accountGETResponse.getData().getAccount();
			for (Account accountGETResponseData : accounts) {
				assertTrue(accountGETResponseData.getAccountId().length() <= 40);
				assertNotNull(accountGETResponseData.getCurrency());
				assertTrue(CharMatcher.JAVA_UPPER_CASE.matchesAllOf(accountGETResponseData.getCurrency()));
				assertNotNull(accountGETResponseData.getNickname());
				assertTrue(accountGETResponseData.getNickname().length() <= 70);
				assertNotNull(accountGETResponseData.getAccount());
				assertNotNull(accountGETResponseData.getAccount().getSchemeName());
				assertNotNull(accountGETResponseData.getAccount().getIdentification());
				assertTrue(accountGETResponseData.getAccount().getIdentification().length() <= 34);
				assertNotNull(accountGETResponseData.getServicer());
				assertNotNull(accountGETResponseData.getServicer().getIdentification());
				assertNotNull(accountGETResponseData.getServicer().getSchemeName());
			}
			assertNotNull(accountGETResponse.getLinks());
			assertNotNull(accountGETResponse.getLinks().getSelf());
			assertNotNull(accountGETResponse.getMeta());
			assertTrue(accountGETResponse.getMeta().getTotalPages().toString().length() <= 32);
		}
		
		
		/* 1. Accounts Count in the response Should be as expected as per the data in the database 
		 * and 
		 * 2. if Accountid is same and AccountNSC does not match with the data From the consent
		 * 3. when consent has 3 accounts for the user but Bank has more than 3 accounts for the same user. 
		the response should show the data for the accounts that are present in consent and bank database
		*/
		@Test
		public void accountsCountWhenAccountNSCisDifferentAndAccountIdIsSameInBankDatabaseAndAccontInMultipleAccountsTest() {
			String url = edgeserverURL /*+accountInformationURL */+"/accounts";
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", refToken);
			headers.add("x-fapi-financial-id", "1234");
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			ResponseEntity<AccountGETResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AccountGETResponse.class);
			assertEquals(HttpStatus.OK, response.getStatusCode());
			AccountGETResponse accountGETResponse = response.getBody();
			assertNotNull(accountGETResponse);
			assertTrue(!accountGETResponse.getData().getAccount().isEmpty());
			List<Account> accounts = accountGETResponse.getData().getAccount();
			assertEquals(2, accounts.size());
			String accountNumber1 = accounts.get(0).getAccount().getIdentification();
			String accountNumber2 = accounts.get(1).getAccount().getIdentification();
			String accountNumberA = "23682877";
			String accountNumberB = "23682876";
			if(accountNumberA.equals(accountNumber1) || accountNumberB.equals(accountNumber1)){}
				else
					assertFalse(false);
			if(accountNumberB.equals(accountNumber1) || accountNumberA.equals(accountNumber1)){}
				else
					assertFalse(false);
			
			for (Account accountGETResponseData : accounts) {
				assertTrue(accountGETResponseData.getAccountId().length() <= 40);
				assertNotNull(accountGETResponseData.getCurrency());
				assertTrue(CharMatcher.JAVA_UPPER_CASE.matchesAllOf(accountGETResponseData.getCurrency()));
				assertNotNull(accountGETResponseData.getNickname());
				assertTrue(accountGETResponseData.getNickname().length() <= 70);
				assertNotNull(accountGETResponseData.getAccount());
				assertNotNull(accountGETResponseData.getAccount().getSchemeName());
				assertNotNull(accountGETResponseData.getAccount().getIdentification());
				assertTrue(accountGETResponseData.getAccount().getIdentification().length() <= 34);
				assertNotNull(accountGETResponseData.getServicer());
				assertNotNull(accountGETResponseData.getServicer().getIdentification());
				assertNotNull(accountGETResponseData.getServicer().getSchemeName());
			}
			assertNotNull(accountGETResponse.getLinks());
			assertNotNull(accountGETResponse.getLinks().getSelf());
			assertNotNull(accountGETResponse.getMeta());
			assertTrue(accountGETResponse.getMeta().getTotalPages().toString().length() <= 32);
		}
		
}