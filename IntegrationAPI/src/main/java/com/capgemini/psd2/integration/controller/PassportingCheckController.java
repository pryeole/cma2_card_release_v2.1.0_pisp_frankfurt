package com.capgemini.psd2.integration.controller;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.service.ITppDetailsService;

@RestController
public class PassportingCheckController {

	@Autowired
	ITppDetailsService iTppDetailsService;

	@RequestMapping(value = "/passporting", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, Boolean> passportingCheck(@RequestParam(value = "passportingId", required = true) String id,
			@RequestParam(value = "roles", required = true) String roles,
			@RequestParam(value = "tenantid", required = true) String tenantid,
			@RequestParam(value = "flag", required = true) String flag) {
		boolean passportingResult = iTppDetailsService.passporting(id, roles, tenantid, flag);
		if (passportingResult) {
			return Collections.singletonMap("passporting", passportingResult);
		}
		throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PASSPORTING_FAILED);
	}

}
