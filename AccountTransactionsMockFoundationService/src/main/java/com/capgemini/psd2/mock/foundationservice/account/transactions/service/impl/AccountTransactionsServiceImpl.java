package com.capgemini.psd2.mock.foundationservice.account.transactions.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Accnt;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Accounts;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.CreditCardTransaction;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.CreditCardTransactions;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.GroupByDate;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Transaction;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Transactions;
import com.capgemini.psd2.mock.foundationservice.account.transactions.exception.InvalidParameterRequestException;
import com.capgemini.psd2.mock.foundationservice.account.transactions.exception.RecordNotFoundException;
import com.capgemini.psd2.mock.foundationservice.account.transactions.repository.AccountTransactionsRepository;
import com.capgemini.psd2.mock.foundationservice.account.transactions.service.AccountTransactionsService;

@Service
public class AccountTransactionsServiceImpl implements AccountTransactionsService {

	@Value("${foundationservice.pageSize}")
	private int defaultPageSize;
	
	@Autowired
	private AccountTransactionsRepository repository;

	Map<CacheAndPagination, Accnt> cache;

	@PostConstruct
	public void init() {
		cache = new ConcurrentHashMap<CacheAndPagination, Accnt>();
	}
	
	@PreDestroy
	public void destroy() {
		cache = null;
	}
	
	/* (non-Javadoc)
	 * @see com.capgemini.psd2.mock.foundationservice.account.transactions.service.AccountTransactionsService#retrieveAccountTransactions(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Accounts retrieveAccountTransactions(String nsc, String accountNumber, String FromBookingDateTime, String ToBookingDateTime, String RequestedPageNumber, String txnType, String pageSize) throws Exception {
		Accnt accnt = null;
		/**Applying Caching */
		CacheAndPagination obj =  new CacheAndPagination(nsc,accountNumber,txnType);
		if(cache.get(obj) == null){
			accnt = repository.findByNscAndAccountNumber(nsc, accountNumber);
			if (accnt == null) {
				throw new RecordNotFoundException("Not Found");			
			}
			Accnt newObject = new Accnt(); 
			//BeanUtils.copyProperties(accnt,newObject);
			//cache.put(obj, newObject);//Disabled caching for now
		}else {
				accnt = cache.get(obj);
				if(cache.size()>50) {
					cache.clear();
				}
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date FromBookingDate = null;
		Date ToBookingDate = null;
        try {
        	FromBookingDate = formatter.parse(FromBookingDateTime);
        	ToBookingDate = formatter.parse(ToBookingDateTime);
        }catch(ParseException e) {
        }
		List<GroupByDate> groupByDateList = new ArrayList<GroupByDate>();
		Transactions transactions = accnt.getTransactions();
		if(transactions != null) {
			for(GroupByDate groupByDate : transactions.getGroupByDate()) {
				String postedDateStr = formatter.format(groupByDate.getDatePosted().toGregorianCalendar().getTime());
				Date postedDate = formatter.parse(postedDateStr);
				if((postedDate.compareTo(FromBookingDate)==0 || postedDate.after(FromBookingDate)) && (postedDate.compareTo(ToBookingDate)==0 || postedDate.before(ToBookingDate))) {
					groupByDateList.add(groupByDate);
				}
			}
			transactions.getGroupByDate().clear();
			for(GroupByDate groupByDate : groupByDateList) {
				transactions.getGroupByDate().add(groupByDate);
			}
		}
		/**Sorting in descending order to show the latest transactions first and so on */
		Collections.sort(accnt.getTransactions().getGroupByDate(), new Comparator<GroupByDate>() {
			  public int compare(GroupByDate o1, GroupByDate o2) {
			      return o2.getDatePosted().toGregorianCalendar().getTime().compareTo(o1.getDatePosted().toGregorianCalendar().getTime());
			  }
			});
		
		/**Applying Transaction Filter */
		if(!("ALL".equalsIgnoreCase(txnType))) {
			String TransactionFilterValue = null;
			if("CREDIT".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "CR";
			}else if("DEBIT".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "DR";
			}
			List<GroupByDate> groupByDateListFiltered = new ArrayList<GroupByDate>();
			for(GroupByDate groupByDateTemp : accnt.getTransactions().getGroupByDate()) {
				List<Transaction> transactionListFiltered = new ArrayList<Transaction>();
				for(Transaction transaction : groupByDateTemp.getTransaction()) {
					if(transaction.getTransactionType().value().equalsIgnoreCase(TransactionFilterValue)) {
						transactionListFiltered.add(transaction);
					}
				}
				GroupByDate temp = new GroupByDate();
				temp.setBalanceType(groupByDateTemp.getBalanceType());
				temp.setCurrency(groupByDateTemp.getCurrency());
				temp.setDatePosted(groupByDateTemp.getDatePosted());
				temp.setEodBalance(groupByDateTemp.getEodBalance());
				for(Transaction transaction : transactionListFiltered) {
					temp.getTransaction().add(transaction);
				}
				groupByDateListFiltered.add(temp);
			}
			accnt.getTransactions().getGroupByDate().clear();
			for(GroupByDate groupByDate : groupByDateListFiltered) {
				accnt.getTransactions().getGroupByDate().add(groupByDate);
			}
		}
		/**Applying Paginations */
		List<Transaction> noOfTransactions = new ArrayList<Transaction>();
		int count = 0;
		//int pageSize = 5;
		int totalNoOfPages = 0;
		int tranxMissed = 0;
		for(GroupByDate groupByDate : accnt.getTransactions().getGroupByDate()) {
			for(Transaction transaction : groupByDate.getTransaction()) {
				noOfTransactions.add(count,transaction);
				count++;
			}
		}
		int pageSizeInt = 0;
		if(pageSize != null && !pageSize.isEmpty() && !"null".equalsIgnoreCase(pageSize) && Integer.parseInt(pageSize)>0) {
			pageSizeInt = Integer.parseInt(pageSize);
		} else {
			pageSizeInt = defaultPageSize;
		}
		
		totalNoOfPages = (count/pageSizeInt);
		tranxMissed = (count%pageSizeInt);
		if(tranxMissed>0) {
			totalNoOfPages = totalNoOfPages + 1;
		}
		int startTrnx = pageSizeInt*Integer.parseInt(RequestedPageNumber)-pageSizeInt;
		int endTranx = pageSizeInt*Integer.parseInt(RequestedPageNumber);;
		if(totalNoOfPages < (Integer.parseInt(RequestedPageNumber))) {
			throw new RecordNotFoundException("No transactions for the requested page.");
			/*accnt.getTransactions().getGroupByDate().clear();
			accnt.getTransactions().setPageNumber(0);
			if(totalNoOfPages <= (Integer.parseInt(RequestedPageNumber))) {
				accnt.getTransactions().setHasMoreTxns(false);
			}
			Accounts accounts = new Accounts();
			accounts.getAccount().add(accnt);
			return accounts;*/
		}
		int tranxCount = 0;
		List<GroupByDate> groupByDateListFiltered = new ArrayList<GroupByDate>();
		for(GroupByDate groupByDateTemp : accnt.getTransactions().getGroupByDate()) {
			List<Transaction> transactionListFiltered = new ArrayList<Transaction>();
			for(Transaction transaction : groupByDateTemp.getTransaction()) {
				if(startTrnx <= tranxCount && tranxCount< endTranx && tranxCount<count) {
					transactionListFiltered.add(transaction);
				}
				tranxCount++;
			}
			GroupByDate temp = new GroupByDate();
			temp.setBalanceType(groupByDateTemp.getBalanceType());
			temp.setCurrency(groupByDateTemp.getCurrency());
			temp.setDatePosted(groupByDateTemp.getDatePosted());
			temp.setEodBalance(groupByDateTemp.getEodBalance());
			for(Transaction transaction : transactionListFiltered) {
				temp.getTransaction().add(transaction);
			}
			groupByDateListFiltered.add(temp);
		}
		accnt.getTransactions().getGroupByDate().clear();
		for(GroupByDate groupByDate : groupByDateListFiltered) {
			if(!groupByDate.getTransaction().isEmpty())
			accnt.getTransactions().getGroupByDate().add(groupByDate);
		}
		accnt.getTransactions().setPageNumber(Integer.parseInt(RequestedPageNumber));
		if(totalNoOfPages <= (Integer.parseInt(RequestedPageNumber))) {
			accnt.getTransactions().setHasMoreTxns(false);
		}
		Accounts accounts = new Accounts();
		accounts.getAccount().add(accnt);

		return accounts;

	}
	
	@Override
	public Accounts retrievePlApplIdTransactions(String refID, String FromBookingDateTime, String ToBookingDateTime, String RequestedPageNumber, String txnType, String pageSize) throws Exception {
		
		Accnt creditCard = repository.findByPlApplId(refID);
		
		if (creditCard == null) {
			throw new RecordNotFoundException("Not Found");
		}else{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date FromBookingDate = null;
			Date ToBookingDate = null;
	     try {
	        FromBookingDate = formatter.parse(FromBookingDateTime);
	        ToBookingDate = formatter.parse(ToBookingDateTime);
	       }catch(ParseException e) {
	        	System.out.println("e" + e);
	       }
	     List<CreditCardTransaction> creditCardTransactionList = new ArrayList<CreditCardTransaction>();
	     CreditCardTransactions transactions = creditCard.getCreditCardTransactions();
	        
	     if(transactions != null){
	        for(CreditCardTransaction creditCardTransaction : transactions.getCreditCardTransaction()){
	        	String postedDateStr = formatter.format(creditCardTransaction.getDatePosted().toGregorianCalendar().getTime());
				Date postedDate = formatter.parse(postedDateStr);
				if((postedDate.compareTo(FromBookingDate)==0 || postedDate.after(FromBookingDate)) && (postedDate.compareTo(ToBookingDate)==0 || postedDate.before(ToBookingDate))) {
					creditCardTransactionList.add(creditCardTransaction);
				}
	         }
	        transactions.getCreditCardTransaction().clear();
			for(CreditCardTransaction ccTransactions : creditCardTransactionList) {
				transactions.getCreditCardTransaction().add(ccTransactions);
			  }
	        }
	        
	     /**Sorting in descending order to show the latest transactions first and so on */
		 Collections.sort(creditCard.getCreditCardTransactions().getCreditCardTransaction(), new Comparator<CreditCardTransaction>() {
				public int compare(CreditCardTransaction o1, CreditCardTransaction o2) {
				return o2.getDatePosted().toGregorianCalendar().getTime().compareTo(o1.getDatePosted().toGregorianCalendar().getTime());
			   }
			});
			
		/**Applying Transaction Filter */
		if(!("ALL".equalsIgnoreCase(txnType))) {
		   String TransactionFilterValue = null;
		   if("CREDIT".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "CR";
		   }else if("DEBIT".equalsIgnoreCase(txnType)) {
			    TransactionFilterValue = "DR";
		   }
		   List<CreditCardTransaction> creditCardTransactionFiltered = new ArrayList<CreditCardTransaction>();
		   for(CreditCardTransaction creditCardTransaction : creditCard.getCreditCardTransactions().getCreditCardTransaction()) {
			   if(creditCardTransaction.getTransactionType().value().equalsIgnoreCase(TransactionFilterValue)) {
				   creditCardTransactionFiltered.add(creditCardTransaction);
				}
		   }
		   creditCard.getCreditCardTransactions().getCreditCardTransaction().clear();
		   for(CreditCardTransaction groupByDate : creditCardTransactionFiltered) {
				creditCard.getCreditCardTransactions().getCreditCardTransaction().add(groupByDate);
			}
		}
		/*if(!("ALL".equalsIgnoreCase(txnType))) {
				String TransactionFilterValue = null;
				if("CREDIT".equalsIgnoreCase(txnType)) {
					TransactionFilterValue = "CR";
				}else if("DEBIT".equalsIgnoreCase(txnType)) {
					TransactionFilterValue = "DR";
				}
				List<CreditCardTransaction> groupByDateListFiltered = new ArrayList<CreditCardTransaction>();
				for(CreditCardTransaction groupByDateTemp : creditCard.getCreditCardTransactions().getCreditCardTransaction()) {
					
					if(groupByDateTemp.getTransactionType().value().equalsIgnoreCase(TransactionFilterValue)) {
						groupByDateListFiltered.add(groupByDateTemp);
					}
					
					CreditCardTransaction temp = new CreditCardTransaction();
					temp.setCurrency(groupByDateTemp.getCurrency());
					temp.setDatePosted(groupByDateTemp.getDatePosted());
					temp.setDayUsed(groupByDateTemp.getDayUsed());
					temp.setValue(groupByDateTemp.getValue());
					temp.setNarrative1(groupByDateTemp.getNarrative1());
					temp.setMerchantDetails(groupByDateTemp.getMerchantDetails());
					
					groupByDateListFiltered.add(temp);
				}
				creditCard.getCreditCardTransactions().getCreditCardTransaction().clear();
				for(CreditCardTransaction groupByDate : groupByDateListFiltered) {
					creditCard.getCreditCardTransactions().getCreditCardTransaction().add(groupByDate);
				}
			}
			
			**/
			
//			List<CreditCardTransaction> creditCardTransaction = new ArrayList<CreditCardTransaction>();
//			for(CreditCardTransaction groupByDateTemp : creditCard.getCreditCardTransactions().getCreditCardTransaction()) {
//				creditCardTransaction.add(groupByDateTemp);
//			}
//			creditCard.getCreditCardTransactions().getCreditCardTransaction().clear();
//			for(CreditCardTransaction groupByDate : creditCardTransaction) {
//				creditCard.getCreditCardTransactions().getCreditCardTransaction().add(groupByDate);
//			}
			
	    /**Applying Paginations */
		List<CreditCardTransaction> noOfTransactions = new ArrayList<CreditCardTransaction>();
		int count = 0;
		int totalNoOfPages = 0;
		int tranxMissed = 0;
		for(CreditCardTransaction groupByDate : creditCard.getCreditCardTransactions().getCreditCardTransaction()) {
			noOfTransactions.add(count,groupByDate);
			count++;
		 }
		int pageSizeInt = 0;
		if(pageSize != null && !pageSize.isEmpty() && !"null".equalsIgnoreCase(pageSize) && Integer.parseInt(pageSize)>0) {
			pageSizeInt = Integer.parseInt(pageSize);
		} else {
			pageSizeInt = defaultPageSize;
		}
		totalNoOfPages = (count/pageSizeInt);
		tranxMissed = (count%pageSizeInt);
		if(tranxMissed>0) {
			totalNoOfPages = totalNoOfPages + 1;
		}
		int startTrnx = pageSizeInt*Integer.parseInt(RequestedPageNumber)-pageSizeInt;
		int endTranx = pageSizeInt*Integer.parseInt(RequestedPageNumber);;
		if(totalNoOfPages < (Integer.parseInt(RequestedPageNumber))) {
			throw new RecordNotFoundException("No transactions for the requested page.");
		}
		int tranxCount = 0;
			
		List<CreditCardTransaction> groupByDateListFiltered = new ArrayList<CreditCardTransaction>();
		for(CreditCardTransaction groupByDateTemp : creditCard.getCreditCardTransactions().getCreditCardTransaction()){
			if(startTrnx <= tranxCount && tranxCount< endTranx && tranxCount<count) {
				groupByDateListFiltered.add(groupByDateTemp);
		}
		tranxCount++;
				
			   /* CreditCardTransaction temp = new CreditCardTransaction();
				temp.setValue(groupByDateTemp.getValue());
				temp.setCurrency(groupByDateTemp.getCurrency());
				temp.setDatePosted(groupByDateTemp.getDatePosted());
				temp.setMerchantDetails(groupByDateTemp.getMerchantDetails());
				
				groupByDateListFiltered.add(temp);*/
		}
			
		creditCard.getCreditCardTransactions().getCreditCardTransaction().clear();
		for(CreditCardTransaction groupByDate : groupByDateListFiltered) {
			if(!(groupByDate==null))
			creditCard.getCreditCardTransactions().getCreditCardTransaction().add(groupByDate);
		}
		creditCard.getCreditCardTransactions().setPageNumber(Integer.parseInt(RequestedPageNumber));
		if(totalNoOfPages <= (Integer.parseInt(RequestedPageNumber))) {
			creditCard.getCreditCardTransactions().setHasMoreTxns(false);
		}	
	}
		
		Accounts accounts = new Accounts();
		accounts.getAccount().add(creditCard);
		
		return accounts;
	}
  }


/**Class to implement caching and paginations */
class CacheAndPagination{
	String nsc;
	String accountNumber;
	String transactionFilter;

	public CacheAndPagination(String nsc, String accountNumber, String transactionFilter) {
		super();
		this.nsc = nsc;
		this.accountNumber = accountNumber;
		this.transactionFilter = transactionFilter;
	}
	public String getNsc() {
		return nsc;
	}
	public void setNsc(String nsc) {
		this.nsc = nsc;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getTransactionFilter() {
		return transactionFilter;
	}
	public void setTransactionFilter(String transactionFilter) {
		this.transactionFilter = transactionFilter;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((nsc == null) ? 0 : nsc.hashCode());
		result = prime * result + ((transactionFilter == null) ? 0 : transactionFilter.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CacheAndPagination other = (CacheAndPagination) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (nsc == null) {
			if (other.nsc != null)
				return false;
		} else if (!nsc.equals(other.nsc))
			return false;
		if (transactionFilter == null) {
			if (other.transactionFilter != null)
				return false;
		} else if (!transactionFilter.equals(other.transactionFilter))
			return false;
		return true;
	}
}
