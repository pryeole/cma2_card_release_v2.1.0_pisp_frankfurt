package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The Channel Classification Code classifies a single channel with a certain
 * code that is defined by the Channel Classification Code Type. The code is
 * unique for a given Channel Classification Code Type. Only certain
 * combinations are valid e.g. Direct: Online, Phone, Branch
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum ChannelClassificationCode {

	ONLINE("Online"),

	PHONE("Phone"),

	BRANCH("Branch");

	private String value;

	ChannelClassificationCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static ChannelClassificationCode fromValue(String text) {
		for (ChannelClassificationCode b : ChannelClassificationCode.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
