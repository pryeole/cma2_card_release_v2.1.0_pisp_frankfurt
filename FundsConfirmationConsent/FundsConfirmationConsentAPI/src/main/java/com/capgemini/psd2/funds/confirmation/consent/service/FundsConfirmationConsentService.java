package com.capgemini.psd2.funds.confirmation.consent.service;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;

public interface FundsConfirmationConsentService {

	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsent(OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest);

	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsent(String consentId);

	public void removeFundsConfirmationConsent(String consentId);

}
