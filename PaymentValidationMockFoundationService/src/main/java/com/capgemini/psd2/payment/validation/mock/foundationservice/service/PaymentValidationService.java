package com.capgemini.psd2.payment.validation.mock.foundationservice.service;

import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.validation.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentValidationService {
	
	public ValidationPassed validatePaymentInstruction(PaymentInstruction paymentInstr);

}
