/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.mask;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * The Class DataMaskRules.
 */
@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app.rules")
public class DataMaskRules {

	/** The response. */
	private Map<String, Map<String,String>> response = new HashMap<>();

	/** The response log. */
	private Map<String, Map<String,String>> responseLog = new HashMap<>();

	/** The request log. */
	private Map<String, Map<String,String>> requestLog = new HashMap<>();

	/** The mresponse. */
	private Map<String, Map<String,String>> mresponse = new HashMap<>();

	/** The mresponse log. */
	private Map<String, Map<String,String>> mresponseLog = new HashMap<>();

	/** The mrequest log. */
	private Map<String, Map<String,String>> mrequestLog = new HashMap<>();
	
	/**
	 * Gets the response.
	 *
	 * @return the response
	 */
	public Map<String, Map<String, String>> getResponse() {
		return response;
	}

	/**
	 * Sets the response.
	 *
	 * @param response the response
	 */
	public void setResponse(Map<String, Map<String, String>> response) {
		this.response = response;
	}

	/**
	 * Gets the response log.
	 *
	 * @return the response log
	 */
	public Map<String, Map<String, String>> getResponseLog() {
		return responseLog;
	}

	/**
	 * Sets the response log.
	 *
	 * @param responseLog the response log
	 */
	public void setResponseLog(Map<String, Map<String, String>> responseLog) {
		this.responseLog = responseLog;
	}

	/**
	 * Gets the request log.
	 *
	 * @return the request log
	 */
	public Map<String, Map<String, String>> getRequestLog() {
		return requestLog;
	}

	/**
	 * Sets the request log.
	 *
	 * @param requestLog the request log
	 */
	public void setRequestLog(Map<String, Map<String, String>> requestLog) {
		this.requestLog = requestLog;
	}

	/**
	 * Gets the mresponse.
	 *
	 * @return the mresponse
	 */
	public Map<String, Map<String, String>> getMresponse() {
		return mresponse;
	}

	/**
	 * Sets the mresponse.
	 *
	 * @param mresponse the mresponse
	 */
	public void setMresponse(Map<String, Map<String, String>> mresponse) {
		this.mresponse = mresponse;
	}

	/**
	 * Gets the mresponse log.
	 *
	 * @return the mresponse log
	 */
	public Map<String, Map<String, String>> getMresponseLog() {
		return mresponseLog;
	}

	/**
	 * Sets the mresponse log.
	 *
	 * @param mresponseLog the mresponse log
	 */
	public void setMresponseLog(Map<String, Map<String, String>> mresponseLog) {
		this.mresponseLog = mresponseLog;
	}

	/**
	 * Gets the mrequest log.
	 *
	 * @return the mrequest log
	 */
	public Map<String, Map<String, String>> getMrequestLog() {
		return mrequestLog;
	}

	/**
	 * Sets the mrequest log.
	 *
	 * @param mrequestLog the mrequest log
	 */
	public void setMrequestLog(Map<String, Map<String, String>> mrequestLog) {
		this.mrequestLog = mrequestLog;
	}

}
