package com.capgemini.psd2.identitymanagement.models;

import org.springframework.security.core.GrantedAuthority;

public class UserAuthorities implements GrantedAuthority{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String authority;
	
	public UserAuthorities(String authority){
		this.authority = authority;
	}
	
	@Override
	public String getAuthority() {
		return authority;
	}
	
	public String toString(){
		return this.authority;
	}
}
