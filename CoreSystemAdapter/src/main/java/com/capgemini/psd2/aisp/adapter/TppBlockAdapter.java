package com.capgemini.psd2.aisp.adapter;

import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;

public interface TppBlockAdapter {

	public TppStatusDetails fetchTppStatusDetails(String tppId, String tenantId);

	void updateTppStatus(String tppId, ActionEnum action, String tenantId);
	
}
