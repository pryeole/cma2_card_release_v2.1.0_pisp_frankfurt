package com.capgemini.psd2.pisp.domain;

import java.util.List;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;

public class GenericPaymentConsentResponse {

	private String consentId;
	/*
	 * creationDateTime, status, tatusUpdateDateTime : Fields will be populated
	 * from product end.
	 */
	private String cutOffDateTime;
	private String expectedExecutionDateTime;
	private String expectedSettlementDateTime;
	private List<OBCharge1> charges;
	private ProcessConsentStatusEnum processConsentStatusEnum;
	private OBExchangeRate2 exchangeRateInformation;

	public String getConsentId() {
		return consentId;
	}

	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	public String getCutOffDateTime() {
		return cutOffDateTime;
	}

	public void setCutOffDateTime(String cutOffDateTime) {
		this.cutOffDateTime = cutOffDateTime;
	}

	public String getExpectedExecutionDateTime() {
		return expectedExecutionDateTime;
	}
 
	public void setExpectedExecutionDateTime(String expectedExecutionDateTime) {
		this.expectedExecutionDateTime = expectedExecutionDateTime;
	}

	public String getExpectedSettlementDateTime() {
		return expectedSettlementDateTime;
	}

	public void setExpectedSettlementDateTime(String expectedSettlementDateTime) {
		this.expectedSettlementDateTime = expectedSettlementDateTime;
	}

	public List<OBCharge1> getCharges() {
		return charges;
	}

	public void setCharges(List<OBCharge1> charges) {
		this.charges = charges;
	}

	public ProcessConsentStatusEnum getProcessConsentStatusEnum() {
		return processConsentStatusEnum;
	}

	public void setProcessConsentStatusEnum(ProcessConsentStatusEnum processConsentStatusEnum) {
		this.processConsentStatusEnum = processConsentStatusEnum;
	}

	public OBExchangeRate2 getExchangeRateInformation() {
		return exchangeRateInformation;
	}

	public void setExchangeRateInformation(OBExchangeRate2 exchangeRateInformation) {
		this.exchangeRateInformation = exchangeRateInformation;
	}
	
	@Override
	public String toString() {
		return "GenericPaymentConsentResponse [consentId=" + consentId + ", cutOffDateTime=" + cutOffDateTime
				+ ", expectedExecutionDateTime=" + expectedExecutionDateTime + ", expectedSettlementDateTime="
				+ expectedSettlementDateTime + ", charges=" + charges + ", processConsentStatusEnum="
				+ processConsentStatusEnum + ", exchangeRateInformation=" + exchangeRateInformation + "]";
	}

}
