package com.capgemini.psd2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {

	private String id;
	
	private boolean status;
	@JsonProperty("urn:openbanking:organisation:1.0:status")
	private String active;

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}


	public Response(String id, boolean status, String active) {
		super();
		this.id = id;
		this.status = status;
		this.active = active;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
