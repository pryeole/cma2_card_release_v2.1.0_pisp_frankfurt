package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;


@Component
public class DomesticStandingOrdersFoundationServiceClient {
	
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	public StandingOrderInstructionComposite restTransportForDomesticStandingOrdersServicePost(RequestInfo requestInfo,
			StandingOrderInstructionComposite standingInstructionProposalRequest, Class<StandingOrderInstructionComposite> responseType,
			HttpHeaders httpHeaders) {
		
		return restClient.callForPost(requestInfo, standingInstructionProposalRequest, responseType, httpHeaders);
	}
	
	public StandingOrderInstructionComposite restTransportForDomesticStandingOrderServiceGet(RequestInfo reqInfo,
			Class<StandingOrderInstructionComposite> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);

	}

}
