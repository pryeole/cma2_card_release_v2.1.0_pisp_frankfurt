package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionStatusCode2;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledResponse1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticScheduledPaymentSubmissionFoundationServiceTransformer {
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Value("${foundationService.endToEndIdentificationMockValue}")
	private String endToEndIdentificationMockValue;

	public <T> GenericPaymentsResponse transformDomesticScheduledPaymentsFromFDToAPIForInsert(
			T paymentInstructionProposalResponse) {
		GenericPaymentsResponse submissionResponse = new GenericPaymentsResponse();
		ScheduledPaymentInstructionComposite scheduledPaymentInstructionComposite = (ScheduledPaymentInstructionComposite) paymentInstructionProposalResponse;
		List<OBCharge1> charges = new ArrayList<>();
		if (!NullCheckUtils
				.isNullOrEmpty(scheduledPaymentInstructionComposite.getPaymentInstructionProposal().getCharges())) {
			OBCharge1 oBCharge1 = new OBCharge1();
			OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
			
			List<PaymentInstructionCharge> paymentInstructionCharges;
			oBCharge1Amount.setAmount(scheduledPaymentInstructionComposite.getPaymentInstructionProposal().getCharges()
					.get(0).getAmount().getTransactionCurrency().toString());
			oBCharge1Amount.setCurrency(scheduledPaymentInstructionComposite.getPaymentInstructionProposal()
					.getCharges().get(0).getCurrency().getIsoAlphaCode());
			paymentInstructionCharges = scheduledPaymentInstructionComposite.getPaymentInstructionProposal()
					.getCharges();
			for (PaymentInstructionCharge charge : paymentInstructionCharges) {
				oBCharge1.setChargeBearer(OBChargeBearerType1Code.fromValue(String.valueOf(charge.getChargeBearer())));
				oBCharge1.setType(charge.getType());
				oBCharge1.setAmount(oBCharge1Amount);
				charges.add(oBCharge1);
			}
		}
		submissionResponse.setSubmissionId(
				scheduledPaymentInstructionComposite.getPaymentInstruction().getPaymentInstructionNumber());

		if (!charges.isEmpty())
			submissionResponse.setCharges(charges);
		PaymentInstructionStatusCode2 status = scheduledPaymentInstructionComposite.getPaymentInstruction()
				.getPaymentInstructionStatusCode();

		if (!NullCheckUtils.isNullOrEmpty(status)) {
			if ((status.toString().equalsIgnoreCase("InitiationCompleted")) && (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionComposite
					.getPaymentInstruction().getPaymentInstructionNumber()))) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.PASS;
				submissionResponse.setProcessExecutionStatusEnum(consentStatus);
			} else if (((status.toString()).equalsIgnoreCase("InitiationFailed"))
					&& (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionComposite.getPaymentInstruction()
							.getPaymentInstructionNumber()))) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.FAIL;
				submissionResponse.setProcessExecutionStatusEnum(consentStatus);
			}
		}

		return submissionResponse;
	}

	public ScheduledPaymentInstructionComposite transformDomesticScheduledPaymentsFromAPIToFDForInsert(
			CustomDSPaymentsPOSTRequest paymentConsentsRequest,Map<String, String> params) {
		ScheduledPaymentInstructionComposite scheduledPaymentInstructionComposite = new ScheduledPaymentInstructionComposite();
		ScheduledPaymentInstructionProposal scheduledinstructionProposal = new ScheduledPaymentInstructionProposal();
		scheduledinstructionProposal.setPaymentInstructionProposalId(paymentConsentsRequest.getData().getConsentId());

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData())) {
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()))
				scheduledinstructionProposal.setInstructionReference(
						paymentConsentsRequest.getData().getInitiation().getInstructionIdentification());
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification())){
				scheduledinstructionProposal.setInstructionEndToEndReference(
						paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification());
			}else{
				scheduledinstructionProposal.setInstructionEndToEndReference(endToEndIdentificationMockValue);
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getLocalInstrument()))
				scheduledinstructionProposal.setInstructionLocalInstrument(
						paymentConsentsRequest.getData().getInitiation().getLocalInstrument());
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime()))
				scheduledinstructionProposal.setRequestedExecutionDateTime(
						paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime());

			Channel channel = new Channel();

			// channel code
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {

				channel.setChannelCode(
						ChannelCode.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
			}

			// channel Brand
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {

				if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
					channel.setBrandCode(BrandCode3.NIGB);
				} else if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
					channel.setBrandCode(BrandCode3.ROI);
				}

			}

			scheduledinstructionProposal.setChannel(channel);
			
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructedAmount())) {
				FinancialEventAmount financialEventAmount = new FinancialEventAmount();
				Currency transactionCurrency = new Currency();
				transactionCurrency.setIsoAlphaCode(
						paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency());
				financialEventAmount.setTransactionCurrency(Double.parseDouble(
						paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getAmount()));
				scheduledinstructionProposal.setFinancialEventAmount(financialEventAmount);
				scheduledinstructionProposal.setTransactionCurrency(transactionCurrency);
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount())) {
				ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

				proposingPartyAccount.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
				proposingPartyAccount.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
				proposingPartyAccount.setAccountName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getName());
				proposingPartyAccount.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation()
						.getCreditorAccount().getSecondaryIdentification());
				scheduledinstructionProposal.setProposingPartyAccount(proposingPartyAccount);
			}
			
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount())) {
				AuthorisingPartyAccount account = new AuthorisingPartyAccount();
				account.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
				account.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
				account.setAccountName(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName());
				account.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation().getDebtorAccount()
						.getSecondaryIdentification());
				scheduledinstructionProposal.setAuthorisingPartyAccount(account);
			}

			// Setting CreditorPostalAddress from API to FD
			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress())) {
				PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
				boolean isInt=false;
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressType())) {
				proposingPartyPostalAddress.setAddressType(String.valueOf(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressType()));
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment())) {
				proposingPartyPostalAddress.setDepartment(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment());
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment())) {
				proposingPartyPostalAddress.setSubDepartment(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment());
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName())) {
				proposingPartyPostalAddress.setGeoCodeBuildingName(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName());
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()
								.getCreditorPostalAddress().getBuildingNumber())) {
				proposingPartyPostalAddress.setGeoCodeBuildingNumber(paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getBuildingNumber());
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode())) {
				proposingPartyPostalAddress.setPostCodeNumber(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode());
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName())) {
				proposingPartyPostalAddress.setTownName(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName());
				isInt=true;
				}
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()
								.getCreditorPostalAddress().getCountrySubDivision())) {
				proposingPartyPostalAddress.setCountrySubDivision(paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getCountrySubDivision());
				isInt=true;
				}
				List<String> addLine=paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressLine();
				if (!NullCheckUtils
						.isNullOrEmpty(addLine) && !addLine.isEmpty()) {
				proposingPartyPostalAddress.setAddressLine(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressLine());
				isInt=true;
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry())) {
					Country addressCountry = new Country();
						addressCountry.setIsoCountryAlphaTwoCode(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry());
										
						proposingPartyPostalAddress.setAddressCountry(addressCountry);
										isInt=true;
				}
				if(isInt)
				scheduledinstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getRemittanceInformation())) {
				scheduledinstructionProposal.setAuthorisingPartyUnstructuredReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured());
				scheduledinstructionProposal.setAuthorisingPartyReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getReference());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk())) {
				PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();
				paymentInstructionRiskFactorReference.setPaymentContextCode(
						String.valueOf(paymentConsentsRequest.getRisk().getPaymentContextCode()));
				paymentInstructionRiskFactorReference
						.setMerchantCategoryCode(paymentConsentsRequest.getRisk().getMerchantCategoryCode());
				paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
						paymentConsentsRequest.getRisk().getMerchantCustomerIdentification());
				scheduledinstructionProposal
						.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);

				// Setting DeliveryAddress value from API to FD
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress())) {

					List<String> addline = paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine();

					Address counterpartyAddress = new Address();

					if (!NullCheckUtils.isNullOrEmpty(addline)) {

						if (!addline.isEmpty()) {
							counterpartyAddress.setFirstAddressLine(addline.get(0));

						}
						if (addline.size() > 1) {
							counterpartyAddress.setSecondAddressLine(addline.get(1));
						}
					}
					counterpartyAddress.setGeoCodeBuildingName(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName());
					counterpartyAddress.setGeoCodeBuildingNumber(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
					counterpartyAddress
							.setPostCodeNumber(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode());
					counterpartyAddress
							.setThirdAddressLine(paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName());
					counterpartyAddress.setFourthAddressLine(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry())) {
						Country addressCountry = new Country();
						addressCountry.setIsoCountryAlphaTwoCode(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry());
						counterpartyAddress.setAddressCountry(addressCountry);
					}

					paymentInstructionRiskFactorReference.setCounterPartyAddress(counterpartyAddress);
				}

				scheduledinstructionProposal
						.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);

			}

			scheduledPaymentInstructionComposite.setPaymentInstructionProposal(scheduledinstructionProposal);
		}
		return scheduledPaymentInstructionComposite;
	}

	public <T> CustomDSPaymentsPOSTResponse transformDomesticScheduledPaymentResponse(T inputBalanceObj) {
		// Root Element
		CustomDSPaymentsPOSTResponse obcustomDSPPOSTResponse = new CustomDSPaymentsPOSTResponse();
		ScheduledPaymentInstructionComposite paymentIns = (ScheduledPaymentInstructionComposite) inputBalanceObj;
		OBWriteDataDomesticScheduledResponse1 oBWriteDataDomesticScheduledResponse1 = new OBWriteDataDomesticScheduledResponse1();

		oBWriteDataDomesticScheduledResponse1
				.setDomesticScheduledPaymentId(paymentIns.getPaymentInstruction().getPaymentInstructionNumber());
		oBWriteDataDomesticScheduledResponse1
				.setConsentId(paymentIns.getPaymentInstructionProposal().getPaymentInstructionProposalId());

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstruction().getInstructionIssueDate())) {
			oBWriteDataDomesticScheduledResponse1
					.setCreationDateTime(paymentIns.getPaymentInstruction().getInstructionIssueDate());
		}

		oBWriteDataDomesticScheduledResponse1.setStatus(OBExternalStatus1Code
				.fromValue(String.valueOf(paymentIns.getPaymentInstruction().getPaymentInstructionStatusCode())));
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstruction().getInstructionStatusUpdateDateTime())) {
			oBWriteDataDomesticScheduledResponse1
					.setStatusUpdateDateTime(paymentIns.getPaymentInstruction().getInstructionStatusUpdateDateTime());
		}
		// status
		if (!NullCheckUtils
				.isNullOrEmpty(paymentIns.getPaymentInstruction().getPaymentInstructionStatusCode().toString())) {

			OBExternalStatus1Code status = OBExternalStatus1Code
					.fromValue(paymentIns.getPaymentInstruction().getPaymentInstructionStatusCode().toString());
			oBWriteDataDomesticScheduledResponse1.setStatus(status);
		}
		// charges
		List<OBCharge1> obCharge1List = null;
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getCharges())
				&& !paymentIns.getPaymentInstructionProposal().getCharges().isEmpty()) {
			obCharge1List = new ArrayList<>();
			OBCharge1 obCharge1 = new OBCharge1();
			List<PaymentInstructionCharge> charges = paymentIns.getPaymentInstructionProposal().getCharges();
			for (PaymentInstructionCharge charge : charges) {

				OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
				obCharge1.setChargeBearer(OBChargeBearerType1Code.fromValue(String.valueOf(charge.getChargeBearer())));
				obCharge1.setType(charge.getType());
				oBCharge1Amount.setAmount(String.valueOf(charge.getAmount().getTransactionCurrency()));
				oBCharge1Amount.setCurrency(charge.getCurrency().getIsoAlphaCode());
				obCharge1.setAmount(oBCharge1Amount);
				obCharge1List.add(obCharge1);
			}
			oBWriteDataDomesticScheduledResponse1.setCharges(obCharge1List);
		}


		// initiation
		OBDomesticScheduled1 oBDomesticScheduled1 = new OBDomesticScheduled1();
		oBDomesticScheduled1
				.setInstructionIdentification(paymentIns.getPaymentInstructionProposal().getInstructionReference());
		oBDomesticScheduled1.setEndToEndIdentification(
				paymentIns.getPaymentInstructionProposal().getInstructionEndToEndReference());
		oBDomesticScheduled1
				.setLocalInstrument(paymentIns.getPaymentInstructionProposal().getInstructionLocalInstrument());
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getRequestedExecutionDateTime())) {
			oBDomesticScheduled1.setRequestedExecutionDateTime(
					paymentIns.getPaymentInstructionProposal().getRequestedExecutionDateTime());
		}

		// InstructedAmount
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getFinancialEventAmount())) {
			OBDomestic1InstructedAmount oBDomestic1InstructedAmount = new OBDomestic1InstructedAmount();
			oBDomestic1InstructedAmount.setAmount(String.valueOf(
					paymentIns.getPaymentInstructionProposal().getFinancialEventAmount().getTransactionCurrency()));
			oBDomestic1InstructedAmount
					.setCurrency(paymentIns.getPaymentInstructionProposal().getTransactionCurrency().getIsoAlphaCode());
			oBDomesticScheduled1.setInstructedAmount(oBDomestic1InstructedAmount);
		}

		// DebtorAccount
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			oBCashAccountDebtor3.setSchemeName(
					paymentIns.getPaymentInstructionProposal().getAuthorisingPartyAccount().getSchemeName());
			oBCashAccountDebtor3.setIdentification(
					paymentIns.getPaymentInstructionProposal().getAuthorisingPartyAccount().getAccountIdentification());
			oBCashAccountDebtor3
					.setName(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyAccount().getAccountName());
			oBCashAccountDebtor3.setSecondaryIdentification(paymentIns.getPaymentInstructionProposal()
					.getAuthorisingPartyAccount().getSecondaryIdentification());
			oBDomesticScheduled1.setDebtorAccount(oBCashAccountDebtor3);
		}

		// CreditorAccount
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			oBCashAccountCreditor2.setSchemeName(
					paymentIns.getPaymentInstructionProposal().getProposingPartyAccount().getSchemeName());
			oBCashAccountCreditor2.setIdentification(
					paymentIns.getPaymentInstructionProposal().getProposingPartyAccount().getAccountIdentification());
			oBCashAccountCreditor2
					.setName(paymentIns.getPaymentInstructionProposal().getProposingPartyAccount().getAccountName());
			oBCashAccountCreditor2.setSecondaryIdentification(
					paymentIns.getPaymentInstructionProposal().getProposingPartyAccount().getSecondaryIdentification());
			oBDomesticScheduled1.setCreditorAccount(oBCashAccountCreditor2);
		}

		// CreditorPostalAddress
		if (!NullCheckUtils
				.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress())) {
			
			OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();
			
			boolean isInit = false;
			
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getAddressType()))
			{
				oBPostalAddress6.setAddressType(OBAddressTypeCode.fromValue(
					paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getAddressType()));
				isInit = true; 
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getDepartment()))
			{
				oBPostalAddress6.setDepartment(
					paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getDepartment());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getSubDepartment()))
			{
				oBPostalAddress6.setSubDepartment(
					paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getSubDepartment());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress()
					.getGeoCodeBuildingName()))
			{
				oBPostalAddress6.setStreetName(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress()
					.getGeoCodeBuildingName());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getGeoCodeBuildingNumber()))
			{
				oBPostalAddress6.setBuildingNumber(paymentIns.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getPostCodeNumber()))
			{
				oBPostalAddress6.setPostCode(
					paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getPostCodeNumber());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getTownName()))
			{
				oBPostalAddress6.setTownName(
					paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getTownName());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getCountrySubDivision()))
			{
				oBPostalAddress6.setCountrySubDivision(paymentIns.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getCountrySubDivision());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(
					paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getAddressCountry())) {
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress()
						.getAddressCountry().getIsoCountryAlphaTwoCode()))
				{
					oBPostalAddress6.setCountry(paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress()
						.getAddressCountry().getIsoCountryAlphaTwoCode());
					isInit = true;
				}
				List<String> addLine=paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getAddressLine();
				if (!NullCheckUtils.isNullOrEmpty(addLine) && !addLine.isEmpty()) {
				oBPostalAddress6.setAddressLine(
						paymentIns.getPaymentInstructionProposal().getProposingPartyPostalAddress().getAddressLine());
				isInit=true;
				}
			}

			if (isInit)
				oBDomesticScheduled1.setCreditorPostalAddress(oBPostalAddress6);
		}

		// RemittanceInformation
		if ((!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 obRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyReference())) {
			obRemittanceInformation1
					.setReference(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyReference());
			isInit=true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposal().getAuthorisingPartyUnstructuredReference())) {
			obRemittanceInformation1.setUnstructured(
					paymentIns.getPaymentInstructionProposal().getAuthorisingPartyUnstructuredReference());
			isInit = true;
			}
			if (isInit)
			oBDomesticScheduled1.setRemittanceInformation(obRemittanceInformation1);
		}

		// final response setup
		oBWriteDataDomesticScheduledResponse1.setInitiation(oBDomesticScheduled1);

		obcustomDSPPOSTResponse.setData(oBWriteDataDomesticScheduledResponse1);
		return obcustomDSPPOSTResponse;

	}
}
