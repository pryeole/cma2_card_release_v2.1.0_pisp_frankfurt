package com.capgemini.psd2.mock.foundationservice.account.transactions.exception;

public class RecordNotFoundException extends Exception {

	public RecordNotFoundException(String s){
		super(s);
		
	}
	
}
