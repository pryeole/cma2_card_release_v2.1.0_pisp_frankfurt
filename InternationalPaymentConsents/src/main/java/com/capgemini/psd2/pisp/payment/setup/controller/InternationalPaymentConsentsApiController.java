package com.capgemini.psd2.pisp.payment.setup.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.service.InternationalPaymentConsentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-10-18T10:12:06.504+05:30")

@Controller
public class InternationalPaymentConsentsApiController implements InternationalPaymentConsentsApi {

	@Autowired
	private InternationalPaymentConsentsService iPaymentConsentsService;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public InternationalPaymentConsentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<OBWriteInternationalConsentResponse1> getInternationalPaymentConsentsConsentId(
			@ApiParam(value = "ConsentId", required = true) @PathVariable("ConsentId") String consentId,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {

		// validate id here
		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setConsentId(consentId);
		CustomIPaymentConsentsPOSTResponse paymentConsentsResponse = iPaymentConsentsService
				.retrieveInternationalPaymentConsentsResource(paymentRetrieveRequest);
		return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<OBWriteInternationalConsentResponse1> createInternationalPaymentConsents(
			@ApiParam(value = "Default", required = true) @Valid @RequestBody OBWriteInternationalConsent1 obWriteInternationalConsent1Param,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "Every request will be processed only once per x-idempotency-key.  The Idempotency Key will be valid for 24 hours.", required = true) @RequestHeader(value = "x-idempotency-key", required = true) String xIdempotencyKey,
			@ApiParam(value = "A detached JWS signature of the body of the payload.", required = true) @RequestHeader(value = "x-jws-signature", required = true) String xJwsSignature,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {

		CustomIPaymentConsentsPOSTRequest customPaymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		customPaymentConsentsRequest.setData(obWriteInternationalConsent1Param.getData());
		customPaymentConsentsRequest.setRisk(obWriteInternationalConsent1Param.getRisk());

		CustomIPaymentConsentsPOSTResponse paymentConsentsResponse = iPaymentConsentsService
				.createInternationalPaymentConsentsResource(customPaymentConsentsRequest);
		return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.CREATED);

	}
}
