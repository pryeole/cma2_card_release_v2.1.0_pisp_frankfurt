package com.capgemini.psd2.revoke.consent.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class RevokeConsentValidatorImpl {
	
	@Value("${app.validAccountRequestIdChars:[a-zA-Z0-9-]{1,40}}")
	private String validAccountRequestIdChars;
	
	public void validateUniqueUUID(String consentId) {

		if (consentId.length() < 1 || consentId.length() > 128) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.VALIDATION_ERROR));
		}

		if (!consentId.matches(validAccountRequestIdChars)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.VALIDATION_ERROR));
		}
	}
	
	public void validateConsentId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		validateUniqueUUID(consentId);		
	}

}